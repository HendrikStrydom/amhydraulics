package haj.com.service;

import java.util.List;

import haj.com.dao.BlankDAO;
import haj.com.entity.Blank;
import haj.com.framework.AbstractService;

public class BlankService extends AbstractService {

	private BlankDAO dao = new BlankDAO();

	public List<Blank> allBlank() throws Exception {
	  	return dao.allBlank();
	}


	public void create(Blank entity) throws Exception  {
	   if (entity.getId() ==null)
		 this.dao.create(entity);
		else
		 this.dao.update(entity);
	}

	public void update(Blank entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(Blank entity) throws Exception  {
		this.dao.delete(entity);
	}

	public Blank findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<Blank> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

/*
	public List<Blank> findByCompany(haj.com.entity.Company company) throws Exception {
		return dao.findByCompany(company.getId());
	}
*/	
}
