package haj.com.service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.techfinium.entity.Resource;

import haj.com.dao.ResourceUserAllocationDAO;
import haj.com.entity.ResourceUser;
import haj.com.entity.ResourceUsers;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class ResourceUserAllocationService extends AbstractService {

	private ResourceUserAllocationDAO dao = new ResourceUserAllocationDAO();

	public List<Users> findUsersThatAreNotAdmin() throws Exception {
		return this.dao.findUsersThatAreNotAdmin();
	}

	public List<ResourceUsers> getAllResourceUsers() throws Exception {
		return this.dao.getAllResourceUsers();
	}

	public List<Resource> findResourceByName(String description) throws Exception {
		return this.dao.findResourceByName(description);
	}

	public void deleteResourceUsers(ResourceUsers ru) throws Exception {
		this.dao.delete(ru);
	}

	public void createResourceUsers(Resource resource, List<Users> userList) throws Exception {
		for (Users users2 : userList) {
			ResourceUsers ru = new ResourceUsers();
			ru.setResource(resource);
			ru.setUser(users2);
			if (ru.getId() == null)
				this.dao.create(ru);
		}
	}

}
