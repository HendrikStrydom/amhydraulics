package haj.com.service;

import java.util.ArrayList;
import java.util.List;

import haj.com.dao.InspectionReportItemDAO;
import haj.com.entity.InspectionReportItem;
import haj.com.framework.AbstractService;

public class InspectionReportItemService extends AbstractService {

	private InspectionReportItemDAO dao = new InspectionReportItemDAO();

	public List<InspectionReportItem> allInspectionReportItem() throws Exception {
		return dao.allInspectionReportItem();
	}

	public List<InspectionReportItem> allInspectionReportItemWhereInspectionReportIsNull() throws Exception {
		List<InspectionReportItem> l = new ArrayList<InspectionReportItem>();
		for (InspectionReportItem inspectionReportItem : dao.allInspectionReportItemWhereInspectionReportIsNull()) {
			inspectionReportItem.setId(null);
			l.add(inspectionReportItem);
		}
		return l;
	}

	public void create(InspectionReportItem entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

	public void update(InspectionReportItem entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(InspectionReportItem entity) throws Exception {
		this.dao.delete(entity);
	}

	public InspectionReportItem findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<InspectionReportItem> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<InspectionReportItem> findByInspectionReport(Long inspectionReportId) throws Exception {
		return dao.findByInspectionReport(inspectionReportId);
	}

	public List<InspectionReportItem> buidlInspectionReportItemList() throws Exception {
		List<InspectionReportItem> l = new ArrayList<InspectionReportItem>();

		InspectionReportItem iri = new InspectionReportItem();
		iri.setComponentDescription("CYLINDER TUBE");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("PISTON ROD");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("PISTON	ASSEMBLY");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("PISTON LOCK NUT");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("PISTON BEARING");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("FRONT CUSHION");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("REAR CUSHION");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("FRONT END CAP");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("FOOT MOUNTING");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("REAR END CAP");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("NECK BUSH");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("GLAND");
		l.add(iri);

		iri = new InspectionReportItem();
		iri.setComponentDescription("GLAND BRUSH");
		l.add(iri);
		return l;
	}
}
