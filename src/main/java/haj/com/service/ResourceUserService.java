package haj.com.service;

import java.util.List;

import haj.com.dao.ResourceUserDAO;
import haj.com.entity.ResourceUser;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class ResourceUserService extends AbstractService {

	private ResourceUserDAO dao = new ResourceUserDAO();

	public List<ResourceUser> allResourceUser() throws Exception {
		return dao.allResourceUser();
	}

	public void create(ResourceUser entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create((IDataEntity) entity);
		else
			this.dao.update((IDataEntity) entity);
	}

	public void update(ResourceUser entity) throws Exception {
		this.dao.update((IDataEntity) entity);
	}

	public void delete(ResourceUser entity) throws Exception {
		this.dao.delete((IDataEntity) entity);
	}

	public ResourceUser findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<ResourceUser> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

}
