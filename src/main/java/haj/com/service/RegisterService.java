package haj.com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import haj.com.constants.HAJConstants;
import haj.com.dao.UsersDAO;
import haj.com.entity.Address;
import haj.com.entity.Users;
import haj.com.entity.enums.UsersStatusEnum;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;
import haj.com.utils.GenericUtility;
import haj.com.utils.PhoneNumberUtils;

public class RegisterService extends AbstractService {
	private UsersDAO dao = new UsersDAO();
	private AddressService addressService = new AddressService();

	public void updateUser(Users u) throws Exception {
		dao.update(u);
	}
	
	public void register(Users u) throws Exception {

		if (dao.getUserByEmail(u.getEmail()) != null)
			throw new Exception("Another user is already registered with this email address");
		String pwd = GenericUtility.genPassord();
		u.setPassword(LogonService.encryptPassword(pwd));
		u.setUsername("");
		u.setLastLogin(null);
		if (u.getCellNumber() != null && u.getCellNumber().trim().length() > 0) {
			u.setInternalCellNumber(PhoneNumberUtils.convertNumberToInternalFormat(u.getCellNumber(), "ZA"));
		}
		u.setStatus(UsersStatusEnum.EmailNotConfrimed);
		u.setEmailGuid(UUID.randomUUID().toString());
		u.setRegistrationDate(new java.util.Date());

		if (u.getId() == null) {
			dao.create(u);
		} else {
			dao.update(u);
		}
		notifyUser(u, pwd);
	}

	public void register(Users user, Address address, Address postalAddress) throws Exception {

		List<IDataEntity> entityList = new ArrayList<IDataEntity>();
		register(user);
		entityList.add(user);
		addressService.lookupLongitudeLatitude(address);
		entityList.add(address);
		entityList.add(postalAddress);
		notifyUser(user);

	}

	public void notifyUser(Users u) throws Exception {
		// welcome message to the user

		String welcome = "<p>Dear #NAME#,</p>" + "<br/>" + "<p>Welcome to AM Hydraulics</p>" + "<p>Please " + "<a href=\"" + HAJConstants.PL_LINK + "confirmemail.jsf?uuid=" + u.getEmailGuid().trim() + "\">confirm</a> your email address.</p>" + "<p>Regards</p>" + "<p>The AM Hydraulics team</p>"
				+ "<br/>";
		welcome = welcome.replaceAll("#NAME#", u.getFirstName());
		GenericUtility.sendMail(u.getEmail(), "Welcome to the AM Hydraulics portal", welcome);
	}

	/*
	 * public void notifyUser(ClientUsers u, String pwd) throws Exception { //
	 * welcome message to the user
	 * 
	 * String welcome = "<p>Dear #NAME#,</p>"+ "<br/>" +
	 * "<p>You have been invited to joining TechFINIUM LTD Q/A</p>"+
	 * "<p>Please "+ "<a href=\""+HAJConstants.PL_LINK +
	 * "confirmemail.jsf?uuid="+u.getUsers().getEmailGuid().trim()+
	 * "\">confirm</a> your email address.</p>"+ "<p>Your username is: <b>"
	 * +u.getUsers().getUsername()+"</b> and your password is: <b>"+pwd+
	 * "</b></p>"+ "<p>You can change it after you have logged in.</p>"+
	 * "<p>Regards</p>"+ "<p>The TechFINIUM LTD team</p>"+ "<br/>"; welcome =
	 * welcome.replaceAll("#NAME#",u.getUsers().getFirstName());
	 * GenericUtility.sendMail(u.getUsers().getEmail(),
	 * "Complete infoFINIUM registration", welcome); }
	 */
	public void confirmEmail(Users u) throws Exception {
		u.setEmailConfirmDate(new java.util.Date());
		u.setStatus(UsersStatusEnum.Active);
		dao.update(u);

	}

	public void notifyUser(Users u, String pwd) throws Exception {

		String welcome = "<p>Dear #NAME#,</p>" + "<br/>" + "<p>Please " + "<a href=\"" + HAJConstants.PL_LINK + "confirmemail.jsf?uuid=" + u.getEmailGuid().trim() + "\">confirm</a> your email address.</p>" + "<p>Your username is: <b>" + u.getUsername() + "</b> and your password is: <b>" + pwd
				+ "</b></p>" + "<p>You can change it after you have logged in.</p>" + "<p>Regards</p>" + "<p>The AM Hydraulics team</p>" + "<br/>";
		welcome = welcome.replaceAll("#NAME#", u.getFirstName());
		GenericUtility.sendMail(u.getEmail(), "AM Hydraulics portal new password", welcome);
	}

}
