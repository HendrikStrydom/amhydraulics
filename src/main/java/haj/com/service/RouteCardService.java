package haj.com.service;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.dao.RouteCardDAO;
import haj.com.entity.RouteCard;
import haj.com.entity.RouteCardItem;
import haj.com.entity.Users;
import haj.com.entity.enums.RouteStatusEnum;
import haj.com.framework.AbstractService;

public class RouteCardService extends AbstractService {

	private RouteCardDAO dao = new RouteCardDAO();
	private TasksService tasksService = new TasksService();

	public void createRCandRCI(RouteCard rc, List<RouteCardItem> rciList, Users sessionUser, boolean completed) throws Exception {
		if (completed)
			rc.setRouteStatus(RouteStatusEnum.RouteComplete);
		if (rc.getId() == null) {
			this.dao.create(rc);
			for (RouteCardItem rci : rciList) {
				rci.setRouteCard(rc);
				if (rci.getId() == null)
					this.dao.create(rci);
				else
					this.dao.update(rci);
			}
		} else {
			if (rc.getRevisionNumber() >= 1)
				rc.setRevisionNumber(rc.getRevisionNumber() + 1);
			this.dao.update(rc);
			for (RouteCardItem rci : rciList) {
				rci.setRouteCard(rc);
				if (rci.getId() == null)
					this.dao.create(rci);
				else
					this.dao.update(rci);
			}
		}
		if (rc.getTask() == null) {
			rc.setTask(tasksService.createRouteCardTask(sessionUser, rc));
			update(rc);
		}
	}

	public void updateRCI(RouteCardItem entity) throws Exception
	{
		dao.update(entity);
	}
	
	public void createRC(RouteCard rc) throws Exception {
		if (rc.getId() == null) {
			this.dao.create(rc);
		} else {
			if (rc.getRevisionNumber() >= 1)
				rc.setRevisionNumber(rc.getRevisionNumber() + 1);
			this.dao.update(rc);
		}
	}

	public void createRCI(RouteCardItem rci) throws Exception {
		if (rci.getId() == null) {
			this.dao.create(rci);
		} else {
			this.dao.update(rci);
		}
	}

	public void updateRCRevisionNumber(RouteCard entity) throws Exception {
		if (entity.getRevisionNumber() >= 1)
			entity.setRevisionNumber(entity.getRevisionNumber() + 1);
		this.dao.update(entity);
	}

	public void deleteRCI(RouteCardItem entity) throws Exception {
		this.dao.delete(entity);
	}

	public List<RouteCardItem> findRCIByRCId(Long rcId) throws Exception {
		return dao.findRCIByRCId(rcId);
	}

	public List<RouteCard> allRouteCard() throws Exception {
		return dao.allRouteCard();
	}

	public RouteCard findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<RouteCard> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<RouteCard> findByContract(Long contractId) throws Exception {
		return dao.findByContract(contractId);
	}

	public void create(RouteCard entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

	public void update(RouteCard entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(RouteCard entity) throws Exception {
		entity.setSoftDelete(true);
		this.dao.update(entity);
	}

	public List<RouteCard> findRouteCardByQueryParameter(Map<String, Object> queryParameters) throws Exception {
		String hql = "select o from RouteCard o where o.softDelete = false";
		if (queryParameters.containsKey("ConEnqNumber")) {
			hql += " and (o.contract.contractNumber LIKE :ConEnqNumber or o.contract.enquiryNumber LIKE :ConEnqNumber)";
		}
		if (queryParameters.containsKey("rsEnum")) {
			hql += " and o.routeStatus = :rsEnum";
		}
		return dao.findRouteCardByQueryParameter(hql, queryParameters);
	}

}
