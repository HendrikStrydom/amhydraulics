package haj.com.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import haj.com.dao.QuoteDAO;
import haj.com.dao.QuoteItemDAO;
import haj.com.entity.Contract;
import haj.com.entity.PurchaseOrder;
import haj.com.entity.Quote;
import haj.com.entity.QuoteItem;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.QuoteStatusEnum;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class QuoteService extends AbstractService {

	private QuoteDAO dao = new QuoteDAO();
	private QuoteItemDAO qidao = new QuoteItemDAO();

	public List<Quote> allQuote() throws Exception {
		return dao.allQuote();
	}

	public List<Quote> allQuoteBySoftDelete(Boolean softDelete) throws Exception {
		return dao.allQuoteBySoftDelete(softDelete);
	}
	public List<Quote> findByContract(Long contractId) throws Exception {
		return dao.findByContract(contractId);
	}

	public List<Contract> findByContractStatusReturnListOfContracts(QuoteStatusEnum quoteStatus) throws Exception {
		return dao.findByContractStatusReturnListOfContracts(quoteStatus);
	}

	public void create(Quote entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
			if (entity.getQuoteNumber() == null) {
				entity.setQuoteNumber("QN" + String.format("%04d", 5000l + entity.getId()));
				this.dao.update(entity);
			}
		} else {
			if (entity.getRevisionNumber() >= 1)
				entity.setRevisionNumber(entity.getRevisionNumber() + 1);
			this.dao.update(entity);
		}
	}

	public void createQuoteAndQuoteItem(Quote quote, List<QuoteItem> quoteItems) throws Exception {
		if (quote.getId() == null) {
			this.dao.create(quote);
			if (quote.getQuoteNumber() == null) {
				quote.setQuoteNumber("QN" + String.format("%04d", 5000l + quote.getId()));
				this.dao.update(quote);
			}
		} else {
			if (quote.getRevisionNumber() >= 1)
				quote.setRevisionNumber(quote.getRevisionNumber() + 1);
			dao.update(quote);
		}
		for (QuoteItem quoteItem : quoteItems) {
			quoteItem.setQuote(quote);
			if (quoteItem.getId() == null)
				qidao.create(quoteItem);
			else
				qidao.update(quoteItem);
		}
	}

	public void update(Quote entity) throws Exception {
		this.dao.update(entity);
	}

	public void updateQuoteRivisionNumber(Quote entity) throws Exception {
		if (entity.getRevisionNumber() >= 1)
			entity.setRevisionNumber(entity.getRevisionNumber() + 1);
		this.dao.update(entity);
	}

	public void delete(Quote entity) throws Exception {
		entity.setSoftDelete(true);
		
		this.dao.update(entity);
		
	}

	public Quote findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}
	
	public Quote findByGuid(String guid) throws Exception {
		return dao.findByGuid(guid);
	}
	

	public List<Quote> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<Quote> findQuoteByQueryParameter(Map<String, Object> queryParameters) throws Exception {
		String hql = "select o from Quote o where o.softDelete = false";
		if (queryParameters.containsKey("ConEnqNumber")) {
			hql += " and (o.contract.contractNumber LIKE :ConEnqNumber or o.contract.enquiryNumber LIKE :ConEnqNumber)";
		}
		if (queryParameters.containsKey("quoteNumber")) {
			hql += " and o.quoteNumber LIKE :quoteNumber";
		}
		if (queryParameters.containsKey("qsEnum")) {
			hql += " and o.quoteStatusEnum = :qsEnum";
		}
		return dao.findQuoteByQueryParameter(hql, queryParameters);
	}

}
