package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.google.code.geocoder.model.LatLng;

import haj.com.dao.AddressDAO;
import haj.com.entity.Address;
import haj.com.entity.Users;
import haj.com.entity.enums.AddressTypeEnum;
import haj.com.framework.AbstractService;
import haj.com.utils.GeoCoderUtil;

public class AddressService extends AbstractService {

	private AddressDAO dao = new AddressDAO();

	/**
	 * Get all Address
	 * 
	 * @author TechFinium
	 * @see Address
	 * @return List<Address>
	 * @throws Exception
	 */
	public List<Address> allAddress() throws Exception {
		return dao.allAddress();
	}

	/**
	 * Create or update Address
	 * 
	 * @author TechFinium
	 * @see Address
	 * @throws Exception
	 */
	public void create(Address entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else
			this.dao.update(entity);
	}

	/**
	 * Update Address
	 * 
	 * @author TechFinium
	 * @see Address
	 * @throws Exception
	 */
	public void update(Address entity) throws Exception {
		this.dao.update(entity);
	}

	/**
	 * Delete Address
	 * 
	 * @author TechFinium
	 * @see Address
	 * @throws Exception
	 */
	public void delete(Address entity) throws Exception {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
	 * 
	 * @author TechFinium
	 * @see Address
	 * @return a Address object
	 * @throws Exception
	 */
	public Address findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	/**
	 * Find Address by description
	 * 
	 * @author TechFinium
	 * @see Address
	 * @return List<Address>
	 * @throws Exception
	 */
	public List<Address> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public Address findByCustAndType(Long customerId, AddressTypeEnum addressType) throws Exception {
		return dao.findByCustAndType(customerId, addressType);
	}

	/**
	 * Lazy load Address
	 * 
	 * @param first
	 *            from key
	 * @param pageSize
	 *            no of rows to fetch
	 * @return List<Address>
	 * @throws Exception
	 */
	public List<Address> allAddress(int first, int pageSize) throws Exception {
		return dao.allAddress(Long.valueOf(first), pageSize);
	}

	/**
	 * @author TechFinium
	 * @return Number of rows in the Address
	 * @throws Exception
	 */
	public Long count() throws Exception {
		return dao.count(Address.class);
	}

	/**
	 * @author TechFinium
	 * @param class1
	 *            Address class
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return List<Address> containing data
	 */
	@SuppressWarnings("unchecked")
	public List<Address> allAddress(Class<Address> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		return (List<Address>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	/**
	 * 
	 * @param entity
	 *            Address class
	 * @param filters
	 * @return Number of rows in the Address entity
	 */
	public int count(Class<Address> entity, Map<String, Object> filters) {
		return dao.count(entity, filters);
	}

	/**
	 * Addresses for a company
	 * 
	 * @param company
	 * @return
	 * @throws Exception
	 */
	// public List<Address> findByCompany(Company company) throws Exception {
	// return dao.findByCompany(company.getId());
	// }

	/**
	 * Addresses for a users
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public List<Address> findByUser(Users user) throws Exception {
		return dao.findByUser(user.getId());
	}

	public void lookupLongitudeLatitude(Address addr) {
		try {
			String address = addr.getAddressLine1() == null ? ""
					: addr.getAddressLine1() + " " + addr.getAddressLine2() == null ? ""
							: addr.getAddressLine2() + " " + addr.getAddressLine3() == null ? "" : addr.getAddressLine3() + " " + addr.getAddressLine4() == null ? "" : addr.getAddressLine4() + " " + addr.getPostcode() == null ? "" : addr.getPostcode();
			if (address.trim().length() > 3) {
				LatLng ll = GeoCoderUtil.calcLatLng(address.trim());
				if (ll != null) {
					addr.setLatitude(ll.getLat().doubleValue());
					addr.setLongitude(ll.getLng().doubleValue());
				}
			}
		} catch (Exception e) {
			logger.fatal(e);
		}

	}

	/**
	 * Copy address details from addressCopy to address
	 * 
	 * @param address
	 * @param addressCopy
	 */
	public void copyAddress(Address address, Address addressCopy, AddressTypeEnum addressType) {
		address.setAddressLine1(addressCopy.getAddressLine1() != null ? addressCopy.getAddressLine1() : null);
		address.setAddressLine2(addressCopy.getAddressLine2() != null ? addressCopy.getAddressLine2() : null);
		address.setAddressLine3(addressCopy.getAddressLine3() != null ? addressCopy.getAddressLine3() : null);
		address.setAddressLine4(addressCopy.getAddressLine4() != null ? addressCopy.getAddressLine4() : null);
		address.setPostcode(addressCopy.getPostcode() != null ? addressCopy.getPostcode() : null);
		address.setLatitude(addressCopy.getLatitude() != null ? addressCopy.getLatitude() : null);
		address.setLongitude(addressCopy.getLongitude() != null ? addressCopy.getLongitude() : null);
		address.setUser(addressCopy.getUser() != null ? addressCopy.getUser() : null);
		address.setCustomer(addressCopy.getCustomer() != null ? addressCopy.getCustomer() : null);
		address.setAddressType(addressType);
	}

}
