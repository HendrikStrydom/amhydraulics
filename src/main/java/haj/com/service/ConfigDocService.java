package haj.com.service;

import java.util.List;

import haj.com.dao.ConfigDocDAO;
import haj.com.entity.ConfigDoc;
import haj.com.framework.AbstractService;

public class ConfigDocService extends AbstractService {

	private ConfigDocDAO dao = new ConfigDocDAO();

	public List<ConfigDoc> allConfigDoc() throws Exception {
		return dao.allConfigDoc();
	}

	public List<ConfigDoc> allConfigDocActive() throws Exception {
		return dao.allConfigDocActive();
	}

	public void create(ConfigDoc entity) throws Exception {
		//if (findIfUniqueUsed(entity.getUniqueId()))
		//	throw new Exception("Unique id already in use");
		if (entity.getId() == null) {
			entity.setActive(true);
			this.dao.create(entity);
		} else {
			this.dao.update(entity);
		}
	}

	public void update(ConfigDoc entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(ConfigDoc entity) throws Exception {
		this.dao.delete(entity);
	}

	public ConfigDoc findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<ConfigDoc> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public Boolean findIfUniqueUsed(String uniqueId) throws Exception {
		return dao.findIfUniqueUsed(uniqueId);
	}

}
