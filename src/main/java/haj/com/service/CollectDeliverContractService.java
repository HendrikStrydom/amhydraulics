package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.CollectDeliverContractDAO;
import haj.com.entity.CollectDeliverContract;
import haj.com.entity.CollectDeliverItem;
import haj.com.entity.enums.CollectDeliverEnum;
import haj.com.framework.AbstractService;

public class CollectDeliverContractService extends AbstractService {

	private CollectDeliverContractDAO dao = new CollectDeliverContractDAO();
	private CollectDeliverItemService cdiService = new CollectDeliverItemService();

	public List<CollectDeliverContract> allCollectDeliverContract() throws Exception {
		return getOverAllStatus(dao.allCollectDeliverContract());
	}

	public List<CollectDeliverContract> getOverAllStatus(List<CollectDeliverContract> cdc) throws Exception {
		for (CollectDeliverContract collectDeliverContract : cdc) {
			List<CollectDeliverEnum> kl = dao.findByOverallStatus(collectDeliverContract);
			if (kl.size() > 0)
				collectDeliverContract.setCollectionDeliveryOverView(kl.get(0));
		}
		return cdc;
	}

	public void create(CollectDeliverContract entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
			if (entity.getCdnNumber() == null)
				entity.setCdnNumber("CDN" + String.format("%04d", entity.getId()));
			this.dao.update(entity);
		} else {
			if (entity.getRevisionNumber() == null)
				entity.setRevisionNumber(1);
			else if (entity.getRevisionNumber() >= 1)
				entity.setRevisionNumber(entity.getRevisionNumber() + 1);
			this.dao.update(entity);
		}
	}

	public void create(CollectDeliverContract entity, List<CollectDeliverItem> cdiList) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
			if (entity.getCdnNumber() == null)
				entity.setCdnNumber("CDN" + String.format("%04d", entity.getId()));
			this.dao.update(entity);
			for (CollectDeliverItem cdi : cdiList) {
				cdi.setCollectDeliverContract(entity);
				cdiService.create(cdi);
			}
		} else {
			if (entity.getRevisionNumber() == null)
				entity.setRevisionNumber(1);
			else if (entity.getRevisionNumber() >= 1)
				entity.setRevisionNumber(entity.getRevisionNumber() + 1);
			this.dao.update(entity);
			for (CollectDeliverItem cdi : cdiList) {
				cdi.setCollectDeliverContract(entity);
				cdiService.create(cdi);
			}
		}
	}

	public void updateCDCRevisionNumber(CollectDeliverContract entity) throws Exception {
		if (entity.getRevisionNumber() >= 1)
			entity.setRevisionNumber(entity.getRevisionNumber() + 1);
		this.dao.update(entity);
	}

	public void update(CollectDeliverContract entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(CollectDeliverContract entity) throws Exception {
		this.dao.delete(entity);
	}

	public CollectDeliverContract findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverContract> allCollectDeliverContract(Class<CollectDeliverContract> class1, int first,
			int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		return (List<CollectDeliverContract>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	public int count(Class<CollectDeliverContract> entity, Map<String, Object> filters) {
		return dao.count(entity, filters);
	}

	public List<CollectDeliverContract> findCollectDeliverContractByQueryParameter(Map<String, Object> queryParameters)
			throws Exception {
		String hql = "select distinct(o.collectDeliverContract) from CollectDeliverItem o where o.id IS NOT null ";
		if (queryParameters.containsKey("ConEnqNumber")) {
			hql += " and (o.collectDeliverContract.contract.contractNumber LIKE :ConEnqNumber or o.collectDeliverContract.contract.enquiryNumber LIKE :ConEnqNumber)";
		}
		if (queryParameters.containsKey("cdNumber")) {
			hql += " and o.collectDeliverContract.cdnNumber LIKE :cdNumber";
		}
		if (queryParameters.containsKey("anyText")) {
			hql += " and o.description  LIKE :anyText";
		}
		return dao.findCollectDeliverContractByQueryParameter(hql, queryParameters);
	}

}
