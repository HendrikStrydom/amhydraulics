package haj.com.service;

import java.util.ArrayList;
import java.util.List;

import haj.com.dao.QuoteItemDAO;
import haj.com.entity.QuoteItem;
import haj.com.framework.AbstractService;

public class QuoteItemService extends AbstractService {

	private QuoteItemDAO dao = new QuoteItemDAO();

	public List<QuoteItem> allQuoteItem() throws Exception {
		return dao.allQuoteItem();
	}

	public void create(QuoteItem entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

	public void update(QuoteItem entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(QuoteItem entity) throws Exception {
		this.dao.delete(entity);
	}

	public QuoteItem findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<QuoteItem> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<QuoteItem> findByQuote(Long quoteId) throws Exception {
		return dao.findByQuote(quoteId);
	}
}
