package haj.com.service;

import java.util.List;

import haj.com.dao.ContractOverviewDownloadDAO;
import haj.com.entity.CollectDeliverContract;
import haj.com.entity.Contract;
import haj.com.entity.Doc;
import haj.com.entity.InspectionReport;
import haj.com.entity.PurchaseOrder;
import haj.com.entity.Quote;
import haj.com.entity.RouteCard;
import haj.com.framework.AbstractService;

public class ContractOverviewDownloadService extends AbstractService {

	private ContractOverviewDownloadDAO dao = new ContractOverviewDownloadDAO();

	public List<InspectionReport> findInspectionReportByContract(Contract contract) throws Exception {
		return dao.findInspectionReportByContract(contract);
	}

	public List<Quote> findQuotesByContract(Contract contract) throws Exception {
		return dao.findQuotesByContract(contract);
	}

	public List<RouteCard> findRouteCardByContract(Contract contract) throws Exception {
		return dao.findRouteCardByContract(contract);
	}

	public List<PurchaseOrder> findPurchaseOrderByContract(Contract contract) throws Exception {
		return dao.findPurchaseOrderByContract(contract);
	}

	public List<CollectDeliverContract> findCollectDeliverByContract(Contract contract) throws Exception {
		return dao.findCollectDeliverByContract(contract);
	}

	public List<Doc> findDocsByContract(Contract contract) throws Exception {
		return dao.findDocsByContract(contract);
	}

}
