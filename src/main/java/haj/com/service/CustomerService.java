package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.AddressDAO;
import haj.com.dao.CustomerDAO;
import haj.com.entity.Address;
import haj.com.entity.Customer;
import haj.com.entity.enums.AddressTypeEnum;
import haj.com.framework.AbstractService;

public class CustomerService extends AbstractService {

	private CustomerDAO dao = new CustomerDAO();
	private AddressDAO addressDAO = new AddressDAO();

	/**
	 * Get all Customer
	 * 
	 * @author TechFinium
	 * @see Customer
	 * @return List<Customer>
	 * @throws Exception
	 */
	public List<Customer> allCustomer() throws Exception {
		return dao.allCustomer();
	}

	/**
	 * Create or update Customer
	 * 
	 * @author TechFinium
	 * @see Customer
	 * @throws Exception
	 */
	public void create(Customer entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else
			this.dao.update(entity);
	}

	/**
	 * Update Customer
	 * 
	 * @author TechFinium
	 * @see Customer
	 * @throws Exception
	 */
	public void update(Customer entity) throws Exception {
		this.dao.update(entity);
	}

	/**
	 * Delete Customer
	 * 
	 * @author TechFinium
	 * @see Customer
	 * @throws Exception
	 */
	public void delete(Customer entity) throws Exception {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
	 * 
	 * @author TechFinium
	 * @see Customer
	 * @return a Customer object
	 * @throws Exception
	 */
	public Customer findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	/**
	 * Find Customer by description
	 * 
	 * @author TechFinium
	 * @see Customer
	 * @return List<Customer>
	 * @throws Exception
	 */
	public List<Customer> findByName(String desc) throws Exception {
		List<Customer> l = dao.findByName(desc);
		Customer cust = new Customer();
		cust.setCustomerName("New Customer");
		cust.setId(-1l);
		l.add(cust);
		return l;
	}
	
	public List<Customer> findByNameNoNew(String desc) throws Exception {
		List<Customer> l = dao.findByName(desc);
		return l;
	}

	/**
	 * Lazy load Customer
	 * 
	 * @param first
	 *            from key
	 * @param pageSize
	 *            no of rows to fetch
	 * @return List<Customer>
	 * @throws Exception
	 */
	public List<Customer> allCustomer(int first, int pageSize) throws Exception {
		return dao.allCustomer(Long.valueOf(first), pageSize);
	}

	/**
	 * @author TechFinium
	 * @return Number of rows in the Customer
	 * @throws Exception
	 */
	public Long count() throws Exception {
		return dao.count(Customer.class);
	}

	/**
	 * @author TechFinium
	 * @param class1
	 *            Customer class
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return List<Customer> containing data
	 */
	@SuppressWarnings("unchecked")
	public List<Customer> allCustomer(Class<Customer> class1, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<Customer>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	/**
	 * 
	 * @param entity
	 *            Customer class
	 * @param filters
	 * @return Number of rows in the Customer entity
	 */
	public int count(Class<Customer> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	/**
	 * 
	 * @param customer
	 * @return
	 * @throws Exception
	 */
	public List<Customer> find(Customer customer) throws Exception {
		return dao.find(customer);
	}

	/**
	 * Saves the customer and their address to database
	 * 
	 * @param customer
	 * @throws Exception
	 */
	public void saveCustomer(Customer customer) throws Exception {

		if (customer.getId() == -1l) {
			customer.setId(null);
			create(customer);
		} else {
			update(customer);
		}

		/* Create Address */
		if (customer.getAddress().getPostcode() != null) {
			if (customer.getAddress().getId() == null) {
				addressDAO.create(customer.getAddress());
			} else {
				addressDAO.update(customer.getAddress());
			}
		}

		/* Billing Address */
		if (customer.getBillingAddress().getPostcode() != null) {
			if (customer.getBillingAddress().getId() == null) {
				addressDAO.create(customer.getBillingAddress());
			} else {
				addressDAO.update(customer.getBillingAddress());
			}
		}

		/* Delivery Address */
		if (customer.getDeliveryAddress().getPostcode() != null) {
			if (customer.getDeliveryAddress().getId() == null) {
				addressDAO.create(customer.getDeliveryAddress());
			} else {
				addressDAO.update(customer.getDeliveryAddress());
			}
		}
		
	}

	/**
	 * Assigns the customer's address to transient fields of customer
	 * 
	 * @param customer
	 * @throws Exception
	 */
	public void customerAddressDetail(Customer customer) throws Exception {
		customer.setAddress(addressDAO.findByCustAndType(customer.getId(), AddressTypeEnum.Address));
		if (customer.getAddress() == null)
			customer.setAddress(new Address(AddressTypeEnum.Address, customer));
		
		customer.setBillingAddress(addressDAO.findByCustAndType(customer.getId(), AddressTypeEnum.BillingAddress));
		if (customer.getBillingAddress() == null)
			customer.setBillingAddress(new Address(AddressTypeEnum.BillingAddress, customer));
		
		customer.setDeliveryAddress(addressDAO.findByCustAndType(customer.getId(), AddressTypeEnum.DeliveryAddress));
		if (customer.getDeliveryAddress() == null)
			customer.setDeliveryAddress(new Address(AddressTypeEnum.DeliveryAddress, customer));
	}
}
