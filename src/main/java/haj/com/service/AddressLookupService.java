package haj.com.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import haj.com.bean.AddressLookup;
import haj.com.entity.Address;
import haj.com.framework.AbstractService;

// TODO: Auto-generated Javadoc
/**
 * The Class AddressLookupService.
 */
public class AddressLookupService extends AbstractService {

	/** The Constant USER_AGENT. */
	private static final String USER_AGENT = "Mozilla/5.0";
	
	/** The address service. */
	private static AddressService addressService = new AddressService();
	
	/** The Constant logger. */
	protected static final Log logger = LogFactory.getLog(AddressLookupService.class);
	
	/**
	 * Find address.
	 *
	 * @param address the address
	 * @throws Exception the exception
	 */
	public static void findAddress(Address address) throws Exception {
		try {
			if (StringUtils.isBlank(address.getPostcode())) {
				throw new Exception("Please enter postcode");
			}
			else {
				String postCode = address.getPostcode().trim();
				postCode = postCode.replaceAll("\\s+","");
			AddressLookup al = sendGet(postCode);
			if (al.getStatus()==200) {
				//System.out.println(al.toString()+"\n");
				address.setPostcode(al.getResult().getPostcode());
				address.setLongitude(al.getResult().getLongitude());
				address.setLatitude(al.getResult().getLatitude());
				address.setAddressLine4(al.getResult().getCountry());
				address.setAddressLine3(al.getResult().getPrimaryCareTrust());
				if (!al.getResult().getParish().contains("unparished"))
				  address.setAddressLine2(al.getResult().getParish());
				else {
					 address.setAddressLine2(al.getResult().getAdminWard());
				}
			}
			}
		} catch (java.io.FileNotFoundException e) {
			logger.fatal(e);
			throw new Exception("Invalid post code!");
			
		}
		if (address.getLongitude()==null) addressService.lookupLongitudeLatitude(address);

	}
	

	
	
	/**
	 * Send get.
	 *
	 * @param pcode the pcode
	 * @return the address lookup
	 * @throws Exception the exception
	 */
	private static AddressLookup sendGet(String pcode) throws Exception {

		String url = "https://api.postcodes.io/postcodes/" + URLEncoder.encode(pcode, "UTF-8");

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		// int responseCode = con.getResponseCode();
		// System.out.println("\nSending 'GET' request to URL : " + url);
		// System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		// System.out.println(response.toString());
		Gson gson = new GsonBuilder().create();
		return gson.fromJson(response.toString(), AddressLookup.class);
	}
}
