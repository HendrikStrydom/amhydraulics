package haj.com.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

import haj.com.framework.AbstractService;

public class AtmospherePushService extends AbstractService {
	protected static final Log logger = LogFactory.getLog(AtmospherePushService.class);
	 
	public static void refreshWorkFlowBadge(String userid) {
		try {
			 EventBus eventBus = EventBusFactory.getDefault().eventBus();
            eventBus.publish(userid,"RefreshBadge");
		} catch (Exception e) {
			logger.fatal(e);
		}
	}
	
	public static void refreshWorkFlowBadge(Long userid) { 
		refreshWorkFlowBadge(""+userid);
	}
}
