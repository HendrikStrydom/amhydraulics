package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.WorkflowDAO;
import haj.com.entity.Workflow;
import haj.com.framework.AbstractService;

public class WorkflowService extends AbstractService {

	private WorkflowDAO dao = new WorkflowDAO();

	/**
	 * Get all Workflow
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @return List<Workflow>
 	 * @throws Exception
 	 */
	public List<Workflow> allWorkflow() throws Exception {
	  	return dao.allWorkflow();
	}

	/**
	 * Create or update  Workflow
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @throws Exception
 	 */
	public void create(Workflow entity) throws Exception  {
	   if (entity.getId() ==null) {
		 this.dao.create(entity);
	   }
		else
		 this.dao.update(entity);
	}

	/**
	 * Update  Workflow
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @throws Exception
 	 */
	public void update(Workflow entity) throws Exception  {
		this.dao.update(entity);
	}

	/**
	 * Delete  Workflow
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @throws Exception
 	 */
	public void delete(Workflow entity) throws Exception  {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @return a Workflow object
 	 * @throws Exception
 	 */
	public Workflow findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	/**
	 * Find Workflow by description
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @return List<Workflow>
 	 * @throws Exception
 	 */
	public List<Workflow> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	/**
	 * Lazy load Workflow
	 * @param first from key
	 * @param pageSize no of rows to fetch
	 * @return List<Workflow>
	 * @throws Exception
	 */
	public List<Workflow> allWorkflow(int first, int pageSize) throws Exception {
		return dao.allWorkflow(Long.valueOf(first), pageSize);
	}
		
	
	/**
	 * @author TechFinium 
	 * @return Number of rows in the Workflow
	 * @throws Exception
	 */
	public Long count() throws Exception {
	       return dao.count(Workflow.class);
	}
	
    /**
     * @author TechFinium 
     * @param class1 Workflow class
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<Workflow> containing data
     */	
	@SuppressWarnings("unchecked")
	public List<Workflow> allWorkflow(Class<Workflow> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<Workflow>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * 
     * @param entity Workflow class
     * @param filters
     * @return Number of rows in the Workflow entity
     */	
	public int count(Class<Workflow> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
}
