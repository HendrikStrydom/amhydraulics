package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.CustomerUsersDAO;
import haj.com.entity.CustomerUsers;
import haj.com.framework.AbstractService;

public class CustomerUsersService extends AbstractService {

	private CustomerUsersDAO dao = new CustomerUsersDAO();

	/**
	 * Get all CustomerUsers
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @return List<CustomerUsers>
 	 * @throws Exception
 	 */
	public List<CustomerUsers> allCustomerUsers() throws Exception {
	  	return dao.allCustomerUsers();
	}

	/**
	 * Create or update  CustomerUsers
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @throws Exception
 	 */
	public void create(CustomerUsers entity) throws Exception  {
	   if (entity.getId() ==null) {
		 this.dao.create(entity);
	   }
		else
		 this.dao.update(entity);
	}

	/**
	 * Update  CustomerUsers
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @throws Exception
 	 */
	public void update(CustomerUsers entity) throws Exception  {
		this.dao.update(entity);
	}

	/**
	 * Delete  CustomerUsers
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @throws Exception
 	 */
	public void delete(CustomerUsers entity) throws Exception  {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @return a CustomerUsers object
 	 * @throws Exception
 	 */
	public CustomerUsers findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	/**
	 * Find CustomerUsers by description
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @return List<CustomerUsers>
 	 * @throws Exception
 	 */
	public List<CustomerUsers> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	/**
	 * Lazy load CustomerUsers
	 * @param first from key
	 * @param pageSize no of rows to fetch
	 * @return List<CustomerUsers>
	 * @throws Exception
	 */
	public List<CustomerUsers> allCustomerUsers(int first, int pageSize) throws Exception {
		return dao.allCustomerUsers(Long.valueOf(first), pageSize);
	}
		
	
	/**
	 * @author TechFinium 
	 * @return Number of rows in the CustomerUsers
	 * @throws Exception
	 */
	public Long count() throws Exception {
	       return dao.count(CustomerUsers.class);
	}
	
    /**
     * @author TechFinium 
     * @param class1 CustomerUsers class
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<CustomerUsers> containing data
     */	
	@SuppressWarnings("unchecked")
	public List<CustomerUsers> allCustomerUsers(Class<CustomerUsers> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<CustomerUsers>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * 
     * @param entity CustomerUsers class
     * @param filters
     * @return Number of rows in the CustomerUsers entity
     */	
	public int count(Class<CustomerUsers> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
}
