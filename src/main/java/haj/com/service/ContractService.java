package haj.com.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.AddressDAO;
import haj.com.dao.ContractDAO;
import haj.com.entity.Blank;
import haj.com.entity.Contract;
import haj.com.entity.ContractOverride;
import haj.com.entity.Customer;
import haj.com.entity.enums.AddressTypeEnum;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.ContractTypeEnum;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class ContractService extends AbstractService {

	private ContractDAO dao = new ContractDAO();
	private AddressDAO addressDAO = new AddressDAO();

	public ContractOverride getOverrideNumber() throws Exception {
		return dao.getOverrideNumber();
	}

	public void overrideContractEnquiryNumber(Integer number, Contract contract, ContractOverride co) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("yy");
		String formattedDate = df.format(Calendar.getInstance().getTime());
		contract.setEnquiryNumber("E" + String.format("%04d", co.getOverrideNumber()) + "/" + formattedDate);
		contract.setContractNumber("A" + String.format("%04d", co.getOverrideNumber()));
		this.dao.update(contract);
		co.setOverrideNumber(co.getOverrideNumber() + 1);
		updateOverrideNumber(co);
	}

	public void saveOverrideNumber(Integer number, Boolean contractB, Boolean enquiry, Contract contract)
			throws Exception {
		if (!checkIfNumberUsedAlready(number))
			throw new Exception("Number already in use");
		ContractOverride co = getOverrideNumber();
		if (co == null) {
			co = new ContractOverride(number, enquiry, contractB);
			dao.create(co);
		} else {
			co.setOverrideNumber(number);
			co.setEnquiry(enquiry);
			co.setContract(contractB);
			dao.update(co);
		}
		overrideContractEnquiryNumber(number, contract, co);
	}

	public Long getOverrideNumberByNumber(Integer number) throws Exception {
		return dao.getOverrideNumberByNumber(number);
	}

	public Integer findByEnquiryOrContractNumber(String number) throws Exception {
		return dao.findByEnquiryOrContractNumber(number).intValue();
	}

	public Boolean checkIfNumberUsedAlready(Integer number) throws Exception {
		Integer countOfCount = 0;
		String numberStrng = number.toString();
		countOfCount = findByEnquiryOrContractNumber(numberStrng);
		countOfCount += getOverrideNumberByNumber(number).intValue();
		if (countOfCount == 0)
			return true;
		else
			return false;
	}

	public void updateOverrideNumber(ContractOverride contractOverride) throws Exception {
		dao.update(contractOverride);
	}

	public List<Contract> allContract() throws Exception {
		return dao.allContract();
	}

	public List<Contract> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public void create(Contract entity) throws Exception {
		if (entity.getId() == null) {
			SimpleDateFormat df = new SimpleDateFormat("yy");
			String formattedDate = df.format(Calendar.getInstance().getTime());
			this.dao.create(entity);
			ContractOverride co = getOverrideNumber();
			if (co == null) {
				entity.setEnquiryNumber("E" + String.format("%04d", 5000l + entity.getId()) + "/" + formattedDate);
				entity.setContractNumber("A" + String.format("%04d", 5000l + entity.getId()));
			} else {
				while (findByEnquiryOrContractNumber(co.getOverrideNumber().toString()) > 0) {
					co.setOverrideNumber(co.getOverrideNumber() + 1);
				}
				entity.setEnquiryNumber("E" + String.format("%04d", co.getOverrideNumber()) + "/" + formattedDate);
				entity.setContractNumber("A" + String.format("%04d", co.getOverrideNumber()));
				co.setOverrideNumber(co.getOverrideNumber() + 1);
				updateOverrideNumber(co);
			}
			this.dao.update(entity);
		} else {
			if (entity.getRevisionNumber() >= 1)
				entity.setRevisionNumber(entity.getRevisionNumber() + 1);
			this.dao.update(entity);
		}
	}

	public void create(IDataEntity entity) throws Exception {
		this.dao.create(entity);
	}

	public void update(IDataEntity entity) throws Exception {
		this.dao.update(entity);
	}

	public void update(Contract entity) throws Exception {
		this.dao.update(entity);
	}

	public void updateWithRevisionNumber(Contract entity) throws Exception {
		if (entity.getRevisionNumber() >= 1)
			entity.setRevisionNumber(entity.getRevisionNumber() + 1);
		this.dao.update(entity);
	}

	public void delete(Contract entity) throws Exception {
		entity.setSoftDelete(true);
		this.dao.update(entity);
	}

	public Contract findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public Contract findByGuid(String guid) throws Exception {
		return dao.findByGuid(guid);
	}

	public List<Contract> findByPossibleNumber(String nr) throws Exception {
		Long id = 0l;
		try {
			id = Long.valueOf(nr.trim());
		} catch (NumberFormatException e) {
			throw new Exception("Enter valid number");
		}
		return dao.findByPossibleNumber(id);
	}

	public List<Contract> findByCustomer(Customer customer) throws Exception {
		return dao.findByCustomer(customer.getId());
	}

	public List<Contract> findByCustomerByEnquiry(Long customerId, String description) throws Exception {
		return dao.findByCustomerByEnquiry(customerId, description);
	}

	public List<Contract> findByCustomerContractStatus(Customer customer, ContractStatusEnum contractStatusEnum)
			throws Exception {
		return dao.findByCustomerContractStatus(customer.getId(), contractStatusEnum);
	}

	public List<Contract> findByContractStatus(ContractStatusEnum contractStatusEnum) throws Exception {
		return dao.findByContractStatus(contractStatusEnum);
	}

	public List<Contract> allContract(int first, int pageSize) throws Exception {
		return dao.allContract(Long.valueOf(first), pageSize);
	}

	public Long count() throws Exception {
		return dao.count(Contract.class);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> allContract(Class<Contract> class1, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<Contract>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> allContract(Class<Contract> class1, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters, ContractStatusEnum contractStatusEnum) throws Exception {

		String whereHql = "where o.contractStatus = " + contractStatusEnum.ordinal();
		return (List<Contract>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters, whereHql,
				null);

	}

	@SuppressWarnings("unchecked")
	public List<Contract> allAwaitingContract(Class<Contract> class1, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		String whereHql = "where o.contractStatus = " + ContractStatusEnum.SubmitForQuotation.ordinal()
				+ " or o.contractStatus = " + ContractStatusEnum.QuoteInProgress.ordinal();
		return (List<Contract>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters, whereHql,
				null);
	}

	public int count(Class<Contract> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	public int count(Class<Contract> entity, Map<String, Object> filters, ContractStatusEnum contractStatusEnum)
			throws Exception {
		String whereHql = "where o.contractStatus = " + contractStatusEnum.ordinal();
		return dao.count(entity, filters, whereHql);
	}

	public int countAwaiting(Class<Contract> entity, Map<String, Object> filters) throws Exception {
		String whereHql = "where o.contractStatus = " + ContractStatusEnum.SubmitForQuotation.ordinal()
				+ " or o.contractStatus = " + ContractStatusEnum.QuoteInProgress.ordinal();
		return dao.count(entity, filters, whereHql);
	}

	public void save(Contract contract) throws Exception {

		if (contract.getCustomer().getId() == -1) {
			create(contract.getCustomer());
		} else {
			update(contract.getCustomer());
		}

		Boolean createContract = previousContracts(contract.getCustomer());
		if (createContract == false) {
			create(contract);
		}

		/* Create Address */
		if (contract.getCustomer().getAddress().getPostcode() != null) {
			if (contract.getCustomer().getAddress().getId() == null) {
				create(contract.getCustomer().getAddress());
			} else {
				update(contract.getCustomer().getAddress());
			}
		}

		/* Billing Address */
		if (contract.getCustomer().getBillingAddress().getPostcode() != null) {
			if (contract.getCustomer().getBillingAddress().getId() == null) {
				create(contract.getCustomer().getBillingAddress());
			} else {
				update(contract.getCustomer().getBillingAddress());
			}
		}

		/* Delivery Address */
		if (contract.getCustomer().getDeliveryAddress().getPostcode() != null) {
			if (contract.getCustomer().getDeliveryAddress().getId() == null) {
				create(contract.getCustomer().getDeliveryAddress());
			} else {
				update(contract.getCustomer().getDeliveryAddress());
			}
		}

		contractDetail(contract);

	}

	private Boolean previousContracts(Customer customer) throws Exception {
		List<Contract> contractList = dao.findByCustomer(customer.getId());
		Boolean previousContracts = false;
		if (contractList.size() > 0) {
			previousContracts = true;
		}
		return previousContracts;
	}

	public void contractDetail(Contract contract) throws Exception {
		contract.getCustomer()
				.setAddress(addressDAO.findByCustAndType(contract.getCustomer().getId(), AddressTypeEnum.Address));
		contract.getCustomer().setBillingAddress(
				addressDAO.findByCustAndType(contract.getCustomer().getId(), AddressTypeEnum.BillingAddress));
		contract.getCustomer().setDeliveryAddress(
				addressDAO.findByCustAndType(contract.getCustomer().getId(), AddressTypeEnum.DeliveryAddress));

	}

	public List<Contract> findContractByQueryParameter(Map<String, Object> queryParameters) throws Exception {
		String hql = "select o from Contract o where o.softDelete = false";
		if (queryParameters.containsKey("ConEnqNumber")) {
			hql += " and (o.contractNumber LIKE :ConEnqNumber or o.enquiryNumber LIKE :ConEnqNumber)";
		} else if (queryParameters.containsKey("orderNumber")) {
			hql += " and (o.orderNumber LIKE :orderNumber)";
		} else if (queryParameters.containsKey("details")) {
			hql += " and (o.details LIKE :details or o.details LIKE :details or o.title LIKE :details)";
		}
		return dao.findContractByQueryParameter(hql, queryParameters);
	}

}
