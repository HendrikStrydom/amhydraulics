package haj.com.service;

import java.util.List;

import haj.com.dao.BlankDAO;
import haj.com.dao.ResourceUsersDAO;
import haj.com.entity.Blank;
import haj.com.entity.ResourceUsers;
import haj.com.framework.AbstractService;

public class ResourceUsersService extends AbstractService {

	private ResourceUsersDAO dao = new ResourceUsersDAO();

	public List<Blank> allBlank() throws Exception {
	  	return dao.allBlank();
	}


	public void create(ResourceUsers entity) throws Exception  {
	   if (entity.getId() ==null)
		 this.dao.create(entity);
		else
		 this.dao.update(entity);
	}

	public void update(Blank entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(Blank entity) throws Exception  {
		this.dao.delete(entity);
	}

	public Blank findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<Blank> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

}
