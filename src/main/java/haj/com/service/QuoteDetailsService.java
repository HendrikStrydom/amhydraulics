package haj.com.service;

import java.util.List;

import haj.com.dao.QuoteDetailsDAO;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.Quote;
import haj.com.entity.QuoteItem;
import haj.com.framework.AbstractService;

public class QuoteDetailsService extends AbstractService {

	private QuoteDetailsDAO dao = new QuoteDetailsDAO();

	public List<QuoteItem> allQuoteDetails() throws Exception {
		return dao.allQuoteDetails();
	}

	public List<ContractReviewItem> findByQuoteReturnDetails(Long quoteId) throws Exception {
		return dao.findByQuoteReturnDetails(quoteId);
	}

	public List<QuoteItem> findByQuote(Long quoteId) throws Exception {
		return dao.findByQuote(quoteId);
	}

	public List<QuoteItem> findByQuoteAndDetail(Long quoteId, Long detailId) throws Exception {
		return dao.findByQuoteAndDetail(quoteId, detailId);
	}

	public void create(QuoteItem entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

/*	public void createQuoteDetail(Quote quote, List<Details> details) throws Exception {
		for (QuoteDetails qd : findByQuote(quote.getId())) {
			details.remove(qd.getDetailsId());
		}
		QuoteDetails qd;
		for (Details detail : details) {
			qd = new QuoteDetails();
			qd.setDetails(detail);
			qd.setQuote(quote);
			this.dao.create(qd);
		}
	}*/

	public void update(QuoteItem entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(QuoteItem entity) throws Exception {
		this.dao.delete(entity);
	}

	public QuoteItem findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<QuoteItem> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

}
