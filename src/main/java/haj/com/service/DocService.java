package haj.com.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.hibernate.sql.Template;

import haj.com.dao.DocDAO;
import haj.com.entity.Contract;
import haj.com.entity.Doc;
import haj.com.entity.Users;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.framework.AbstractService;

public class DocService extends AbstractService {

	private DocDAO dao = new DocDAO();
	private UsersService usersService = new UsersService();

	/*
	 * private UsersStructureDocsService usersStructureDocsService = new
	 * UsersStructureDocsService();
	 * 
	 * public List<Doc> search(Template template) throws Exception { return
	 * resolveVersions(dao.search(template), false); }
	 * 
	 * public Users search(Users user) throws Exception { user =
	 * usersService.findByKey(user.getUid()); for (UsersDoc ud :
	 * user.getUsersDocs()) { if (ud.getDoc() != null)
	 * ud.setDocs(search(ud.getDoc())); } return user; }
	 * 
	 * public List<Doc> searchParent(Long id) throws Exception { return
	 * dao.searchParent(id); }
	 * 
	 * public void search(UsersStructure usersStructure) throws Exception {
	 * List<UsersStructureDocs> usersStructureDocs =
	 * usersStructureDocsService.findUsersStructure(usersStructure); for
	 * (UsersStructureDocs ud : usersStructureDocs) {
	 * ud.setDocs(search(ud.getDoc())); }
	 * usersStructure.setStructureDocs(usersStructureDocs); }
	 */
	public List<Doc> search(Doc doc) throws Exception {
		if (doc == null || doc.getId() == null)
			return null;
		if (doc.getDoc() != null) {
			return resolveVersions(dao.search(doc.getDoc().getId()), false);
		} else {
			return resolveVersions(dao.search(doc.getId()), false);
		}
	}

	public List<Doc> findByContract(Contract contract) throws Exception {
		return resolveVersions(dao.findByContract(contract), false);
	}

	public List<Doc> findByCollectDeliverContract(Long collectDeliverContractId) throws Exception {
		return dao.findByCollectDeliverContract(collectDeliverContractId);
	}

	public List<Doc> findByCollectDeliverItem(Long collectDeliverItemId) throws Exception {
		return dao.findByCollectDeliverItem(collectDeliverItemId);
	}

	public List<Doc> findFileName(String filename, Contract contract) throws Exception {
		return dao.findFileName(filename, contract);
	}

	public List<Doc> resolveVersions(List<Doc> search, boolean clearcontents) {
		List<Doc> result = new ArrayList<Doc>();
		try {
			for (Doc doc : search) {
				if (clearcontents)
					doc.setFileContent(null);
				resolveVersionCommon(result, doc, clearcontents);
			}
		} catch (Exception e) {
			logger.fatal(e);
		}
		return result;
	}

	private void resolveVersionCommon(List<Doc> result, Doc doc, boolean clearcontents) throws Exception {

		List<Doc> t = getVersions(doc);

		if (t != null && t.size() > 0) {
			Doc x = t.get(0);
			List<Doc> versions = new ArrayList<Doc>();

			for (Doc doc2 : t) {
				if (clearcontents)
					doc2.setFileContent(null);
				if (doc2.getId() != x.getId())
					versions.add(doc2);
			}

			versions.add(doc);
			x.setDocVerions(versions);
			result.add(x);
		} else {
			result.add(doc);
		}

	}

	public List<Doc> getVersions(Doc d) throws Exception {
		return dao.getVersions(d);
	}

	public void save(Doc doc, byte[] contents, String fileName, Users user) throws Exception {
		doc.setFileContent(contents);
		doc.setOriginalFname(fileName);
		doc.setExtension(FilenameUtils.getExtension(fileName));
		doc.setAddDate(new java.util.Date());
		// doc.setDocRef(createRef(doc.getTemplate()));
		doc.setUser(user);
		doc.setVersionNo(1);
		// if (doc.getTemplate() != null) {
		// doc.setKeyword(doc.getTemplate().getTitle());
		// }

		doc.setSearch(true);
		dao.create(doc);
		DocumentTrackerService.create(user, doc, DocumentTrackerEnum.Upload);
	}

	private String createRef(Template template) {
		if (template == null)
			return "";
		else {
			String ref = null;
			// ref = template.getTitle().trim();
			if (ref.length() > 200) {
				ref = ref.substring(0, 198);
			}
			return ref;
		}
	}

	public Doc getRootDoc(List<Doc> docs, Doc originalDoc) {
		Doc t = null;
		if (docs == null || docs.size() == 0) {
			t = originalDoc;
		} else {
			for (Doc doc : docs) {
				if (doc.getDoc() == null)
					t = doc;
			}
		}
		return t;
	}

	public void save(Doc doc, byte[] contents, String fileName, Users user, List<Doc> docs) throws Exception {
		if (docs == null || docs.size() == 0) {
			save(doc, contents, fileName, user);
		} else {
			doc.setFileContent(contents);
			doc.setOriginalFname(fileName);
			doc.setExtension(FilenameUtils.getExtension(fileName));
			doc.setAddDate(new java.util.Date());
			// doc.setDocRef(createRef(doc.getTemplate()));
			doc.setUser(user);
			Doc orig = docs.get(0);
			doc.setDoc(getRootDoc(orig.getDocVerions(), docs.get(0)));
			/*
			 * if (orig.getDocVerions()!=null && orig.getDocVerions().size()>0)
			 * { Doc tdoc = null; for (Doc doc2 : orig.getDocVerions()) { tdoc =
			 * doc2; break; } doc.setVersionNo(tdoc.getVersionNo()+1); } else {
			 * doc.setVersionNo(orig.getVersionNo()+1); }
			 */
			doc.setVersionNo(orig.getVersionNo() + 1);
			// if (doc.getTemplate() != null) {
			// doc.setKeyword(doc.getTemplate().getTitle());
			// }
			//
			// else
			doc.setKeyword("");
			doc.setSearch(false);
			dao.create(doc);
			DocumentTrackerService.create(user, doc.getDoc(), DocumentTrackerEnum.UploadVersion);
		}

	}

}
