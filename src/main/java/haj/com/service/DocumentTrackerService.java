package haj.com.service;

import java.util.List;

import haj.com.dao.DocumentTrackerDAO;
import haj.com.entity.Doc;
import haj.com.entity.DocumentTracker;
import haj.com.entity.Users;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.framework.AbstractService;

public class DocumentTrackerService extends AbstractService {

	private static DocumentTrackerDAO dao = new DocumentTrackerDAO();
	private DocService docService = new DocService();

	public List<DocumentTracker> allDocumentTracker() throws Exception {
	  	return dao.allDocumentTracker();
	}


	public void create(DocumentTracker entity) throws Exception  {
		this.dao.create(entity);
	}

	
	public static void create(Users user, Doc doc, DocumentTrackerEnum documentTrackerEnum) throws Exception  {
		dao.create(new DocumentTracker(doc, user, new java.util.Date(), documentTrackerEnum));
	}
	
	public void update(DocumentTracker entity) throws Exception  {
		dao.update(entity);
	}

	public void delete(DocumentTracker entity) throws Exception  {
		dao.delete(entity);
	}

	public DocumentTracker findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<DocumentTracker> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<DocumentTracker> byDoc( Doc doc) throws Exception  { 
		return dao.byDoc(doc.getId());
	}
	
	public List<DocumentTracker> byRoot( Doc originalDoc) throws Exception  { 
		Doc d = docService.getRootDoc(originalDoc.getDocVerions(), originalDoc);
		return byDoc(d);
	}
}
