package haj.com.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.techfinium.entity.ResourceAllocation;
import com.techfinium.framework.IDataEntity;

import haj.com.entity.AMResourceAllocation;

public class ResourceAllocationService extends com.techfinium.service.ResourceAllocationService {

	
	
	   public List<AMResourceAllocation> resourceAllocation(Date fromDate, Date toDate) throws Exception {
		   List<AMResourceAllocation> ralsit = new  ArrayList<AMResourceAllocation>();
		   List<IDataEntity> l = super.dao.allResourceAllocation(fromDate,toDate);
		   for (IDataEntity iDataEntity : l) {
			 if (AMResourceAllocation.class.getName().equals( ((ResourceAllocation)iDataEntity).getType()) ) {
				 ralsit.add((AMResourceAllocation)iDataEntity);
			 }
		   }
		   return ralsit;
	}	
}
