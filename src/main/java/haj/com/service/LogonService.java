package haj.com.service;

import java.util.Date;

import org.jasypt.util.password.StrongPasswordEncryptor;

import haj.com.dao.UsersDAO;
//import haj.com.entity.HostingCompany;
import haj.com.entity.Users;
import haj.com.entity.enums.UsersStatusEnum;
import haj.com.framework.AbstractService;
import haj.com.utils.GenericUtility;

public class LogonService extends AbstractService {

	private UsersDAO dao = new UsersDAO();
	private static StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();

	public static String encryptPassword(String pwd) throws Exception {
		return passwordEncryptor.encryptPassword(pwd);
	}

	public Users logonByEmail(String email, String inputPassword) throws Exception {
		Users u = dao.getUserByEmail(email);
		if (u == null)
			throw new Exception("The email address is currently not registered on the system! If you typed in the correct email please contact support.");

		else if (u.getStatus() == UsersStatusEnum.InActive)
			throw new Exception("Your profile is not active. Please contact support!");
		else if (u.getStatus() == UsersStatusEnum.EmailNotConfrimed)
			throw new Exception("You have not confirmed your email address. Please check your mail box");

		else {
			StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
			if (!passwordEncryptor.checkPassword(inputPassword.trim(), u.getPassword().trim())) {
				throw new Exception("Invalid password for your profile email address");
			}
		}
		if (u.getLastLogin() == null) {
			u.setChgPwdNow(true);
		} else {
			u.setLastLogin(new Date());
			u.setChgPwdNow(false);
			dao.update(u);
		}
		return u;
	}

	public Users changePassword(String email, String password, String newPassword) throws Exception {
		Users u = new UsersDAO().getUserByEmail(email);
		if (u == null)
			throw new Exception("User with email address: " + email + " is not registered on the system! If you typed in the correct email please contact support.");
		else {
			StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
			if (!passwordEncryptor.checkPassword(password.trim(), u.getPassword().trim())) {
				throw new Exception("Invalid password for user id: " + email);
			}
		}
		u.setPassword(encryptPassword(newPassword));
		dao.update(u);
		return u;
	}

	public Users changePassword(Users u, String pwd) throws Exception {
		u.setPassword(encryptPassword(pwd));
		u.setLastLogin(new java.util.Date());
		dao.update(u);
		return u;
	}

	public Users changePassword(Users u) throws Exception {
		u.setPassword(encryptPassword(u.getPassword()));
		dao.update(u);
		return u;
	}
/*	public void notifyUserNewPassword(String email, HostingCompany hostingCompany) throws Exception {
		Users ul = dao.getUserByEmail(email);
		if (ul == null)
			throw new Exception("User with email : " + email + " is not registered on the system! If you typed in the correct username please contact support.");
		notifyUserNewPassword(ul, hostingCompany);
	}

	public void notifyUserNewPassword(Users u, HostingCompany hostingCompany) throws Exception {
		String pwd = GenericUtility.genPassord();
		u.setPassword(encryptPassword(pwd));
		dao.update(u);
		String welcome = "<p>Dear #NAME#,</p>" + "<br/>" + "<p>This is your new password: <b>" + pwd + "</b> for email: <b>" + u.getEmail() + "</b></p>"
				+ "<p>You can change it after you have logged in.</p>" + "<p>Regards</p>" + "<p>The Compliance Portal team</p>" + "<br/>";

	
		welcome = welcome.replaceAll("#NAME#", u.getFirstName());
		GenericUtility.sendMail(u.getEmail(), "Rock COP password reset", welcome);
	}	
	*/
	
	public void notifyUserNewPasswordEmail(String email) throws Exception {
		Users ul = dao.getUserByEmail(email);
		if (ul==null) throw new Exception("User with email : "+ email +" is not registered on the system! If you typed in the correct username please contact support.");
		notifyUserNewPassword(ul);
	}
	public void notifyUserNewPassword(Users u) throws Exception {
		String pwd = GenericUtility.genPassord(); 
		 u.setPassword(encryptPassword(pwd));
		 dao.update(u);
		String welcome = "<p>Dear #NAME#,</p>"+	
				"<br/>" +
				"<p>This is your new password: <b>"+pwd+ "</b> for email: <b>"+u.getEmail()+"</b></p>"+
				"<p>You can change it after you have logged in.</p>"+
				"<p>Regards</p>"+
				"<p>The Tideway team</p>"+
				"<br/>";
		welcome = welcome.replaceAll("#NAME#",u.getFirstName());
		GenericUtility.sendMail(u.getEmail(), "Timesheet password reset", welcome);
	}
}
