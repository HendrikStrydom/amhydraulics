package haj.com.service;

import java.util.List;
import java.util.Map;

import haj.com.dao.InspectionReportDAO;
import haj.com.entity.InspectionReport;
import haj.com.entity.RouteCard;
import haj.com.framework.AbstractService;

public class InspectionReportService extends AbstractService {

	private InspectionReportDAO dao = new InspectionReportDAO();

	public List<InspectionReport> allInspectionReport() throws Exception {
		return dao.allInspectionReport();
	}

	public List<InspectionReport> findByContract(Long contractId) throws Exception {
		return dao.findByContract(contractId);
	}

	public void create(InspectionReport entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
			if (entity.getFormNumber() == null)
				entity.setFormNumber("AM" + String.format("%04d", 5000l + entity.getId()));
			this.dao.update(entity);
		} else {
			if (entity.getRevisionNumber() >= 1)
				entity.setRevisionNumber(entity.getRevisionNumber() + 1);
			this.dao.update(entity);
		}
	}

	public void update(InspectionReport entity) throws Exception {
		this.dao.update(entity);
	}

	public void updateInspecReportRevisionNumber(InspectionReport entity) throws Exception {
		if (entity.getRevisionNumber() >= 1)
			entity.setRevisionNumber(entity.getRevisionNumber() + 1);
		this.dao.update(entity);
	}

	public void delete(InspectionReport entity) throws Exception {
		this.dao.delete(entity);
	}

	public InspectionReport findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<InspectionReport> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<InspectionReport> findInspectionReportByQueryParameter(Map<String, Object> queryParameters) throws Exception {
		String hql = "select o from InspectionReport o where o.softDelete = false";
		if (queryParameters.containsKey("ConEnqNumber")) {
			hql += " and (o.contract.contractNumber LIKE :ConEnqNumber or o.contract.enquiryNumber LIKE :ConEnqNumber)";
		}
		if (queryParameters.containsKey("customerOrderNum")) {
			hql += " and o.customerOrderNumber LIKE :customerOrderNum";
		}
		if (queryParameters.containsKey("formNumber")) {
			hql += " and o.formNumber LIKE :formNumber";
		}
		if (queryParameters.containsKey("completed")) {
			hql += " and o.completedReport = :completed";
		}
		return dao.findInspectionReportByQueryParameter(hql, queryParameters);
	}

}
