package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.TaskUsersDAO;
import haj.com.entity.TaskUsers;
import haj.com.framework.AbstractService;

public class TaskUsersService extends AbstractService {

	private TaskUsersDAO dao = new TaskUsersDAO();

	/**
	 * Get all TaskUsers
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @return List<TaskUsers>
 	 * @throws Exception
 	 */
	public List<TaskUsers> allTaskUsers() throws Exception {
	  	return dao.allTaskUsers();
	}

	/**
	 * Create or update  TaskUsers
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @throws Exception
 	 */
	public void create(TaskUsers entity) throws Exception  {
	   if (entity.getId() ==null) {
		 this.dao.create(entity);
	   }
		else
		 this.dao.update(entity);
	}

	/**
	 * Update  TaskUsers
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @throws Exception
 	 */
	public void update(TaskUsers entity) throws Exception  {
		this.dao.update(entity);
	}

	/**
	 * Delete  TaskUsers
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @throws Exception
 	 */
	public void delete(TaskUsers entity) throws Exception  {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @return a TaskUsers object
 	 * @throws Exception
 	 */
	public TaskUsers findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<TaskUsers> findByTask(Long taskId) throws Exception {
	       return dao.findByTask(taskId);
		}
	/**
	 * Find TaskUsers by description
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @return List<TaskUsers>
 	 * @throws Exception
 	 */
	public List<TaskUsers> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	/**
	 * Lazy load TaskUsers
	 * @param first from key
	 * @param pageSize no of rows to fetch
	 * @return List<TaskUsers>
	 * @throws Exception
	 */
	public List<TaskUsers> allTaskUsers(int first, int pageSize) throws Exception {
		return dao.allTaskUsers(Long.valueOf(first), pageSize);
	}
		
	
	/**
	 * @author TechFinium 
	 * @return Number of rows in the TaskUsers
	 * @throws Exception
	 */
	public Long count() throws Exception {
	       return dao.count(TaskUsers.class);
	}
	
    /**
     * @author TechFinium 
     * @param class1 TaskUsers class
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<TaskUsers> containing data
     */	
	@SuppressWarnings("unchecked")
	public List<TaskUsers> allTaskUsers(Class<TaskUsers> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<TaskUsers>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * 
     * @param entity TaskUsers class
     * @param filters
     * @return Number of rows in the TaskUsers entity
     */	
	public int count(Class<TaskUsers> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
	
	
	
	
	
}
