package haj.com.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.primefaces.model.SortOrder;

import haj.com.constants.HAJConstants;
import haj.com.dao.TasksDAO;
import haj.com.entity.Contract;
import haj.com.entity.InspectionReport;
import haj.com.entity.Quote;
import haj.com.entity.RouteCard;
import haj.com.entity.TaskUsers;
import haj.com.entity.Tasks;
import haj.com.entity.Users;
import haj.com.entity.enums.TaskStatusEnum;
import haj.com.entity.enums.UserTypeEnum;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;
import haj.com.utils.GenericUtility;

public class TasksService extends AbstractService {

	private TasksDAO dao = new TasksDAO();
	private UsersService usersService = new UsersService();

	public TasksService() {
		super();
	}

	public TasksService(Map<String, Object> auditlog) {
		super(auditlog);
	}

	public List<Tasks> allTasks() throws Exception {
		return dao.allTasks();
	}

	public void create(Tasks entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else
			this.dao.update(entity);
	}

	public void update(Tasks entity) throws Exception {
		this.dao.update(entity);
	}

	public Tasks findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<Tasks> findTaskListByKey(Long id) throws Exception {
		return dao.findTaskListByKey(id);
	}

	public List<Tasks> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<Tasks> allTasks(int first, int pageSize) throws Exception {
		return dao.allTasks(Long.valueOf(first), pageSize);
	}

	public Long count() throws Exception {
		return dao.count(Tasks.class);
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> allTasks(Class<Tasks> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<Tasks>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	public int count(Class<Tasks> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	public List<Tasks> findTasksByUser(Users user) throws Exception {
		return dao.findTasksByUser(user.getId());
	}

	public List<Tasks> findTasksByUserContract(Users user, Contract contract) throws Exception {
		return dao.findTasksByUserContract(user.getId(), contract.getId());
	}

	public Boolean findTasksByContractB(Contract contract) throws Exception {
		return dao.findTasksByContractB(contract.getId());
	}

	public List<Tasks> findTasksByUserIncomplete(Users user) throws Exception {
		return dao.findTasksByUserIncomplete(user.getId());
	}

	public List<Tasks> findPriorityTasksByUserIncomplete(Users user) throws Exception {
		return dao.findPriorityTasksByUserIncomplete(user.getId());
	}

	public List<Tasks> findTasksByUserAndStatus(Users user, TaskStatusEnum status) throws Exception {
		return dao.findTasksByUserAndStatus(user.getId(), status);
	}

	public void openTask(Tasks task, Users user) throws Exception {
		task.setCompletionDate(new Date());
		if (task.getDueDate().before(new Date()))
			task.setTaskStatus(TaskStatusEnum.NotStarted);
		else
			task.setTaskStatus(TaskStatusEnum.Overdue);
		task.setActionUser(user);
		dao.update(task);
	}

	public void closeTask(Tasks task, Users user) throws Exception {
		if (task.getTaskStatus() != TaskStatusEnum.Closed) {
			task.setCompletionDate(new Date());
			task.setTaskStatus(TaskStatusEnum.Closed);
			task.setActionUser(user);
			dao.update(task);
		}
	}

	public void completeTask(Tasks task, Users user) throws Exception {
		if (task.getTaskStatus() != TaskStatusEnum.Completed) {
			task.setCompletionDate(new Date());
			if (task.getTaskStatus() != TaskStatusEnum.Closed)
				task.setTaskStatus(TaskStatusEnum.Completed);
			task.setActionUser(user);
			dao.update(task);
		}
	}

	public Tasks contractTaskBasedOnRepairNew(Users createUser, IDataEntity object) throws Exception {
		List<Users> taskReceivers = new ArrayList<Users>();
		Tasks newTask = intialiseTask(createUser, object);
		if (object instanceof Contract) {
			Contract contract = (Contract) object;
			newTask.setTargetClass(contract.getClass().getName());
			if (contract.getContractType() != null) {
				switch (contract.getContractType().ordinal()) {
				case 0:// Repair
					taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Mechanic));
					newTask.setTaskDirectPage("/pages/inspectionReportForm.jsf?contractId=" + contract.getId());
					newTask.setDueDate(GenericUtility.addDaysToDateExcludeWeekends(new java.util.Date(), 1));
					newTask.setTaskDescription("Repair." + System.getProperty("line.separator") + " S/I Awaiting for " + contract.getCustomer().getCustomerName() + System.getProperty("line.separator") + "Enquiry Number: " + contract.getEnquiryNumber());
					contractEnquiryIfRepair(createUser, object);
					break;
				case 1:// New
					taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Management));
					taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Employee));
					newTask.setDueDate(GenericUtility.addDaysToDateExcludeWeekends(new java.util.Date(), 2));
					newTask.setTaskDirectPage("/pages/quoteForm.jsf?contractId=" + contract.getId());
					newTask.setTaskDescription("New Component." + System.getProperty("line.separator") + " Prepare Quote for " + contract.getCustomer().getCustomerName() + System.getProperty("line.separator") + "Enquiry Number: " + contract.getEnquiryNumber());
					break;
				case 2:// Order
					taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Management));
					taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Employee));
					newTask.setDueDate(new java.util.Date());
					newTask.setTaskDirectPage("/pages/quoteForm.jsf?contractId=" + contract.getId());
					newTask.setTaskDescription("Order." + System.getProperty("line.separator") + " Prepare Quote for " + contract.getCustomer().getCustomerName() + System.getProperty("line.separator") + "Enquiry Number: " + contract.getEnquiryNumber());
					break;
				default:
					break;
				}
			}
		}
		sendTaskToUsers(newTask, taskReceivers);
		sendMailTaskUsers(newTask, taskReceivers);
		return newTask;
	}

	public Tasks createTaskForNewContract(Users createUser, IDataEntity object) throws Exception {
		List<Users> taskReceivers = new ArrayList<Users>();
		Tasks newTask = intialiseTask(createUser, object);
		if (object instanceof Contract) {
			taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Management));
			taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Employee));
			Contract contract = (Contract) object;
			newTask.setTargetClass(contract.getClass().getName());
			newTask.setDueDate(GenericUtility.addDaysToDateExcludeWeekends(new java.util.Date(), 1));
			newTask.setTaskDirectPage("/pages/enquiryForm.jsf?enqId=" + contract.getId());
			newTask.setTaskDescription("Enquiry waiting for " + contract.getCustomer().getCustomerName() + System.getProperty("line.separator") + "Enquiry Number: " + contract.getEnquiryNumber());
		}
		sendTaskToUsers(newTask, taskReceivers);
		sendMailTaskUsers(newTask, taskReceivers);
		return newTask;
	}

	public void contractEnquiryIfRepair(Users createUser, IDataEntity object) throws Exception {
		List<Users> taskReceivers = new ArrayList<Users>();
		Tasks newTask = intialiseTask(createUser, object);
		if (object instanceof Contract) {
			Contract contract = (Contract) object;
			taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Management));
			taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Employee));
			newTask.setTargetClass(contract.getClass().getName());
			newTask.setDueDate(new java.util.Date());
			newTask.setTaskDirectPage("/pages/quoteForm.jsf?contractId=" + contract.getId());
			newTask.setTaskDescription("Order." + System.getProperty("line.separator") + " Prepare Quote for " + contract.getCustomer().getCustomerName() + System.getProperty("line.separator") + "Enquiry Number: " + contract.getEnquiryNumber());
		}

	}

	public Tasks createTaskForQuote(Users createUser, IDataEntity object) throws Exception {
		List<Users> taskReceivers = new ArrayList<Users>();
		Tasks newTask = intialiseTask(createUser, object);
		newTask.setDueDate(GenericUtility.addDaysToDateExcludeWeekends(new Date(), 3));
		if (object instanceof Quote) {
			Quote quote = (Quote) object;
			newTask.setTargetClass(quote.getClass().getName());
			switch (quote.getQuoteStatusEnum().ordinal()) {
			case 0:// QuotePrep
				taskReceivers.addAll(usersService.findUsersMangers());
				newTask.setDueDate(GenericUtility.addDaysToDateExcludeWeekends(new Date(), 1));
				newTask.setTaskDirectPage("/pages/quoteForm.jsf?contractId=" + quote.getContract().getId());
				newTask.setTaskDescription("Quote Being Processed " + System.getProperty("line.separator") + "Enquiry Number: " + quote.getQuoteNumber());
				break;
			case 1:// QuoteCustomer
				taskReceivers.addAll(usersService.findUsersMangers());
				newTask.setTaskDirectPage("/pages/quoteForm.jsf?quoteId=" + quote.getQuoteGuid());
				newTask.setDueDate(GenericUtility.addDaysToDateExcludeWeekends(new Date(), 2));
				newTask.setTaskDescription("Quote " + quote.getQuoteNumber() + " sent to customer. Follow up with Customer on " + GenericUtility.sdf2.format(GenericUtility.addDaysToDateExcludeWeekends(new Date(), 2)));
				break;
			case 2:// QuoteAccept
				taskReceivers.addAll(usersService.findUsersMangers());
				newTask.setTaskDirectPage("/pages/routeCard.jsf?contractId=" + quote.getContract().getId());
				newTask.setTaskDescription("Quote " + quote.getQuoteNumber() + " accepted. Create Route Card To Continue The Process");
				break;
			case 3:// QuoteDecline
				taskReceivers.addAll(usersService.findUsersMangers());
				newTask.setTaskDirectPage("/pages/quoteForm.jsf?quoteId=" + quote.getQuoteGuid());
				newTask.setTaskDescription("Quote " + quote.getQuoteNumber() + " declined by customer. Reason: " + quote.getRejectReason());
				break;
			default:
				break;
			}
		}
		sendTaskToUsers(newTask, taskReceivers);
		sendMailTaskUsers(newTask, taskReceivers);
		return newTask;
	}

	public Tasks createRouteCardTask(Users createUser, IDataEntity object) throws Exception {
		List<Users> taskReceivers = new ArrayList<Users>();
		Tasks newTask = intialiseTask(createUser, object);
		if (object instanceof RouteCard) {
			RouteCard rcard = (RouteCard) object;
			newTask.setTargetClass(rcard.getClass().getName());
			if (rcard.getRouteStatus().ordinal() == 0) {
				taskReceivers.addAll(usersService.findUsersMangers());
				newTask.setTaskDirectPage("/pages/routeCard.jsf?rcardId=" + rcard.getId());
				newTask.setTaskDescription("Route Card in progress for " + rcard.getContract().getEnquiryNumber());
				newTask.setDueDate(GenericUtility.addDaysToDateExcludeWeekends(new Date(), 4));
			}
		}
		sendTaskToUsers(newTask, taskReceivers);
		sendMailTaskUsers(newTask, taskReceivers);
		return newTask;
	}

	public Tasks createSITask(Users createUser, IDataEntity object) throws Exception {
		List<Users> taskReceivers = new ArrayList<Users>();
		Tasks newTask = intialiseTask(createUser, object);
		if (object instanceof InspectionReport) {
			InspectionReport ir = (InspectionReport) object;
			newTask.setTargetClass(ir.getClass().getName());
			taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Mechanic));
			taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Management));
			newTask.setTaskDirectPage("/pages/inspectionReportForm.jsf?inspecId=" + ir.getId());
			newTask.setTaskDescription("Strip and Inspection in progress for " + ir.getContract().getContractNumber());
			newTask.setDueDate(GenericUtility.addDaysToDateExcludeWeekends(new Date(), 4));
		}
		sendTaskToUsers(newTask, taskReceivers);
		sendMailTaskUsers(newTask, taskReceivers);
		return newTask;
	}

	public Tasks intialiseTask(Users createUser, IDataEntity object) {
		Tasks newTask = new Tasks();
		newTask.setTaskStatus(TaskStatusEnum.NotStarted);
		newTask.setTargetKey(object.getId());
		newTask.setCreateUser(createUser);
		newTask.setTaskGuid(UUID.randomUUID().toString());
		newTask.setActionUser(createUser);
		return newTask;
	}

	@SuppressWarnings("unused")
	public void sendMailTaskUsers(Tasks task, List<Users> receivers) throws Exception {
		String msg;
		for (Users users : receivers) {
			msg = "<p>Dear #NAME#,</p>" + "<p>A new contract has been created. Please Login and check on your tasks.</p>" + "<p>Message: <b>" + task.getTaskDescription() + "</b></p>" + "Due date: " + checkDueDate(task.getDueDate()) + "</b></p><p>" + "Click here to login in: <b><p><a href="
					+ HAJConstants.PL_LINK + ">Login</a></p>" + "<p>Regards,</p>" + "<p>The AM Hydraulics Portal Team</p><br />";
			msg = msg.replaceAll("#NAME#", users.getFullName());
			GenericUtility.sendTaskMail(users.getEmail(), "Task ID: " + task.getId(), msg, users.getFirstName() + " " + users.getLastName());
		}
	}

	public Tasks createTaskForNewContractByRepairNewEnum(Users sessionUser, Contract contract) throws Exception {
		List<Users> taskReceivers = new ArrayList<Users>();
		taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Management));
		taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Employee));
		Tasks task = new Tasks();
		switch (contract.getContractType().ordinal()) {
		case 0:// repair
			task = new Tasks("Repair Enquiry[" + contract.getEnquiryNumber() + "] has been started.", sessionUser, contract.getId(), contract.getClass().getName(), TaskStatusEnum.NotStarted, new Date(), null, UUID.randomUUID().toString(),
					"/pages/contractOverviewDownloadForm.jsf?contractId=" + contract.getContractGuid());
			taskReceivers.addAll(usersService.findByUserType(UserTypeEnum.Mechanic));
			sendTaskToUsers(task, taskReceivers);
			sendMailAboutTaskForContractNew(task, taskReceivers, contract);
			break;
		case 1:// new
			task = new Tasks("New Component Enquiry[" + contract.getEnquiryNumber() + "] has been started.", sessionUser, contract.getId(), contract.getClass().getName(), TaskStatusEnum.NotStarted, new Date(), null, UUID.randomUUID().toString(),
					"/pages/contractOverviewDownloadForm.jsf?contractId=" + contract.getContractGuid());
			sendTaskToUsers(task, taskReceivers);
			sendMailAboutTaskForContractNew(task, taskReceivers, contract);
			break;
		case 2:// order
			task = new Tasks("Order Enquiry[" + contract.getContractNumber() + "] has been started.", sessionUser, contract.getId(), contract.getClass().getName(), TaskStatusEnum.NotStarted, new Date(), null, UUID.randomUUID().toString(),
					"/pages/contractOverviewDownloadForm.jsf?contractId=" + contract.getContractGuid());
			sendTaskToUsers(task, taskReceivers);
			sendMailAboutTaskForContractNew(task, taskReceivers, contract);
			break;
		default:
			break;
		}
		return task;
	}

	public void sendTaskToUsers(Tasks task, List<Users> taskReceivers) throws Exception {
		if (taskReceivers.size() > 0) {
			createTaskAndTaskUsers(task, taskReceivers);
			updateUserScreen(taskReceivers);
		}
	}

	private void updateUserScreen(List<Users> receivers) throws Exception {
		for (Users user : receivers) {
			AtmospherePushService.refreshWorkFlowBadge(user.getId());
		}
	}

	private void createTaskAndTaskUsers(Tasks tasks, List<Users> userList) throws Exception {
		List<IDataEntity> entityList = new ArrayList<IDataEntity>();
		entityList.add(tasks);
		TaskUsers tu = null;
		for (Users user : userList) {
			tu = new TaskUsers();
			tu.setUser(user);
			tu.setTask(tasks);
			entityList.add(tu);
		}
		dao.createBatch(entityList);
	}

	@SuppressWarnings("unused")
	public void sendMailAboutTaskForContractNew(Tasks task, List<Users> receivers, Contract contract) throws Exception {
		String msg;

		for (Users users : receivers) {
			msg = "<p>Dear #NAME#,</p>" + "<p>A new contract has been created. Please Login and check on your tasks.</p>" + "<p>Message: <b>" + task.getTaskDescription() + "</b></p>" + "Due date: " + checkDueDate(task.getDueDate()) + "</b></p><p>" + "Click here to login in: <b><p><a href="
					+ HAJConstants.PL_LINK + ">Login</a></p>" + "<p>Regards,</p>" + "<p>The AM Hydraulics Portal Team</p><br />";
			msg = msg.replaceAll("#NAME#", users.getFullName());
			GenericUtility.sendTaskMail(users.getEmail(), contract.getContractType().getFriendlyName() + " contract " + contract.getEnquiryNumber(), msg, users.getFirstName() + " " + users.getLastName());
		}
	}

	public String checkDueDate(Date dueDate) {
		if (dueDate != null)
			return GenericUtility.sdf2.format(dueDate);
		else
			return "N/A";
	}

}
