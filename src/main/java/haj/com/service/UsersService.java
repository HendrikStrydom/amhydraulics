package haj.com.service;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.UsersDAO;
import haj.com.entity.Users;
import haj.com.entity.enums.UserTypeEnum;
import haj.com.framework.AbstractService;
import haj.com.framework.GenericAuditLog;

public class UsersService extends AbstractService {

	private UsersDAO dao = new UsersDAO();

	public UsersService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UsersService(Map<String, Object> auditlog) {
		super(auditlog);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Get all Users
	 * 
	 * @author TechFinium
	 * @see Users
	 * @return List<Users>
	 * @throws Exception
	 */
	public List<Users> allUsers() throws Exception {
		return dao.allUsers();
	}

	/**
	 * Create or update Users
	 * 
	 * @author TechFinium
	 * @see Users
	 * @throws Exception
	 */
	public void create(Users entity) throws Exception {
		if (entity.getId() == null) {
			// Default to active
			entity.setActive(Boolean.TRUE);
			// check if email already used
			checkEmailUsedEmailOnly(entity.getEmail());
			this.dao.create(entity);

		} else {
			this.dao.update(entity);
		}
	}

	/**
	 * Update Users
	 * 
	 * @author TechFinium
	 * @see Users
	 * @throws Exception
	 */
	/*
	 * public void update(Users entity) throws Exception {
	 * this.dao.update(entity); }
	 */
	public void update(Users entity) throws Exception {
		if (checkEmailUsed(entity.getEmail(), entity.getId()) != null)
			throw new Exception("Another user already use this email");
		this.dao.update(entity);

		/**
		 * JMB 10 Oct 2016
		 * 
		 * log when a user is updated
		 */
		GenericAuditLog.log(auditlog, "targetKey", entity.getId(), "Update User", "Updated User Information - ID: " + entity.getId() + " ( " + entity.getFirstName() + " " + entity.getLastName() + " )");

	}

	/**
	 * Delete Users
	 * 
	 * @author TechFinium
	 * @see Users
	 * @throws Exception
	 */
	public void delete(Users entity) throws Exception {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
	 * 
	 * @author TechFinium
	 * @see Users
	 * @return a Users object
	 * @throws Exception
	 */
	public Users findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	/**
	 * Find Users by description
	 * 
	 * @author TechFinium
	 * @see Users
	 * @return List<Users>
	 * @throws Exception
	 */
	public List<Users> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<Users> findUsersMangers() throws Exception {
		return dao.findUsersMangers();
	}

	public List<Users> findUsersEmployee() throws Exception {
		return dao.findUsersEmployee();
	}

	public List<Users> findByUserType(UserTypeEnum userTypeEnum) throws Exception {
		return dao.findByUserType(userTypeEnum);
	}

	/**
	 * Lazy load Users
	 * 
	 * @param first
	 *            from key
	 * @param pageSize
	 *            no of rows to fetch
	 * @return List<Users>
	 * @throws Exception
	 */
	public List<Users> allUsers(int first, int pageSize) throws Exception {
		return dao.allUsers(Long.valueOf(first), pageSize);
	}

	/**
	 * @author TechFinium
	 * @return Number of rows in the Users
	 * @throws Exception
	 */
	public Long count() throws Exception {
		return dao.count(Users.class);
	}

	/**
	 * @author TechFinium
	 * @param class1
	 *            Users class
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return List<Users> containing data
	 */
	@SuppressWarnings("unchecked")
	public List<Users> allUsers(Class<Users> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		return (List<Users>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	/**
	 * 
	 * @param entity
	 *            Users class
	 * @param filters
	 * @return Number of rows in the Users entity
	 */
	public int count(Class<Users> entity, Map<String, Object> filters) {
		return dao.count(entity, filters);
	}

	public Users findUserByEmailGUID(String emailGuid) throws Exception {
		return dao.findUserByEmailGUID(emailGuid);
	}

	public Users checkEmailUsed(String email, Long uid) throws Exception {
		return dao.checkEmailUsed(email, uid);
	}

	public Users getUserByEmail(String email) throws Exception {
		return dao.getUserByEmail(email);
	}

	/**
	 * Check if email is already used
	 * 
	 * @param email
	 * @throws Exception
	 */
	public void checkEmailUsedEmailOnly(String email) throws Exception {
		Users u = dao.checkEmailUsedEmailOnly(email);
		if (u != null)
			throw new Exception("This email is already in use!");
	}

	public void isMailUsed(String email, Users user) throws Exception {
		Users u = dao.checkEmailUsed(email, user.getId());
		if (u != null)
			throw new Exception("This email is already in use!");
	}
	
	/**
	 * Checks if user exists (id not null) 
	 * creates if null,
	 * updates if not null
	 * @param user
	 * @throws Exception
	 */
	public void checkUserExists(Users user) throws Exception {
		boolean update;
		if (user.getId() == null) {
			update = false;
			RegisterService registerService = new RegisterService();
			registerService.register(user);
		} else {
			update = true;
			update(user);
		}
		String booleanAction = (update == true) ? "Update" : "create";
		GenericAuditLog.log(auditlog, "targetKey", user.getId(), booleanAction, "Updated User Information - ID: " + user.getId() + " ( " + user.getFirstName() + " " + user.getLastName() + " )");
	}
}
