package haj.com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import haj.com.dao.PurchaseOrderDAO;
import haj.com.entity.PurchaseOrder;
import haj.com.entity.PurchaseOrderItem;
import haj.com.entity.QuoteItem;
import haj.com.framework.AbstractService;

public class PurchaseOrderService extends AbstractService {

	private PurchaseOrderDAO dao = new PurchaseOrderDAO();

	public void createPurchaseOrder(PurchaseOrder entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else {
			if (entity.getRevisionNumber() >= 1)
				entity.setRevisionNumber(entity.getRevisionNumber() + 1);
			this.dao.update(entity);
		}
	}

	public void createPOAndPOItem(PurchaseOrder entity, List<PurchaseOrderItem> list) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else {
			if (entity.getRevisionNumber() >= 1)
				entity.setRevisionNumber(entity.getRevisionNumber() + 1);
			this.dao.update(entity);
		}

		for (PurchaseOrderItem poItem : list) {
			poItem.setPurchaseOrder(entity);
			if (poItem.getId() == null)
				this.dao.create(poItem);
			else if (poItem.getId() != null)
				this.dao.update(poItem);
		}
	}

	public void updatePurchaseOrder(PurchaseOrder entity) throws Exception {
		this.dao.update(entity);
	}

	public void updatePurchaseOrderRevisionNumber(PurchaseOrder entity) throws Exception {
		if (entity.getRevisionNumber() >= 1)
			entity.setRevisionNumber(entity.getRevisionNumber() + 1);
		this.dao.update(entity);
	}

	public void deletePurchaseOrder(PurchaseOrder entity) throws Exception {
		entity.setSoftDelete(true);
		this.dao.update(entity);
	}

	public PurchaseOrder findPurchaseOrderByKey(long id) throws Exception {
		return dao.findPurchaseOrderByKey(id);
	}

	public List<PurchaseOrder> allPurchaseOrder() throws Exception {
		return dao.allPurchaseOrder();
	}

	public List<PurchaseOrder> findPurchaseOrderByName(String desc) throws Exception {
		return dao.findPurchaseOrderByName(desc);
	}

	/**
	 * Purchase Order Items create delete update
	 */

	public void createPurchaseOrderItem(PurchaseOrderItem entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else if (entity.getId() != null)
			this.dao.update(entity);
	}

	public void updatePurchaseOrderItem(PurchaseOrderItem entity) throws Exception {
		if (entity.getId() != null)
			this.dao.update(entity);
	}

	public void deletePurchaseOrderItem(PurchaseOrderItem entity) throws Exception {
		if (entity.getId() != null)
			this.dao.delete(entity);
	}

	public PurchaseOrderItem findPurchaseOrderItemByKey(long id) throws Exception {
		return dao.findPurchaseOrderItemByKey(id);
	}

	public List<PurchaseOrderItem> allPurchaseOrderItem() throws Exception {
		return dao.allPurchaseOrderItem();
	}

	public List<PurchaseOrderItem> findPurchaseOrderItemByName(String desc) throws Exception {
		return dao.findPurchaseOrderItemByName(desc);
	}

	public List<PurchaseOrderItem> findPurchaseOrderItemByPurchaseOrderId(Long purchaseOrderId) throws Exception {
		return dao.findPurchaseOrderItemByPurchaseOrderId(purchaseOrderId);
	}

	public List<PurchaseOrderItem> findPurchaseOrderItemByPurchaseOrderIdLineNumbers(Long purchaseOrderId)
			throws Exception {
		List<PurchaseOrderItem> l = new ArrayList<PurchaseOrderItem>();
		l = dao.findPurchaseOrderItemByPurchaseOrderId(purchaseOrderId);
		for (int i = 0; i < l.size(); i++) {
			l.get(i).setLineNumber(i + 1);
			updatePurchaseOrderItem(l.get(i));
		}
		return l;
	}

	public Double calculateTotalForPurchaseOrder(Long purchaseOrderId) throws Exception {
		return dao.calculateTotalForPurchaseOrder(purchaseOrderId);
	}

	public List<PurchaseOrder> findPurchaseOrderByQueryParameter(Map<String, Object> queryParameters) throws Exception {
		String hql = "select o from PurchaseOrder o where o.softDelete = false";
		if (queryParameters.containsKey("ConEnqNumber")) {
			hql += " and (o.contract.contractNumber LIKE :ConEnqNumber or o.contract.enquiryNumber LIKE :ConEnqNumber)";
		}
		if (queryParameters.containsKey("poNumber")) {
			hql += " and o.purchaseOrderNumber LIKE :poNumber";
		}
		if (queryParameters.containsKey("poForEnum")) {
			hql += " and o.purchaseOrderForEnum = :poForEnum";
		}
		if (queryParameters.containsKey("poEnum")) {
			hql += " and o.purchaseOrderEnum = :poEnum";
		}
		return dao.findPurchaseOrderByQueryParameter(hql, queryParameters);
	}

}
