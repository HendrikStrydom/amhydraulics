package haj.com.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.HtmlEmail;

import haj.com.bean.AttachmentBean;
import haj.com.constants.HAJConstants;
import haj.com.entity.MailLog;
import haj.com.framework.AbstractService;
import haj.com.utils.GenericUtility;

public class SendMail extends AbstractService {

	/**
	 * 
	 */

	public String zipFiles(List<String> files) throws Exception {
		byte[] buf = new byte[20480];
		String filename = "pn_" + new java.util.Date().getTime() + ".zip";
		String outFilename = HAJConstants.DOC_PATH + filename;
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFilename));
		for (String f : files) {
			FileInputStream in = new FileInputStream(f);
			// Add ZIP entry to output stream.

			out.putNextEntry(new ZipEntry(GenericUtility.convertFileName(f)));

			// Transfer bytes from the file to the ZIP file
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			// Complete the entry
			out.closeEntry();
			in.close();
		}

		// Complete the ZIP file
		out.close();

		return filename;
	}

	public void mailWithAttachement(String from, String to, String subject, String body, List<AttachmentBean> files, boolean custom, String logo) {
		try {

			String mailServer = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver");
			final String uid = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_uid");
			final String pwd = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_pwd");
			String portS = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_port");

			HtmlEmail email = new HtmlEmail();

			email.setHostName(mailServer);
			email.setSmtpPort(Integer.valueOf(portS.trim()));
			email.setAuthenticator(new DefaultAuthenticator(uid, pwd));
			// email.setStartTLSRequired(true);
			email.setDebug(HAJConstants.MAIL_DEBUG);
			email.addTo(to);
			email.setFrom(HAJConstants.MAIL_FROM);
			email.setSubject(subject);
			email.setHtmlMsg(createHtml(email, body));

			// add the attachments
			for (AttachmentBean ab : files) {
				email.attach(new ByteArrayDataSource(ab.getFile(), "application/" + ab.getExt()), ab.getFilename(), ab.getFilename(), EmailAttachment.ATTACHMENT);
			}
			email.send();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendMailCommons(String to_address, String subject, String text) {
		try {
			String mailServer = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver");
			final String uid = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_uid");
			final String pwd = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_pwd");
			String portS = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_port");

			logger.info("about to send mail! uid:" + uid + " pwd:" + pwd);
			
			HtmlEmail email = new HtmlEmail();

			email.setHostName(mailServer);
			email.setSmtpPort(Integer.valueOf(portS.trim()));
			email.setAuthenticator(new DefaultAuthenticator(uid, pwd));
			email.setStartTLSRequired(true);
			email.setDebug(HAJConstants.MAIL_DEBUG);
			email.addTo(to_address);
			email.setFrom(HAJConstants.MAIL_FROM);
			email.setSubject(subject);
			email.setHtmlMsg(createHtml(email, text));
			email.send();

			/*
			 * email.setStartTLSRequired(true);
			 * email.setDebug(HAJConstants.MAIL_DEBUG);
			 * email.setHostName(mailServer.trim());
			 * email.setSmtpPort(Integer.valueOf(portS.trim()));
			 * javax.mail.Authenticator newAuthenticator = new
			 * javax.mail.Authenticator() { protected PasswordAuthentication
			 * getPasswordAuthentication() { return new
			 * PasswordAuthentication(uid, pwd); }};
			 * 
			 * email.setAuthenticator(newAuthenticator);
			 * email.setFrom(mailFrom.trim()); email.setSubject(subject.trim());
			 * email.setHtmlMsg(createHtml(email, text));
			 * email.addTo(to_address);
			 * 
			 * email.send();
			 */
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.fatal(e);
		}

	}

	private static String createHtml(HtmlEmail email, String body) {
		String html = "";
		html = HAJConstants.EMAIL_TEMPLATE;
		html = html.replace("#BODY#", body);
		return html;
	}

	private static String createTaskHtml(HtmlEmail email, String body, String name) {
		String html = "";
		html = HAJConstants.EMAIL_TEMPLATE;
		html = html.replace("#BODY#", body);
		html = html.replace("#NAME#", name);
		return html;
	}


	public void sendMailCommonsGmailWithAttachement(String to_address, String subject, String text, byte[] attachment, String filename, String extension) {
		mailWithAttachement(to_address, subject, text, attachment, filename, filename, extension);
	}

	private void mailWithAttachement(String to_address, String subject, String text, byte[] attachment, String attachmentDescription, String attachmentName, String mime) {
		try {
			String mailServer = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver");
			final String uid = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_uid");
			final String pwd = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_pwd");
			String portS = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_port");

			logger.info("about to send mail! uid:" + uid + " pwd:" + pwd);
			HtmlEmail email = new HtmlEmail();
			email.setHostName(mailServer);
			email.setSmtpPort(Integer.valueOf(portS.trim()));
			email.setAuthenticator(new DefaultAuthenticator(uid, pwd));
			email.setStartTLSRequired(true);
			email.setDebug(HAJConstants.MAIL_DEBUG);
			email.addTo(to_address);
			email.setFrom(uid);
			email.setSubject(subject);
			email.setHtmlMsg(createHtml(email, text));

			// add the attachment

			email.attach(new ByteArrayDataSource(attachment, "application/" + mime.toLowerCase()), attachmentName, attachmentDescription, EmailAttachment.ATTACHMENT);

			// send the email
			email.send();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendMailCommonsGmailWithAttachements(String to_address, String subject, String text, List<AttachmentBean> files) {
		mailWithAttachement(to_address, subject, text, files);
	}

	private void mailWithAttachement(String to_address, String subject, String text, List<AttachmentBean> files) {
		try {
			String mailServer = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver");
			final String uid = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_uid");
			final String pwd = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_pwd");
			String portS = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_port");

			logger.info("about to send mail! uid:" + uid + " pwd:" + pwd);
			HtmlEmail email = new HtmlEmail();
			email.setHostName(mailServer);
			email.setSmtpPort(Integer.valueOf(portS.trim()));
			email.setAuthenticator(new DefaultAuthenticator(uid, pwd));
			email.setStartTLSRequired(true);
			email.setDebug(HAJConstants.MAIL_DEBUG);
			email.addTo(to_address);
			email.setFrom(uid);
			email.setSubject(subject);
			email.setHtmlMsg(createHtml(email, text));

			// add the attachments
			for (AttachmentBean ab : files) {
				email.attach(new ByteArrayDataSource(ab.getFile(), "application/" + ab.getExt().toLowerCase()), ab.getFilename(), ab.getFilename(), EmailAttachment.ATTACHMENT);
			}

			// send the email
			email.send();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
