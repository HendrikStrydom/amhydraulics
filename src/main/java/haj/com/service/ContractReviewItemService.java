package haj.com.service;

import java.util.List;

import haj.com.dao.ContractReviewItemDAO;
import haj.com.entity.ContractReviewItem;
import haj.com.framework.AbstractService;

public class ContractReviewItemService extends AbstractService {

	private ContractReviewItemDAO dao = new ContractReviewItemDAO();

	public List<ContractReviewItem> allContractReviewItem() throws Exception {
		return dao.allContractReviewItem();
	}

	public void create(ContractReviewItem entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

	public void update(ContractReviewItem entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(ContractReviewItem entity) throws Exception {
		this.dao.delete(entity);
	}

	public ContractReviewItem findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<ContractReviewItem> findByDescription(String desc) throws Exception {
		return dao.findByDescription(desc);
	}

	public List<ContractReviewItem> findByContract(Long contractId) throws Exception {
		return dao.findByContract(contractId);
	}
}
