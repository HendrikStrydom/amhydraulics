package haj.com.service;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.CollectDeliverItemDAO;
import haj.com.entity.CollectDeliverContract;
import haj.com.entity.CollectDeliverItem;
import haj.com.entity.Quote;
import haj.com.entity.QuoteItem;
import haj.com.entity.RouteCardItem;
import haj.com.framework.AbstractService;

public class CollectDeliverItemService extends AbstractService {

	private CollectDeliverItemDAO dao = new CollectDeliverItemDAO();

	public List<CollectDeliverItem> allCollectDeliverItem() throws Exception {
		return dao.allCollectDeliverItem();
	}
	
	
	public void createCDI(CollectDeliverItem cdi) throws Exception {
		if (cdi.getId() == null) {
			this.dao.create(cdi);
		} else {
			this.dao.update(cdi);
		}
	}

	public void create(CollectDeliverItem entity) throws Exception {
		//checkAndCalculateTotalPrice(entity);
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}
	
	

	public void checkAndCalculateTotalPrice(CollectDeliverItem entity) {
		if (entity.getQuantity() != null && entity.getPricePerItem() != null)
			entity.setTotalPrice(entity.getPricePerItem() * entity.getQuantity());
	}

	public void update(CollectDeliverItem entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(CollectDeliverItem entity) throws Exception {
		this.dao.delete(entity);
	}

	public CollectDeliverItem findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverItem> allCollectDeliverItem(Class<CollectDeliverItem> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		return (List<CollectDeliverItem>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	public int count(Class<CollectDeliverItem> entity, Map<String, Object> filters) {
		return dao.count(entity, filters);
	}

	public List<CollectDeliverItem> findCollectDeliverItemByQueryParameter(Map<String, Object> queryParameters) throws Exception {
		String hql = "select o from CollectDeliverItem o where o.id IS NOT null ";
		if (queryParameters.containsKey("ConEnqNumber")) {
			hql += " and (o.contract.contractNumber LIKE :ConEnqNumber or o.contract.enquiryNumber LIKE :ConEnqNumber)";
		}
		if (queryParameters.containsKey("cdNumber")) {
			hql += " and o.cdnNumber LIKE :cdNumber";
		}
		if (queryParameters.containsKey("cdEnum")) {
			hql += " and o.collectionDelivery = :cdEnum";
		}
		return dao.findCollectDeliverItemByQueryParameter(hql, queryParameters);
	}

	public List<CollectDeliverItem> findByCollectionDeliveryContract(Long cdcId) throws Exception {
		return dao.findByCollectionDeliveryContract(cdcId);
	}

	public static CollectDeliverContract findByCollectDeliverItem(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
