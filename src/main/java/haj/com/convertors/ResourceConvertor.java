package haj.com.convertors;

import com.techfinium.service.ResourceService;
import com.techfinium.entity.Resource;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@FacesConverter(value = "ResourceConvertor")
public class ResourceConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	 private ResourceService resourceService = new ResourceService();
	
	/**
	 * Used by JSF to get a Resource
 	 * @author TechFinium 
 	 * @see    Resource
 	 * @return Resource
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {
			try {
				Resource resource = resourceService.findByKey(Long.parseLong(value) );
				return resource;
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}
		}
	    return null;
	}


	/**
	 * Convert Resource key to String object
 	 * @author TechFinium 
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ""+((Resource)arg2).getId();
	}

/*
       <p:selectOneMenu id="ResourceId" value="#{xxxUI.Resource.xxxType}" converter="ResourceConvertor" style="width:95%">
         <f:selectItems value="#{ResourceUI.ResourceList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>
       
        <h:outputLabel value="Resource" for="ResourceID"/>
        <p:autoComplete id="ResourceID" value="#{ResourceUI.Resource.municipality}" completeMethod="#{ResourceUI.completeResource}"
                            var="rv" itemLabel="#{rv.ResourceDescription}" itemValue="#{rv}" 
                            forceSelection="true" converter="ResourceConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="Resource" style="white-space: nowrap">#{rv.ResourceDescription}</p:column>
       </p:autoComplete>         
       
*/

}
