package haj.com.convertors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.techfinium.entity.ResourceType;
import com.techfinium.service.ResourceTypeService;

@FacesConverter(value = "ResourceTypeConvertor")
public class ResourceTypeConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	 private ResourceTypeService service = new ResourceTypeService();
	
	 @Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return service.findByKey(Long.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ""+((ResourceType)arg2).getId();
	}

/*
       <p:selectOneMenu id="BlankId" value="#{xxxUI.Blank.xxxType}" converter="BlankConvertor" style="width:95%">
         <f:selectItems value="#{BlankUI.BlankList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>
       
        <h:outputLabel value="Blank" for="BlankID"/>
        <p:autoComplete id="BlankID" value="#{BlankUI.Blank.municipality}" completeMethod="#{BlankUI.completeBlank}"
                            var="rv" itemLabel="#{rv.BlankDescription}" itemValue="#{rv}" 
                            forceSelection="true" converter="BlankConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="Blank" style="white-space: nowrap">#{rv.BlankDescription}</p:column>
       </p:autoComplete>         
       
*/

}
