package haj.com.convertors;

import haj.com.entity.Users;
import haj.com.service.UsersService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@FacesConverter(value = "UsersConvertor")
public class UsersConvertor implements Converter {
	private UsersService usersService = new UsersService();
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				Users u = null;
				if (!value.equals("null")) {
					u = new Users();
					u = usersService.findByKey(Long.valueOf(value));
				}
				if (u == null) {
					// logger.fatal("UsersConvertor: Users is NULL!! value = " +
					// value);
				}
				return u;

			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 != null) {
			return "" + ((Users) arg2).getId();
		} else {
			// logger.fatal("UsersConvertor: arg2 is NULL!!");
			return null;
		}
	}

}
