package haj.com.convertors;

import haj.com.entity.Workflow;
import haj.com.service.WorkflowService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@FacesConverter(value = "WorkflowConvertor")
public class WorkflowConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	
	/**
	 * Used by JSF to get a Workflow
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @return Workflow
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return new WorkflowService()
						.findByKey(Integer.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert Workflow key to String object
 	 * @author TechFinium 
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ""+((Workflow)arg2).getId();
	}

/*
       <p:selectOneMenu id="WorkflowId" value="#{xxxUI.Workflow.xxxType}" converter="WorkflowConvertor" style="width:95%">
         <f:selectItems value="#{WorkflowUI.WorkflowList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>
       
        <h:outputLabel value="Workflow" for="WorkflowID"/>
        <p:autoComplete id="WorkflowID" value="#{WorkflowUI.Workflow.municipality}" completeMethod="#{WorkflowUI.completeWorkflow}"
                            var="rv" itemLabel="#{rv.WorkflowDescription}" itemValue="#{rv}" 
                            forceSelection="true" converter="WorkflowConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="Workflow" style="white-space: nowrap">#{rv.WorkflowDescription}</p:column>
       </p:autoComplete>         
       
*/

}
