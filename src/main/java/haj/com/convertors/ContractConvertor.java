package haj.com.convertors;

import haj.com.entity.Contract;
import haj.com.service.ContractService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@FacesConverter(value = "ContractConvertor")
public class ContractConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	
	/**
	 * Used by JSF to get a Contract
 	 * @author TechFinium 
 	 * @see    Contract
 	 * @return Contract
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return new ContractService()
						.findByKey(Integer.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert Contract key to String object
 	 * @author TechFinium 
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ""+((Contract)arg2).getId();
	}

/*
       <p:selectOneMenu id="ContractId" value="#{xxxUI.Contract.xxxType}" converter="ContractConvertor" style="width:95%">
         <f:selectItems value="#{ContractUI.ContractList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>
       
        <h:outputLabel value="Contract" for="ContractID"/>
        <p:autoComplete id="ContractID" value="#{ContractUI.Contract.municipality}" completeMethod="#{ContractUI.completeContract}"
                            var="rv" itemLabel="#{rv.ContractDescription}" itemValue="#{rv}" 
                            forceSelection="true" converter="ContractConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="Contract" style="white-space: nowrap">#{rv.ContractDescription}</p:column>
       </p:autoComplete>         
       
*/

}
