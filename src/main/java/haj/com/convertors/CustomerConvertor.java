package haj.com.convertors;

import haj.com.entity.Customer;
import haj.com.service.CustomerService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@FacesConverter(value = "CustomerConvertor")
public class CustomerConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	 private CustomerService customerService = new CustomerService();
	
	/**
	 * Used by JSF to get a Customer
 	 * @author TechFinium 
 	 * @see    Customer
 	 * @return Customer
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				if (Long.valueOf(value)==-1) {
					Customer customer = new Customer();
					customer.setId(-1l);
					return customer;
					
				}
				else {
				   return customerService.findByKey(Long.valueOf(value));
				}
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert Customer key to String object
 	 * @author TechFinium 
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ""+((Customer)arg2).getId();
	}

/*
       <p:selectOneMenu id="CustomerId" value="#{xxxUI.Customer.xxxType}" converter="CustomerConvertor" style="width:95%">
         <f:selectItems value="#{CustomerUI.CustomerList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>
       
        <h:outputLabel value="Customer" for="CustomerID"/>
        <p:autoComplete id="CustomerID" value="#{CustomerUI.Customer.municipality}" completeMethod="#{CustomerUI.completeCustomer}"
                            var="rv" itemLabel="#{rv.CustomerDescription}" itemValue="#{rv}" 
                            forceSelection="true" converter="CustomerConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="Customer" style="white-space: nowrap">#{rv.CustomerDescription}</p:column>
       </p:autoComplete>         
       
*/

}
