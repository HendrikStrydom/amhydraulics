package haj.com.convertors;

import haj.com.entity.CustomerUsers;
import haj.com.service.CustomerUsersService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@FacesConverter(value = "CustomerUsersConvertor")
public class CustomerUsersConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	
	/**
	 * Used by JSF to get a CustomerUsers
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @return CustomerUsers
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return new CustomerUsersService()
						.findByKey(Integer.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert CustomerUsers key to String object
 	 * @author TechFinium 
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ""+((CustomerUsers)arg2).getId();
	}

/*
       <p:selectOneMenu id="CustomerUsersId" value="#{xxxUI.CustomerUsers.xxxType}" converter="CustomerUsersConvertor" style="width:95%">
         <f:selectItems value="#{CustomerUsersUI.CustomerUsersList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>
       
        <h:outputLabel value="CustomerUsers" for="CustomerUsersID"/>
        <p:autoComplete id="CustomerUsersID" value="#{CustomerUsersUI.CustomerUsers.municipality}" completeMethod="#{CustomerUsersUI.completeCustomerUsers}"
                            var="rv" itemLabel="#{rv.CustomerUsersDescription}" itemValue="#{rv}" 
                            forceSelection="true" converter="CustomerUsersConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="CustomerUsers" style="white-space: nowrap">#{rv.CustomerUsersDescription}</p:column>
       </p:autoComplete>         
       
*/

}
