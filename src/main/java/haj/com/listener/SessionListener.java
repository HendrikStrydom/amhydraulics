package haj.com.listener;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SessionListener implements HttpSessionListener
{
  private final Log logger = LogFactory.getLog(this.getClass());

  @Override
  public void sessionCreated(HttpSessionEvent event)
  {
    logger.info("Current Session created : " + event.getSession().getId());
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent event)
  {
    HttpSession session = event.getSession();
    logger.info("Current Session destroyed :" + session.getId());
    
    /*
     * 
     * nobody can reach user data after this point because session is
     * invalidated already. So, get the user data from session and save its
     * logout information before losing it. User's redirection to the timeout
     * page will be handled by the SessionTimeoutFilter.
     */
    try
    {
      redirect("http://www.metra.co.za");
    }
    catch (Exception e)
    {
      e.printStackTrace();
      logger.fatal("Error while logging out at session destroyed.", e);
    }
  }
  
  public void prepareLogoutInfoAndLogoutActiveUser(HttpSession session)
  {
    session.setAttribute("uobj", null);
  }
  
  private void redirect(String outcome)
  {
    try
    {
      FacesContext facesContext = FacesContext.getCurrentInstance();
      if (facesContext!=null && facesContext.getExternalContext()!=null)
      {
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        if (response!=null)
          response.sendRedirect(outcome);
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
