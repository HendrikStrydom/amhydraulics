package haj.com.constants;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.enums.UsersStatusEnum;
import haj.com.utils.GenericUtility;


public class HAJConstants implements Serializable {

	
	private static final long serialVersionUID = 1L;
	protected static final Log logger = LogFactory.getLog(HAJConstants.class);
	
	public static String CSS_PREFIX = "resource";
	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
	public static final SimpleDateFormat sdf3 = new SimpleDateFormat("dd MMMM yyyy (hh:mm a)");
	public static final SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat sdfYYYY = new SimpleDateFormat("yyyy");
//	public static Properties PROP = getPorperties();
	 public static final int NO_ROWS = 21;
	 public static final int NO_ROWS_CHK = 20;
	 
	 public static String DOC_PATH= ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("docPath");
	 public static String DOC_SERVER= ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("docServer");
	 public static String MAIL_SERVER = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("mailServer");
	 public static boolean MAIL_DEBUG = getMAIL_DEBUG();
	 public static String APP_PATH = (String)System.getProperties().get("DD-APP-PATH");
	 public static String APNS_CERTIFICATE = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("APNS_CERTIFICATE");
	 public static boolean APNS_PROD = getAPNS_PROD();
	
	  public static String PL_LINK = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("PL");
	  public static String INFO_REQ_MAIL = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("INFO_REQ_MAIL");
	  public static String WEBSOCKET_SERVER = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("WEBSOCKET_SERVER");
		  
	  public static String DEV_TEST_PROD = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("DEV_TEST_PROD");
	  
	  public static Map<Integer,String> MONTHS = genereateMonthMap();
	

	public static List<SelectItem> userStatusDD = createUserStatusDD();

	
	public static final Long HOSTING_ROCK = 1l;

    public static double TOTAL_CPD = 35.0;
    public static double TOTAL_STRUCTURED = 21.0;
    public static double TOTAL_UNSTRUCTURED = 14.0;


	public static String EMAIL_TEMPLATE = getTemplate();

	public static String MAIL_FROM = ((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("ds_mailserver_mailFrom");
	







	private static boolean getAPNS_PROD() {
		boolean rtnVal = false;
		try {
			rtnVal = Boolean.parseBoolean(((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("APNS_PROD").trim());
		} catch (Exception e) {
		}

		return rtnVal;
	}



	private static Map<Integer, String> genereateMonthMap() {
		Map<Integer,String> m = new HashMap<Integer,String>();
		m.put(1, "Jan");
		m.put(2, "Feb");
		m.put(3, "March");
		m.put(4, "April");
		m.put(5, "May");
		m.put(6, "Jun");
		m.put(7, "Jul");
		m.put(8, "Aug");
		m.put(9, "Sep");
		m.put(10, "Oct");
		m.put(11, "Nov");
		m.put(12, "Dec");
		
		
		return m;
	}

	private static List<SelectItem> createUserStatusDD() {
		List<SelectItem> l =new ArrayList<SelectItem>();
		for (UsersStatusEnum val : UsersStatusEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	

	private static boolean getMAIL_DEBUG() {
		boolean rtnVal = false;
		try {
			rtnVal = Boolean.parseBoolean(((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty("MAIL_DEBUG").trim());
		} catch (Exception e) {
		}

		return rtnVal;
}
	

	private static String getTemplate() {
		String html = null;
		try {
			//InputStream inputStream = HAJConstants.class.getResourceAsStream("content.html");
			//html = IOUtils.toString(inputStream, "UTF8");

			html =   GenericUtility.readFile(HAJConstants.APP_PATH+"/emailTemplate/content.html");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return html;
	}


}
