package haj.com.framework;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class AbstractService implements Serializable
{
  private static final long serialVersionUID = 1L;
  
  protected final Log logger = LogFactory.getLog(this.getClass());
 
  public Map<String, Object> auditlog;
  
  public AbstractService() {
	super();
	// TODO Auto-generated constructor stub
   }


public AbstractService(Map<String, Object> auditlog) {
	super();
	this.auditlog = auditlog;
}


/**
   * Get an instance of current date, no idea why this is synchronized
   * but has been left so from original e-port code.
   * 
   * @return
   */
  protected synchronized Date getSynchronizedDate()
  {
    return new Date();
  }
}
