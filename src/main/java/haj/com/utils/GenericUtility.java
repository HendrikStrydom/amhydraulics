package haj.com.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Months;
import org.joda.time.Years;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import org.primefaces.model.UploadedFile;

import haj.com.bean.AttachmentBean;
import haj.com.constants.HAJConstants;
import haj.com.entity.Doc;
import haj.com.entity.MailLog;
import haj.com.entity.Users;
import haj.com.service.MailLogService;
import haj.com.service.SendMail;
import haj.com.service.UsersService;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;

public class GenericUtility implements Serializable {
	private static final long serialVersionUID = 1L;
	protected static final Log logger = LogFactory.getLog(GenericUtility.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	public static final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat sdf3 = new SimpleDateFormat("dd MMMM yyyy (HH:mm:ss)");
	private static final SimpleDateFormat timestamp = new SimpleDateFormat("yyyyMMddhhmmssSSSS");
	public static final SimpleDateFormat sdf4 = new SimpleDateFormat("HH:mm");
	private static SendMail mailer = new SendMail();

	public static List<String> allPermutations(String sf) {
		List<String> fl = new ArrayList<String>();
		List<String> tl = new ArrayList<String>();
		List<String> l = new ArrayList<String>(Arrays.asList(sf.split(" ")));
		for (String ts : l) {
			tl.add(ts.trim());
		}
		ICombinatoricsVector<String> originalVector = Factory.createVector(tl);
		Generator<String> gen = Factory.createPermutationGenerator(originalVector);
		for (ICombinatoricsVector<String> perm : gen) {
			String fs = "";
			for (String s : perm) {
				fs += '%' + s.trim();
			}
			fl.add(fs + '%');
		}

		return fl;
	}

	public static String createAllPermutationsLike(String field, String sf) {
		String like = "and ( ";
		List<String> l = GenericUtility.allPermutations(sf);
		for (String s : l) {
			like = like + field + " like '" + s + "' or ";
		}
		return like.substring(0, like.length() - 3) + ")";
	}

	public static String mobileDate(Date date) {
		String d = "";
		try {
			if (date != null)
				d = sdf3.format(date);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return d;
	}

	public static String sqlRegExp(String value) {
		if (value == null || value.trim().length() == 0)
			return "";
		String[] x = value.split(" ");
		String f = "";
		for (String string : x) {
			f = f + string.trim() + "|";
		}
		return "'" + (f.substring(0, f.lastIndexOf('|'))) + "'";
	}

	public static String removeListChar(String s) {
		s = s.replace(']', ' ');
		s = s.replace('[', ' ');
		return s.trim();
	}

	public static String createDMS_IN_Variables(List<String> l) throws Exception {
		String ss[] = GenericUtility.removeListChar(l.toString()).trim().split(",");
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < ss.length; i++) {
			buf.append('\'');
			buf.append(ss[i].trim());
			buf.append('\'');
			buf.append(',');
		}
		return buf.toString().substring(0, buf.toString().length() - 1);
	}

	public static String initSqlInClause(String in) {
		in = removeListChar(in);
		String[] c = in.split(",");
		String result = "(";
		if (c.length == 1)
			result = result + "'" + in.trim() + "'";
		else {
			for (String s : c) {
				result = result + "'" + s.trim() + "',";
			}
			result = result.substring(0, result.lastIndexOf(','));
		}

		result += ")";

		return result;
	}

	public static List<String> convertToList(String s) {
		if (s == null || s.length() == 0)
			return new ArrayList<String>();
		List<String> l = null;
		try {
			s = removeListChar(s);
			String[] pieces = s.split(",");
			for (int i = 0; i < pieces.length; i++) {
				pieces[i] = pieces[i].trim();
			}
			l = new ArrayList<String>(Arrays.asList(pieces));
		} catch (Exception e) {
			logger.fatal(e);
		}
		return l;
	}

	public static Map<String, String> convertToMap(String s) {
		Map<String, String> m = new HashMap<String, String>();
		try {
			s = removeListChar(s);
			String[] pieces = s.split(",");
			for (int i = 0; i < pieces.length; i++) {
				pieces[i] = pieces[i].trim();
				m.put(pieces[i], pieces[i]);
			}

		} catch (Exception e) {
			logger.fatal(e);
		}
		return m;
	}

	public static Date adjustDate(Date start, int days, int min) {
		DateTime ld = new DateTime(start);
		if (days != 0) {
			if (days < 0)
				ld.minusDays(days);
			else
				ld.plusDays(days);
		}

		if (min != 0) {
			if (min < 0)
				ld.minusMinutes(min);
			else
				ld.plusMinutes(min);
		}
		return ld.toDate();
	}

	public static Date addDaysToDate(Date start, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}

	/*
	 * public static Date addDaysToDateExcludeWeekends(Date start, int days) {
	 * // FromToDateBean fdt = new FromToDateBean(); DateTime dateTime = new
	 * DateTime(start); int dayOfWeek = dateTime.getDayOfWeek(); if (dayOfWeek
	 * == DateTimeConstants.SUNDAY) { dateTime = dateTime.plusDays(1); } else if
	 * (dayOfWeek == DateTimeConstants.SATURDAY) { dateTime =
	 * dateTime.plusDays(2); } else if (dayOfWeek == DateTimeConstants.THURSDAY)
	 * { dateTime = dateTime.plusDays(1); } int i = 0; while (i < days) {
	 * dayOfWeek = dateTime.getDayOfWeek(); if (dayOfWeek ==
	 * DateTimeConstants.SATURDAY) { dateTime = dateTime.plusDays(2); } else {
	 * dateTime = dateTime.plusDays(1); } i++; } dayOfWeek =
	 * dateTime.getDayOfWeek(); if (dayOfWeek == DateTimeConstants.SATURDAY) {
	 * dateTime = dateTime.plusDays(2); } // dateTime.plusDays(days); return
	 * dateTime.toDate(); }
	 */

	/**
	 * 
	 * @param start
	 * @param days
	 * @return
	 */
	public static Date addDaysToDateExcludeWeekends(Date start, int days) {
		if (days < 1) {
			return start;
		}
		LocalDate date = LocalDate.parse(HAJConstants.sdf.format(start));
		LocalDate result = date;
		int addedDays = 0;
		while (addedDays < days) {
			result = result.plusDays(1);
			if (!(result.getDayOfWeek() == DayOfWeek.SATURDAY.getValue() || result.getDayOfWeek() == DayOfWeek.SUNDAY.getValue())) {
				++addedDays;
			}
		}

		return result.toDate();
	}

	public static Date deductDaysFromDate(Date start, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.DATE, -days);
		return cal.getTime();
	}

	public static Date getFirstDayOfThisMonth() {
		return (new LocalDate().withDayOfMonth(1)).toDate();
	}

	public static Date getLasttDayOfThisMonth() {
		return (new LocalDate().dayOfMonth().withMaximumValue()).toDate();
	}

	public static Date getLasttDayOfDate(Date date) {
		return (new LocalDate(date).dayOfMonth().withMaximumValue()).toDate();
	}

	public static Date getFirstDayOfMonth(Date date) {
		return (new LocalDate(date.getTime()).withDayOfMonth(1)).toDate();
	}

	public static Date getStartOfDay(Date date) {
		return (new LocalDateTime(date.getTime()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)).toDate();
	}

	public static Date getEndOfDay(Date date) {
		return (new LocalDateTime(date.getTime()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999)).toDate();
	}

	public static Date getLasttDayOfMonth(Date date) {
		return (new LocalDate(date.getTime()).dayOfMonth().withMaximumValue()).toDate();
	}

	public static Date getSecondLastDay(Date date) {
		return deductDaysFromDate(getLasttDayOfMonth(addMonthsToDate(date, 1)), 1);
	}

	public static Date getSecondLastDayNew(Date date) {
		return deductDaysFromDate(getLasttDayOfMonth(date), 1);
	}

	public static int getDaysBetweenDates(Date startDate, Date endDate) {
		return Days.daysBetween(new LocalDate(startDate.getTime()), new LocalDate(endDate.getTime())).getDays();
	}

	public static int getMonthsBetweenDates(Date startDate, Date endDate) {
		return Months.monthsBetween(new LocalDate(startDate.getTime()), new LocalDate(endDate.getTime())).getMonths();
	}

	public static List<Date> getMonthsDates(Date startDate, Date endDate) {
		int size = getMonthsBetweenDates(getFirstDayOfMonth(startDate), getLasttDayOfMonth(endDate));
		List<Date> dates = new ArrayList<Date>();
		if (size != 0) {
			dates.add(startDate);
		}
		for (int i = 0; i < size; i++) {
			if (i > 0) {
				dates.add(getFirstDayOfMonth(addMonthsToDate(startDate, i)));
			}

		}
		dates.add(endDate);
		return dates;
	}

	public static int getYearsBetweenDates(Date startDate, Date endDate) {
		return Years.yearsBetween(new LocalDate(startDate.getTime()), new LocalDate(endDate.getTime())).getYears();
	}

	public static int getDaysBetweenDatesExcludeWeekends(Date start, Date end) {
		DateTime startDateTime = new DateTime(GenericUtility.getEndOfDay(start));
		DateTime endDateTime = new DateTime(GenericUtility.getEndOfDay(end));

		int dayOfWeek;
		int days = 0;

		while (startDateTime.isBefore(endDateTime)) {
			dayOfWeek = startDateTime.getDayOfWeek();
			if ((dayOfWeek == DateTimeConstants.SUNDAY || dayOfWeek == DateTimeConstants.SATURDAY) == false) {
				days++;
			}
			startDateTime = startDateTime.plusDays(1);
		}
		days++;
		return days;
	}

	public static Date getFirstDayOfMonthOneYearAgo() {
		return (new LocalDate().minusYears(1).withDayOfMonth(1)).toDate();
	}

	public static String mTrim(String string) {
		if (string == null)
			return string;
		else
			return string.trim();
	}

	public static String fixReportName(String report) {
		try {
			return report.substring(0, report.lastIndexOf(".")) + ".pdf";
		} catch (Exception e) {
			logger.fatal(e);
			return "genericReport.pdf";
		}
	}

	public static String initLikeField(String like) {
		if (like == null)
			return "%";
		else
			return like.trim() + "%";
	}

	public static String initregExpField(String like) {
		if (like == null)
			return "";
		else
			return like.trim();
	}

	public static boolean checkRsaId(String idVal) {
		if (idVal == null || (idVal != null && idVal.trim().length() == 0))
			return true;
		idVal = idVal.trim();
		if (idVal.length() < 13)
			return false;
		int checkDigit = ((Integer.valueOf("" + (idVal.charAt(idVal.length() - 1)))).intValue());
		String odd = "0";
		String even = "";
		int evenResult = 0;
		int result;
		for (int c = 1; c <= idVal.length(); c++) {
			if (c % 2 == 0) {
				even += idVal.charAt(c - 1);
			} else {
				if (c == idVal.length()) {
					continue;
				} else {
					odd = "" + (Integer.valueOf("" + odd).intValue() + Integer.valueOf("" + (idVal.charAt(c - 1))));
				}
			}
		}
		String evenS = "" + (Integer.valueOf(even) * 2);
		for (int r = 1; r <= evenS.length(); r++) {
			evenResult += Integer.valueOf("" + evenS.charAt(r - 1));
		}
		result = (Integer.valueOf(odd) + Integer.valueOf(evenResult));
		String resultS = "" + result;
		resultS = "" + (10 - (Integer.valueOf("" + (resultS.charAt(resultS.length() - 1)))).intValue());
		if (resultS.length() > 1) {
			resultS = "" + resultS.charAt(resultS.length() - 1);
		}
		if (Integer.valueOf(resultS) != checkDigit) {
			return false;
		} else {
			return true;
		}
	}

	public static Date startDateOfCurrentYear() {
		DateTime dt = new DateTime();
		try {
			return (sdf.parse("" + dt.getYear() + "0101"));

		} catch (ParseException e) {
			logger.fatal(e);
			return null;
		}
	}

	public static Date lastDateOfCurrentYear() {
		DateTime dt = new DateTime();
		try {
			return (sdf.parse("" + dt.getYear() + "1231"));

		} catch (ParseException e) {
			logger.fatal(e);
			return null;
		}
	}

	public static String saveFile(UploadedFile file) throws Exception {
		if (file == null)
			return null;
		String logo = (timestamp.format(new java.util.Date()) + "." + FilenameUtils.getExtension(file.getFileName()));
		FileUtils.writeByteArrayToFile(new File((HAJConstants.DOC_PATH + logo).trim()), IOUtils.toByteArray(file.getInputstream()));
		return logo;
	}

	public static String readFile(String filename) throws Exception {

		FileInputStream fileStream = null;
		BufferedReader reader = null;
		StringBuffer strbuf = null;

		try {

			fileStream = new FileInputStream(filename);
			reader = new BufferedReader(new InputStreamReader(fileStream));
			boolean endFlag = true;
			String line = null;
			strbuf = new StringBuffer(300);

			while (endFlag) {

				line = reader.readLine();

				if (line != null) {
					strbuf.append(line).append("\n");
				}

				if (line == null) {
					endFlag = false;
				}
			}

		} finally {

			if (fileStream != null) {
				fileStream.close();
			}

			if (reader != null) {
				reader.close();
			}
		}
		return strbuf.toString();
	}

	public static void deleteFile(String fn) throws IOException {
		File f = new File(fn);
		if (f.exists())
			f.delete();
	}

	public static String convertFileName(String fullPath) {
		String ret = null;
		int pos = -1;
		if (fullPath.lastIndexOf('/') > -1)
			pos = fullPath.lastIndexOf('/');
		else if (fullPath.lastIndexOf('\\') > -1)
			pos = fullPath.lastIndexOf('\\');

		if (pos > -1)
			ret = fullPath.substring(pos + 1);

		return ret;
	}

	public static String contains(String s) {
		if (s == null)
			return "%";
		else
			return "%" + s.trim() + "%";
	}

	public static String startWith(String s) {
		if (s == null)
			return "%";
		else
			return s.trim() + "%";
	}

	public static String constructLike(String yourString) {
		String result = yourString.replaceAll("[\\-\\+\\.\\^:,]", "%");
		result = result.replaceAll(" ", "%");
		String s[] = result.split("%");
		String t = "%";
		for (String string : s) {
			t += string.trim() + "%";
		}
		return t;
	}

	public static void sendMail(final String to, final String subject, final String msg) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					MailLog mailLog = new MailLog(to, subject, msg);
					final UsersService usersService = new UsersService();
					try {
						Users user = usersService.getUserByEmail(to);
						if (user != null)
							mailLog.setUser(user);
						MailLogService.create(mailLog);
					} catch (Exception e) {
						logger.fatal(e);
					}

					mailer.sendMailCommons(to, subject, msg);
				} catch (Exception e) {
					logger.fatal(e);
				}
			}
		}).start();
	}

	public static void sendTaskMail(final String to, final String subject, final String msg, final String name) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					MailLog mailLog = new MailLog(to, subject, msg);
					final UsersService usersService = new UsersService();
					try {
						Users user = usersService.getUserByEmail(to);
						if (user != null)
							mailLog.setUser(user);
						MailLogService.create(mailLog);
					} catch (Exception e) {
						logger.fatal(e);
					}
					mailer.sendMailCommons(to, subject, msg);
				} catch (Exception e) {
					logger.fatal(e);
				}
			}
		}).start();
	}

	public static void sendMail(final Users to, final String subject, final String msg) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					MailLog mailLog = null;
					try {
						mailLog = new MailLog(to, subject, msg);
						MailLogService.create(mailLog);
					} catch (Exception e) {
						logger.fatal(e);
					}
					mailer.sendMailCommons(to.getEmail(), subject, msg);
				} catch (Exception e) {
					logger.fatal(e);
				}
			}
		}).start();
	}

	public static void sendMailWithAttachement(final String to, final String subject, final String msg, final byte[] file, final String fileName, final String extension, final boolean thistle, final String logo) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {

					mailer.sendMailCommonsGmailWithAttachement(to, subject, msg, file, fileName, extension);
				} catch (Exception e) {
					logger.fatal(e);
				}
			}
		}).start();
	}

	public static void sendMailWithAttachements(final String to, final String subject, final String msg, final List<AttachmentBean> files) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {

					mailer.sendMailCommonsGmailWithAttachements(to, subject, msg, files);
				} catch (Exception e) {
					logger.fatal(e);
				}
			}
		}).start();
	}

	// public static void sendMail(final String from,final String to, final
	// String subject, final String body, final List<AttachmentBean> files,
	// final boolean custom, final String logo ) {
	// new Thread(new Runnable() {
	// @Override
	// public void run() {
	// try {
	// mailer.mailWithAttachement(from, to, subject, body, files, custom, logo);
	// } catch (Exception e) {
	// logger.fatal(e);
	// }
	// }
	// }).start();
	// }

	public static String convertDate(Date date) {
		if (date == null)
			return null;
		else
			return sdf2.format(date);
	}

	public static int getAge(Date dob) {
		return Years.yearsBetween(new LocalDate(dob.getTime()), new LocalDate()).getYears();
	}

	public static String genPassord() {
		return RandomStringUtils.randomAlphabetic(7);

	}

	public static Date getFirstDayOfWeek(Date date) {
		LocalDate now = new LocalDate(date);
		// System.out.println(now.withDayOfWeek(DateTimeConstants.MONDAY));
		// //prints 2011-01-17
		// System.out.println(now.withDayOfWeek(DateTimeConstants.SUNDAY));
		// //prints 2011-01-23
		return now.withDayOfWeek(1).toDate();
	}

	public static Date getFirstDayOfYear(Date date) {
		return new DateTime(date).dayOfYear().withMinimumValue().withTimeAtStartOfDay().toDate();
	}

	public static Date getLastDayOfYear(Date date) {
		return deductDaysFromDate(new DateTime(getFirstDayOfYear(date)).plusYears(1).toDate(), 1);
	}

	public static Date getLastDayOfWeek(Date date) {
		LocalDate now = new LocalDate(date);
		return now.withDayOfWeek(7).toDate();
	}

	public static BigDecimal roundToPrecision(BigDecimal val, int precision) {
		val = val.setScale(precision, RoundingMode.HALF_EVEN);
		return val;
	}

	public static Double roundToPrecision(Double value, int precision) {
		BigDecimal val = BigDecimal.valueOf(value);
		val = val.setScale(precision, RoundingMode.HALF_EVEN);
		return val.doubleValue();
	}

	public static String getMimeType(byte[] data) {
		String mimeType = "";
		try {
			MagicMatch match = Magic.getMagicMatch(data);
			mimeType = match.getMimeType();
		} catch (MagicParseException e) {
			logger.fatal(e);
		} catch (MagicMatchNotFoundException e) {
			logger.fatal(e);
		} catch (MagicException e) {
			logger.fatal(e);
		}
		return mimeType;
	}

	public static Date addMiniutesToDate(Date date, int minutes) {
		if (date == null)
			return null;
		return (new DateTime(date.getTime()).plusMinutes(minutes)).toDate();
	}

	public static Date addHoursToDate(Date date, int hours) {
		if (date == null)
			return null;
		return (new DateTime(date.getTime()).plusHours(hours)).toDate();
	}

	public static Date deductHoursFromDate(Date date, int hours) {
		if (date == null)
			return null;
		return (new DateTime(date.getTime()).minusHours(hours)).toDate();
	}

	public static Date deductMinutesFromDate(Date date, int minutes) {
		if (date == null)
			return null;
		return (new DateTime(date.getTime()).minusMinutes(minutes)).toDate();
	}

	public static Date addWeeksToDate(Date date, int weeks) {
		if (date == null)
			return null;
		return (new DateTime(date.getTime()).plusWeeks(weeks)).toDate();
	}

	public static Date addMonthsToDate(Date date, int months) {
		if (date == null)
			return null;
		return (new DateTime(date.getTime()).plusMonths(months)).toDate();
	}

	public static Date deductMonthsFromDate(Date date, int months) {
		if (date == null)
			return null;
		return (new DateTime(date.getTime()).minusMonths(months)).toDate();
	}

	public static Date addYearsToDate(Date date, int years) {
		if (date == null)
			return null;
		return (new DateTime(date.getTime()).plusYears(years)).toDate();
	}

	public static Long getHoursBetweenDate(Date startDate, Date endDate) {
		if (startDate == null || endDate == null)
			return null;
		return (endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60);
	}

	public static String getMonthForInt(int num) {
		String month = "wrong";
		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();
		if (num >= 0 && num <= 11) {
			month = months[num];
		}
		return month;
	}

	public static String removeLastComma(String string) {
		string = string.substring(0, string.lastIndexOf(','));
		return string;
	}

	public static String newLine(String string) {
		return string + "\n";
	}

	public static String finishLine(String string) {
		return newLine(removeLastComma(string));
	}

	public static String removeSpecialChars(String string) {
		return string.replaceAll(",", " ");
	}

	/**
	 * Temporary measure to mail severe issues to development team
	 * 
	 * @param subject
	 * @param msg
	 */
	public static void mailError(String subject, String msg) {
		if ("D".equals(HAJConstants.DEV_TEST_PROD)) {
			String pmsg = "<h1>Development error</h1><p>";
			pmsg = pmsg + msg + "</p>";
			// sendMail("resolutionerrors@hlspro.com", subject + "
			// (Development)", pmsg, false, null);

			logger.error("-------------> Possible config error (Development error) " + subject + "\t" + msg);

		} else if ("T".equals(HAJConstants.DEV_TEST_PROD)) {
			String pmsg = "<h1>Test System error</h1><p>";
			pmsg = pmsg + msg + "</p>";
			sendMail("a@a.com", subject + " (Test System)", pmsg);
			sendMail("a@a.com", subject + " (Test System)", pmsg);
			sendMail("a@a.com", subject + " (Test System)", pmsg);
		} else if ("P".equals(HAJConstants.DEV_TEST_PROD)) {
			String pmsg = "<h1>PRODUCTION SYSTEM ERROR</h1><p>";
			pmsg = pmsg + msg + "</p>";
			sendMail("a@a.com", subject + " (Production System)", pmsg);
			sendMail("a@a.com", subject + " (Production System)", pmsg);
			sendMail("a@a.com", subject + " (Production System)", pmsg);
		}
		// sendMail("a@a.com", subject, msg, false, null);
	}

	public static Date add3DaysCountingFromStartDate(Date from) {
		LocalDate date = LocalDate.parse(HAJConstants.sdf.format(from));
		if (date.getDayOfWeek() < 4) {
			date = date.plusDays(2);
		} else if (date.getDayOfWeek() == 4 || date.getDayOfWeek() == 5 || date.getDayOfWeek() == 6) {
			date = date.plusDays(4);
		} else if (date.getDayOfWeek() == 7) {
			date = date.plusDays(3);
		}
		return date.toDate();
	}

	public static String buidFileName(Doc doc) {
		String fname = "";
		// if (doc.getTemplate() != null) {
		// fname = doc.getTemplate().getTitle();
		// }
		if (doc.getOriginalFname() != null) {
			fname = doc.getOriginalFname();
		}

		fname = fname.replaceAll(" ", "");
		fname = fname.trim() + "_V" + doc.getVersionNo() + "." + doc.getExtension().trim();
		return fname;
	}

}
