package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import haj.com.entity.enums.CollectDeliverEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "collect_deliver_contract")
/*
 * @AuditTable(value = "collect_deliver_contract_hist_v2") // TODO audit table
 * 
 * @Audited
 */
public class CollectDeliverContract implements IDataEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Column(name = "cdn_number", length = 15)
	private String cdnNumber;

	/* @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED) */
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "contract_id", nullable = true)
	private Contract contract;

	@Enumerated
	@Column(name = "collect_deliver_enum")
	private CollectDeliverEnum collectionDelivery;

	@Column(name = "note", columnDefinition = "LONGTEXT")
	private String note;

	@Column(name = "address_line", columnDefinition = "LONGTEXT")
	private String addressLine;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date", length = 19)
	private Date actionDate;

	/* @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED) */
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "action_user", nullable = true)
	private Users actionUser;

	@Column(name = "soft_delete")
	private Boolean softDelete;

	@Column(name = "revision_number")
	private Integer revisionNumber;

	@Transient
	private List<CollectDeliverItem> cdiList;

	@Transient
	private CollectDeliverEnum collectionDeliveryOverView;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectDeliverContract other = (CollectDeliverContract) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public CollectDeliverContract() {
		super();
		this.softDelete = false;
		this.revisionNumber = 1;
	}

	public CollectDeliverContract(Users actionUser) {
		super();
		this.softDelete = false;
		this.actionUser = actionUser;
		this.revisionNumber = 1;
	}

	public CollectDeliverContract(Users actionUser, Address address) {
		super();
		this.softDelete = false;
		this.actionUser = actionUser;
		this.revisionNumber = 1;
		this.addressLine = address.getAddressLine1() + "\n" + address.getAddressLine2() + "\n"
				+ address.getAddressLine3() + "\n" + address.getAddressLine4() + "\n" + address.getPostcode();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public CollectDeliverEnum getCollectionDelivery() {
		return collectionDelivery;
	}

	public void setCollectionDelivery(CollectDeliverEnum collectionDelivery) {
		this.collectionDelivery = collectionDelivery;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Users getActionUser() {
		return actionUser;
	}

	public void setActionUser(Users actionUser) {
		this.actionUser = actionUser;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public String getCdnNumber() {
		return cdnNumber;
	}

	public void setCdnNumber(String cdnNumber) {
		this.cdnNumber = cdnNumber;
	}

	public List<CollectDeliverItem> getCdiList() {
		return cdiList;
	}

	public void setCdiList(List<CollectDeliverItem> cdiList) {
		this.cdiList = cdiList;
	}

	public Integer getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public String getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	public CollectDeliverEnum getCollectionDeliveryOverView() {
		return collectionDeliveryOverView;
	}

	public void setCollectionDeliveryOverView(CollectDeliverEnum collectionDeliveryOverView) {
		this.collectionDeliveryOverView = collectionDeliveryOverView;
	}

}
