package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import haj.com.entity.enums.ContractTypeEnum;
import haj.com.entity.enums.RepairNewEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "inspection_report")
/*@AuditTable(value = "inspection_report_hist_v2") // TODO audit table
@Audited*/
public class InspectionReport implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "form_number", columnDefinition = "LONGTEXT")
	private String formNumber;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "contract")
	private Contract contract;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "task")
	private Tasks task;

	@Column(name = "customer_order_number", columnDefinition = "LONGTEXT")
	private String customerOrderNumber;

	@Column(name = "mounting", columnDefinition = "LONGTEXT")
	private String mounting;

	@Column(name = "contruction_type", columnDefinition = "LONGTEXT")
	private String constructionType;

	@Column(name = "cylinder_type", columnDefinition = "LONGTEXT")
	private String cylinderType;

	@Column(name = "actual_rod_size", columnDefinition = "LONGTEXT")
	private String actualRodSize;

	@Column(name = "actual_bore_size", columnDefinition = "LONGTEXT")
	private String actualBoreSize;

	@Column(name = "rod_size", columnDefinition = "LONGTEXT")
	private String rodSize;

	@Column(name = "bore_size", columnDefinition = "LONGTEXT")
	private String boreSize;

	@Column(name = "stoke", columnDefinition = "LONGTEXT")
	private String stroke;

	@Column(name = "medium", columnDefinition = "LONGTEXT")
	private String medium;

	@Column(name = "drawing_number", columnDefinition = "LONGTEXT")
	private String drawingNumber;

	@Enumerated
	@Column(name = "contract_type_enum")
	private ContractTypeEnum contractType;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "fitter", nullable = true)
	private Users fitter;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date", length = 19)
	private Date actionDate;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "action_user", nullable = true)
	private Users actionUser;

	@Column(name = "completed_report")
	private Boolean completedReport;
	
	@Column(name = "close_report")
	private Boolean closeReport;

	@Column(name = "soft_delete")
	private Boolean softDelete;

	@Transient
	private InspectionReport inspectionReport;
	
	@Column(name = "revision_number")
	private Integer revisionNumber;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InspectionReport other = (InspectionReport) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public InspectionReport() {
		super();
		this.closeReport = false;
		this.completedReport = false;
		this.softDelete = false;
		this.revisionNumber = 1;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFormNumber() {
		return formNumber;
	}

	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Tasks getTask() {
		return task;
	}

	public void setTask(Tasks task) {
		this.task = task;
	}

	public String getMounting() {
		return mounting;
	}

	public void setMounting(String mounting) {
		this.mounting = mounting;
	}

	public String getConstructionType() {
		return constructionType;
	}

	public void setConstructionType(String constructionType) {
		this.constructionType = constructionType;
	}

	public String getCylinderType() {
		return cylinderType;
	}

	public void setCylinderType(String cylinderType) {
		this.cylinderType = cylinderType;
	}

	public String getActualRodSize() {
		return actualRodSize;
	}

	public void setActualRodSize(String actualRodSize) {
		this.actualRodSize = actualRodSize;
	}

	public String getActualBoreSize() {
		return actualBoreSize;
	}

	public void setActualBoreSize(String actualBoreSize) {
		this.actualBoreSize = actualBoreSize;
	}

	public String getRodSize() {
		return rodSize;
	}

	public void setRodSize(String rodSize) {
		this.rodSize = rodSize;
	}

	public String getBoreSize() {
		return boreSize;
	}

	public void setBoreSize(String boreSize) {
		this.boreSize = boreSize;
	}

	public String getStroke() {
		return stroke;
	}

	public void setStroke(String stroke) {
		this.stroke = stroke;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}


	public Users getFitter() {
		return fitter;
	}

	public void setFitter(Users fitter) {
		this.fitter = fitter;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Users getActionUser() {
		return actionUser;
	}

	public void setActionUser(Users actionUser) {
		this.actionUser = actionUser;
	}

	public Boolean getCompletedReport() {
		return completedReport;
	}

	public void setCompletedReport(Boolean completedReport) {
		this.completedReport = completedReport;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public String getCustomerOrderNumber() {
		return customerOrderNumber;
	}

	public void setCustomerOrderNumber(String customerOrderNumber) {
		this.customerOrderNumber = customerOrderNumber;
	}

	public String getDrawingNumber() {
		return drawingNumber;
	}

	public void setDrawingNumber(String drawingNumber) {
		this.drawingNumber = drawingNumber;
	}

	public InspectionReport getInspectionReport() {
		return inspectionReport;
	}

	public void setInspectionReport(InspectionReport inspectionReport) {
		this.inspectionReport = inspectionReport;
	}

	public Boolean getCloseReport() {
		return closeReport;
	}

	public void setCloseReport(Boolean closeReport) {
		this.closeReport = closeReport;
	}

	public Integer getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public ContractTypeEnum getContractType() {
		return contractType;
	}

	public void setContractType(ContractTypeEnum contractType) {
		this.contractType = contractType;
	}

}
