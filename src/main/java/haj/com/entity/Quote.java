package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import haj.com.entity.enums.QuoteStatusEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "quote")
/*@AuditTable(value = "quote_hist_v2") // TODO audit table
@Audited*/
public class Quote implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date", length = 19)
	private Date actionDate;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "action_user", nullable = true)
	private Users actionUser;

	@Enumerated
	@Column(name = "quote_status_enum")
	private QuoteStatusEnum quoteStatusEnum;

	@Column(name = "soft_delete")
	private Boolean softDelete;

	@Column(name = "revision_number")
	private Integer revisionNumber;

	@Column(name = "quote_number")
	private String quoteNumber;

	@Column(name = "reject_reason", columnDefinition = "LONGTEXT")
	private String rejectReason;

	@Column(name = "customer_reference", length = 60)
	private String customerReference;

	@Column(name = "to_do", length = 80)
	private String toDo;

	@Column(name = "for_attention_of", length = 60)
	private String forAttentionOf;

	@Column(name = "description", length = 250)
	private String description;

	@Column(name = "additional_notes", columnDefinition = "LONGTEXT")
	private String additonalNotes;

	@Column(name = "quote_guid", length = 100, nullable = true)
	public String quoteGuid;

	// OurRef: uses the contractNumber
	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "contract")
	private Contract contract;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "task", nullable = true)
	private Tasks task;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Quote() {
		super();
		this.softDelete = false;
		this.quoteStatusEnum = QuoteStatusEnum.QuotePrep;
		this.revisionNumber = 1;
		this.quoteGuid = UUID.randomUUID().toString();
		this.additonalNotes = "<p>Notes:<br />&ndash; <strong>In the event of an order, please state the required material clearly on the order.</strong><br />&ndash; All prices quoted exclude VAT<br />&ndash; A.M. Hydraulics Conditions of Business apply (available on request)<br /><br />We trust that the above information meets with your approval and await your instructions. Should you require any further information concerning this quotation, then please do not hesitate to contact the undersigned. <strong>Please quote our reference number on all correspondence concerning this quotation.</strong><br /><br />Yours faithfully<br /><br />Lashkar Singh<br />Managing Director</p>";
		this.description = "<p>Dear Sir/Madam,</p>" + "<p>We thank you for your recent enquiry and take pleasure in submitting below our price and delivery for your consideration and approval.</p>";
	}

	public Quote(Users actionUser, Contract contract) {
		super();
		this.actionUser = actionUser;
		this.contract = contract;
		this.softDelete = false;
		this.quoteStatusEnum = QuoteStatusEnum.QuotePrep;
		this.revisionNumber = 1;
		this.quoteGuid = UUID.randomUUID().toString();
		this.additonalNotes = "<p>Notes:<br />&ndash; <strong>In the event of an order, please state the required material clearly on the order.</strong><br />&ndash; All prices quoted exclude VAT<br />&ndash; A.M. Hydraulics Conditions of Business apply (available on request)<br /><br />We trust that the above information meets with your approval and await your instructions. Should you require any further information concerning this quotation, then please do not hesitate to contact the undersigned. <strong>Please quote our reference number on all correspondence concerning this quotation.</strong><br /><br />Yours faithfully<br /><br />Lashkar Singh<br />Managing Director</p>";
		this.description = "<p>Dear Sir/Madam,</p>" + "<p>We thank you for your recent enquiry and take pleasure in submitting below our price and delivery for your consideration and approval.</p>";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quote other = (Quote) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Users getActionUser() {
		return actionUser;
	}

	public void setActionUser(Users actionUser) {
		this.actionUser = actionUser;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Tasks getTask() {
		return task;
	}

	public void setTask(Tasks task) {
		this.task = task;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public QuoteStatusEnum getQuoteStatusEnum() {
		return quoteStatusEnum;
	}

	public void setQuoteStatusEnum(QuoteStatusEnum quoteStatusEnum) {
		this.quoteStatusEnum = quoteStatusEnum;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public Integer getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public String getToDo() {
		return toDo;
	}

	public void setToDo(String toDo) {
		this.toDo = toDo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdditonalNotes() {
		return additonalNotes;
	}

	public void setAdditonalNotes(String additonalNotes) {
		this.additonalNotes = additonalNotes;
	}

	public String getQuoteNumber() {
		return quoteNumber;
	}

	public void setQuoteNumber(String quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public String getForAttentionOf() {
		return forAttentionOf;
	}

	public void setForAttentionOf(String forAttentionOf) {
		this.forAttentionOf = forAttentionOf;
	}

	public String getQuoteGuid() {
		return quoteGuid;
	}

	public void setQuoteGuid(String quoteGuid) {
		this.quoteGuid = quoteGuid;
	}

}
