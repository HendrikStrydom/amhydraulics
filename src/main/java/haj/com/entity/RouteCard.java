package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import haj.com.entity.enums.RouteStatusEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "route_card")
/*@AuditTable(value = "route_card_hist_v2") // TODO audit table
@Audited*/
public class RouteCard implements IDataEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "contract")
	private Contract contract;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "task")
	private Tasks task;

	@Column(name = "shop_instruction", columnDefinition = "LONGTEXT")
	private String shopInstruction;
	
	@Column(name = "material_spec", columnDefinition = "LONGTEXT", length= 255)
	private String materialSpec;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "delivery_date", length = 19)
	private Date deliveryDate;

	@Enumerated
	@Column(name = "route_status")
	private RouteStatusEnum routeStatus;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date", length = 19)
	private Date actionDate;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "action_user", nullable = true)
	private Users actionUser;

	@Column(name = "soft_delete")
	private Boolean softDelete;

	@Column(name = "revision_number")
	private Integer revisionNumber;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RouteCard other = (RouteCard) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public RouteCard() {
		super();
		this.softDelete = false;
		this.routeStatus = RouteStatusEnum.RouteStart;
		this.revisionNumber = 1;
		this.shopInstruction = "";
		this.materialSpec = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Tasks getTask() {
		return task;
	}

	public void setTask(Tasks task) {
		this.task = task;
	}

	public String getShopInstruction() {
		return shopInstruction;
	}

	public void setShopInstruction(String shopInstruction) {
		this.shopInstruction = shopInstruction;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public RouteStatusEnum getRouteStatus() {
		return routeStatus;
	}

	public void setRouteStatus(RouteStatusEnum routeStatus) {
		this.routeStatus = routeStatus;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Users getActionUser() {
		return actionUser;
	}

	public void setActionUser(Users actionUser) {
		this.actionUser = actionUser;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public Integer getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}
	public String getMaterialSpec() {
		return materialSpec;
	}

	public void setMaterialSpec(String materialSpec) {
		this.materialSpec = materialSpec;
	}

}
