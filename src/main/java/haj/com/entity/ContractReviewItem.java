package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.techfinium.entity.Resource;

import haj.com.entity.enums.ContractDetailTypeEnum;
import haj.com.entity.enums.MaterialTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "contract_review_item")
public class ContractReviewItem implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "contract")
	private Contract contract;

	@Column(name = "description", columnDefinition = "LONGTEXT")
	private String description;

	@Enumerated
	@Column(name = "detail_type")
	private ContractDetailTypeEnum contractDetailType;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "resource", nullable = true)
	private Resource resource;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "resource_allocation_date", length = 19, nullable = true)
	private Date resourceAllocationDate;

	@Column(name = "days", nullable = true)
	private Integer days;

	@Column(name = "estimated_hours", nullable = true)
	private Double estimatedHours;

	@Enumerated
	@Column(name = "material_type", nullable = true)
	private MaterialTypeEnum materialType;

	@Column(name = "price")
	private Double price;

	@Column(name = "amount_materials")
	private Integer amountMaterials;

	@Column(name = "soft_delete")
	private Boolean softDelete;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public ContractReviewItem() {
		super();
		this.softDelete = false;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractReviewItem other = (ContractReviewItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Date getResourceAllocationDate() {
		return resourceAllocationDate;
	}

	public void setResourceAllocationDate(Date resourceAllocationDate) {
		this.resourceAllocationDate = resourceAllocationDate;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public Double getEstimatedHours() {
		return estimatedHours;
	}

	public void setEstimatedHours(Double estimatedHours) {
		this.estimatedHours = estimatedHours;
	}

	public MaterialTypeEnum getMaterialType() {
		return materialType;
	}

	public void setMaterialType(MaterialTypeEnum materialType) {
		this.materialType = materialType;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getAmountMaterials() {
		return amountMaterials;
	}

	public void setAmountMaterials(Integer amountMaterials) {
		this.amountMaterials = amountMaterials;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public ContractDetailTypeEnum getContractDetailType() {
		return contractDetailType;
	}

	public void setContractDetailType(ContractDetailTypeEnum contractDetailType) {
		this.contractDetailType = contractDetailType;
	}

}
