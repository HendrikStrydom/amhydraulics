package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import haj.com.entity.enums.PurchaseOrderEnum;
import haj.com.entity.enums.PurchaseOrderForEnum;
import haj.com.framework.IDataEntity;

/**
 * This is only used for sending P.O. to sub-contractors and suppliers Customer
 * Will never see these but they do get assigned to a Contract(Optional) with
 * haj.com.entity.PurchaseOrderContract
 * 
 * @author User
 */

@Entity
@Table(name = "purchase_order")
/*@AuditTable(value = "purchase_order_hist_v2") // TODO audit table
@Audited*/
public class PurchaseOrder implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "contract")
	private Contract contract;

	@Column(name = "purchase_order_number", length = 15)
	private String purchaseOrderNumber;

	@Column(name = "name_of", columnDefinition = "LONGTEXT")
	private String nameOf;

	@Column(name = "total_price")
	private Double totalPrice;

	@Enumerated
	@Column(name = "purchase_order_enum")
	private PurchaseOrderEnum purchaseOrderEnum;

	@Enumerated
	@Column(name = "purchase_order_for_enum")
	private PurchaseOrderForEnum purchaseOrderForEnum;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date", length = 19)
	private Date actionDate;

	/*@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)*/
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "action_user", nullable = true)
	private Users actionUser;

	@Column(name = "soft_delete")
	private Boolean softDelete;
	
	@Column(name = "revision_number")
	private Integer revisionNumber;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseOrder other = (PurchaseOrder) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public PurchaseOrder() {
		super();
		this.softDelete = false;
		this.revisionNumber = 1;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getNameOf() {
		return nameOf;
	}

	public void setNameOf(String nameOf) {
		this.nameOf = nameOf;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public PurchaseOrderEnum getPurchaseOrderEnum() {
		return purchaseOrderEnum;
	}

	public void setPurchaseOrderEnum(PurchaseOrderEnum purchaseOrderEnum) {
		this.purchaseOrderEnum = purchaseOrderEnum;
	}

	public PurchaseOrderForEnum getPurchaseOrderForEnum() {
		return purchaseOrderForEnum;
	}

	public void setPurchaseOrderForEnum(PurchaseOrderForEnum purchaseOrderForEnum) {
		this.purchaseOrderForEnum = purchaseOrderForEnum;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Users getActionUser() {
		return actionUser;
	}

	public void setActionUser(Users actionUser) {
		this.actionUser = actionUser;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Integer getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

}
