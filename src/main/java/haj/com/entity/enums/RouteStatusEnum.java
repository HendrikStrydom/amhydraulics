package haj.com.entity.enums;

public enum RouteStatusEnum {

	/** 0 */
	RouteStart("Route Started"),
	/** 1 */
	RouteComplete("Route Completed");

	private String displayName;

	private RouteStatusEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
