package haj.com.entity.enums;

public enum ContractTypeEnum {

	/**0*/
	Repair("Repair")
	/**1*/	
	,New("New Component")
	/**2*/
	,Order("Order");

	private String displayName;

	private ContractTypeEnum(String displayNameX) {
		displayName = displayNameX;
	}

	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
