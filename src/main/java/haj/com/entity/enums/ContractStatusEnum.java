package haj.com.entity.enums;

public enum ContractStatusEnum {

	/** 0 */
	NewEnquiry("New Enquiry"),
	/** 1 */
	SubmitForQuotation("Contract's Submitted for Quotation"),
	/** 2 */
	QuoteInProgress("Contract's Quotation in progress"),
	/** 3 */
	QuoteSentCustomer("Contract's Quote submitted to Customer"),
	/** 4 */
	QuoteReject("Contract's Quote rejected by Customer"),
	/** 5 */
	QuoteAccept("Contract's Quote accepted by Customer"),
	/** 6 */
	ContractInProgress("Contract in Progress"),
	/** 7 */
	Closed("Contract Close"),
	/** 8 */
	Completed("Contract Completed");

	private String displayName;

	private ContractStatusEnum(String displayNameX) {
		displayName = displayNameX;
	}

	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
