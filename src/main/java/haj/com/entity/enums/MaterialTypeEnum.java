package haj.com.entity.enums;

public enum MaterialTypeEnum {

  CustomerProvided("To be provided by customer"), Purchased("To be purchased");

  private String displayName;

  private MaterialTypeEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
