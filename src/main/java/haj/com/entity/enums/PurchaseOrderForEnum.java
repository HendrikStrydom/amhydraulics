package haj.com.entity.enums;

public enum PurchaseOrderForEnum {

	/** 0 **/
	Supplier("Supplier"),
	/** 1 */
	Subcontractor("Subcontractor");

	private String displayName;

	private PurchaseOrderForEnum(String displayNameX) {
		displayName = displayNameX;
	}

	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
