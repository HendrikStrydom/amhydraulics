package haj.com.entity.enums;

public enum AddressTypeEnum {

  Address("Address"), DeliveryAddress("Delivery Address") , BillingAddress("Billing Address") ;

  private String displayName;

  private AddressTypeEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
