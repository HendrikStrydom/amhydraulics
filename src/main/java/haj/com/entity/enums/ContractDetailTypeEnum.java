package haj.com.entity.enums;

public enum ContractDetailTypeEnum {

  Resource("Resource"), Material("Material");

  private String displayName;

  private ContractDetailTypeEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
