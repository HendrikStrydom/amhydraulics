package haj.com.entity.enums;

public enum TaskStatusEnum {

  NotStarted("Not Started"), 
  Underway("Underway"), 
  Completed("Completed"), 
  Closed("Closed"), 
  Overdue("Overdue"),
  /* For Reporting */
  All("All");

  private String displayName;

  private TaskStatusEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
