package haj.com.entity.enums;

public enum RepairNewEnum {

	/**0*/
	Repair("Repair")
	/**1*/	
	, New("New Component")
	/**2*/
	, Order("Order");

	private String displayName;

	private RepairNewEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
