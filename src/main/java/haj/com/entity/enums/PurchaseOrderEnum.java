package haj.com.entity.enums;

public enum PurchaseOrderEnum {

	/** 0 */
	Sent("Sent"),
	/** 1 */
	Amended("Amended"),
	/** 2 */
	Completed("Completed"),
	/** 3 */
	Received("Received");

	private String displayName;

	private PurchaseOrderEnum(String displayNameX) {
		displayName = displayNameX;
	}

	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
