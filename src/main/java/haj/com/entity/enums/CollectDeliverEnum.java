package haj.com.entity.enums;

public enum CollectDeliverEnum {

	/** 0 */
	ActionNeed("Action Needed"),
	/** 1 */
	AwaitingCollect("Awaiting Collection"),
	/** 2 */
	AwaitingDelivery("Awaiting Delivery"),
	/** 3 */
	Collected("Collected"),
	/** 4 */
	Delivered("Delivered"),
	/** 5 */
	NotRequired("Not Required");

	private String displayName;

	private CollectDeliverEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
