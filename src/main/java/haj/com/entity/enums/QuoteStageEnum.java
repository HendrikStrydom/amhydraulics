package haj.com.entity.enums;

public enum QuoteStageEnum {

	/** 0 */
	Stage1("Stage 1"),
	/** 1 */
	Stage2("Stage 2");

	private String displayName;

	private QuoteStageEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
