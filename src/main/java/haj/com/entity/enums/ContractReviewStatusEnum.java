package haj.com.entity.enums;

public enum ContractReviewStatusEnum {

	/** 0 */
	prepReview("Contract Review Initialised"),
	/** 1 */
	awaitReview("Contract Review With Customer"),
	/** 2 */
	acceptReview("Contract Review Accepted"),
	/** 3 */
	declineReview("Contract Review Declined");

	private String displayName;

	private ContractReviewStatusEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
