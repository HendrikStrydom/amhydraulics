package haj.com.entity.enums;

public enum AwaitingDispatchEnum {

	Delivered("Delivered"), Collected("Collected");

  private String displayName;

  private AwaitingDispatchEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
