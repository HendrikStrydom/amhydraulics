package haj.com.entity.enums;

public enum UserTypeEnum {

	Admin("Admin"), //0
	Employee("Employee"), //1
	Management("Management"), //2
	Customer("Customer"), //3
	Mechanic("Engineer/Mechanic");//4

	private String displayName;

	private UserTypeEnum(String displayNameX) {
		displayName = displayNameX;
	}

	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
