package haj.com.entity.enums;

public enum DocumentTrackerEnum {
	  // 0
	  Upload("Uploaded"),
	  // 1
	  Downloaded("Downloaded"),
	  // 2
	  UploadVersion("Uploaded a new version"),
	  // 3
	  Viewed("Viewed"),
	  // 4
	  ViewedContent("Viewed Content"),
	  // 5
	  ChangeType("Changed document type"),
	  // 6
	  CheckOut("Checked document out"),
	  // 7
	  CheckIn("Checked document in"),
	  // 8
	  ChangeStatus("Change document status"),
	  // 9
	  Form("Form Completed");

	
  private String displayName;

  private DocumentTrackerEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
