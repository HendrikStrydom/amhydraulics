package haj.com.entity.enums;

public enum QuoteStatusEnum {

	/** 0 */
	QuotePrep("Quote Being Prepared"),
	/** 1 */
	QuoteCustomer("Awaiting Customer"),
	/** 2 */
	QuoteAccept("Quote Accepted"),
	/** 3 */
	QuoteDecline("Quote Declined");

	private String displayName;

	private QuoteStatusEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
