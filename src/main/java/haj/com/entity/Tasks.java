package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import haj.com.entity.enums.TaskStatusEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "tasks")
public class Tasks implements IDataEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "task_description", columnDefinition = "LONGTEXT")
	private String taskDescription;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "create_user", insertable = true, updatable = true, nullable = true)
	private Users createUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "action_user", insertable = true, updatable = true, nullable = true)
	private Users actionUser;

	@Column(name = "target_key", nullable = true)
	private Long targetKey;

	@Column(name = "target_class", nullable = true)
	private String targetClass;

	@Enumerated
	@Column(name = "task_status")
	private TaskStatusEnum taskStatus;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date", length = 19)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "due_date", length = 19)
	private Date dueDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date", length = 19)
	private Date actionDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "completion_date", length = 19)
	private Date completionDate;

	@Column(name = "task_guid", length = 100, nullable = true)
	public String taskGuid;

	@Column(name = "task_direct_page", columnDefinition = "LONGTEXT", nullable = true)
	public String taskDirectPage;

	public Tasks() {
		super();
	}

	public Tasks(String description, Users actionUser, Long targetKey, String targetClass, TaskStatusEnum taskStatus, Date startDate, Date dueDate, String taskGuid, String taskDirectPage) {
		super();
		this.taskDescription = description;
		this.actionUser = actionUser;
		this.targetKey = targetKey;
		this.targetClass = targetClass;
		this.taskStatus = taskStatus;
		this.startDate = startDate;
		this.dueDate = dueDate;
		this.taskGuid = taskGuid;
		this.taskDirectPage = taskDirectPage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tasks other = (Tasks) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public Users getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Users createUser) {
		this.createUser = createUser;
	}

	public Users getActionUser() {
		return actionUser;
	}

	public void setActionUser(Users actionUser) {
		this.actionUser = actionUser;
	}

	public Long getTargetKey() {
		return targetKey;
	}

	public void setTargetKey(Long targetKey) {
		this.targetKey = targetKey;
	}

	public String getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}

	public TaskStatusEnum getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(TaskStatusEnum taskStatus) {
		this.taskStatus = taskStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Date getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	public String getTaskDirectPage() {
		return taskDirectPage;
	}

	public void setTaskDirectPage(String taskDirectPage) {
		this.taskDirectPage = taskDirectPage;
	}

	public String getTaskGuid() {
		return taskGuid;
	}

	public void setTaskGuid(String taskGuid) {
		this.taskGuid = taskGuid;
	}

}
