package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;

import haj.com.entity.enums.CollectDeliverEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "collect_deliver_item")
public class CollectDeliverItem implements IDataEntity {

	private static final long serialVersionUID = 1L;
	
	/** 
	 * data gets populated based on contract.
	 * */

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "collect_deliver_contract_id", nullable = true)
	private CollectDeliverContract collectDeliverContract;

	@Column(name = "description", columnDefinition = "LONGTEXT")
	private String description;

	@Column(name = "size", columnDefinition = "LONGTEXT")
	private String size;

	@Column(name = "company")
	private String company;

	@Column(name = "quantity")
	private Integer quantity;

	@Column(name = "price_per_item")
	private Double pricePerItem;

	@Column(name = "total_price")
	private Double totalPrice;

	@Enumerated
	@Column(name = "collect_deliver_enum")
	private CollectDeliverEnum collectionDelivery;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date", length = 19)
	private Date actionDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "action_user", nullable = true)
	private Users actionUser;

	@Column(name = "soft_delete")
	private Boolean softDelete;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "collectDeliverItem")
	@Fetch(FetchMode.JOIN)
	private Set<Doc> docs = new HashSet<Doc>(0);
	
	@Column(name = "po_number", columnDefinition = "LONGTEXT")
	private String poNumber;
	
	@Column(name = "line_number", columnDefinition = "LONGTEXT")
	private String lineNumber;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectDeliverItem other = (CollectDeliverItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public CollectDeliverItem() {
		super();
		this.softDelete = false;
	}

	public CollectDeliverItem(Users actionUser) {
		super();
		this.softDelete = false;
		this.actionUser = actionUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public CollectDeliverContract getCollectDeliverContract() {
		return collectDeliverContract;
	}

	public void setCollectDeliverContract(CollectDeliverContract collectDeliverContract) {
		this.collectDeliverContract = collectDeliverContract;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getPricePerItem() {
		return pricePerItem;
	}

	public void setPricePerItem(Double pricePerItem) {
		this.pricePerItem = pricePerItem;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public CollectDeliverEnum getCollectionDelivery() {
		return collectionDelivery;
	}

	public void setCollectionDelivery(CollectDeliverEnum collectionDelivery) {
		this.collectionDelivery = collectionDelivery;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public Users getActionUser() {
		return actionUser;
	}

	public void setActionUser(Users actionUser) {
		this.actionUser = actionUser;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public Set<Doc> getDocs() {
		return docs;
	}

	public void setDocs(Set<Doc> docs) {
		this.docs = docs;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

}
