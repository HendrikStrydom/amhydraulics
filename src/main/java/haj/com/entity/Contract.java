package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.ContractTypeEnum;
import haj.com.entity.enums.RepairNewEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "contract")
@AuditTable(value = "contract_hist") // TODO audit table
@Audited
public class Contract implements IDataEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "enquiry_number", length = 15)
	private String enquiryNumber;

	@Column(name = "contract_number", length = 15)
	private String contractNumber;

	@Column(name = "revision_number")
	private Integer revisionNumber;

	@Column(name = "title", columnDefinition = "LONGTEXT")
	private String title;

	@Column(name = "details", columnDefinition = "LONGTEXT")
	private String details;

	@Column(name = "quantity", length = 15)
	private String quantity;

	@Column(name = "drawing_number", columnDefinition = "LONGTEXT")
	private String drawingNumber;

	@Column(name = "order_number", columnDefinition = "LONGTEXT")
	private String orderNumber;

	@Column(name = "stock_number", columnDefinition = "LONGTEXT")
	private String stockNumber;

	@Column(name = "item_number", columnDefinition = "LONGTEXT")
	private String itemNumber;

	@Enumerated
	@Column(name = "contract_status_enum")
	private ContractStatusEnum contractStatus;

	@Enumerated
	@Column(name = "contract_type_enum")
	private ContractTypeEnum contractType;

	@Column(name = "test_pressure_certificate_b")
	private Boolean testPressureCertificateB;

	@Column(name = "conformity_certificate_b")
	private Boolean conformityCertificateB;

	@Column(name = "dimensional_report_b")
	private Boolean dimensionalReportB;

	@Column(name = "material_certificate_b")
	private Boolean materialCertificateB;
	
	@Column(name = "chief_inspector_comment", columnDefinition = "LONGTEXT")
	private String chiefInspectorComment;
	
	@Column(name = "deviations_comment", columnDefinition = "LONGTEXT")
	private String deviationsComment;
	
	@Column(name = "your_comment", columnDefinition = "LONGTEXT")
	private String yourComment;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "customer", nullable = true)
	private Customer customer;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "task", nullable = true)
	private Tasks task;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "previous_contract", nullable = true)
	private Contract previousContract;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "action_user", nullable = true)
	private Users actionUser;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "action_date", length = 19)
	private Date actionDate;

	@Column(name = "contract_guid", length = 100, nullable = true)
	public String contractGuid;

	@Column(name = "soft_delete")
	private Boolean softDelete;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contract other = (Contract) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Contract() {
		super();
		this.softDelete = false;
		this.contractGuid = UUID.randomUUID().toString();
		this.revisionNumber = 1;
		this.testPressureCertificateB = false;
		this.conformityCertificateB = false;
		this.dimensionalReportB = false;
		this.materialCertificateB = false;
		this.contractStatus = ContractStatusEnum.NewEnquiry;
	}

	public Contract(Customer customer) {
		super();
		this.softDelete = false;
		this.contractGuid = UUID.randomUUID().toString();
		this.revisionNumber = 1;
		this.testPressureCertificateB = false;
		this.conformityCertificateB = false;
		this.dimensionalReportB = false;
		this.materialCertificateB = false;
		this.customer = customer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnquiryNumber() {
		return enquiryNumber;
	}

	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public Integer getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(Integer revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getDrawingNumber() {
		return drawingNumber;
	}

	public void setDrawingNumber(String drawingNumber) {
		this.drawingNumber = drawingNumber;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getStockNumber() {
		return stockNumber;
	}

	public void setStockNumber(String stockNumber) {
		this.stockNumber = stockNumber;
	}

	public String getItemNumber() {
		return itemNumber;
	}

	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public ContractStatusEnum getContractStatus() {
		return contractStatus;
	}

	public void setContractStatus(ContractStatusEnum contractStatus) {
		this.contractStatus = contractStatus;
	}

	public ContractTypeEnum getContractType() {
		return contractType;
	}

	public void setContractType(ContractTypeEnum contractType) {
		this.contractType = contractType;
	}

	public Boolean getConformityCertificateB() {
		return conformityCertificateB;
	}

	public void setConformityCertificateB(Boolean conformityCertificateB) {
		this.conformityCertificateB = conformityCertificateB;
	}

	public Boolean getDimensionalReportB() {
		return dimensionalReportB;
	}

	public void setDimensionalReportB(Boolean dimensionalReportB) {
		this.dimensionalReportB = dimensionalReportB;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Tasks getTask() {
		return task;
	}

	public void setTask(Tasks task) {
		this.task = task;
	}

	public Contract getPreviousContract() {
		return previousContract;
	}

	public void setPreviousContract(Contract previousContract) {
		this.previousContract = previousContract;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Users getActionUser() {
		return actionUser;
	}

	public void setActionUser(Users actionUser) {
		this.actionUser = actionUser;
	}

	public Date getActionDate() {
		return actionDate;
	}

	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}

	public String getContractGuid() {
		return contractGuid;
	}

	public void setContractGuid(String contractGuid) {
		this.contractGuid = contractGuid;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public Boolean getTestPressureCertificateB() {
		return testPressureCertificateB;
	}

	public void setTestPressureCertificateB(Boolean testPressureCertificateB) {
		this.testPressureCertificateB = testPressureCertificateB;
	}

	public Boolean getMaterialCertificateB() {
		return materialCertificateB;
	}

	public void setMaterialCertificateB(Boolean materialCertificateB) {
		this.materialCertificateB = materialCertificateB;
	}

	public String getChiefInspectorComment() {
		return chiefInspectorComment;
	}

	public void setChiefInspectorComment(String chiefInspectorComment) {
		this.chiefInspectorComment = chiefInspectorComment;
	}

	public String getDeviationsComment() {
		return deviationsComment;
	}

	public void setDeviationsComment(String deviationsComment) {
		this.deviationsComment = deviationsComment;
	}

	public String getYourComment() {
		return yourComment;
	}

	public void setYourComment(String yourComment) {
		this.yourComment = yourComment;
	}

}
