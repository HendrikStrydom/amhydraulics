package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import haj.com.framework.IDataEntity;

@Entity
@Table(name = "contract_override")
public class ContractOverride implements IDataEntity
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name="override_number")
    private Integer overrideNumber;
	
	@Column(name = "enquiry")
	private Boolean enquiry;
	
	@Column(name = "contract")
	private Boolean contract;
	
	public ContractOverride() {
		super();
	}
	
	public ContractOverride(Integer OverrideNumber, Boolean enquiry, Boolean contract) {
		super();
		this.overrideNumber = OverrideNumber;
		this.enquiry = enquiry;
		this.contract = contract;
	}

    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractOverride other = (ContractOverride) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOverrideNumber() {
		return overrideNumber;
	}

	public void setOverrideNumber(Integer overrideNumber) {
		this.overrideNumber = overrideNumber;
	}

	public Boolean getEnquiry() {
		return enquiry;
	}

	public void setEnquiry(Boolean enquiry) {
		this.enquiry = enquiry;
	}

	public Boolean getContract() {
		return contract;
	}

	public void setContract(Boolean contract) {
		this.contract = contract;
	}

}
