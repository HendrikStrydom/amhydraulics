package haj.com.entity;
// Generated May 30, 2014 10:27:06 AM by Hibernate Tools 3.6.0

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import haj.com.entity.enums.AddressTypeEnum;
import haj.com.framework.IDataEntity;

/**
 * Address generated by hbm2java
 */
@Entity
@Table(name = "address")
public class Address implements IDataEntity, Cloneable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String addressLine4;
	private String postcode;
	private Double latitude;
	private Double longitude;
	private Boolean primaryAddr;
	private AddressTypeEnum addressType;
	private Date createDate;
	private Users user;
	private Customer customer;

	public Address() {
		super();
	}

	public Address(AddressTypeEnum addressType, Customer customer) {
		super();
		this.addressType = addressType;
		this.customer = customer;
	}

	public Address(Customer customer) {
		super();
		this.customer = customer;
	}

	public Address(Address address, AddressTypeEnum addressTypeEnum) {
		super();
		if (address.getCreateDate() != null)
			this.createDate = address.getCreateDate();
		else
			this.createDate = new Date();

		if (address.getAddressLine1() != null)
			this.addressLine1 = address.getAddressLine1();

		if (address.getAddressLine2() != null)
			this.addressLine2 = address.getAddressLine2();

		if (address.getAddressLine3() != null)
			this.addressLine3 = address.getAddressLine3();

		if (address.getAddressLine4() != null)
			this.addressLine4 = address.getAddressLine4();

		if (address.getPostcode() != null)
			this.postcode = address.getPostcode();

		if (address.getLatitude() != null)
			this.latitude = address.getLatitude();

		if (address.getLongitude() != null)
			this.longitude = address.getLongitude();

		if (address.getUser() != null)
			this.user = address.getUser();

		if (address.getCustomer() != null)
			this.customer = address.getCustomer();

		this.addressType = addressTypeEnum;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "address_line_1", length = 100)
	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	@Column(name = "address_line_2", length = 100)
	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	@Column(name = "address_line_3", length = 100)
	public String getAddressLine3() {
		return this.addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	@Column(name = "address_line_4", length = 100)
	public String getAddressLine4() {
		return this.addressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}

	@Column(name = "postcode", length = 10)
	public String getPostcode() {
		return this.postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	@Column(name = "latitude", precision = 22, scale = 0)
	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Column(name = "longitude", precision = 22, scale = 0)
	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@Column(name = "primary_addr")
	public Boolean getPrimaryAddr() {
		return this.primaryAddr;
	}

	public void setPrimaryAddr(Boolean primaryAddr) {
		this.primaryAddr = primaryAddr;
	}

	public Object clone() {
		Object clone = null;
		try {
			clone = super.clone();
		} catch (CloneNotSupportedException e) {
			// should never happen
		}
		return clone;

	}

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = true, length = 19)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "user_id", nullable = true)
	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "customer_id", nullable = true)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Enumerated
	@Column(name = "address_type")
	public AddressTypeEnum getAddressType() {
		return addressType;
	}

	public void setAddressType(AddressTypeEnum addressType) {
		this.addressType = addressType;
	}

}
