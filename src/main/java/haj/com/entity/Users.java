package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Email;

import haj.com.entity.enums.UsersStatusEnum;
import haj.com.framework.IDataEntity;
import haj.com.entity.enums.UserTypeEnum;

@Entity
@Table(name = "users")

// @DynamicInsert(value = true)
// @AuditTable(value = "users_hist")
// @Audited

public class Users implements IDataEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "first_name", length = 100, nullable = false)
	private String firstName;

	@Column(name = "last_name", length = 100, nullable = false)
	private String lastName;

	@Column(name = "passport_number", length = 30, nullable = true)
	private String passportNumber;

	@Column(name = "tel_number", length = 20, nullable = true)
	private String telNumber;

	@Column(name = "cell_number", length = 20, nullable = true)
	private String cellNumber;

	@Column(name = "password", length = 1000, nullable = false)
	private String password;

	@Column(name = "last_login", nullable = true)
	private Date lastLogin;

	@Column(name = "email", length = 100, nullable = true)
	@Email(message = "Please enter a valid Email Address")
	private String email;

	@Column(name = "user_id", length = 15, nullable = true)
	private String userId;

	@Column(name = "username", length = 100, nullable = true)
	private String username;

	@Enumerated
	private UsersStatusEnum status;

	@Column(name = "alternative_contact_name", length = 100, nullable = true)
	private String alternativeContactName;

	@Column(name = "alternative_contact_number", length = 20, nullable = true)
	private String alternativeContactNumber;

	@Column(name = "registration_date", nullable = true)
	private Date registrationDate;

	@Column(name = "email_guid", length = 100, nullable = true)
	public String emailGuid;

	@Column(name = "email_confirm_date", nullable = true)
	private Date emailConfirmDate;

	@Column(name = "internal_cell_number", length = 20, nullable = true)
	private String internalCellNumber;

	@Column(name = "phone_id", length = 100)
	private String phoneId;

	@Column(name = "ios_android", length = 1, nullable = true)
	private String iosAndroid;

	@Column(name = "accepted_t_and_c")
	private Boolean acceptedTAndC;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date", length = 19)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "license_valid_from", length = 19)
	private Date licenseValidFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "license_valid_to", length = 19)
	private Date licenseValidTo;

	@Column(name = "points")
	private Integer points;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "last_chg_user_id", nullable = true)
	private Users lastChgUser;

	@Enumerated
	@Column(name = "user_type")
	private UserTypeEnum userType;

	@Transient
	private boolean chgPwdNow;

	@Transient
	private boolean selected;

	@Transient
	private List<Address> addressList;

	public Users() {
		super();
	}

	public Users(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonIgnore
	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonIgnore
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@JsonIgnore
	public UsersStatusEnum getStatus() {
		return status;
	}

	public void setStatus(UsersStatusEnum status) {
		this.status = status;
	}

	@JsonIgnore
	public String getAlternativeContactName() {
		return alternativeContactName;
	}

	public void setAlternativeContactName(String alternativeContactName) {
		this.alternativeContactName = alternativeContactName;
	}

	@JsonIgnore
	public String getAlternativeContactNumber() {
		return alternativeContactNumber;
	}

	public void setAlternativeContactNumber(String alternativeContactNumber) {
		this.alternativeContactNumber = alternativeContactNumber;
	}

	@JsonIgnore
	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	@JsonIgnore
	public String getEmailGuid() {
		return emailGuid;
	}

	public void setEmailGuid(String emailGuid) {
		this.emailGuid = emailGuid;
	}

	@JsonIgnore
	public Date getEmailConfirmDate() {
		return emailConfirmDate;
	}

	public void setEmailConfirmDate(Date emailConfirmDate) {
		this.emailConfirmDate = emailConfirmDate;
	}

	public String getInternalCellNumber() {
		return internalCellNumber;
	}

	public void setInternalCellNumber(String internalCellNumber) {
		this.internalCellNumber = internalCellNumber;
	}

	@JsonIgnore
	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}

	public String getIosAndroid() {
		return iosAndroid;
	}

	public void setIosAndroid(String iosAndroid) {
		this.iosAndroid = iosAndroid;
	}

	@JsonIgnore
	public Boolean getAcceptedTAndC() {
		return acceptedTAndC;
	}

	public void setAcceptedTAndC(Boolean acceptedTAndC) {
		this.acceptedTAndC = acceptedTAndC;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public boolean isChgPwdNow() {
		return chgPwdNow;
	}

	public void setChgPwdNow(boolean chgPwdNow) {
		this.chgPwdNow = chgPwdNow;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public List<Address> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getLicenseValidFrom() {
		return licenseValidFrom;
	}

	public void setLicenseValidFrom(Date licenseValidFrom) {
		this.licenseValidFrom = licenseValidFrom;
	}

	public Date getLicenseValidTo() {
		return licenseValidTo;
	}

	public void setLicenseValidTo(Date licenseValidTo) {
		this.licenseValidTo = licenseValidTo;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Users getLastChgUser() {
		return lastChgUser;
	}

	public void setLastChgUser(Users lastChgUser) {
		this.lastChgUser = lastChgUser;
	}

	public UserTypeEnum getUserType() {
		return userType;
	}

	public void setUserType(UserTypeEnum userType) {
		this.userType = userType;
	}

}
