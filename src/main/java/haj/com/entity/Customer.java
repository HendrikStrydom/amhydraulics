package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Email;

import haj.com.framework.IDataEntity;

@Entity
@Table(name = "customer")
public class Customer implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "customer_name", columnDefinition = "LONGTEXT")
	private String customerName;

	@Column(name = "phone_number", length = 20, nullable = true)
	private String phoneNumber;

	@Column(name = "fax_number", length = 20, nullable = true)
	private String faxNumber;

	@Column(name = "company_email", length = 200, nullable = true)
	@Email(message = "Please Enter A Valid Email Address")
	private String companyEmail;

	@Column(name = "company_number", length = 100, nullable = true)
	private String companyNumber;

	@Column(name = "vat_number", length = 100, nullable = true)
	private String vatNumber;

	@Column(name = "point_of_contact_name", columnDefinition = "LONGTEXT")
	private String pointOfContactName;

	@Column(name = "point_of_contact_phone_number", columnDefinition = "LONGTEXT")
	private String pointOfContactPhoneNumber;

	@Column(name = "point_of_contact_email", length = 200, nullable = true)
	@Email(message = "Please Enter A Valid Email Address")
	private String pointOfContactEmail;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Transient
	private Address address;
	@Transient
	private Address billingAddress;
	@Transient
	private Address deliveryAddress;
	@Transient
	private Set<Address> otherAddress;
	@Transient
	private Boolean newCustomer;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public String getCompanyNumber() {
		return companyNumber;
	}

	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getPointOfContactName() {
		return pointOfContactName;
	}

	public void setPointOfContactName(String pointOfContactName) {
		this.pointOfContactName = pointOfContactName;
	}

	public String getPointOfContactPhoneNumber() {
		return pointOfContactPhoneNumber;
	}

	public void setPointOfContactPhoneNumber(String pointOfContactPhoneNumber) {
		this.pointOfContactPhoneNumber = pointOfContactPhoneNumber;
	}

	public String getPointOfContactEmail() {
		return pointOfContactEmail;
	}

	public void setPointOfContactEmail(String pointOfContactEmail) {
		this.pointOfContactEmail = pointOfContactEmail;
	}

	public Boolean getNewCustomer() {
		return newCustomer;
	}

	public void setNewCustomer(Boolean newCustomer) {
		this.newCustomer = newCustomer;
	}

	public Set<Address> getOtherAddress() {
		return otherAddress;
	}

	public void setOtherAddress(Set<Address> otherAddress) {
		this.otherAddress = otherAddress;
	}

}
