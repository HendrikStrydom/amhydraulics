package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Contract;
import haj.com.entity.PurchaseOrder;
import haj.com.entity.PurchaseOrderItem;
import haj.com.entity.enums.PurchaseOrderEnum;
import haj.com.entity.enums.PurchaseOrderForEnum;
import haj.com.entity.enums.QuoteStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.JasperService;
import haj.com.service.PurchaseOrderService;

@ManagedBean(name = "purchaseOrderFormUI")
@ViewScoped
public class PurchaseOrderFormUI extends AbstractUI {

	private ContractService contractService = new ContractService();
	private PurchaseOrderService purchaseOrderService = new PurchaseOrderService();

	private PurchaseOrder purchaseOrder;
	private PurchaseOrderItem purchaseOrderItem;

	private List<PurchaseOrderItem> purchaseOrderItemList;

	private PurchaseOrderEnum[] purchasesOrderEnum = PurchaseOrderEnum.values();
	private PurchaseOrderForEnum[] purchasesOrderForEnum = PurchaseOrderForEnum.values();

	private JasperService jasperService = new JasperService();

	private Double totalPrice;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (super.getParameter("poId", false) != null) {
			this.purchaseOrder = purchaseOrderService.findPurchaseOrderByKey(Long.parseLong((String) super.getParameter("poId", false)));
			preparePurchaseOrderItemList();
		} else {
			this.purchaseOrder = new PurchaseOrder();
			this.purchaseOrderItem = new PurchaseOrderItem();
			this.purchaseOrderItemList = new ArrayList<PurchaseOrderItem>();
		}
	}

	public void downloadPurchaseOrder() {
		try {
			if (this.purchaseOrder.getId() != null) {
				// TODO task for P.O.
				jasperService.jasperDownloadPdfForPurchaseOrder(this.purchaseOrder);
				addInfoMessage("Purchase Order Downloaded");
			} else {
				addWarningMessage("No Purchase Order");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = new ArrayList<Contract>();
		try {
			l = contractService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public void savePurchaseOrder() {
		try {
			this.purchaseOrder.setActionUser(getSessionUI().getUser());
			purchaseOrderService.createPOAndPOItem(purchaseOrder, purchaseOrderItemList);
			preparePurchaseOrderItem();
			preparePurchaseOrderItemList();
			addInfoMessage("Purchase Order Saved");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void addItemsToPOlist() {
		try {
			if (this.purchaseOrderItem.getId() == null) {
				purchaseOrderItemList.add(this.purchaseOrderItem);
				calculatePOTotal();
				preparePurchaseOrderItem();

				addInfoMessage("P.O. Item added to list");
			} else {
				purchaseOrderService.updatePurchaseOrderItem(this.purchaseOrderItem);
				preparePurchaseOrderItemList();
				addInfoMessage("P.O. Item updated");
			}

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void calculatePOTotal() {
		this.totalPrice = 0.00;
		for (PurchaseOrderItem poi : purchaseOrderItemList) {
			if (!poi.getExcludePriceB()) {
				if (poi.getPrice() != null && poi.getQuantity() != null)
					totalPrice += (poi.getPrice() * poi.getQuantity());
			}
		}
		this.purchaseOrder.setTotalPrice(totalPrice);
	}

	public void deletePurchaseOrderItem() {
		try {
			if (this.purchaseOrderItem.getId() != null) 
				purchaseOrderService.deletePurchaseOrderItem(this.purchaseOrderItem);
			
			this.purchaseOrderItemList.removeIf(poi -> poi == this.purchaseOrderItem);
			//preparePurchaseOrderItemList();
			preparePurchaseOrderItem();
			calculatePOTotal();
			this.purchaseOrder.setTotalPrice(totalPrice);
			purchaseOrderService.updatePurchaseOrder(this.purchaseOrder);
			addInfoMessage("Purchase Order Item Deleted");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void preparePurchaseOrderItem() {
		this.purchaseOrderItem = new PurchaseOrderItem();
	}

	public void preparePurchaseOrder() {
		this.purchaseOrder = new PurchaseOrder();
	}

	public void preparePurchaseOrderItemList() {
		try {
			preparePurchaseOrderItem();
			this.purchaseOrderItemList = new ArrayList<PurchaseOrderItem>();
			this.purchaseOrderItemList = purchaseOrderService.findPurchaseOrderItemByPurchaseOrderIdLineNumbers(this.purchaseOrder.getId());
			calculatePOTotal();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void newPurchaseOrder() {
		preparePurchaseOrder();
		// this.purchaseOrder.setContract(this.contract.getItemNumber());
		preparePurchaseOrderItem();
		this.purchaseOrderItemList = new ArrayList<PurchaseOrderItem>();
		this.totalPrice = 0.00;
		addInfoMessage("P.O. Cleared");
	}

	public void newPOItem() {
		try {
			this.purchaseOrderItem = new PurchaseOrderItem();
			if (this.purchaseOrder.getId() != null) {
				this.purchaseOrderItemList = new ArrayList<PurchaseOrderItem>();
				this.purchaseOrderItemList = purchaseOrderService.findPurchaseOrderItemByPurchaseOrderIdLineNumbers(this.purchaseOrder.getId());
			}
			addInfoMessage("Item Cleared");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public PurchaseOrderItem getPurchaseOrderItem() {
		return purchaseOrderItem;
	}

	public void setPurchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
		this.purchaseOrderItem = purchaseOrderItem;
	}

	public List<PurchaseOrderItem> getPurchaseOrderItemList() {
		return purchaseOrderItemList;
	}

	public void setPurchaseOrderItemList(List<PurchaseOrderItem> purchaseOrderItemList) {
		this.purchaseOrderItemList = purchaseOrderItemList;
	}

	public PurchaseOrderEnum[] getPurchasesOrderEnum() {
		return purchasesOrderEnum;
	}

	public void setPurchasesOrderEnum(PurchaseOrderEnum[] purchasesOrderEnum) {
		this.purchasesOrderEnum = purchasesOrderEnum;
	}

	public PurchaseOrderForEnum[] getPurchasesOrderForEnum() {
		return purchasesOrderForEnum;
	}

	public void setPurchasesOrderForEnum(PurchaseOrderForEnum[] purchasesOrderForEnum) {
		this.purchasesOrderForEnum = purchasesOrderForEnum;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
}
