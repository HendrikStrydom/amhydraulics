package  haj.com.ui;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.validator.constraints.Email;

import haj.com.framework.AbstractUI;
import haj.com.service.LogonService;



@ManagedBean(name = "logonUI")
@ViewScoped
public class LogonUI extends AbstractUI {

	@Email(message = "Please enter a valid Email Address")
	private String email;
	private String inputPassword, newpassword;
	

	private LogonService logonService = new LogonService();


    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
	}

	

	public void logon() {
		try {
			getSessionUI().setUser(logonService.logonByEmail(email, inputPassword));
			super.setParameter("uobj", getSessionUI().getUser(), true);
			logonSalesStaff();
		} catch (Exception e) {
			log("invalid logon for email", email);
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void logonSalesStaff() throws Exception {

		if (getSessionUI().getUser().isChgPwdNow()) {
			super.runClientSideExecute("PF('dlgPwdChg').show()");
			super.runClientSideUpdate("usersPwdChgForm");
		} else {
			completeLogon();
		}
		
	}

	

	
	public void completeLogon() throws Exception {
		switch (getSessionUI().getUser().getUserType()) {
		case Admin:
			super.ajaxRedirect("/admin/dashboard.jsf");
			break;
		case Employee:
			super.ajaxRedirect("/pages/dashboardEmployee.jsf");
			break;
		case Mechanic:
			super.ajaxRedirect("/pages/dashboardEmployee.jsf");
			break;
		case Management:
			super.ajaxRedirect("/pages/dashboardManagement.jsf");
			break;		
		case Customer:
			super.ajaxRedirect("/pages/dashboardCustomer.jsf");
			break;	
		default:
			break;
		}
		
	}
	


	public void logoff() throws Exception {
		logoffGeneric();
		super.removeParm("sessionUI", true);
		super.removeParm("uobj", true);
		log();
		super.ajaxRedirect("/login.jsf");
	}

	
	private void logoffGeneric() {

		getSessionUI().setUser(null);
	
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInputPassword() {
		return inputPassword;
	}

	public void setInputPassword(String inputPassword) {
		this.inputPassword = inputPassword;
	}

	public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}
	
	public void changePassword() {
		try {
			logonService.changePassword(getSessionUI().getUser(), inputPassword);
			addInfoMessage("Password changed.");

			 completeLogon();
			
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void newPassword() {
		try {
			new LogonService().notifyUserNewPasswordEmail(email);
			addInfoMessage("A new password has been mailed to you.");
			super.runClientSideExecute("PF('dlgPwd').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}


	
	
}
