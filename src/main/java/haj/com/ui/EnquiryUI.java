
package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

import haj.com.entity.Contract;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.ContractTypeEnum;
import haj.com.entity.enums.RepairNewEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.DocService;
import haj.com.service.TasksService;

@ManagedBean(name = "enquiryUI")
@ViewScoped
public class EnquiryUI extends AbstractUI {

	private ContractService contractService = new ContractService();
	// private WorkflowService workflowService = new WorkflowService();
	private TasksService tasksService = new TasksService();

	private List<Contract> contractfilteredList;
	private List<Contract> contractList;

	private Contract contract;

	private Boolean showFields = false;

	private int docCount;

	@ManagedProperty(value = "#{uploadDocUI}")
	private UploadDocUI uploadDocUI;

	public void saveContract() {
		try {
			contractService.create(this.contract);
			addInfoMessage("Update successful for " + this.contract.getEnquiryNumber());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void createTaskAndSendEmailNoAjax() {
		try {
			if (this.contract.getId() != null && this.contract.getContractStatus() == ContractStatusEnum.NewEnquiry) {
				if (this.contract.getTask() != null) {
					tasksService.closeTask(tasksService.findByKey(this.contract.getTask().getId()), getSessionUI().getUser());
				}
				this.contract.setTask(tasksService.createTaskForNewContractByRepairNewEnum(getSessionUI().getUser(), this.contract));
				contractService.update(this.contract);
			} else
				addWarningMessage("Either no contract was been found OR this enquiry is in progess already.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (super.getParameter("enqId", false) != null) {
			this.contract = contractService.findByKey(Long.valueOf("" + super.getParameter("enqId", false)));
			this.showFields = this.contract.getConformityCertificateB();
			addInfoMessage("Now reviewing enquiry: " + this.contract.getEnquiryNumber());
			getSessionUI().setCustomer(this.contract.getCustomer());
		} else if (super.getParameter("contractId", false) != null) {
			this.contract = contractService.findByGuid("" + super.getParameter("contractId", false));
			this.showFields = this.contract.getConformityCertificateB();
			addInfoMessage("Now reviewing enquiry: " + this.contract.getEnquiryNumber());
			getSessionUI().setCustomer(this.contract.getCustomer());
		} else {
			prepNewContract();
		}

		prepContractList();
	}

	public void prepNewContract() {
		try {
			this.contract = new Contract();
			this.contract.setActionUser(getSessionUI().getUser());
			this.contract.setCustomer(getSessionUI().getCustomer());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void prepContractList() {
		try {
			contractList = new ArrayList<Contract>();
			contractfilteredList = new ArrayList<Contract>();
			contractList = contractService.findByCustomer(getSessionUI().getCustomer());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void contractSubmit() {
		try {
			if (new DocService().findByContract(this.contract).size() > 0) {
				if (this.contract.getTask() != null) {
					tasksService.closeTask(tasksService.findByKey(this.contract.getTask().getId()), getSessionUI().getUser());
				}
				this.contract.setTask(tasksService.createTaskForNewContractByRepairNewEnum(getSessionUI().getUser(), this.contract));
				prepContractList();
				super.addInfoMessage("Update successful for " + this.contract.getEnquiryNumber());
				super.runClientSideUpdate("enquiryGrwl");
			} else {
				addWarningMessage("No Documents Attached");
			}
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void contractSave() {
		try {
			if (!this.contract.getConformityCertificateB()) {
				this.contract.setChiefInspectorComment("");
				this.contract.setDeviationsComment(""); 
				this.contract.setYourComment("");
			}
			contractService.create(this.contract);
			getSessionUI().setContract(this.contract);
			prepContractList();
			addInfoMessage("Update successful for " + this.contract.getEnquiryNumber());
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void uploadDocButSaveFirst() {
		try {
			if (this.contract.getContractStatus() == ContractStatusEnum.NewEnquiry)
				contractService.update(this.contract);
			uploadDocUI.runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void redirectDashboard() {
		getSessionUI().setContract(null);
		getSessionUI().setCustomer(null);
		super.redirect("/pages/customerEnquiry.jsf");
	}

	public void deleteContract() {
		try {
			if (this.contract.getId() != null) {
				this.contract.setSoftDelete(true);
				contractService.update(this.contract);
				if (this.contract.getTask() != null) {
					tasksService.closeTask(tasksService.findByKey(this.contract.getTask().getId()), getSessionUI().getUser());
				}
				super.addInfoMessage("Delete successful for " + this.contract.getEnquiryNumber());
				this.contract = new Contract();
				prepNewContract();
				prepContractList();
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = null;
		try {
			l = contractService.findByCustomerByEnquiry(getSessionUI().getCustomer().getId(), desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		if (l == null && l.size() <= 0)
			addWarningMessage("No Contracts Available");
		return l;
	}

	public void checkBoxListenerTo(ValueChangeEvent event) {
		try {
			showFields = (Boolean) event.getNewValue();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public List<Contract> getContractfilteredList() {
		return contractfilteredList;
	}

	public void setContractfilteredList(List<Contract> contractfilteredList) {
		this.contractfilteredList = contractfilteredList;
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public int getDocCount() {
		return docCount;
	}

	public void setDocCount(int docCount) {
		this.docCount = docCount;
	}

	public UploadDocUI getUploadDocUI() {
		return uploadDocUI;
	}

	public void setUploadDocUI(UploadDocUI uploadDocUI) {
		this.uploadDocUI = uploadDocUI;
	}

	public Boolean getShowFields() {
		return showFields;
	}

	public void setShowFields(Boolean showFields) {
		this.showFields = showFields;
	}

}