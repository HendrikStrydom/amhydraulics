package  haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.Blank;
import haj.com.entity.Customer;
import haj.com.framework.AbstractUI;
import haj.com.service.BlankService;
import haj.com.service.CustomerService;



@ManagedBean(name = "customerDetailsUI")
@ViewScoped
public class CustomerDetailsUI extends AbstractUI {

	private CustomerService service = new CustomerService();
	private Customer customer = null;
	

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		customerInfo();
	}

	public void customerInfo() {
		try {
			
			customer = getSessionUI().getCustomer();
			
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


}
