package haj.com.ui;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import haj.com.framework.AbstractUI;
import haj.com.entity.Contract;
import haj.com.service.ContractService;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@ManagedBean(name = "contractUI")
@ViewScoped
public class ContractUI extends AbstractUI {

	private ContractService service = new ContractService();
	private List<Contract> contractList = null;
	private List<Contract> contractfilteredList = null;
	private Contract contract = null;
	private LazyDataModel<Contract> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all Contract and prepare a for a create of a new
	 * Contract
	 * 
	 * @author TechFinium
	 * @see Contract
	 * @throws Exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		contractInfo();
	}

	/**
	 * Get all Contract for data table
	 * 
	 * @author TechFinium
	 * @see Contract
	 */
	public void contractInfo() {

		dataModel = new LazyDataModel<Contract>() {

			private static final long serialVersionUID = 1L;
			private List<Contract> retorno = new ArrayList<Contract>();

			@Override
			public List<Contract> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allContract(Contract.class, first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(service.count(Contract.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Contract obj) {
				return obj.getId();
			}

			@Override
			public Contract getRowData(String rowKey) {
				for (Contract obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

	}

	/**
	 * Insert Contract into database
	 * 
	 * @author TechFinium
	 * @see Contract
	 */
	public void contractInsert() {
		try {
			service.create(this.contract);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			contractInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete Contract from database
	 * 
	 * @author TechFinium
	 * @see Contract
	 */
	public void contractDelete() {
		try {
			service.delete(this.contract);
			prepareNew();
			contractInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted "));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	/**
	 * Create new instance of Contract
	 * 
	 * @author TechFinium
	 * @see Contract
	 */
	public void prepareNew() {
		contract = new Contract();
	}

	/**
	 * Update Contract in database
	 * 
	 * @author TechFinium
	 * @see Contract
	 */
	public void contractUpdate() {
		try {
			service.update(this.contract);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			contractInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Prepare select one menu data
	 * 
	 * @author TechFinium
	 * @see Contract
	 */
	public List<SelectItem> getContractDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), "-------------Select-------------"));
		contractInfo();
		for (Contract ug : contractList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<Contract> getContractfilteredList() {
		return contractfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @see Contract
	 */
	public void setContractfilteredList(List<Contract> contractfilteredList) {
		this.contractfilteredList = contractfilteredList;
	}

	public List<Contract> complete(String desc) {
		List<Contract> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public LazyDataModel<Contract> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Contract> dataModel) {
		this.dataModel = dataModel;
	}

}
