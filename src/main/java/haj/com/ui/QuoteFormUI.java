package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.techfinium.entity.Resource;
import com.techfinium.service.ResourceService;

import haj.com.entity.Address;
import haj.com.entity.Contract;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.Customer;
import haj.com.entity.Quote;
import haj.com.entity.QuoteItem;
import haj.com.entity.enums.AddressTypeEnum;
import haj.com.entity.enums.ContractDetailTypeEnum;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.MaterialTypeEnum;
import haj.com.entity.enums.QuoteStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.AddressService;
import haj.com.service.ContractReviewItemService;
import haj.com.service.ContractService;
import haj.com.service.JasperService;
import haj.com.service.QuoteItemService;
import haj.com.service.QuoteService;
import haj.com.service.TasksService;

@ManagedBean(name = "quoteFormUI")
@ViewScoped
public class QuoteFormUI extends AbstractUI {

	private Contract contract;
	private Customer customer;
	private Quote quote;
	private QuoteItem quoteItem;
	private Address address;
	private List<QuoteItem> quoteItemList;

	private QuoteService quoteService = new QuoteService();
	private QuoteItemService quoteItemService = new QuoteItemService();
	private ContractService contractService = new ContractService();
	private AddressService addressService = new AddressService();
	private TasksService taskService = new TasksService();

	private Date quoteDate = new java.util.Date();
	private Double totalPrice;
	private JasperService jasperService = new JasperService();

	private ContractReviewItemService contractReviewItemService = new ContractReviewItemService();
	private ResourceService resourceService = new ResourceService();
	private ContractReviewItem contractReviewItem;
	private List<ContractReviewItem> contractReviewItemList;
	private Contract contractReview;
	private Double totalPriceContractReview;

	private ContractDetailTypeEnum cdtEnum;

	private Double total_price;
	
	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (getSessionUI().getCustomer() != null && getSessionUI().getContract() != null) {
			this.customer = getSessionUI().getCustomer();
			this.contract = getSessionUI().getContract();
		}
		if (super.getParameter("contractId", false) != null) {
			getSessionUI().setContract(contractService.findByGuid((String) super.getParameter("contractId", false)));
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			this.customer = getSessionUI().getCustomer();
			this.contract = getSessionUI().getContract();
			prepareNewQuote();
			prepareContractReview();
		} else if (super.getParameter("quoteId", false) != null) {
			prepareExistingQuote();
		} else {
			prepareNewQuote();
			prepareContractReview();
		}
		prepareCustomerAddress();

	}

	public void prepareCustomerAddress() {
		try {
			this.address = new Address();
			this.address = addressService.findByCustAndType(getSessionUI().getCustomer().getId(), AddressTypeEnum.Address);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void downloadQuote() {
		try {
			if (this.quote.getId() != null) {
				jasperService.jasperDownloadPdfForQuote(this.quote);
				addInfoMessage("Quote Downloaded");
			} else {
				addWarningMessage("No Quote");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	public void markSent() {
		try {
			if (this.quote.getId() != null) {
				this.quote.setQuoteStatusEnum(QuoteStatusEnum.QuoteCustomer);
				this.quote.setTask(taskService.createTaskForQuote(getSessionUI().getUser(), this.quote));
				quoteService.update(this.quote);
				addInfoMessage("Quote Downloaded");
			} else {
				addWarningMessage("No Quote");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	public void rejectQuote() {
		try {
			if (this.quote.getTask() != null)
				taskService.closeTask(this.quote.getTask(), getSessionUI().getUser());
			this.quote.setQuoteStatusEnum(QuoteStatusEnum.QuoteDecline);
			this.quote.setTask(taskService.createTaskForQuote(getSessionUI().getUser(), this.quote));
			quoteService.update(this.quote);
			this.contract.setContractStatus(ContractStatusEnum.QuoteReject);
			contractService.update(this.contract);
			addInfoMessage("Quote has been rejected");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void clearRejection() {
		this.quote.setRejectReason(null);
	}

	public void acceptQuote() {
		try {
			if (this.quote.getTask() != null)
				taskService.closeTask(this.quote.getTask(), getSessionUI().getUser());
			this.quote.setQuoteStatusEnum(QuoteStatusEnum.QuoteAccept);
			this.quote.setTask(taskService.createTaskForQuote(getSessionUI().getUser(), this.quote));
			quoteService.update(this.quote);
			this.contract.setContractStatus(ContractStatusEnum.QuoteAccept);
			contractService.update(this.contract);
			addInfoMessage("Quote has been accepeted");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareNewQuote() {
		this.quote = new Quote();
		this.quote.setCustomerReference(this.contract.getItemNumber());
		this.quoteItem = new QuoteItem();
		this.quoteItemList = new ArrayList<QuoteItem>();
		this.totalPrice = 0.00;
		this.quote.setContract(this.contract);
		addInfoMessage("Quote item cleared");
	}

	public void prepareExistingQuote() {
		try {
			this.quote = new Quote();
			this.quoteItem = new QuoteItem();
			this.quote = quoteService.findByGuid((String) super.getParameter("quoteId", false));
			getSessionUI().setContract(contractService.findByKey(this.quote.getContract().getId()));
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			this.customer = getSessionUI().getCustomer();
			this.contract = getSessionUI().getContract();
			prepareQuoteItemList();
			prepareContractReview();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void calculatePriceOfItems() {
		this.totalPrice = 0.00;
		for (QuoteItem quoteItemTemp : quoteItemList) {
			if (quoteItemTemp.getPrice() != null)
				this.totalPrice += quoteItemTemp.getPrice();
		}
	}

	public void saveQuote() {
		try {
			quoteService.createQuoteAndQuoteItem(quote, quoteItemList);
			addInfoMessage("Quote Created/Updated " + this.quote.getQuoteNumber());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void addQuotToList() {
		try {
			quoteItemList.add(this.quoteItem);
			this.quoteItem = new QuoteItem();
			addInfoMessage("Quote item added to a list ");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void removeQuoteItem() {
		try {
			if (this.quoteItem.getId() != null) {
				quoteItemService.delete(this.quoteItem);
				this.quoteItemList = new ArrayList<QuoteItem>();
				this.quoteItemList = quoteItemService.findByQuote(this.quote.getId());
				addInfoMessage("Quote item removed");
			} else {
				//this.quoteItemList.remove(this.quoteItem);
				this.quoteItemList.removeIf(qi -> qi == this.quoteItem);
			}
			if (this.quoteItemList.size() > 0)
				calculatePriceOfItems();
			prepareNewQuoteItem();
			addWarningMessage("Quote Item removed");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareNewQuoteItem() {
		try {
			this.quoteItem = new QuoteItem();
			addInfoMessage("Quote item input cleared");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		
	}

	public void prepareQuoteItemList() {
		try {
			this.quoteItemList = new ArrayList<QuoteItem>();
			this.quoteItemList = quoteItemService.findByQuote(this.quote.getId());
			if (this.quoteItemList.size() > 0)
				calculatePriceOfItems();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareContractReview() {
		try {
			this.contractReviewItemList = new ArrayList<ContractReviewItem>();
			this.contractReviewItemList = contractReviewItemService.findByContract(getSessionUI().getContract().getId());
			this.contract = new Contract();
			this.contract = getSessionUI().getContract();
			prepareNewContractReviewItem();
			this.totalPriceContractReview = 0.00;
			calculateTotal();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void calculateTotal() {
		this.totalPriceContractReview = 0.00;
		for (ContractReviewItem contractReviewItem : contractReviewItemList) {
			if (contractReviewItem.getPrice() != null)
				this.totalPriceContractReview += contractReviewItem.getPrice();
		}
	}

	public void prepareContractReviewItemList() {
		try {
			this.contractReviewItemList = new ArrayList<ContractReviewItem>();
			this.contractReviewItemList = contractReviewItemService.findByContract(this.contract.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveContractReviewItem() {
		try {
			contractReviewItemService.create(this.contractReviewItem);
			prepareNewContractReviewItem();
			prepareContractReviewItemList();
			calculateTotal();
			addInfoMessage("Review Item add/update successful");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void deleteContractReviewItem() {
		try {
			if (this.contractReviewItem.getId() != null) {
				contractReviewItemService.delete(this.contractReviewItem);
				prepareNewContractReviewItem();
				prepareContractReviewItemList();
				calculateTotal();
				addInfoMessage("Review Item delete successfull");
			} else {
				addWarningMessage("Detail could not be deleted");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void prepareNewContractReviewItem() {
		this.contractReviewItem = new ContractReviewItem();
		this.contractReviewItem.setContract(this.contract);
	}

	public List<SelectItem> getContractReviewDetailTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ContractDetailTypeEnum selectItem : ContractDetailTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getMaterialTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (MaterialTypeEnum selectItem : MaterialTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<Resource> completeResource(String desc) {
		List<Resource> l = null;
		try {
			l = resourceService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote quote) {
		this.quote = quote;
	}

	public List<QuoteItem> getQuoteItemList() {
		return quoteItemList;
	}

	public void setQuoteItemList(List<QuoteItem> quoteItemList) {
		this.quoteItemList = quoteItemList;
	}

	public QuoteItem getQuoteItem() {
		return quoteItem;
	}

	public void setQuoteItem(QuoteItem quoteItem) {
		this.quoteItem = quoteItem;
	}

	public Date getQuoteDate() {
		return quoteDate;
	}

	public void setQuoteDate(Date quoteDate) {
		this.quoteDate = quoteDate;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ContractReviewItem getContractReviewItem() {
		return contractReviewItem;
	}

	public void setContractReviewItem(ContractReviewItem contractReviewItem) {
		this.contractReviewItem = contractReviewItem;
	}

	public List<ContractReviewItem> getContractReviewItemList() {
		return contractReviewItemList;
	}

	public void setContractReviewItemList(List<ContractReviewItem> contractReviewItemList) {
		this.contractReviewItemList = contractReviewItemList;
	}

	public Contract getContractReview() {
		return contractReview;
	}

	public void setContractReview(Contract contractReview) {
		this.contractReview = contractReview;
	}

	public Double getTotalPriceContractReview() {
		return totalPriceContractReview;
	}

	public void setTotalPriceContractReview(Double totalPriceContractReview) {
		this.totalPriceContractReview = totalPriceContractReview;
	}

	public ContractDetailTypeEnum getCdtEnum() {
		return cdtEnum;
	}

	public void setCdtEnum(ContractDetailTypeEnum cdtEnum) {
		this.cdtEnum = cdtEnum;
	}

	public Double getTotal_price() {
		return total_price;
	}

	public void setTotal_price(Double total_price) {
		this.total_price = total_price;
	}

}
