package haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.techfinium.entity.Resource;
import com.techfinium.entity.ResourceType;
import com.techfinium.service.ResourceService;
import com.techfinium.service.ResourceTypeService;

import haj.com.framework.AbstractUI;

@ManagedBean(name = "resourceUI")
@ViewScoped
public class ResourceUI extends AbstractUI {

	// resource management
	private ResourceService resourceService = new ResourceService();
	private ResourceTypeService resourceTypeService = new ResourceTypeService();
	private Resource resource = null;
	private List<Resource> resourceList = null;
	private List<Resource> resourcefilteredList = null;
	private List<ResourceType> resourceTypeList = null;
	private LazyDataModel<Resource> resourceDataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepareNew();
		resourceTypeInfo();
		resourceInfo();
		resourceList = resourceService.allResources();
	}

	public void prepareNew() {
		resource = new Resource();
	}

	public void resourceInfo() {
		
		resourceDataModel = new LazyDataModel<Resource>() {

			private static final long serialVersionUID = 1L;
			private List<Resource> retorno = new ArrayList<Resource>();

			@Override
			public List<Resource> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				try {
					retorno = resourceService.allResource(Resource.class, first, pageSize, sortField, sortOrder,
							filters);
					resourceDataModel.setRowCount(resourceService.count(Resource.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Resource obj) {
				return obj.getId();
			}

			@Override
			public Resource getRowData(String rowKey) {
				for (Resource obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

		try {
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void resourceTypeInfo() {
		try {
			resourceTypeList = new ArrayList<>();
			ResourceType tempResourceType = new ResourceType();
			tempResourceType.setDescription("Select a Resource Type");
			resourceTypeList.add(tempResourceType);
			resourceTypeList.addAll(resourceTypeService.allResourceType());

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void resourceInsert() {
		try {
			if (resource.getResourceType() == null || resource.getResourceType().getId() == null) {
				throw new Exception("Please select a resource type.");
			}
			resourceService.create(this.resource);
			prepareNew();
			addInfoMessage("Update successful");
			resourceInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void resourceDelete() {
		try {
			resourceService.delete(this.resource);
			prepareNew();
			resourceInfo();
			resourceTypeInfo();
			super.addWarningMessage("Row deleted.");
		}catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void resourceUpdate() {
		try {
			resourceService.update(this.resource);
			addInfoMessage("Update successful");
			resourceInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public List<Resource> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<Resource> resourceList) {
		this.resourceList = resourceList;
	}

	public List<Resource> getResourcefilteredList() {
		return resourcefilteredList;
	}

	public void setResourcefilteredList(List<Resource> resourcefilteredList) {
		this.resourcefilteredList = resourcefilteredList;
	}

	public LazyDataModel<Resource> getResourceDataModel() {
		return resourceDataModel;
	}

	public void setResourceDataModel(LazyDataModel<Resource> resourceDataModel) {
		this.resourceDataModel = resourceDataModel;
	}

	public List<ResourceType> getResourceTypeList() {
		return resourceTypeList;
	}

	public void setResourceTypeList(List<ResourceType> resourceTypeList) {
		this.resourceTypeList = resourceTypeList;
	}
}
