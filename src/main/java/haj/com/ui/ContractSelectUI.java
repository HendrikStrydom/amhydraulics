package  haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Contract;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;



@ManagedBean(name = "contractSelectUI")
@ViewScoped
public class ContractSelectUI extends AbstractUI {

	private ContractService service = new ContractService();
	private List<Contract> contractList = null;
	

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
	
		
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}


	public List<Contract> complete(String contractNr) {
	List<Contract> l = null;
	try {
		l = service.findByPossibleNumber(contractNr);
	}
	catch (Exception e) {
	addErrorMessage(e.getMessage(), e);
	}
	return l;
}

	
}
