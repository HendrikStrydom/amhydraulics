package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import haj.com.entity.ConfigDoc;
import haj.com.entity.Doc;
import haj.com.entity.Users;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ConfigDocService;
import haj.com.service.DocService;
import haj.com.service.DocumentTrackerService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "contractUploadDocUI")
@SessionScoped
public class ContractUploadDocUI extends AbstractUI {

	@ManagedProperty(value = "#{uploadDocUI}")
	private UploadDocUI uploadDocUI;
	private DocService docService = new DocService();
	private Doc doc;
	private List<Doc> docList;
	private List<Doc> docFilterList;

	private ConfigDoc configDoc;
	private List<ConfigDoc> configDocList;
	private ConfigDocService configDocService = new ConfigDocService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepConfigDoc();
		if (getSessionUI().getContract() != null)
			docListInfo();
	}

	public void downloadDoc() {
		try {
			Users user = getSessionUI().getUser();
			DocumentTrackerService.create(user, doc, DocumentTrackerEnum.Downloaded);
			Faces.sendFile(doc.getFileContent(), GenericUtility.buidFileName(doc), true);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveFile(FileUploadEvent event) {
		try {
			if (this.configDoc != null || this.configDoc.getId() != null) {
				prepDoc();
				uploadDocUI.setDocs(null);
				uploadDocUI.saveFile(event, this.doc, getSessionUI().getUser());
				docListInfo();
				RequestContext.getCurrentInstance().update("docUploadForm");
				RequestContext.getCurrentInstance().update("contractUploadGrwl");
			} else {
				addWarningMessage("Please Select Document Type");
				RequestContext.getCurrentInstance().update("contractUploadGrwl");
			}

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepDoc() throws Exception {
		this.doc = new Doc();
		this.doc.setUser(getSessionUI().getUser());
		this.doc.setConfigDoc(this.configDoc);
		if (getSessionUI().getContract() != null)
			this.doc.setContract(getSessionUI().getContract());
	}

	public void docListInfo() {
		try {
			docFilterList = new ArrayList<Doc>();
			docList = new ArrayList<Doc>();
			if (getSessionUI().getContract() != null)
				docList = docService.findByContract(getSessionUI().getContract());
			else
				docList = new ArrayList<Doc>();
			RequestContext.getCurrentInstance().update(":docUploadForm");
			RequestContext.getCurrentInstance().update(":docUploadForm");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void prepConfigDoc() {
		try {
			configDocList = new ArrayList<ConfigDoc>();
			configDocList = configDocService.allConfigDocActive();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public ConfigDoc emptyConfigDoc() {
		return new ConfigDoc();
	}

	public UploadDocUI getUploadDocUI() {
		return uploadDocUI;
	}

	public void setUploadDocUI(UploadDocUI uploadDocUI) {
		this.uploadDocUI = uploadDocUI;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public List<Doc> getDocList() {
		return docList;
	}

	public void setDocList(List<Doc> docList) {
		this.docList = docList;
	}

	public ConfigDoc getConfigDoc() {
		return configDoc;
	}

	public void setConfigDoc(ConfigDoc configDoc) {
		this.configDoc = configDoc;
	}

	public List<ConfigDoc> getConfigDocList() {
		return configDocList;
	}

	public void setConfigDocList(List<ConfigDoc> configDocList) {
		this.configDocList = configDocList;
	}

	public List<Doc> getDocFilterList() {
		return docFilterList;
	}

	public void setDocFilterList(List<Doc> docFilterList) {
		this.docFilterList = docFilterList;
	}

}
