package haj.com.ui;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.framework.AbstractUI;
import haj.com.service.SendMail;

@ManagedBean(name = "blankUI")
@ViewScoped
public class BlankUI extends AbstractUI {

	private int someInt = 1;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {

	}

	public void sendMailTest() {
		new SendMail().sendMailCommons("tgwilson35@gmail.com", "A Test mail " + new Date(), "Testing, testing, 1,2,3.. ");
	}

	public void incrVar() {
		this.someInt += 1;
		if (this.someInt > 7)
			this.someInt = 0;
		//System.out.println(this.someInt);
	}

	public int getSomeInt() {
		return someInt;
	}

}
