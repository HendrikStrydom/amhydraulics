package haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.Users;
import haj.com.entity.enums.UserTypeEnum;
import haj.com.entity.enums.UsersStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.LogonService;
import haj.com.service.RegisterService;
import haj.com.service.UsersService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "usersUI")
@ViewScoped
public class UsersUI extends AbstractUI {

	private Users user;
	private LazyDataModel<Users> usersDataModel;
	private UsersService usersService = new UsersService();
	private List<Users> usersfilteredList;
	private List<UsersStatusEnum> usersStatusList;
	private RegisterService registerService = new RegisterService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepareNewUser();
		prepareUsersStatus();
		usersInfo();
	}

	public void prepareUsersStatus() {
		usersStatusList = new ArrayList<UsersStatusEnum>();
		for (UsersStatusEnum stat : UsersStatusEnum.values()) {
			usersStatusList.add(stat);
		}
	}

	public void usersInfo() {
		try {
			usersfilteredList = usersService.allUsers();
			usersDataModel = new LazyDataModel<Users>() {

				private static final long serialVersionUID = 1L;
				private List<Users> retorno = new ArrayList<Users>();

				@Override
				public List<Users> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

					try {
						retorno = usersService.allUsers(Users.class, first, pageSize, sortField, sortOrder, filters);
						usersDataModel.setRowCount(usersService.count(Users.class, filters));
					} catch (Exception e) {
						logger.fatal(e);
					}
					return retorno;
				}

				@Override
				public Object getRowKey(Users obj) {
					return obj.getId();
				}

				@Override
				public Users getRowData(String rowKey) {
					for (Users obj : retorno) {
						if (obj.getId().equals(Long.valueOf(rowKey)))
							return obj;
					}
					return null;
				}

			};
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareNewUser() {
		this.user = new Users();
		this.user.setStatus(UsersStatusEnum.Active);
		this.user.setUserType(UserTypeEnum.Employee);
	}

	public void sendMailForReg() {
		try {
			String pwd = GenericUtility.genPassord();
			user.setPassword(LogonService.encryptPassword(pwd));
			registerService.updateUser(user);
			registerService.notifyUser(user, pwd);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void usersInsert() {
		try {
//			registerService.register(user);
//			usersService.create(this.user);
			usersService.checkUserExists(this.user);
			prepareNewUser();
			addInfoMessage("Update successful");
			usersInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void usersUpdate() {
		try {
			usersService.update(this.user);
			addInfoMessage("Update successful");
			usersInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Sets users status to active
	 */
	public void usersUpdateGrantAccess() {
		try {
			this.user.setStatus(UsersStatusEnum.Active);
			usersService.update(this.user);
			addInfoMessage("Update Successful. User Is Now Active");
			usersInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Sets users status to in-active
	 */
	public void usersUpdateRevokeAccess() {
		try {
			this.user.setStatus(UsersStatusEnum.InActive);
			usersService.update(this.user);
			addInfoMessage("Update Successful. User Is Now In Active");
			usersInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public LazyDataModel<Users> getUsersDataModel() {
		return usersDataModel;
	}

	public void setUsersDataModel(LazyDataModel<Users> usersDataModel) {
		this.usersDataModel = usersDataModel;
	}

	public List<Users> getUsersfilteredList() {
		return usersfilteredList;
	}

	public void setUsersfilteredList(List<Users> usersfilteredList) {
		this.usersfilteredList = usersfilteredList;
	}

	public List<UsersStatusEnum> getUsersStatusList() {
		return usersStatusList;
	}

	public void setUsersStatusList(List<UsersStatusEnum> usersStatusList) {
		this.usersStatusList = usersStatusList;
	}

}
