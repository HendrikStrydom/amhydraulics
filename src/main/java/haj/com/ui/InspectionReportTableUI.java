package haj.com.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Contract;
import haj.com.entity.InspectionReport;
import haj.com.entity.enums.PurchaseOrderEnum;
import haj.com.entity.enums.PurchaseOrderForEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.InspectionReportService;
import haj.com.service.JasperService;

@ManagedBean(name = "inspectionReportTableUI")
@ViewScoped
public class InspectionReportTableUI extends AbstractUI {

	private InspectionReportService inspectionReportService = new InspectionReportService();
	private List<InspectionReport> inspectionReportList;
	private InspectionReport inspectionReport;

	private Contract contract;
	private ContractService contractService = new ContractService();

	private Map<String, Object> queryParameters = new Hashtable<String, Object>();

	private String formNumber, contractNumber, customerOrderNum;
	private Boolean completed;
	private int triStateValue;
	
	private JasperService jasperService =new JasperService();
	
	public void downloadPDF() {
		try {
			jasperService.jasperDownloadPdfForStripInspectionReport(this.inspectionReport);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.inspectionReportList = new ArrayList<InspectionReport>();
		this.inspectionReportList = inspectionReportService.allInspectionReport();
	}

	public void deleteInspectionReport() {
		try {
			this.inspectionReport.setActionUser(getSessionUI().getUser());
			this.inspectionReport.setSoftDelete(true);
			inspectionReportService.update(this.inspectionReport);
			this.inspectionReportList = inspectionReportService.allInspectionReport();
			addInfoMessage("Strip Inspection Report Deleted");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void zoomTo() {
		if (this.inspectionReport.getId() != null) {
			getSessionUI().setContract(this.inspectionReport.getContract());
			getSessionUI().setCustomer(this.inspectionReport.getContract().getCustomer());
			super.redirect("/pages/inspectionReportForm.jsf?inspecId=" + this.inspectionReport.getId());
		}
	}

	/**
	 * This will set contract into the session and redirect to quoteForm.jsf
	 */
	public void createRouteCardWithContract() {
		super.redirect("/pages/inspectionReportForm.jsf?contractId=" + this.contract.getId());
	}

	public void searchFor() {
		try {
			queryParameters.put("ConEnqNumber", "%" + contractNumber + "%");
			queryParameters.put("customerOrderNum", "%" + customerOrderNum + "%");
			queryParameters.put("formNumber", "%" + formNumber + "%");
			if (completed != null)
				queryParameters.put("completed", completed);
			this.inspectionReportList = inspectionReportService.findInspectionReportByQueryParameter(queryParameters);
			if (this.inspectionReportList.size() > 0)
				addInfoMessage("Search results found: " + this.inspectionReportList.size());
			else
				addWarningMessage("No results found");

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void clearSearch() {
		this.contractNumber = null;
		this.customerOrderNum = null;
		this.formNumber = null;
		this.completed = null;
		this.triStateValue = 0;
		queryParameters = new Hashtable<String, Object>();
	}

	public void triStateCheckAroo() {
		switch (this.triStateValue) {
		case 0:
			this.completed = null;
			break;
		case 1:
			this.completed = true;
			break;
		case 2:
			this.completed = false;
			break;
		default:
			this.completed = null;
			break;
		}

	}

	public void clearContract() {
		this.contract = new Contract();
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = new ArrayList<Contract>();
		try {
			l = contractService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<InspectionReport> getInspectionReportList() {
		return inspectionReportList;
	}

	public void setInspectionReportList(List<InspectionReport> inspectionReportList) {
		this.inspectionReportList = inspectionReportList;
	}

	public InspectionReport getInspectionReport() {
		return inspectionReport;
	}

	public void setInspectionReport(InspectionReport inspectionReport) {
		this.inspectionReport = inspectionReport;
	}

	public Map<String, Object> getQueryParameters() {
		return queryParameters;
	}

	public void setQueryParameters(Map<String, Object> queryParameters) {
		this.queryParameters = queryParameters;
	}

	public String getFormNumber() {
		return formNumber;
	}

	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getCustomerOrderNum() {
		return customerOrderNum;
	}

	public void setCustomerOrderNum(String customerOrderNum) {
		this.customerOrderNum = customerOrderNum;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public int getTriStateValue() {
		return triStateValue;
	}

	public void setTriStateValue(int triStateValue) {
		this.triStateValue = triStateValue;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

}
