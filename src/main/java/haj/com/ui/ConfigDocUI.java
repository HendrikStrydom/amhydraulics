package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.ConfigDoc;
import haj.com.framework.AbstractUI;
import haj.com.service.ConfigDocService;

@ManagedBean(name = "configDocUI")
@ViewScoped
public class ConfigDocUI extends AbstractUI {

	// Entity layer
	private ConfigDoc configDoc = new ConfigDoc();

	// Service layer
	private ConfigDocService configDocService = new ConfigDocService();

	// Lists
	private List<ConfigDoc> configDocList = new ArrayList<ConfigDoc>();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepNewConfig();
		configInfo();
	}

	public void prepNewConfig() throws Exception {
		configDoc = new ConfigDoc();
	}

	private void configInfo() throws Exception {
		configDocList = configDocService.allConfigDoc();
	}

	public void createUpdateConfig() {
		try {
			configDocService.create(configDoc);
			prepNewConfig();
			configInfo();
			addInfoMessage("Submit Complete");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void deleteConfig(){
		try {
			configDocService.delete(configDoc);
			prepNewConfig();
			configInfo();
			addInfoMessage("Delete Complete");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}	
	}
	
	public void setActiveOrInactive(){
		try {
			if (configDoc.getActive()) {
				configDoc.setActive(false);
			}else {
				configDoc.setActive(true);
			}
			configDocService.create(configDoc);
			prepNewConfig();
			configInfo();
			addInfoMessage("Update Complete");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}	
	}

	public ConfigDoc getConfigDoc() {
		return configDoc;
	}

	public void setConfigDoc(ConfigDoc configDoc) {
		this.configDoc = configDoc;
	}

	public List<ConfigDoc> getConfigDocList() {
		return configDocList;
	}

	public void setConfigDocList(List<ConfigDoc> configDocList) {
		this.configDocList = configDocList;
	}
}
