package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import haj.com.entity.Contract;
import haj.com.entity.Quote;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.QuoteService;
import haj.com.service.TasksService;

@ManagedBean(name = "qctUI")
@ViewScoped
public class QuoteCustomerTableUI extends AbstractUI {

	private ContractService contractService = new ContractService();
	private QuoteService quoteService = new QuoteService();
	private TasksService tasksService = new TasksService();

	private List<Contract> contractList;
	private List<Quote> quoteList;

	private Quote quote;
	private Contract contract;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (getSessionUI().getContract() == null) {
			prepareContractList();
		} else {
			prepareContractList();
			this.contract = getSessionUI().getContract();
			prepareQuoteDetailsList(getSessionUI().getContract().getId());
		}
	}

	public void prepareContractList() {
		try {
			contractList = new ArrayList<Contract>();
			contract = new Contract();
			contractList = contractService.findByCustomer(getSessionUI().getCustomer());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void prepareQuoteDetailsList(Long contractId) {
		try {
			quoteList = new ArrayList<Quote>();
			quote = new Quote();
			quoteList = quoteService.findByContract(contractId);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void viewQuote() {
		prepareQuoteDetailsList(this.contract.getId());
		if (this.quoteList.size() <= 0) {
			contract = new Contract();
			addWarningMessage("No Quotes Have Been Made");
			RequestContext.getCurrentInstance().update("quoteCustomerTableGrwl");
		}
	}

	public void enquiryDirect() {
		try {
			super.ajaxRedirect("/pages/enquiryForm.jsf");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void createQuote() {
		getSessionUI().setContract(this.contract);
		super.ajaxRedirect("/pages/quoteForm.jsf");
	}

	public void goToQuote() {
		getSessionUI().setContract(this.contract);
		super.redirect("/pages/quoteForm.jsf?quoteId=" + this.quote.getId());
	}

	public void deleteQuote() {
		try {
			this.quote.setSoftDelete(true);
			if (this.quote.getTask() != null)
				tasksService.closeTask(this.quote.getTask(), getSessionUI().getUser());
			quoteService.update(this.quote);
			prepareQuoteDetailsList(this.contract.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public List<Quote> getQuoteList() {
		return quoteList;
	}

	public void setQuoteList(List<Quote> quoteList) {
		this.quoteList = quoteList;
	}

	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote quote) {
		this.quote = quote;
	}

}
