package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.enums.AddressTypeEnum;
import haj.com.entity.enums.AwaitingDispatchEnum;
import haj.com.entity.enums.ContractDetailTypeEnum;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.ContractTypeEnum;
import haj.com.entity.enums.MaterialTypeEnum;
import haj.com.entity.enums.RepairNewEnum;
import haj.com.entity.enums.TaskStatusEnum;
import haj.com.entity.enums.UserTypeEnum;
import haj.com.entity.enums.YesNoEnum;
import haj.com.framework.AbstractUI;

@ManagedBean(name = "enumUI")
@ViewScoped
public class EnumUI extends AbstractUI {

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
	}

	public List<SelectItem> getUserTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (UserTypeEnum selectItem : UserTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	/**
	 * Populates list of: TaskStatusEnum Ordered for reports
	 * 
	 * @return List<TaskStatusEnum>
	 */
	public List<SelectItem> getTaskStatusOrderedReportEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(TaskStatusEnum.All, "All"));
		l.add(new SelectItem(TaskStatusEnum.Completed, "Completed"));
		l.add(new SelectItem(TaskStatusEnum.Overdue, "Overdue"));
		l.add(new SelectItem(TaskStatusEnum.Underway, "Underway"));
		l.add(new SelectItem(TaskStatusEnum.NotStarted, "Not Started"));
		l.add(new SelectItem(TaskStatusEnum.Closed, "Closed"));
		return l;
	}

	public List<SelectItem> getAddressTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (AddressTypeEnum selectItem : AddressTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getRepairNewDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (RepairNewEnum selectItem : RepairNewEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getContractTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ContractTypeEnum selectItem : ContractTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getQuoteDetailTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ContractDetailTypeEnum selectItem : ContractDetailTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getQuoteMaterialTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (MaterialTypeEnum selectItem : MaterialTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getYesNoEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (YesNoEnum selectItem : YesNoEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getAwaitingDispatchEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (AwaitingDispatchEnum selectItem : AwaitingDispatchEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getContractStatusEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ContractStatusEnum selectItem : ContractStatusEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}
}
