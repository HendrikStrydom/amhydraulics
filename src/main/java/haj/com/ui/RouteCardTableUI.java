package haj.com.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Contract;
import haj.com.entity.Quote;
import haj.com.entity.RouteCard;
import haj.com.entity.enums.QuoteStatusEnum;
import haj.com.entity.enums.RouteStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.JasperService;
import haj.com.service.RouteCardService;
import haj.com.service.TasksService;

@ManagedBean(name = "routeCardTableUI")
@ViewScoped
public class RouteCardTableUI extends AbstractUI {

	private ContractService contractService = new ContractService();
	private RouteCardService routeCardService = new RouteCardService();
	private TasksService tasksService = new TasksService();
	public JasperService getJasperService() {
		return jasperService;
	}

	public void setJasperService(JasperService jasperService) {
		this.jasperService = jasperService;
	}

	private JasperService jasperService = new JasperService();

	private List<RouteCard> routeCardList = new ArrayList<RouteCard>();

	private RouteCard routeCard;
	private Contract contract;

	private Map<String, Object> queryParameters = new Hashtable<String, Object>();

	private RouteStatusEnum[] routeCardStatusEnum = RouteStatusEnum.values();

	private String ceNumber;
	private RouteStatusEnum rsEnum;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.routeCardList = routeCardService.allRouteCard();
	}

	public void deleteRouteCard() {
		try {
			this.routeCard.setSoftDelete(true);
			if (this.routeCard.getTask() != null)
				tasksService.closeTask(this.routeCard.getTask(), getSessionUI().getUser());
			routeCardService.delete(this.routeCard);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/***
	 * Here is where the Map<String, Object> gets built with its relevant
	 * information
	 */
	public void searchFor() {
		try {
			queryParameters.put("ConEnqNumber", "%" + ceNumber + "%");
			if (rsEnum != null)
				queryParameters.put("rsEnum", rsEnum);
			this.routeCardList = routeCardService.findRouteCardByQueryParameter(queryParameters);
			if (this.routeCardList.size() > 0)
				addInfoMessage("Search results found: " + this.routeCardList.size());
			else
				addWarningMessage("No results found");

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/***
	 * Clears search criterias
	 */
	public void clearSearch() {
		this.ceNumber = null;
		this.rsEnum = null;
		queryParameters = new Hashtable<String, Object>();
	}

	public void clearContract() {
		this.contract = new Contract();
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = new ArrayList<Contract>();
		try {
			l = contractService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	/**
	 * this zooms into the quoteForm.jsf to view/edit selected quote from data
	 * table
	 */
	public void zoomTo() {
		if (this.routeCard.getId() != null) {
			getSessionUI().setContract(this.routeCard.getContract());
			getSessionUI().setCustomer(this.routeCard.getContract().getCustomer());
			super.redirect("/pages/routeCard.jsf?rcardId=" + this.routeCard.getId());
		}
	}
	
	
	
	public void downloadDoc() {
		try {
			if (this.routeCard.getId() != null)
				jasperService.jasperDownloadPdfForRouteCard(this.routeCard);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * This will set contract into the session and redirect to quoteForm.jsf
	 */
	public void createRouteCardWithContract() {
		getSessionUI().setContract(this.contract);
		getSessionUI().setCustomer(this.contract.getCustomer());
		super.redirect("/pages/routeCard.jsf?contractId=" + this.contract.getId());
	}

	public List<RouteCard> getRouteCardList() {
		return routeCardList;
	}

	public void setRouteCardList(List<RouteCard> routeCardList) {
		this.routeCardList = routeCardList;
	}

	public RouteCard getRouteCard() {
		return routeCard;
	}

	public void setRouteCard(RouteCard routeCard) {
		this.routeCard = routeCard;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Map<String, Object> getQueryParameters() {
		return queryParameters;
	}

	public void setQueryParameters(Map<String, Object> queryParameters) {
		this.queryParameters = queryParameters;
	}

	public String getCeNumber() {
		return ceNumber;
	}

	public void setCeNumber(String ceNumber) {
		this.ceNumber = ceNumber;
	}

	public RouteStatusEnum getRsEnum() {
		return rsEnum;
	}

	public void setRsEnum(RouteStatusEnum rsEnum) {
		this.rsEnum = rsEnum;
	}

	public RouteStatusEnum[] getRouteCardStatusEnum() {
		return routeCardStatusEnum;
	}

	public void setRouteCardStatusEnum(RouteStatusEnum[] routeCardStatusEnum) {
		this.routeCardStatusEnum = routeCardStatusEnum;
	}

}
