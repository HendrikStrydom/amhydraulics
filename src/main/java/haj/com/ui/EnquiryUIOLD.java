
package haj.com.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

import com.sun.xml.bind.v2.TODO;

import haj.com.entity.ConfigDoc;
import haj.com.entity.Contract;
import haj.com.entity.Customer;
import haj.com.entity.Doc;
import haj.com.entity.Tasks;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ConfigDocService;
import haj.com.service.ContractService;
import haj.com.service.CustomerService;
import haj.com.service.DocService;
import haj.com.service.TasksService;
import haj.com.service.WorkflowService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "enquiryUIOLD")
@ViewScoped
public class EnquiryUIOLD extends AbstractUI {

	private DocService docService = new DocService();

	private ContractService service = new ContractService();
	private CustomerService customerService = new CustomerService();
	private WorkflowService workflowService = new WorkflowService();
	private TasksService tasksService = new TasksService();

	private LazyDataModel<Contract> dataModel;
	private List<Contract> contractList = null;
	private List<Contract> contractfilteredList = null;
	private Contract contract = null;
	private Contract tcontract = null;
	private Customer customer = null;
	private boolean rowSelect;
	private boolean newCustomer = false;
	private boolean showDocUpload = false;

	private boolean displayEnquiries;
	private ConfigDoc configDoc = new ConfigDoc();
	private List<ConfigDoc> docTypeList = new ArrayList<ConfigDoc>();
	private ConfigDocService configDocService = new ConfigDocService();

	// upload docs
	@ManagedProperty(value = "#{uploadDocUI}")
	private UploadDocUI uploadDocUI;
	private Doc doc;
	private List<Doc> docList = null;
	private List<Doc> docfilteredList = null;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		displayEnquiries = false;
		prepareNew();
		contractInfo();
	}

	public void contractInfo() {
		try {
			if (getSessionUI().getCustomer().getNewCustomer() != null)
				newCustomer = getSessionUI().getCustomer().getNewCustomer();
			else
				newCustomer = false;
			if (newCustomer == false) {
				contractList = service.findByCustomer(getSessionUI().getCustomer());
				displayEnquiries = true;
				prepareNew();
			}

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareNew() {
		contract = new Contract();
		contract.setCustomer(getSessionUI().getCustomer());
		try {
			if (super.getParameter("enqId", false) != null) {
				this.contract = service.findByKey(Long.valueOf("" + super.getParameter("enqId", false)));
				addInfoMessage("Now reviewing enquiry: " + contract.getEnquiryNumber());
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void docListInfo() {
		try {
			docList = docService.findByContract(contract);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void contractInsert() {
		try {
			service.create(contract);
			addInfoMessage("Update successful");
			contractInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void contractSubmit() {
		try {
			// contract.setWorkflow(workflowService.findByKey(1));
			contract.setContractStatus(ContractStatusEnum.NewEnquiry);
			contract.setSoftDelete(false);
			service.create(contract);
			getSessionUI().setContract(contract);
			if (tasksService.findTasksByContractB(contract)) {
				List<Tasks> taskList = new ArrayList<Tasks>();
				taskList = tasksService.findTasksByUserContract(getSessionUI().getUser(), contract);
				for (Tasks task : taskList) {
					tasksService.closeTask(task, getSessionUI().getUser());
				}
			}
			//tasksService.createContractTask(getSessionUI().getUser(), contract);
			addInfoMessage("Update successful");
			contractInfo();
			if (newCustomer) {
				showDocUpload = true;
			} else
				super.runClientSideUpdate("enquiryDetailForm");

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void contractDelete() {
		try {
			service.delete(this.contract);
			prepareNew();
			contractInfo();
			super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void contractUpdate() {
		try {
			service.update(this.contract);
			addInfoMessage("Update successful");
			contractInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Customer> complete(String desc) {
		List<Customer> l = null;
		try {
			l = customerService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	// onRowSelect
	public void showRowSelect(SelectEvent event) {
		try {
			contract = tcontract;
			rowSelect = true;
			docListInfo();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepDocUpload() {
		try {
			doc = new Doc();
			doc.setContract(contract);
			configDoc = new ConfigDoc();
			ConfigDoc blackConfig = new ConfigDoc();
			docTypeList = new ArrayList<ConfigDoc>();
			blackConfig.setName("Select Document Type");
			docTypeList.add(blackConfig);
			docTypeList.addAll(configDocService.allConfigDocActive());
			docListInfo();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveFile(FileUploadEvent event) {
		try {
			if (configDoc == null || configDoc.getId() == null) {
				throw new Exception("Please Select Type Of Document Upload");
			}
			this.doc.setConfigDoc(configDoc);
			uploadDocUI.setDocs(null);
			uploadDocUI.saveFile(event, this.doc);
			docListInfo();
			prepDocUpload();
			super.runClientSideUpdate("buttonForm");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void deleteContract() {
		try {
			if (this.contract.getId() != null) {
				this.contract.setSoftDelete(true);
				service.update(this.contract);
				addInfoMessage("Contract has been removed!");
				// Removes old tasks for user and contract id
				if (!tasksService.findTasksByUserContract(getSessionUI().getUser(), contract).isEmpty()) {
					List<Tasks> taskList = new ArrayList<Tasks>();
					taskList = tasksService.findTasksByUserContract(getSessionUI().getUser(), contract);
					for (Tasks task : taskList) {
						tasksService.closeTask(task, getSessionUI().getUser());
					}
				}
				this.contract = new Contract();
				contractInfo();
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void sendContract() {
		try {
			docListInfo();
			if (this.docList.size() > 0) {
				// Removes old tasks for user and contract id
				if (!tasksService.findTasksByUserContract(getSessionUI().getUser(), contract).isEmpty()) {
					List<Tasks> taskList = new ArrayList<Tasks>();
					taskList = tasksService.findTasksByUserContract(getSessionUI().getUser(), contract);
					for (Tasks task : taskList) {
						tasksService.closeTask(task, getSessionUI().getUser());
					}
				}
				getSessionUI().setCustomer(contract.getCustomer());
				getSessionUI().setContract(contract);
				getSessionUI().getContract().setContractStatus(ContractStatusEnum.SubmitForQuotation);
				addInfoMessage("Contract " + contract.getEnquiryNumber() + " sent for evaluation");
				service.update(getSessionUI().getContract());
				prepareNew();
				contract = getSessionUI().getContract();
				//tasksService.createContractTask(getSessionUI().getUser(), contract);
				if (getSessionUI().getCustomer().getNewCustomer() != null && getSessionUI().getCustomer().getNewCustomer())
					super.ajaxRedirect("/admin/dashboard.jsf");
				prepareNew();
			} else {
				addWarningMessage("No Documents Uploaded");
				prepareNew();
			}
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Sends Quote to Customer and Manager Manager receive task with chase days
	 * of 2, 5 and 10 Client is sent an email with a pdf of he quotes attached.
	 * Employee/Manager manually inputs if rejected or accepted
	 */
	public void sendQuote() {
		try {
			// Removes old tasks for user and contract id
			if (!tasksService.findTasksByUserContract(getSessionUI().getUser(), getSessionUI().getContract()).isEmpty()) {
				List<Tasks> taskList = new ArrayList<Tasks>();
				taskList = tasksService.findTasksByUserContract(getSessionUI().getUser(), getSessionUI().getContract());
				for (Tasks task : taskList) {
					tasksService.closeTask(task, getSessionUI().getUser());
				}
			}
			getSessionUI().getContract().setContractStatus(ContractStatusEnum.QuoteSentCustomer);
			// For next screen
			service.update(getSessionUI().getContract());
			//tasksService.createContractTask(getSessionUI().getUser(), getSessionUI().getContract());
			// TODO: Email client with
			// TODO: Email client with
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void redirectDashboard() {
		getSessionUI().setContract(null);
		getSessionUI().setCustomer(null);
		super.redirect("/admin/dashboard.jsf");
	}

	public UploadDocUI getUploadDocUI() {
		return uploadDocUI;
	}

	public void setUploadDocUI(UploadDocUI uploadDocUI) {
		this.uploadDocUI = uploadDocUI;
	}

	public LazyDataModel<Contract> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Contract> dataModel) {
		this.dataModel = dataModel;
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public List<Contract> getContractfilteredList() {
		return contractfilteredList;
	}

	public void setContractfilteredList(List<Contract> contractfilteredList) {
		this.contractfilteredList = contractfilteredList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean isRowSelect() {
		return rowSelect;
	}

	public void setRowSelect(boolean rowSelect) {
		this.rowSelect = rowSelect;
	}

	public boolean isDisplayEnquiries() {
		return displayEnquiries;
	}

	public void setDisplayEnquiries(boolean displayEnquiries) {
		this.displayEnquiries = displayEnquiries;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public List<Doc> getDocList() {
		return docList;
	}

	public void setDocList(List<Doc> docList) {
		this.docList = docList;
	}

	public List<Doc> getDocfilteredList() {
		return docfilteredList;
	}

	public void setDocfilteredList(List<Doc> docfilteredList) {
		this.docfilteredList = docfilteredList;
	}

	public boolean isNewCustomer() {
		return newCustomer;
	}

	public void setNewCustomer(boolean newCustomer) {
		this.newCustomer = newCustomer;
	}

	public boolean isShowDocUpload() {
		return showDocUpload;
	}

	public void setShowDocUpload(boolean showDocUpload) {
		this.showDocUpload = showDocUpload;
	}

	public Contract getTcontract() {
		return tcontract;
	}

	public void setTcontract(Contract tcontract) {
		this.tcontract = tcontract;
	}

	public List<ConfigDoc> getDocTypeList() {
		return docTypeList;
	}

	public void setDocTypeList(List<ConfigDoc> docTypeList) {
		this.docTypeList = docTypeList;
	}

	public ConfigDoc getConfigDoc() {
		return configDoc;
	}

	public void setConfigDoc(ConfigDoc configDoc) {
		this.configDoc = configDoc;
	}

}
