package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.jfree.data.gantt.Task;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.LazyDataModel;

import haj.com.entity.Address;
import haj.com.entity.Contract;
import haj.com.entity.Customer;
import haj.com.entity.Doc;
import haj.com.entity.enums.AddressTypeEnum;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.AddressLookupService;
import haj.com.service.AddressService;
import haj.com.service.ContractService;
import haj.com.service.CustomerService;
import haj.com.service.DocService;
import haj.com.service.TasksService;

@ManagedBean(name = "customerEnquiryUI")
@ViewScoped
public class CustomerEnquiryUI extends AbstractUI {

	private CustomerService customerService = new CustomerService();
	private AddressService addressService = new AddressService();
	private Customer customer = null;
	private boolean copyAddress;

	private ContractService contractService = new ContractService();
	private List<Contract> contractfilteredList;
	private List<Contract> contractList;
	private Contract contract;
	
	public void prepareContractsForCustomerSelected() {
		try {
			this.contractfilteredList = new ArrayList<Contract>();
			this.contractList = new ArrayList<Contract>();
			this.contract = new Contract();
			if (this.customer.getId() != null) {
				this.contractList = contractService.findByCustomer(this.customer);
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void zoomTo() {
		getSessionUI().setContract(this.contract);
		getSessionUI().setCustomer(this.contract.getCustomer());
		super.redirect("/pages/enquiryForm.jsf?enqId=" + this.contract.getId());
	}

	public void zoomToReview() {
		getSessionUI().setContract(this.contract);
		getSessionUI().setCustomer(this.contract.getCustomer());
		super.redirect("/pages/contractOverviewDownloadForm.jsf?contractId=" + this.contract.getContractGuid());
	}

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepareNew();
	}

	public void prepareNew() {
		customer = new Customer();
		customer.setNewCustomer(true);
		prepAddress();
		copyAddress = false;
	}

	/**
	 * Prepares new addresses for the customer
	 */
	public void prepAddress() {
		customer.setAddress(new Address(AddressTypeEnum.Address, customer));
		customer.setBillingAddress(new Address(AddressTypeEnum.BillingAddress, customer));
		customer.setDeliveryAddress(new Address(AddressTypeEnum.DeliveryAddress, customer));
	}

	/**
	 * Complete method for autocomplete element
	 * 
	 * @param desc
	 * @return List<Customer>
	 */
	public List<Customer> complete(String desc) {
		List<Customer> l = null;
		try {
			l = customerService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	/**
	 * Item select event for the Auto complete
	 * 
	 * @param event
	 */
	public void onItemSelect(SelectEvent event) {
		try {
			if (event.getObject() instanceof Customer) {
				if (customer.getId() == -1l) {
					prepareNew();
					customer.setId(-1l);
				} else {
					customer.setNewCustomer(false);
					customerService.customerAddressDetail(customer);
				}
			}
			prepareContractsForCustomerSelected();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Post code lookup function to autocomplete the rest of the address details
	 * 
	 * @param inx
	 */
	public void findByPostcode(int inx) {
		try {
			switch (inx) {
			case 1:
				AddressLookupService.findAddress(customer.getAddress());
				break;
			case 2:
				AddressLookupService.findAddress(customer.getDeliveryAddress());
				break;
			case 3:
				AddressLookupService.findAddress(customer.getBillingAddress());
				break;
			default:
				break;
			}

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void cpyAddresses() {
		try {
			if (this.copyAddress) {
				if (customer.getBillingAddress().getId() != null)
					addressService.copyAddress(customer.getBillingAddress(), customer.getAddress(), AddressTypeEnum.BillingAddress);
				else
					customer.setBillingAddress(new Address(customer.getAddress(), AddressTypeEnum.BillingAddress));

				if (customer.getDeliveryAddress().getId() != null)
					addressService.copyAddress(customer.getDeliveryAddress(), customer.getAddress(), AddressTypeEnum.DeliveryAddress);
				else
					customer.setDeliveryAddress(new Address(customer.getAddress(), AddressTypeEnum.DeliveryAddress));
			} else {
				if (customer.getBillingAddress().getId() == null)
					customer.setBillingAddress(new Address(AddressTypeEnum.BillingAddress, customer));

				if (customer.getDeliveryAddress().getId() == null)
					customer.setDeliveryAddress(new Address(AddressTypeEnum.DeliveryAddress, customer));
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void submit() {
		try {
			customerService.saveCustomer(customer);
			addInfoMessage("New Customer Created");
			RequestContext.getCurrentInstance().update("contractInsForm:custumerGrwl");
			RequestContext.getCurrentInstance().update("contractInsForm");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void enquiryDirect() {
		try {
			customerService.saveCustomer(customer);
			super.getSessionUI().setCustomer(customer);
			getSessionUI().setContract(null);
			super.ajaxRedirect("/pages/enquiryForm.jsf");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	// re-writing 
	public void onTabChanges(TabChangeEvent event) {
		try {
			if (event.getTab().getId().equals("esTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.NewEnquiry));
			}
			if (event.getTab().getId().equals("qsTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.QuoteInProgress));
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.QuoteSentCustomer));
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.SubmitForQuotation));
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.QuoteReject));
			}
			if (event.getTab().getId().equals("inProgTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.ContractInProgress));
			}
			if (event.getTab().getId().equals("completedTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.Completed));
			}
			if (event.getTab().getId().equals("closedTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.Closed));
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void onTabChange(TabChangeEvent event) {
		try {
			this.contractList = new ArrayList<>();
			
			switch (event.getTab().getTitle()) {
			case "Enquiry Started":
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.NewEnquiry));
				break;
				
			case "Quote Started":
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.QuoteInProgress));
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.QuoteSentCustomer));
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.SubmitForQuotation));
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.QuoteReject));		
				break;
				
			case "In Progress":
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.ContractInProgress));
				break;
				
			case "Completed":
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.ContractInProgress));
				break;
				
			case "Closed":
				this.contractList.addAll(contractService.findByCustomerContractStatus(this.customer, ContractStatusEnum.Closed));
				break;

			default:
				super.addWarningMessage("Cannot locate specified status; please contact support");
				break;
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean isCopyAddress() {
		return copyAddress;
	}

	public void setCopyAddress(boolean copyAddress) {
		this.copyAddress = copyAddress;
	}

	public List<Contract> getContractfilteredList() {
		return contractfilteredList;
	}

	public void setContractfilteredList(List<Contract> contractfilteredList) {
		this.contractfilteredList = contractfilteredList;
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

}
