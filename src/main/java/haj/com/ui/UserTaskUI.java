package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Contract;
import haj.com.entity.InspectionReport;
import haj.com.entity.Quote;
import haj.com.entity.RouteCard;
import haj.com.entity.TaskUsers;
import haj.com.entity.Tasks;
import haj.com.entity.enums.TaskStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.InspectionReportService;
import haj.com.service.QuoteService;
import haj.com.service.RouteCardService;
import haj.com.service.TaskUsersService;
import haj.com.service.TasksService;

@ManagedBean(name = "userTaskUI")
@ViewScoped
public class UserTaskUI extends AbstractUI {

	// Entity Layers
	private Tasks task = new Tasks();
	private TaskUsers taskUsers = new TaskUsers();
	private Contract contract;

	// Service layers
	private TasksService taskService = new TasksService();
	private TaskUsersService taskUsersService = new TaskUsersService();
	private ContractService contractService = new ContractService();

	// Lists
	private List<Tasks> taskList = new ArrayList<Tasks>();
	private List<Tasks> taskFilteredList = new ArrayList<Tasks>();
	private List<Tasks> taskByStatusList = new ArrayList<Tasks>();
	private List<Tasks> taskByStatusFilteredList = new ArrayList<Tasks>();

	// Enums
	private TaskStatusEnum taskStatusSelected;

	// Boolean
	private Boolean displayPreviousTasks;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		task = new Tasks();
		taskUsers = new TaskUsers();
		displayPreviousTasks = false;
		populateUsersTasksList();
	}

	/**
	 * Locates all tasks assigned to session user
	 * 
	 * @throws Exception
	 */
	private void populateUsersTasksList() throws Exception {
		taskList = new ArrayList<>();
		// locates all incomplete tasks
		taskList = taskService.findTasksByUserIncomplete(getSessionUI().getUser());
	}

	/**
	 * Directs the user to the page the task is for
	 */
	public void taskRedirect() {
		try {
			if (this.task != null && this.task.getId() != null) {
				if (task.getStartDate() == null) {
					task.setStartDate(new Date());
				}
				if (task.getTaskStatus() == TaskStatusEnum.NotStarted) {
					task.setTaskStatus(TaskStatusEnum.Underway);
				}
				task.setActionDate(new Date());
				task.setActionUser(getSessionUI().getUser());
				taskService.update(task);
				directUsers();
			} else {
				throw new Exception("Unable to locate task, please contact support!");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Check for directing users to specific pages url direct now added to task
	 * (task create)
	 * 
	 * @throws Exception
	 */
	private void directUsers() throws Exception {
		if (this.task.getTargetClass().equals("haj.com.entity.Contract")) {
			contract = new Contract();
			contract = contractService.findByKey(this.task.getTargetKey());
			getSessionUI().setContract(contract);
			getSessionUI().setCustomer(contract.getCustomer());
			super.redirect(this.task.getTaskDirectPage());
		}

		if (this.task.getTargetClass().equals(new Quote().getClass().getName())) {
			getSessionUI().setContract((new QuoteService().findByKey(this.task.getTargetKey())).getContract());
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			super.redirect(this.task.getTaskDirectPage());
		}

		if (this.task.getTargetClass().equals(new RouteCard().getClass().getName())) {
			getSessionUI().setContract((new RouteCardService().findByKey(this.task.getTargetKey())).getContract());
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			super.redirect(this.task.getTaskDirectPage());
		}

		if (this.task.getTargetClass().equals(new InspectionReport().getClass().getName())) {
			getSessionUI().setContract((new InspectionReportService().findByKey(this.task.getTargetKey())).getContract());
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			super.redirect(this.task.getTaskDirectPage());
		}
	}

	/**
	 * When status selected runs this method
	 */
	public void onStatusSelect() {
		try {
			populateTaskStatusList();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void closeTask() {
		try {
			taskService.closeTask(this.task, getSessionUI().getUser());
			this.task = new Tasks();
			populateUsersTasksList();
			addInfoMessage("Task closed");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reopenTask() {
		try {
			taskService.openTask(this.task, getSessionUI().getUser());
			this.task = new Tasks();
			populateUsersTasksList();
			addInfoMessage("Task closed");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Displays tasks to be search via status
	 */
	public void displayPreviousTasksSection() {
		try {
			displayPreviousTasks = true;
			taskStatusSelected = TaskStatusEnum.All;
			populateTaskStatusList();
			runClientSideUpdate("taskStatusTable");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Hides tasks to be search via status
	 */
	public void hidePreviousTasksSection() {
		try {
			displayPreviousTasks = false;
			taskByStatusList = new ArrayList<>();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Populates previous tasks list by status selected case statement
	 */
	private void populateTaskStatusList() throws Exception {

		taskByStatusList = new ArrayList<>();
		taskByStatusFilteredList = new ArrayList<>();
		switch (taskStatusSelected) {
		case All:
			taskByStatusList = taskService.findTasksByUser(getSessionUI().getUser());
			break;
		case Closed:
			taskByStatusList = taskService.findTasksByUserAndStatus(getSessionUI().getUser(), TaskStatusEnum.Closed);
			break;
		case Completed:
			taskByStatusList = taskService.findTasksByUserAndStatus(getSessionUI().getUser(), TaskStatusEnum.Completed);
			break;
		case NotStarted:
			taskByStatusList = taskService.findTasksByUserAndStatus(getSessionUI().getUser(), TaskStatusEnum.NotStarted);
			break;
		case Overdue:
			taskByStatusList = taskService.findTasksByUserAndStatus(getSessionUI().getUser(), TaskStatusEnum.Overdue);
			break;
		case Underway:
			taskByStatusList = taskService.findTasksByUserAndStatus(getSessionUI().getUser(), TaskStatusEnum.Underway);
			break;
		default:
			throw new Exception("Unable to locate status please contact support!");
		}
	}

	public Tasks getTask() {
		return task;
	}

	public void setTask(Tasks task) {
		this.task = task;
	}

	public TaskUsers getTaskUsers() {
		return taskUsers;
	}

	public void setTaskUsers(TaskUsers taskUsers) {
		this.taskUsers = taskUsers;
	}

	public List<Tasks> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Tasks> taskList) {
		this.taskList = taskList;
	}

	public List<Tasks> getTaskFilteredList() {
		return taskFilteredList;
	}

	public void setTaskFilteredList(List<Tasks> taskFilteredList) {
		this.taskFilteredList = taskFilteredList;
	}

	public TaskStatusEnum getTaskStatusSelected() {
		return taskStatusSelected;
	}

	public void setTaskStatusSelected(TaskStatusEnum taskStatusSelected) {
		this.taskStatusSelected = taskStatusSelected;
	}

	public Boolean getDisplayPreviousTasks() {
		return displayPreviousTasks;
	}

	public void setDisplayPreviousTasks(Boolean displayPreviousTasks) {
		this.displayPreviousTasks = displayPreviousTasks;
	}

	public List<Tasks> getTaskByStatusFilteredList() {
		return taskByStatusFilteredList;
	}

	public void setTaskByStatusFilteredList(List<Tasks> taskByStatusFilteredList) {
		this.taskByStatusFilteredList = taskByStatusFilteredList;
	}

	public List<Tasks> getTaskByStatusList() {
		return taskByStatusList;
	}

	public void setTaskByStatusList(List<Tasks> taskByStatusList) {
		this.taskByStatusList = taskByStatusList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

}
