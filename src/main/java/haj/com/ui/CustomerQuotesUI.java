package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.jfree.data.gantt.Task;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

import haj.com.entity.Address;
import haj.com.entity.Contract;
import haj.com.entity.Customer;
import haj.com.entity.Doc;
import haj.com.entity.enums.AddressTypeEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.AddressLookupService;
import haj.com.service.AddressService;
import haj.com.service.ContractService;
import haj.com.service.CustomerService;
import haj.com.service.DocService;
import haj.com.service.TasksService;

@ManagedBean(name = "customerQuotesUI")
@ViewScoped
public class CustomerQuotesUI extends AbstractUI {

	private CustomerService customerService = new CustomerService();
	private AddressService addressService = new AddressService();
	private Customer customer = null;
	private boolean copyAddress;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		getSessionUI().setContract(null);
		getSessionUI().setCustomer(null);
		prepareNew();
	}

	public void prepareNew() {
		customer = new Customer();
		customer.setNewCustomer(true);
		prepAddress();
		copyAddress = false;
	}

	/**
	 * Prepares new addresses for the customer
	 */
	public void prepAddress() {
		customer.setAddress(new Address(AddressTypeEnum.Address, customer));
		customer.setBillingAddress(new Address(AddressTypeEnum.BillingAddress, customer));
		customer.setDeliveryAddress(new Address(AddressTypeEnum.DeliveryAddress, customer));
	}

	public List<Customer> complete(String desc) {
		List<Customer> l = null;
		try {
			l = customerService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public void onItemSelect(SelectEvent event) {
		try {
			if (event.getObject() instanceof Customer) {
				if (customer.getId() == -1l) {
					prepareNew();
					customer.setId(-1l);
				} else {
					customer.setNewCustomer(false);
					customerService.customerAddressDetail(customer);
				}
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void findByPostcode(int inx) {
		try {
			switch (inx) {
			case 1:
				AddressLookupService.findAddress(customer.getAddress());
				break;
			case 2:
				AddressLookupService.findAddress(customer.getDeliveryAddress());
				break;
			case 3:
				AddressLookupService.findAddress(customer.getBillingAddress());
				break;
			default:
				break;
			}

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void cpyAddresses() {
		try {
			if (this.copyAddress) {
				if (customer.getBillingAddress().getId() != null)
					addressService.copyAddress(customer.getBillingAddress(), customer.getAddress(), AddressTypeEnum.BillingAddress);
				else
					customer.setBillingAddress(new Address(customer.getAddress(), AddressTypeEnum.BillingAddress));

				if (customer.getDeliveryAddress().getId() != null)
					addressService.copyAddress(customer.getDeliveryAddress(), customer.getAddress(), AddressTypeEnum.DeliveryAddress);
				else
					customer.setDeliveryAddress(new Address(customer.getAddress(), AddressTypeEnum.DeliveryAddress));
			} else {
				if (customer.getBillingAddress().getId() == null)
					customer.setBillingAddress(new Address(AddressTypeEnum.BillingAddress, customer));
				if (customer.getDeliveryAddress().getId() == null)
					customer.setDeliveryAddress(new Address(AddressTypeEnum.DeliveryAddress, customer));
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void submit() {
		try {
			customerService.saveCustomer(customer);
			super.getSessionUI().setCustomer(customer);
			super.ajaxRedirect("/pages/quoteCustomerTableForm.jsf");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean isCopyAddress() {
		return copyAddress;
	}

	public void setCopyAddress(boolean copyAddress) {
		this.copyAddress = copyAddress;
	}

}
