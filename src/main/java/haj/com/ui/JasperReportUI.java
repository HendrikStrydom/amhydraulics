package haj.com.ui;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import haj.com.entity.Contract;
import haj.com.entity.Quote;
import haj.com.framework.AbstractUI;
import haj.com.service.JasperService;

@ManagedBean(name = "jasperReportUI")
@SessionScoped
public class JasperReportUI extends AbstractUI {

	private JasperService jasperService = new JasperService();

	@SuppressWarnings("unused")
	private void runInit() throws Exception {

	}

	public void quoteDownloadPDFfromObject(Date reportDate, Contract contractSelected) {
		jasperReportDownloadPDFfromObject(reportDate, contractSelected);
	}

	private void jasperReportDownloadPDFfromObject(Date reportDate, Contract contractSelected) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("enquiryNum", contractSelected.getEnquiryNumber());
		params.put("contractNum", contractSelected.getContractNumber());
		params.put("customer", contractSelected.getCustomer().getCustomerName());
		// params.put("orderNum", contractSelected.getOrderNumber());
		params.put("description", contractSelected.getTitle());
		params.put("drawingNum", "DN 800649");
		params.put("itemNo", "IN 8616");
		params.put("qty", "10");
		params.put("testRequired", true);
		params.put("conformityRequired", true);
		params.put("testPressure", true);
		params.put("orderPressure", true);
		params.put("verbalPressure", true);
		params.put("hardStamped", true);
		params.put("1500psi", true);
		params.put("100psi", true);
		params.put("previousContract", true);
		String fname = "QuoteAMHydraulics.pdf";
		try {
			jasperService.createReportFromDBtoServletOutputStream("amhyrdaQuote.jasper", params, fname);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void quoteDownloadPDFfromSession(Date fromDate, Date toDate) {
		jasperReportDownloadPDFfromSession(fromDate, toDate);
	}

	/**
	 * jasper report change details and put parameters
	 */
	private void jasperReportDownloadPDFfromSession(Date fromDate, Date toDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("enquiryNum", getSessionUI().getContract().getEnquiryNumber());
		params.put("contractNum", getSessionUI().getContract().getContractNumber());
		params.put("customer", getSessionUI().getCustomer().getCustomerName());
		// params.put("orderNum",
		// getSessionUI().getContract().getOrderNumber());
		params.put("description", getSessionUI().getContract().getTitle());
		params.put("drawingNum", "DN 800649");
		params.put("itemNo", "IN 8616");
		params.put("qty", "10");
		params.put("testRequired", true);
		params.put("conformityRequired", true);
		params.put("testPressure", true);
		params.put("orderPressure", true);
		params.put("verbalPressure", true);
		params.put("hardStamped", true);
		params.put("1500psi", true);
		params.put("100psi", true);
		params.put("previousContract", true);
		String fname = "QuoteAMHydraulics.pdf";
		try {
			jasperService.createReportFromDBtoServletOutputStream("amhyrdaQuote.jasper", params, fname);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void jasperReportQuoteDownload(Quote quote) {
		// $P{quoteId}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("quoteId", quote.getId());
		String fname = quote.getQuoteNumber() + ".pdf";
		try {
			jasperService.createReportFromDBtoServletOutputStream("quote.jasper", params, fname);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
