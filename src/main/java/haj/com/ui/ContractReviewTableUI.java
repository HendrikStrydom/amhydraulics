package haj.com.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.TabChangeEvent;

import haj.com.entity.Contract;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractReviewItemService;
import haj.com.service.ContractService;
import haj.com.service.JasperService;

@ManagedBean(name = "contractreviewTableUI")
@ViewScoped
public class ContractReviewTableUI extends AbstractUI {

	private List<ContractReviewItem> criList = new ArrayList<ContractReviewItem>();
	private ContractReviewItem cri = new ContractReviewItem();
	private ContractReviewItemService criService = new ContractReviewItemService();

	private ContractService contractService = new ContractService();
	private List<Contract> contractfilteredList;
	private List<Contract> contractList;
	private Contract contract;

	private JasperService jasperService = new JasperService();

	private Map<String, Object> queryParameters = new Hashtable<String, Object>();

	private String ceNumber;
	private String orderNumberCriteria;
	private String descriptionCriteria;

	private Double totalPrice;

	public void findContractReviewItems() {
		try {
			this.criList = new ArrayList<ContractReviewItem>();
			this.criList = criService.findByContract(this.contract.getId());
			reviewContract();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void reviewContract() {
		totalPrice = 0.00;
		for (ContractReviewItem contractReviewItem : this.criList) {
			this.totalPrice += contractReviewItem.getPrice();
		}
	}

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.contractList = new ArrayList<>();
		this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.NewEnquiry));
	}

	/***
	 * Here is where the Map<String, Object> gets built with its relevant
	 * information
	 */
	public void searchFor() {
		try {
			if (this.ceNumber != null && !this.ceNumber.isEmpty())
				queryParameters.put("ConEnqNumber", "%" + ceNumber + "%");
			if (this.descriptionCriteria != null && !this.descriptionCriteria.isEmpty())
				queryParameters.put("details", "%" + this.descriptionCriteria + "%");
			if (this.orderNumberCriteria != null && !this.orderNumberCriteria.isEmpty())
				queryParameters.put("orderNumber", "%" + this.orderNumberCriteria + "%");
			this.contractList = contractService.findContractByQueryParameter(queryParameters);
			if (this.contractList.size() > 0)
				addInfoMessage("Search results found: " + this.contractList.size());
			else
				addWarningMessage("No results found");

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/***
	 * Clears search criterias
	 */
	public void clearSearch() {
		this.ceNumber = null;
		this.orderNumberCriteria = null;
		this.descriptionCriteria = null;
		queryParameters = new Hashtable<String, Object>();
	}

	public void clearContract() {
		this.contract = new Contract();
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = new ArrayList<Contract>();
		try {
			l = contractService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	/**
	 * this zooms into the quoteForm.jsf to view/edit selected quote from data table
	 */
	public void zoomTo() {
		getSessionUI().setContract(this.contract);
		getSessionUI().setCustomer(this.contract.getCustomer());
		super.redirect("/pages/contractOverviewDownloadForm.jsf?contractId=" + this.contract.getContractGuid());

	}

	public void onTabChange(TabChangeEvent event) {
		try {
			if (event.getTab().getId().equals("esTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.NewEnquiry));
			}
			if (event.getTab().getId().equals("qsTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.QuoteInProgress));
				this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.QuoteSentCustomer));
				this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.SubmitForQuotation));
				this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.QuoteReject));
			}
			if (event.getTab().getId().equals("inProgTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.ContractInProgress));
			}
			if (event.getTab().getId().equals("completedTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.Completed));
			}
			if (event.getTab().getId().equals("closedTab")) {
				this.contractList = new ArrayList<>();
				this.contractList.addAll(contractService.findByContractStatus(ContractStatusEnum.Closed));
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void downloadDoc() {
		try {
			jasperService.jasperReportDownloadPDF(this.contract);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * This will set contract into the session and redirect to quoteForm.jsf
	 */
	public void createQuoteWithContract() {
		super.redirect("/pages/contractReviewForm.jsf?contractId=" + this.contract.getId());
	}

	/**
	 * Getter and Setters
	 * 
	 * @return
	 */
	public List<ContractReviewItem> getCriList() {
		return criList;
	}

	public void setCriList(List<ContractReviewItem> criList) {
		this.criList = criList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getCeNumber() {
		return ceNumber;
	}

	public void setCeNumber(String ceNumber) {
		this.ceNumber = ceNumber;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public List<Contract> getContractfilteredList() {
		return contractfilteredList;
	}

	public void setContractfilteredList(List<Contract> contractfilteredList) {
		this.contractfilteredList = contractfilteredList;
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public String getOrderNumberCriteria() {
		return orderNumberCriteria;
	}

	public void setOrderNumberCriteria(String orderNumberCriteria) {
		this.orderNumberCriteria = orderNumberCriteria;
	}

	public String getDescriptionCriteria() {
		return descriptionCriteria;
	}

	public void setDescriptionCriteria(String descriptionCriteria) {
		this.descriptionCriteria = descriptionCriteria;
	}

}
