package  haj.com.ui;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.framework.AbstractUI;



@ManagedBean(name = "toggleUI")
@ViewScoped
public class ToggleUI extends AbstractUI {
	
	//hide cards
		private Boolean hideCustomerDetails = false;
		private Boolean hideEnquiryDetails = false;
		private Boolean hideResourceDetails = false;
		private Boolean hideMaterialTable = false;
		
		private Boolean hideAcceptedQuote = false;
		private Boolean hideRejectionReason = false;
		private Boolean hideItemControl = false;

	

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
	
		
	}

	public Boolean getHideCustomerDetails() {
		return hideCustomerDetails;
	}

	public void setHideCustomerDetails(Boolean hideCustomerDetails) {
		this.hideCustomerDetails = hideCustomerDetails;
	}

	public Boolean getHideEnquiryDetails() {
		return hideEnquiryDetails;
	}

	public void setHideEnquiryDetails(Boolean hideEnquiryDetails) {
		this.hideEnquiryDetails = hideEnquiryDetails;
	}

	public Boolean getHideResourceDetails() {
		return hideResourceDetails;
	}

	public void setHideResourceDetails(Boolean hideResourceDetails) {
		this.hideResourceDetails = hideResourceDetails;
	}

	public Boolean getHideMaterialTable() {
		return hideMaterialTable;
	}

	public void setHideMaterialTable(Boolean hideMaterialTable) {
		this.hideMaterialTable = hideMaterialTable;
	}

	public Boolean getHideAcceptedQuote() {
		return hideAcceptedQuote;
	}

	public void setHideAcceptedQuote(Boolean hideAcceptedQuote) {
		this.hideAcceptedQuote = hideAcceptedQuote;
	}

	public Boolean getHideRejectionReason() {
		return hideRejectionReason;
	}

	public void setHideRejectionReason(Boolean hideRejectionReason) {
		this.hideRejectionReason = hideRejectionReason;
	}

	public Boolean getHideItemControl() {
		return hideItemControl;
	}

	public void setHideItemControl(Boolean hideItemControl) {
		this.hideItemControl = hideItemControl;
	}

	
}
