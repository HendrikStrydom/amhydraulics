package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;

import com.techfinium.entity.Resource;
import com.techfinium.service.ResourceService;

import haj.com.entity.Contract;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.enums.ContractDetailTypeEnum;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.MaterialTypeEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractReviewItemService;
import haj.com.service.ContractService;
import haj.com.service.TasksService;

@ManagedBean(name = "criiUI")
@ViewScoped
public class ContractReviewItemInputUI extends AbstractUI {

	private static final long serialVersionUID = 1L;

	private ResourceService resourceService = new ResourceService();
	private ContractReviewItemService contractReviewItemService = new ContractReviewItemService();
	private TasksService tasksService = new TasksService();
	private ContractService contractService = new ContractService();

	private ContractReviewItem contractReviewItem;
	private Contract contract;

	private List<ContractReviewItem> contractReviewItemList;

	private Double totalPrice;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (super.getParameter("contractId", false) != null) {
			this.contract = (contractService.findByKey(Long.parseLong((String) super.getParameter("contractId", false))));
			getSessionUI().setCustomer(this.contract.getCustomer());
			getSessionUI().setContract(this.contract);
			// TODO get check for new or already existing one for Contract
			prepareNewContractReviewItem();
			prepareContractReviewItemList();
			//scrollToTop();
		} else {
			super.redirect("/pages/userTasks.jsf");
		}

	}

	public void reviewContract() {
		totalPrice = 0.00;
		for (ContractReviewItem contractReviewItem : contractReviewItemList) {
			this.totalPrice += contractReviewItem.getPrice();
		}
	}

	public void prepareNewContractReviewItem() {
		this.contractReviewItem = new ContractReviewItem();
		this.contractReviewItem.setContract(this.contract);
	}

	public void prepareContractReviewItemList() {
		try {
			this.contractReviewItemList = new ArrayList<ContractReviewItem>();
			this.contractReviewItemList = contractReviewItemService.findByContract(this.contract.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveContractReviewItem() {
		try {
			contractReviewItemService.create(this.contractReviewItem);
			prepareNewContractReviewItem();
			prepareContractReviewItemList();
			addInfoMessage("Review Item add/update successful");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void deleteContractReviewItem() {
		try {
			if (this.contractReviewItem.getId() != null) {
				/*
				 * this.contractReviewItem.setSoftDelete(true);
				 * contractReviewItemService.update(this.contractReviewItem);
				 */
				contractReviewItemService.delete(this.contractReviewItem);
				prepareNewContractReviewItem();
				prepareContractReviewItemList();
				addInfoMessage("Review Item delete successfull");
			} else {
				addWarningMessage("Detail could not be deleted");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void redirectToQuote() throws Exception {
		if (getSessionUI().getContract().getTask() != null) {
			tasksService.closeTask(tasksService.findByKey(this.contract.getTask().getId()), getSessionUI().getUser());
		}
		this.contract.setContractStatus(ContractStatusEnum.QuoteInProgress);
		//this.contract.setTask(tasksService.createContractTask(getSessionUI().getUser(), this.contract));
		contractService.update(this.contract);
		super.ajaxRedirect("/pages/quoteForm.jsf?contractId=" + this.contract.getId());
	}

	public void scrollToTop() {
		RequestContext.getCurrentInstance().scrollTo("contractReviewForm");

	}

	public List<SelectItem> getContractReviewDetailTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ContractDetailTypeEnum selectItem : ContractDetailTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getMaterialTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (MaterialTypeEnum selectItem : MaterialTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<Resource> completeResource(String desc) {
		List<Resource> l = null;
		try {
			l = resourceService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public ContractReviewItem getContractReviewItem() {
		return contractReviewItem;
	}

	public void setContractReviewItem(ContractReviewItem contractReviewItem) {
		this.contractReviewItem = contractReviewItem;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public List<ContractReviewItem> getContractReviewItemList() {
		return contractReviewItemList;
	}

	public void setContractReviewItemList(List<ContractReviewItem> contractReviewItemList) {
		this.contractReviewItemList = contractReviewItemList;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
}
