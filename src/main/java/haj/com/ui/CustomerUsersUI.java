package  haj.com.ui;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import haj.com.framework.AbstractUI;
import haj.com.entity.CustomerUsers;
import haj.com.service.CustomerUsersService;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@ManagedBean(name = "customerusersUI")
@ViewScoped
public class CustomerUsersUI extends AbstractUI {

	private CustomerUsersService service = new CustomerUsersService();
	private List<CustomerUsers> customerusersList = null;
	private List<CustomerUsers> customerusersfilteredList = null;
	private CustomerUsers customerusers = null;
	private LazyDataModel<CustomerUsers> dataModel; 

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all CustomerUsers and prepare a for a create of a new CustomerUsers
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @throws Exception
 	 */
	private void runInit() throws Exception {
		prepareNew();
		customerusersInfo();
	}

	/**
	 * Get all CustomerUsers for data table
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 */
	public void customerusersInfo() {
	 
			
			 dataModel = new LazyDataModel<CustomerUsers>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<CustomerUsers> retorno = new  ArrayList<CustomerUsers>();
			   
			   @Override 
			   public List<CustomerUsers> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					retorno = service.allCustomerUsers(CustomerUsers.class,first,pageSize,sortField,sortOrder,filters);
					dataModel.setRowCount(service.count(CustomerUsers.class,filters));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return retorno; 
			   }
			   
			    @Override
			    public Object getRowKey(CustomerUsers obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public CustomerUsers getRowData(String rowKey) {
			        for(CustomerUsers obj : retorno) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
			
	
	}

	/**
	 * Insert CustomerUsers into database
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 */
	public void customerusersInsert() {
		try {
			 service.create(this.customerusers);
			 prepareNew();
			 addInfoMessage(super.getEntryLanguage("update.successful"));
			 customerusersInfo();
		} catch (Exception e) {
		    super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Delete CustomerUsers from database
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 */
	public void customerusersDelete() {
		try {
			 service.delete(this.customerusers);
			  prepareNew();
			 customerusersInfo();
			 super.addWarningMessage(super.getEntryLanguage("row.deleted "));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<CustomerUsers> getCustomerUsersList() {
		return customerusersList;
	}
	public void setCustomerUsersList(List<CustomerUsers> customerusersList) {
		this.customerusersList = customerusersList;
	}
	public CustomerUsers getCustomerusers() {
		return customerusers;
	}
	public void setCustomerusers(CustomerUsers customerusers) {
		this.customerusers = customerusers;
	}


	/**
	 * Create new instance of CustomerUsers 
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 */
	public void prepareNew() {
		customerusers = new CustomerUsers();
	}


	/**
	 * Update CustomerUsers in database
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 */
    public void customerusersUpdate() {
		try {
			 service.update(this.customerusers);
			  addInfoMessage(super.getEntryLanguage("update.successful"));
			 customerusersInfo();
		} catch (Exception e) {
		   super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Prepare select one menu data
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 */	
    public List<SelectItem> getCustomerUsersDD() {
    	List<SelectItem> l =new ArrayList<SelectItem>();
    	l.add(new SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
    	customerusersInfo();
    	for (CustomerUsers ug : customerusersList) {
    		// l.add(new SelectItem(ug.getUserGroupId(), ug.getUserGroupDesc()));
		}
    	return l;
    }

    public List<CustomerUsers> getCustomerUsersfilteredList() {
		return customerusersfilteredList;
	}
	
	/**
	 * Prepare auto complete data
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 */	
	public void setCustomerUsersfilteredList(List<CustomerUsers> customerusersfilteredList) {
		this.customerusersfilteredList = customerusersfilteredList;
	}

		public List<CustomerUsers> complete(String desc) {
		List<CustomerUsers> l = null;
		try {
			l = service.findByName(desc);
		}
		catch (Exception e) {
		addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
	
	
	public LazyDataModel<CustomerUsers> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<CustomerUsers> dataModel) {
		this.dataModel = dataModel;
	}
	
}
