package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;

import haj.com.entity.Address;
import haj.com.entity.CollectDeliverContract;
import haj.com.entity.CollectDeliverItem;
import haj.com.entity.ConfigDoc;
import haj.com.entity.Contract;
import haj.com.entity.Doc;
import haj.com.entity.DocumentTracker;
import haj.com.entity.RouteCardItem;
import haj.com.entity.Users;
import haj.com.entity.enums.AddressTypeEnum;
import haj.com.entity.enums.CollectDeliverEnum;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.AddressService;
import haj.com.service.CollectDeliverContractService;
import haj.com.service.CollectDeliverItemService;
import haj.com.service.ConfigDocService;
import haj.com.service.ContractService;
import haj.com.service.DocService;
import haj.com.service.DocumentTrackerService;
import haj.com.service.JasperService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "collectDeliverUI")
@ViewScoped
public class CollectDeliverUI extends AbstractUI {

	private CollectDeliverItemService cdiService = new CollectDeliverItemService();
	private CollectDeliverContractService cdcService = new CollectDeliverContractService();
	private ContractService contractService = new ContractService();
	private DocService docService = new DocService();
	private DocumentTrackerService documentTrackerService = new DocumentTrackerService();
	private AddressService addressService = new AddressService();
	private JasperService jasperService = new JasperService();
	
	private CollectDeliverContract cdc;
	private CollectDeliverItem cdi;
	private Doc doc;

	private List<CollectDeliverItem> cdiList;
	private List<Doc> docs;
	private List<DocumentTracker> documentTrackers;

	private List<ConfigDoc> configDocList;
	private ConfigDocService configDocService = new ConfigDocService();
	private ConfigDoc configDoc;

	private Contract contract = new Contract();
	
	private Address address;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (super.getParameter("cdId", false) != null) {
			this.cdc = cdcService.findByKey(Long.parseLong((String) super.getParameter("cdId", false)));
			this.contract = this.cdc.getContract();
			prepareExistingCDCWithCDI();
			findDoc();
			getSessionUI().setContract(this.contract);
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
		} else if (super.getParameter("contractId", false) != null) {
			this.contract = contractService.findByGuid((String) super.getParameter("contractId", false));
			getSessionUI().setContract(this.contract);
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			prepareNewCDCWithCDI();
		} else {
			prepareNewCDCWithCDI();
		}
	}
	
	public void addCDI(){
		try {
			if (this.cdi.getId() == null)
			{
				this.cdiList.add(this.cdi);
				addInfoMessage("Correctly Added Item");
			}
			else{
				cdiService.createCDI(this.cdi);
				this.cdiList = cdiService.findByCollectionDeliveryContract(this.cdc.getId());
				addInfoMessage("Item has been updated");
			}
			this.cdi = new CollectDeliverItem();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

/*	public void removeCDI() {
		try {
			if (this.cdi.getId() != null) {
				this.docs = docService.findByCollectDeliverItem(cdi.getId());
				if (this.docs.isEmpty())
					cdiService.delete(this.cdi);
				else {
					this.cdi.setSoftDelete(true);
					cdiService.update(this.cdi);
				}
			}
			addInfoMessage("Item Removed");
			prepareExistingCDCWithCDI();
			this.docs = new ArrayList<Doc>();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}*/
	
	public void removeCDI() {
		try {
			if (this.cdi.getId() == null) {
				this.cdiList.removeIf(predCDI -> predCDI == this.cdi);
			} else {
				if (docService.findByCollectDeliverItem(cdi.getId()) == null || docService.findByCollectDeliverItem(cdi.getId()).isEmpty()) {
					cdiService.delete(this.cdi);
				} else {
					this.cdi.setSoftDelete(true);
					cdiService.update(this.cdi);
				}
				this.cdiList.removeIf(predCDI -> predCDI == this.cdi);
			}
			this.docs = new ArrayList<Doc>();
			clearCDI();
			addInfoMessage("Item Removed");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void updateFromList() {
	//	this.cdiList.remove(this.cdi);
		this.cdiList.removeIf(predCDI -> predCDI == this.cdi);
	}

	public void saveCDC() {
		try {
			cdcService.create(this.cdc, this.cdiList);
			addInfoMessage("Collection/Delivery Form Saved");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareExistingCDCWithCDI() {
		try {
			this.cdi = new CollectDeliverItem(getSessionUI().getUser());
			this.cdiList = new ArrayList<CollectDeliverItem>();
			this.cdiList = cdiService.findByCollectionDeliveryContract(this.cdc.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void clearCDI() {
		this.cdi = new CollectDeliverItem(getSessionUI().getUser());
	}

	public void prepareNewCDCWithCDI() {
		prepareCustomerAddress();
		this.cdc = new CollectDeliverContract(getSessionUI().getUser(), this.address);
		if (this.contract != null || this.contract.getId() != null)
			this.cdc.setContract(this.contract);
		this.cdi = new CollectDeliverItem(getSessionUI().getUser());
		this.cdiList = new ArrayList<CollectDeliverItem>();
	}
	
	public void prepareCustomerAddress() {
		try {
			this.address = new Address();
			this.address = addressService.findByCustAndType(this.contract.getCustomer().getId(), AddressTypeEnum.Address);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareNewCDI() {
		this.cdi = new CollectDeliverItem();
	}

	public void saveFile(FileUploadEvent event) {
		try {
			if (this.cdc.getId() == null)
				throw new Exception("Save form first");
			initDoc();
			docService.save(this.doc, event.getFile().getContents(), event.getFile().getFileName(),
					getSessionUI().getUser(), this.docs);
			addInfoMessage("Your file has been uploaded successfully!");
			initDoc();
			findDoc();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void initDoc() {
		this.doc = new Doc();
		this.doc.setUser(getSessionUI().getUser());
		this.doc.setConfigDoc(this.configDoc);
		if (this.cdi.getId() != null)
			this.doc.setCollectDeliverItem(this.cdi);
	}

	public void findDoc() {
		try {
			this.docs = new ArrayList<Doc>();
			if (this.cdi.getId() != null)
				this.docs = docService.findByCollectDeliverItem(cdi.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void download() {
		try {
			Users user = getSessionUI().getUser();
			DocumentTrackerService.create(user, doc, DocumentTrackerEnum.Downloaded);
			Faces.sendFile(doc.getFileContent(), GenericUtility.buidFileName(this.doc), true);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void showHistory() {
		try {
			this.documentTrackers = documentTrackerService.byRoot(this.doc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void clearCollectDeliverContract() {
		this.cdc = new CollectDeliverContract(getSessionUI().getUser());
		addInfoMessage("New Collection/Delivery Form");
	}

	public List<SelectItem> getCollectDeliverEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (CollectDeliverEnum c : CollectDeliverEnum.values()) {
			l.add(new SelectItem(c, c.getFriendlyName()));
		}
		return l;
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = new ArrayList<Contract>();
		try {
			l = contractService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public void prepareDocAndDocsForCDI() {
		prepConfigDoc();
		initDoc();
		findDoc();
	}

	private void prepConfigDoc() {
		try {
			configDoc = new ConfigDoc();
			configDocList = new ArrayList<ConfigDoc>();
			configDocList = configDocService.allConfigDocActive();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<ConfigDoc> completeConfigDoc(String desc) {
		List<ConfigDoc> l = new ArrayList<ConfigDoc>();
		try {
			l = new ConfigDocService().findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
	
	public void downloadDoc() {
		try {
			if(this.cdc.getId() != null)
				jasperService.jasperCollectionDeliveryReportDownloadPDF(this.cdc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		
	}

	public CollectDeliverContract getCdc() {
		return cdc;
	}

	public void setCdc(CollectDeliverContract cdc) {
		this.cdc = cdc;
	}

	public CollectDeliverItem getCdi() {
		return cdi;
	}

	public void setCdi(CollectDeliverItem cdi) {
		this.cdi = cdi;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public List<CollectDeliverItem> getCdiList() {
		return cdiList;
	}

	public CollectDeliverItemService getCdiService() {
		return cdiService;
	}

	public void setCdiService(CollectDeliverItemService cdiService) {
		this.cdiService = cdiService;
	}

	public CollectDeliverContractService getCdcService() {
		return cdcService;
	}

	public void setCdcService(CollectDeliverContractService cdcService) {
		this.cdcService = cdcService;
	}

	public void setCdiList(List<CollectDeliverItem> cdiList) {
		this.cdiList = cdiList;
	}

	public List<Doc> getDocs() {
		return docs;
	}

	public void setDocs(List<Doc> docs) {
		this.docs = docs;
	}

	public List<DocumentTracker> getDocumentTrackers() {
		return documentTrackers;
	}

	public void setDocumentTrackers(List<DocumentTracker> documentTrackers) {
		this.documentTrackers = documentTrackers;
	}

	public List<ConfigDoc> getConfigDocList() {
		return configDocList;
	}

	public void setConfigDocList(List<ConfigDoc> configDocList) {
		this.configDocList = configDocList;
	}

	public ConfigDoc getConfigDoc() {
		return configDoc;
	}

	public void setConfigDoc(ConfigDoc configDoc) {
		this.configDoc = configDoc;
	}
}
