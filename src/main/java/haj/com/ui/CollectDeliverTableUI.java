package haj.com.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.CollectDeliverContract;
import haj.com.entity.Contract;
import haj.com.entity.enums.CollectDeliverEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.CollectDeliverContractService;
import haj.com.service.ContractService;
import haj.com.service.JasperService;

@ManagedBean(name = "collectDeliverTableUI")
@ViewScoped
public class CollectDeliverTableUI extends AbstractUI {

	private CollectDeliverContractService collectDeliverContractService = new CollectDeliverContractService();
	private List<CollectDeliverContract> collectDeliverContractList;
	private List<CollectDeliverContract> collectDeliverContractFilterList;
	private CollectDeliverContract collectDeliverContract = null;

	private JasperService jasperService = new JasperService();
	private ContractService contractService = new ContractService();
	private Contract contract = new Contract();

	private String ceNumber, cdNumber, anyText;
	private CollectDeliverEnum cdEnum;

	private Map<String, Object> queryParameters = new Hashtable<String, Object>();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.collectDeliverContract = new CollectDeliverContract();
		prepareCollectDeliverContractInfo();
	}

	public void createCDWithContract() {
		if (this.contract != null)
			super.redirect("/pages/collectionDeliveryForm.jsf?contractId=" + this.contract.getContractGuid());
		else
			super.redirect("/pages/collectionDeliveryForm.jsf");
	}

	public void clearContract() {
		this.contract = new Contract();
	}

	public void prepareCollectDeliverContractInfo() {
		try {
			collectDeliverContractList = new ArrayList<>();
			collectDeliverContractFilterList = new ArrayList<>();
			collectDeliverContractList = collectDeliverContractService.allCollectDeliverContract();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = new ArrayList<Contract>();
		try {
			Contract c = new Contract();
			c.setId(-1l);
			c.setContractNumber("No Contract");
			l.add(c);
			l.addAll(contractService.findByName(desc));
			// l = contractService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<SelectItem> getCollectDeliverEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (CollectDeliverEnum c : CollectDeliverEnum.values()) {
			l.add(new SelectItem(c, c.getFriendlyName()));
		}
		return l;
	}

	public void zoomTo() {
		if (this.collectDeliverContract.getId() != null) {
			if (this.collectDeliverContract.getContract() != null) {
				getSessionUI().setContract(this.collectDeliverContract.getContract());
				getSessionUI().setCustomer(this.collectDeliverContract.getContract().getCustomer());
			}
			super.redirect("/pages/collectionDeliveryForm.jsf?cdId=" + this.collectDeliverContract.getId());
		} else
			super.redirect("/pages/collectionDeliveryForm.jsf");
	}
	
	public void downloadDoc() {
		try {
			if(this.collectDeliverContract.getId() != null);
				jasperService.jasperCollectionDeliveryReportDownloadPDF(this.collectDeliverContract);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		
	}

	public void clearSearch() {
		this.ceNumber = null;
		this.cdNumber = null;
		this.cdEnum = null;
		this.anyText = null;
		this.queryParameters = new Hashtable<String, Object>();
	}

	public void searchFor() {
		try {
			if (ceNumber != null && !ceNumber.isEmpty())
				queryParameters.put("ConEnqNumber", "%" + ceNumber + "%");

			if (cdNumber != null && !cdNumber.isEmpty())
				queryParameters.put("cdNumber", "%" + cdNumber + "%");

			if (anyText != null && !anyText.isEmpty())
				queryParameters.put("anyText", "%" + anyText + "%");

			if (cdEnum != null)
				queryParameters.put("cdEnum", cdEnum);

			this.collectDeliverContractList = collectDeliverContractService
					.findCollectDeliverContractByQueryParameter(queryParameters);
			if (this.collectDeliverContractList.size() > 0)
				addInfoMessage("Search results found: " + this.collectDeliverContractList.size());
			else
				addWarningMessage("No results found");
			clearSearch();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void clearCollectDeliverContract() {
		this.collectDeliverContract = new CollectDeliverContract(getSessionUI().getUser());
	}

	public CollectDeliverContract getCollectDeliverContract() {
		return collectDeliverContract;
	}

	public void setCollectDeliverContract(CollectDeliverContract collectDeliverContract) {
		this.collectDeliverContract = collectDeliverContract;
	}

	public List<CollectDeliverContract> getCollectDeliverContractList() {
		return collectDeliverContractList;
	}

	public void setCollectDeliverContractList(List<CollectDeliverContract> collectDeliverContractList) {
		this.collectDeliverContractList = collectDeliverContractList;
	}

	public List<CollectDeliverContract> getCollectDeliverContractFilterList() {
		return collectDeliverContractFilterList;
	}

	public void setCollectDeliverContractFilterList(List<CollectDeliverContract> collectDeliverContractFilterList) {
		this.collectDeliverContractFilterList = collectDeliverContractFilterList;
	}

	public String getCeNumber() {
		return ceNumber;
	}

	public void setCeNumber(String ceNumber) {
		this.ceNumber = ceNumber;
	}

	public String getCdNumber() {
		return cdNumber;
	}

	public void setCdNumber(String cdNumber) {
		this.cdNumber = cdNumber;
	}

	public CollectDeliverEnum getCdEnum() {
		return cdEnum;
	}

	public void setCdEnum(CollectDeliverEnum cdEnum) {
		this.cdEnum = cdEnum;
	}

	public String getAnyText() {
		return anyText;
	}

	public void setAnyText(String anyText) {
		this.anyText = anyText;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

}
