package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

import haj.com.entity.InspectionReport;
import haj.com.entity.InspectionReportItem;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.InspectionReportItemService;
import haj.com.service.InspectionReportService;
import haj.com.service.JasperService;
import haj.com.service.TasksService;

@ManagedBean(name = "inspectionReportFormUI")
@ViewScoped
public class InspectionReportFormUI extends AbstractUI {

	private InspectionReport inspectionReport;
	private InspectionReportItem inspectionReportItem;

	private List<InspectionReportItem> inspectionReportItemList;

	private InspectionReportService inspectionReportService = new InspectionReportService();
	private InspectionReportItemService inspectionReportItemService = new InspectionReportItemService();
	private ContractService contractService = new ContractService();
	private TasksService tasksService = new TasksService();

	private JasperService jasperService = new JasperService();

	public void downloadPDF() {
		try {
			jasperService.jasperDownloadPdfForStripInspectionReport(this.inspectionReport);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (super.getParameter("contractId", false) != null) {
			getSessionUI().setContract(contractService.findByKey(Long.parseLong((String) super.getParameter("contractId", false))));
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			prepareNewInspectionReport();
		} else if (super.getParameter("inspecId", false) != null) {
			this.inspectionReport = inspectionReportService.findByKey(Long.parseLong((String) super.getParameter("inspecId", false)));
			getSessionUI().setContract(this.inspectionReport.getContract());
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			this.inspectionReportItemList = new ArrayList<InspectionReportItem>();
			this.inspectionReportItemList = inspectionReportItemService.findByInspectionReport(this.inspectionReport.getId());
		}
	}

	public void prepareNewInspectionReport() throws Exception {
		this.inspectionReport = new InspectionReport();
		this.inspectionReport.setContract(getSessionUI().getContract());
		this.inspectionReport.setFitter(getSessionUI().getUser());
		this.inspectionReport.setContractType(getSessionUI().getContract().getContractType());
		this.inspectionReportItemList = new ArrayList<InspectionReportItem>();
		this.inspectionReportItemList = inspectionReportItemService.buidlInspectionReportItemList();
		super.addInfoMessage("New S/I created");
	}

	public void saveInspectionReport() {
		try {
			this.inspectionReport.setActionUser(getSessionUI().getUser());
			if (this.inspectionReport.getTask() != null) {
				tasksService.closeTask(tasksService.findByKey(this.inspectionReport.getTask().getId()), getSessionUI().getUser());
			}
			inspectionReportService.create(this.inspectionReport);
			this.inspectionReport.setTask(tasksService.createSITask(getSessionUI().getUser(), this.inspectionReport));
			inspectionReportService.update(this.inspectionReport);
			for (InspectionReportItem inspectionReportItem : inspectionReportItemList) {
				inspectionReportItem.setInspectionReport(this.inspectionReport);
				inspectionReportItemService.create(inspectionReportItem);
			}
			addInfoMessage("S/I " + this.inspectionReport.getFormNumber() + " has been Updated");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void completeInspectionReport() {
		try {
			if (this.inspectionReport.getTask() != null) {
				tasksService.closeTask(tasksService.findByKey(this.inspectionReport.getTask().getId()), getSessionUI().getUser());
			}
			this.inspectionReport.setActionUser(getSessionUI().getUser());
			this.inspectionReport.setCompletedReport(true);
			inspectionReportService.create(this.inspectionReport);
			for (InspectionReportItem inspectionReportItem : inspectionReportItemList) {
				inspectionReportItem.setInspectionReport(this.inspectionReport);
				inspectionReportItemService.create(inspectionReportItem);
			}
			if (getSessionUI().getContract().getTask() != null) {
				tasksService.closeTask(tasksService.findByKey(getSessionUI().getContract().getTask().getId()), getSessionUI().getUser());
			}
			addInfoMessage("S/I " + this.inspectionReport.getFormNumber() + " Marked Complete");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void closeInspectionReport() {
		try {
			if (this.inspectionReport.getTask() != null) {
				tasksService.closeTask(tasksService.findByKey(this.inspectionReport.getTask().getId()), getSessionUI().getUser());
			}
			this.inspectionReport.setActionUser(getSessionUI().getUser());
			this.inspectionReport.setCloseReport(true);
			inspectionReportService.create(this.inspectionReport);
			for (InspectionReportItem inspectionReportItem : inspectionReportItemList) {
				inspectionReportItem.setInspectionReport(this.inspectionReport);
				inspectionReportItemService.create(inspectionReportItem);
			}
			if (getSessionUI().getContract().getTask() != null) {
				tasksService.closeTask(tasksService.findByKey(getSessionUI().getContract().getTask().getId()), getSessionUI().getUser());
			}
			addInfoMessage("S/I " + this.inspectionReport.getFormNumber() + " Closed");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void redirectToInspectionReportTable() throws Exception {
		if ((inspectionReportService.findByContract(getSessionUI().getContract().getId())).size() > 0)
			super.redirect("/pages/inspectionReportTable.jsf?contractId=" + getSessionUI().getContract().getId());
		else
			super.addWarningMessage("No S/I have been created yet");
	}

	public void onRowEdit(RowEditEvent event) {
		addInfoMessage("Edit/Update on " + ((InspectionReportItem) event.getObject()).getComponentDescription());
	}

	public void onRowCancel(RowEditEvent event) {
		addWarningMessage("Cancel insert on " + ((InspectionReportItem) event.getObject()).getComponentDescription());
	}

	public List<InspectionReportItem> getInspectionReportItemList() {
		return inspectionReportItemList;
	}

	public void setInspectionReportItemList(List<InspectionReportItem> inspectionReportItemList) {
		this.inspectionReportItemList = inspectionReportItemList;
	}

	public InspectionReportItem getInspectionReportItem() {
		return inspectionReportItem;
	}

	public void setInspectionReportItem(InspectionReportItem inspectionReportItem) {
		this.inspectionReportItem = inspectionReportItem;
	}

	public InspectionReport getInspectionReport() {
		return inspectionReport;
	}

	public void setInspectionReport(InspectionReport inspectionReport) {
		this.inspectionReport = inspectionReport;
	}

}
