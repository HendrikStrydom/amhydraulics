package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import haj.com.constants.HAJConstants;
import haj.com.entity.Contract;
import haj.com.entity.Tasks;
import haj.com.entity.Users;
import haj.com.entity.enums.TaskStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.TasksService;
import haj.com.service.UsersService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "testUI")
@ViewScoped
public class TestUI extends AbstractUI {

	private Contract contract;
	private ContractService contractService = new ContractService();
	@ManagedProperty(value = "#{uploadDocUI}")
	private UploadDocUI uploadDocUI;

	private Tasks task;
	private TasksService tService = new TasksService();

	private UsersService uService = new UsersService();

	public void sendTaskTestMail() {
		try {
			List<Users> uList = new ArrayList<>();
			uList.add(uService.findByKey(2l));
			// this.task = new Tasks("A task descrip", uService.findByKey(2l),
			// 1l, TaskStatusEnum.NotStarted, new Date(), new Date(), new
			// Date(), UUID.randomUUID().toString());
			this.task.setId(1l);
			tService.sendMailTaskUsers(this.task, uList);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void sendAMailTest() {
		try {
			String msg;
			msg = "<p>Dear #NAME#,";

			GenericUtility.sendTaskMail("tgwilson35@gmail.com", msg, "Subjecto", "Nakme bra");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		try {
			this.contract = new Contract();
			this.contract = contractService.findByKey(1l);
			getSessionUI().setContract(this.contract);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void crap() throws Exception {
		uploadDocUI.runInit();
	}

	public UploadDocUI getUploadDocUI() {
		return uploadDocUI;
	}

	public void setUploadDocUI(UploadDocUI uploadDocUI) {
		this.uploadDocUI = uploadDocUI;
	}

}
