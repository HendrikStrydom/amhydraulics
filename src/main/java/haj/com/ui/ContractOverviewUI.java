package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.Contract;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.JasperService;

@ManagedBean(name = "contractOverviewUI")
@ViewScoped
public class ContractOverviewUI extends AbstractUI {

	private ContractService service = new ContractService();

	private List<Contract> newEnquiryfilteredList = null;
	private List<Contract> awaitingQuotationfilteredList = null;
	private List<Contract> submitToCustomerfilteredList = null;
	private Contract contractPrint;

	private LazyDataModel<Contract> newEnquiryDataModel;
	private LazyDataModel<Contract> awaitingQuotationDataModel;
	private LazyDataModel<Contract> submitToCustomerDataModel;

	private JasperService jasperService = new JasperService();
	@ManagedProperty(value = "#{jasperReportUI}")
	private JasperReportUI jasperReportUI;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		newEnquiryInfo();
		submitToCustomerInfo();
		awaitingQuotationInfo();
		contractPrint = new Contract();

	}

	public void newEnquiryInfo() {

		newEnquiryDataModel = new LazyDataModel<Contract>() {

			private static final long serialVersionUID = 1L;
			private List<Contract> retorno = new ArrayList<Contract>();

			@Override
			public List<Contract> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allContract(Contract.class, first, pageSize, sortField, sortOrder, filters, ContractStatusEnum.NewEnquiry);
					newEnquiryDataModel.setRowCount(service.count(Contract.class, filters, ContractStatusEnum.NewEnquiry));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Contract obj) {
				return obj.getId();
			}

			@Override
			public Contract getRowData(String rowKey) {
				for (Contract obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

		try {
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void submitToCustomerInfo() {

		submitToCustomerDataModel = new LazyDataModel<Contract>() {

			private static final long serialVersionUID = 1L;
			private List<Contract> retorno = new ArrayList<Contract>();

			@Override
			public List<Contract> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allContract(Contract.class, first, pageSize, sortField, sortOrder, filters, ContractStatusEnum.QuoteSentCustomer);
					submitToCustomerDataModel.setRowCount(service.count(Contract.class, filters, ContractStatusEnum.QuoteSentCustomer));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Contract obj) {
				return obj.getId();
			}

			@Override
			public Contract getRowData(String rowKey) {
				for (Contract obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

		try {
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void awaitingQuotationInfo() {

		awaitingQuotationDataModel = new LazyDataModel<Contract>() {

			private static final long serialVersionUID = 1L;
			private List<Contract> retorno = new ArrayList<Contract>();

			@Override
			public List<Contract> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allAwaitingContract(Contract.class, first, pageSize, sortField, sortOrder, filters);
					awaitingQuotationDataModel.setRowCount(service.countAwaiting(Contract.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Contract obj) {
				return obj.getId();
			}

			@Override
			public Contract getRowData(String rowKey) {
				for (Contract obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

		try {
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void projectsFromToDateDownload() {
		jasperReportDownload(new java.util.Date(), new java.util.Date());
	}

	public void projectsFromToDateDownload1() {
		jasperReportUI.quoteDownloadPDFfromObject(new java.util.Date(), contractPrint);
	}

	private void jasperReportDownload(Date fromDate, Date toDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("enquiryNum", contractPrint.getEnquiryNumber());
		params.put("contractNum", contractPrint.getContractNumber());
		params.put("customer", contractPrint.getCustomer().getCustomerName());
		//params.put("orderNum", contractPrint.getOrderNumber());
		params.put("description", contractPrint.getTitle());
		params.put("drawingNum", "DN 800649");
		params.put("itemNo", "IN 8616");
		params.put("qty", "10");
		params.put("testRequired", true);
		params.put("conformityRequired", true);
		params.put("testPressure", true);
		params.put("orderPressure", true);
		params.put("verbalPressure", true);
		params.put("hardStamped", true);
		params.put("1500psi", true);
		params.put("100psi", true);
		params.put("previousContract", true);
		String fname = "QuoteAMHydraulics.pdf";
		try {
			jasperService.createReportFromDBtoServletOutputStream("amhyrdaQuote.jasper", params, fname);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Contract> getNewEnquiryfilteredList() {
		return newEnquiryfilteredList;
	}

	public void setNewEnquiryfilteredList(List<Contract> newEnquiryfilteredList) {
		this.newEnquiryfilteredList = newEnquiryfilteredList;
	}

	public List<Contract> getAwaitingQuotationfilteredList() {
		return awaitingQuotationfilteredList;
	}

	public void setAwaitingQuotationfilteredList(List<Contract> awaitingQuotationfilteredList) {
		this.awaitingQuotationfilteredList = awaitingQuotationfilteredList;
	}

	public List<Contract> getSubmitToCustomerfilteredList() {
		return submitToCustomerfilteredList;
	}

	public void setSubmitToCustomerfilteredList(List<Contract> submitToCustomerfilteredList) {
		this.submitToCustomerfilteredList = submitToCustomerfilteredList;
	}

	public LazyDataModel<Contract> getNewEnquiryDataModel() {
		return newEnquiryDataModel;
	}

	public void setNewEnquiryDataModel(LazyDataModel<Contract> newEnquiryDataModel) {
		this.newEnquiryDataModel = newEnquiryDataModel;
	}

	public LazyDataModel<Contract> getAwaitingQuotationDataModel() {
		return awaitingQuotationDataModel;
	}

	public void setAwaitingQuotationDataModel(LazyDataModel<Contract> awaitingQuotationDataModel) {
		this.awaitingQuotationDataModel = awaitingQuotationDataModel;
	}

	public LazyDataModel<Contract> getSubmitToCustomerDataModel() {
		return submitToCustomerDataModel;
	}

	public void setSubmitToCustomerDataModel(LazyDataModel<Contract> submitToCustomerDataModel) {
		this.submitToCustomerDataModel = submitToCustomerDataModel;
	}

	public Contract getContractPrint() {
		return contractPrint;
	}

	public void setContractPrint(Contract contractPrint) {
		this.contractPrint = contractPrint;
	}

	public JasperReportUI getJasperReportUI() {
		return jasperReportUI;
	}

	public void setJasperReportUI(JasperReportUI jasperReportUI) {
		this.jasperReportUI = jasperReportUI;
	}

}
