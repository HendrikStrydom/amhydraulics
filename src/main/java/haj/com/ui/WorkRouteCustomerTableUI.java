package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import haj.com.entity.Contract;
import haj.com.entity.RouteCard;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.QuoteStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.QuoteService;
import haj.com.service.RouteCardService;
import haj.com.service.TasksService;

@ManagedBean(name = "wrctUI")
@ViewScoped
public class WorkRouteCustomerTableUI extends AbstractUI {

	private ContractService contractService = new ContractService();
	private QuoteService quoteService = new QuoteService();
	private RouteCardService routeCardService = new RouteCardService();
	private TasksService tasksService = new TasksService();

	private List<Contract> contractList;
	private List<RouteCard> routeCardList;

	private RouteCard routeCard;
	private Contract contract;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (getSessionUI().getContract() == null) {
			prepareContractList();
		} else {
			prepareContractList();
			this.contract = getSessionUI().getContract();
			prepareRouteCardList(getSessionUI().getContract().getId());
			if (routeCardList.size() <= 0) {
				this.contract = new Contract();
			}
		}
	}

	public void prepareContractList() {
		try {
			contractList = new ArrayList<Contract>();
			contract = new Contract();
			contractList = contractService.findByCustomerContractStatus(getSessionUI().getCustomer(), ContractStatusEnum.QuoteAccept);
			contractList = quoteService.findByContractStatusReturnListOfContracts(QuoteStatusEnum.QuoteAccept);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void prepareRouteCardList(Long contractId) {
		try {
			routeCardList = new ArrayList<RouteCard>();
			routeCard = new RouteCard();
			routeCardList = routeCardService.findByContract(contractId);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void viewRouteCard() {
		prepareRouteCardList(this.contract.getId());
		if (this.routeCardList.size() <= 0) {
			contract = new Contract();
			addWarningMessage("No Route Have Been Made");
			RequestContext.getCurrentInstance().update("quoteCustomerTableGrwl");
		}
	}

	public void createRouteCard() {
		getSessionUI().setContract(this.contract);
		super.ajaxRedirect("/pages/routeCard.jsf");
	}

	public void goToRouteCard() {
		getSessionUI().setContract(this.contract);
		super.redirect("/pages/routeCard.jsf?rcard=" + this.routeCard.getId());
	}

	public void deleteRouteCard() {
		try {
			this.routeCard.setSoftDelete(true);
			if (this.routeCard.getTask() != null)
				tasksService.closeTask(this.routeCard.getTask(), getSessionUI().getUser());
			routeCardService.update(this.routeCard);
			prepareRouteCardList(this.contract.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public List<RouteCard> getRouteCardList() {
		return routeCardList;
	}

	public void setRouteCardList(List<RouteCard> routeCardList) {
		this.routeCardList = routeCardList;
	}

	public RouteCard getRouteCard() {
		return routeCard;
	}

	public void setRouteCard(RouteCard routeCard) {
		this.routeCard = routeCard;
	}
}
