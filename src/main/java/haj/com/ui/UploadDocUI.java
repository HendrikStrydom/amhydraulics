package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

import haj.com.entity.ConfigDoc;
import haj.com.entity.Contract;
import haj.com.entity.Doc;
import haj.com.entity.DocumentTracker;
import haj.com.entity.Users;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.entity.enums.PurchaseOrderEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ConfigDocService;
import haj.com.service.DocService;
import haj.com.service.DocumentTrackerService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "uploadDocUI")
@SessionScoped
public class UploadDocUI extends AbstractUI {

	private Doc doc;
	private List<Doc> docs;
	private List<Doc> docList;
	private List<Doc> docsfiltered;
	private DocService docService = new DocService();
	private List<DocumentTracker> documentTrackers;
	private DocumentTrackerService documentTrackerService = new DocumentTrackerService();
	private List<ConfigDoc> configDocList;
	private ConfigDocService configDocService = new ConfigDocService();
	private ConfigDoc configDoc;
	private PurchaseOrderEnum[] purchaseOrderEnumList;

	public void runInit() throws Exception {
		initDoc();
		prepConfigDoc();
		prepareDocList();
	}

	public void buildStreamContent() {
		getSessionUI().setSelDoc(doc);
		try {
			DocumentTrackerService.create(getSessionUI().getUser(), doc, DocumentTrackerEnum.Viewed);
		} catch (Exception e) {
			logger.fatal(e);
		}
	}

	public void download() {
		try {
			Users user = getSessionUI().getUser();
			DocumentTrackerService.create(user, doc, DocumentTrackerEnum.Downloaded);
			Faces.sendFile(doc.getFileContent(), GenericUtility.buidFileName(doc), true);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveFile(FileUploadEvent event, Doc doc) {
		try {
			logger.info(getSessionUI().getContract().getId());
			logger.info(" Doc" +doc.getContract().getId());
			docService.save(doc, event.getFile().getContents(), event.getFile().getFileName(), getSessionUI().getUser(), this.docs);
			addInfoMessage("Your file has been uploaded successfully!");
			initDoc();
			findDoc(doc);
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareDocList() {
		try {
			this.docList = new ArrayList<Doc>();
			this.docList = docService.findByContract(getSessionUI().getContract());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveFile(FileUploadEvent event, Doc doc, Users user) {
		try {
			docService.save(doc, event.getFile().getContents(), event.getFile().getFileName(), user, this.docs);
			addInfoMessage("Your file has been uploaded successfully!");
			initDoc();
			findDoc(doc);
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveFile(FileUploadEvent event) {
		try {
			doc.setConfigDoc(configDoc);
			doc.setPurchaseOrderDate(new java.util.Date());
			docService.save(doc, event.getFile().getContents(), event.getFile().getFileName(), getSessionUI().getUser(), this.docs);
			addInfoMessage("Your file has been uploaded successfully!");
			initDoc();
			prepareDocList();
			findDoc(doc);
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void findDoc(Doc doc) {
		try {
			this.docs = new ArrayList<Doc>();
			this.docs = docService.search(doc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void findDocs() {
		try {
			this.docs = new ArrayList<Doc>();
			this.docs = docService.search(this.doc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void initDoc() {
		this.doc = new Doc();
		this.doc.setUser(getSessionUI().getUser());
		logger.info(getSessionUI().getContract().getId());
		if (getSessionUI().getContract() != null)
			this.doc.setContract(getSessionUI().getContract());
		
	}

	public void showHistory() {
		try {
			this.documentTrackers = documentTrackerService.byRoot(doc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void prepConfigDoc() {
		try {
			purchaseOrderEnumList = PurchaseOrderEnum.values(); 
			configDoc = new ConfigDoc();
			configDocList = new ArrayList<ConfigDoc>();
			configDocList = configDocService.allConfigDocActive();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<ConfigDoc> completeConfigDoc(String desc) {
		List<ConfigDoc> l = new ArrayList<ConfigDoc>();
		try {
			l = new ConfigDocService().findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<DocumentTracker> getDocumentTrackers() {
		return documentTrackers;
	}

	public void setDocumentTrackers(List<DocumentTracker> documentTrackers) {
		this.documentTrackers = documentTrackers;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public List<Doc> getDocs() {
		return docs;
	}

	public void setDocs(List<Doc> docs) {
		this.docs = docs;
	}

	public List<Doc> getDocsfiltered() {
		return docsfiltered;
	}

	public void setDocsfiltered(List<Doc> docsfiltered) {
		this.docsfiltered = docsfiltered;
	}

	public List<ConfigDoc> getConfigDocList() {
		return configDocList;
	}

	public void setConfigDocList(List<ConfigDoc> configDocList) {
		this.configDocList = configDocList;
	}

	public ConfigDoc getConfigDoc() {
		return configDoc;
	}

	public void setConfigDoc(ConfigDoc configDoc) {
		this.configDoc = configDoc;
	}

	public List<Doc> getDocList() {
		return docList;
	}

	public void setDocList(List<Doc> docList) {
		this.docList = docList;
	}

	public PurchaseOrderEnum[] getPurchaseOrderEnumList() {
		return purchaseOrderEnumList;
	}

	public void setPurchaseOrderEnumList(PurchaseOrderEnum[] purchaseOrderEnumList) {
		this.purchaseOrderEnumList = purchaseOrderEnumList;
	}

}
