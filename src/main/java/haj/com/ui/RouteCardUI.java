package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.techfinium.entity.Resource;
import com.techfinium.service.ResourceService;

import haj.com.entity.Contract;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.RouteCard;
import haj.com.entity.RouteCardItem;
import haj.com.entity.enums.ContractDetailTypeEnum;
import haj.com.entity.enums.MaterialTypeEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractReviewItemService;
import haj.com.service.ContractService;
import haj.com.service.JasperService;
import haj.com.service.RouteCardService;
import haj.com.service.TasksService;

@ManagedBean(name = "rcUI")
@ViewScoped
public class RouteCardUI extends AbstractUI {

	private RouteCard rc;
	private RouteCardItem rci;
	private List<RouteCardItem> rciList;
	private Contract contract;
	private RouteCardService rcService = new RouteCardService();
	private ResourceService resourceService = new ResourceService();
	private ContractService contractService = new ContractService();
	private JasperService jasperService = new JasperService();

	private ContractReviewItemService contractReviewItemService = new ContractReviewItemService();
	private ContractReviewItem contractReviewItem;
	private List<ContractReviewItem> contractReviewItemList;
	private Contract contractReview;
	private Double totalPriceContractReview;

	private ContractDetailTypeEnum cdtEnum;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (getSessionUI().getCustomer() == null)
			super.redirect("/pages/routeCardTable.jsf");

		if (super.getParameter("contractId", false) != null) {
			this.contract = (contractService
					.findByKey(Long.parseLong((String) super.getParameter("contractId", false))));
			getSessionUI().setContract(contractService.findByKey(this.contract.getId()));
			getSessionUI().setCustomer(this.contract.getCustomer());
			this.rc = new RouteCard();
			this.rciList = new ArrayList<RouteCardItem>();
			this.rci = new RouteCardItem();
			this.rc.setContract(this.contract);
			prepareContractReview();
		} else if (super.getParameter("rcardId", false) != null) {
			this.rc = new RouteCard();
			this.rciList = new ArrayList<RouteCardItem>();
			this.rci = new RouteCardItem();
			this.rc = rcService.findByKey(Long.parseLong((String) super.getParameter("rcardId", false)));
			getSessionUI().setContract(contractService.findByKey(this.rc.getContract().getId()));
			getSessionUI().setCustomer(getSessionUI().getContract().getCustomer());
			this.rciList = rcService.findRCIByRCId(this.rc.getId());
			prepareContractReview();
		}
	}

	public void clearRCI() {
		this.rci = new RouteCardItem();
		addInfoMessage("Item Cleared");
	}

	public void downloadDoc() {
		try {
			if (this.rc.getId() != null)
				// jasperService.jasperReportDownloadPDF(this.rc);
				jasperService.jasperDownloadPdfForRouteCard(this.rc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveRCAndRCIList() {
		try {
//			if (this.rc.getShopInstruction().isEmpty() || this.rc.getDeliveryDate() == null)
			if (this.rc.getMaterialSpec().isEmpty() || this.rc.getDeliveryDate() == null) 
				throw new Exception("Please Provide The Delivery Date And Shop Instructions");
			if (this.rciList.size() < 0)
				throw new Exception("No items add to Route Card");
			rcService.createRCandRCI(this.rc, this.rciList, getSessionUI().getUser(), false);
			this.rciList = new ArrayList<RouteCardItem>();
			this.rciList = rcService.findRCIByRCId(this.rc.getId());
			addInfoMessage("Route Card has been created");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void completeRCAndRCIList() {
		try {
			if (this.rc.getShopInstruction().isEmpty() || this.rc.getDeliveryDate() == null)
				throw new Exception("Please Provide The Delivery Date And Shop Instructions");
			if (this.rciList.size() < 0)
				throw new Exception("No items add to Route Card");
			rcService.createRCandRCI(this.rc, this.rciList, getSessionUI().getUser(), true);
			addInfoMessage("Route Card Completed");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void clearRC() {
		this.rc = new RouteCard();
		this.rciList = new ArrayList<RouteCardItem>();
		this.rci = new RouteCardItem();
		this.rc.setContract(getSessionUI().getContract());
		addInfoMessage("Formed Cleared");
	}

	public void addRCI() {
		try {
			if (this.rci.getId() == null) {
				this.rciList.add(this.rci);
				addInfoMessage("Route Card Item has been saved to a list");
			} else {
				rcService.createRCI(this.rci);
				this.rciList = rcService.findRCIByRCId(this.rc.getId());
				addInfoMessage("Route Card Item has been updated");

			}
			this.rci = new RouteCardItem();

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void updateFromList() {
		this.rciList.remove(this.rci);
	}

	public void removeRCI() {
		try {
			/*this.rciList.remove(this.rci);
			if (this.rci.getId() != null)
				rcService.deleteRCI(this.rci);
				*/
			this.rciList.removeIf(rc -> rc == this.rci);
			if (this.rci.getId() != null) {
				rcService.deleteRCI(this.rci);
			}
			this.rci = new RouteCardItem();
			addInfoMessage("Rout Card Item Removed");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void createRCI() {
		this.rciList.remove(this.rci);
	}

	public void prepareContractReview() {
		try {
			this.contractReviewItemList = new ArrayList<ContractReviewItem>();
			this.contractReviewItemList = contractReviewItemService
					.findByContract(getSessionUI().getContract().getId());
			this.contract = new Contract();
			this.contract = getSessionUI().getContract();
			prepareNewContractReviewItem();
			this.totalPriceContractReview = 0.00;
			calculateTotal();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void calculateTotal() {
		this.totalPriceContractReview = 0.00;
		for (ContractReviewItem contractReviewItem : contractReviewItemList) {
			if (contractReviewItem.getPrice() != null)
				this.totalPriceContractReview += contractReviewItem.getPrice();
		}
	}

	public void prepareContractReviewItemList() {
		try {
			this.contractReviewItemList = new ArrayList<ContractReviewItem>();
			this.contractReviewItemList = contractReviewItemService.findByContract(this.contract.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveContractReviewItem() {
		try {
			contractReviewItemService.create(this.contractReviewItem);
			prepareNewContractReviewItem();
			prepareContractReviewItemList();
			calculateTotal();
			addInfoMessage("Review Item add/update successful");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void deleteContractReviewItem() {
		try {
			if (this.contractReviewItem.getId() != null) {
				contractReviewItemService.delete(this.contractReviewItem);
				prepareNewContractReviewItem();
				prepareContractReviewItemList();
				calculateTotal();
				addInfoMessage("Review Item delete successfull");
			} else {
				addWarningMessage("Detail could not be deleted");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void prepareNewContractReviewItem() {
		this.contractReviewItem = new ContractReviewItem();
		this.contractReviewItem.setContract(this.contract);
	}

	public List<SelectItem> getContractReviewDetailTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ContractDetailTypeEnum selectItem : ContractDetailTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getMaterialTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (MaterialTypeEnum selectItem : MaterialTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<Resource> completeResource(String desc) {
		List<Resource> l = null;
		try {
			l = resourceService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public RouteCard getRc() {
		return rc;
	}

	public void setRc(RouteCard rc) {
		this.rc = rc;
	}

	public RouteCardItem getRci() {
		return rci;
	}

	public void setRci(RouteCardItem rci) {
		this.rci = rci;
	}

	public List<RouteCardItem> getRciList() {
		return rciList;
	}

	public void setRciList(List<RouteCardItem> rciList) {
		this.rciList = rciList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public ContractReviewItem getContractReviewItem() {
		return contractReviewItem;
	}

	public void setContractReviewItem(ContractReviewItem contractReviewItem) {
		this.contractReviewItem = contractReviewItem;
	}

	public List<ContractReviewItem> getContractReviewItemList() {
		return contractReviewItemList;
	}

	public void setContractReviewItemList(List<ContractReviewItem> contractReviewItemList) {
		this.contractReviewItemList = contractReviewItemList;
	}

	public Contract getContractReview() {
		return contractReview;
	}

	public void setContractReview(Contract contractReview) {
		this.contractReview = contractReview;
	}

	public Double getTotalPriceContractReview() {
		return totalPriceContractReview;
	}

	public void setTotalPriceContractReview(Double totalPriceContractReview) {
		this.totalPriceContractReview = totalPriceContractReview;
	}

	public ContractDetailTypeEnum getCdtEnum() {
		return cdtEnum;
	}

	public void setCdtEnum(ContractDetailTypeEnum cdtEnum) {
		this.cdtEnum = cdtEnum;
	}

}
