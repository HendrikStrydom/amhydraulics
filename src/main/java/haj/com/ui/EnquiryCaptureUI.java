
package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;

import haj.com.entity.ConfigDoc;
import haj.com.entity.Contract;
import haj.com.entity.Doc;
import haj.com.entity.Users;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.entity.enums.PurchaseOrderEnum;
import haj.com.entity.enums.RepairNewEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ConfigDocService;
import haj.com.service.ContractService;
import haj.com.service.DocService;
import haj.com.service.DocumentTrackerService;
import haj.com.service.TasksService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "enqcapUI")
@ViewScoped
public class EnquiryCaptureUI extends AbstractUI {

	private ContractService contractService = new ContractService();
	private TasksService tasksService = new TasksService();
	private DocService docService = new DocService();
	private ConfigDocService configDocService = new ConfigDocService();

	private Contract contract;

	private Doc doc;
	private List<Doc> docs;

	private ConfigDoc configDoc;
	private List<ConfigDoc> configDocList;

	private PurchaseOrderEnum[] purchaseOrderEnumList;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepNewContract();
		this.contract = contractService.findByKey(1);
		prepNewContract();
	}

	public void prepNewContract() {
		try {
			this.contract = new Contract();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveContract() {
		try {
			contractService.create(this.contract);
			addInfoMessage("Update successful for " + this.contract.getEnquiryNumber());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void createTaskAndSendEmailNoAjax() {
		try {
			if (this.contract.getId() != null && this.contract.getContractStatus() == ContractStatusEnum.NewEnquiry) {
				if (this.contract.getTask() != null) {
					tasksService.closeTask(tasksService.findByKey(this.contract.getTask().getId()), getSessionUI().getUser());
				}
				this.contract.setTask(tasksService.createTaskForNewContractByRepairNewEnum(getSessionUI().getUser(), this.contract));
				contractService.update(this.contract);
			} else
				addWarningMessage("Either no contract was been found OR this enquiry is in progess already.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void initDoc() {
		this.doc = new Doc();
		this.doc.setUser(getSessionUI().getUser());
	}

	public void prepareDocList() {
		try {
			this.docs = new ArrayList<Doc>();
			this.docs = docService.findByContract(getSessionUI().getContract());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void download() {
		try {
			Users user = getSessionUI().getUser();
			DocumentTrackerService.create(user, doc, DocumentTrackerEnum.Downloaded);
			Faces.sendFile(doc.getFileContent(), GenericUtility.buidFileName(doc), true);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveFile(FileUploadEvent event, Doc doc) {
		try {
			//docService.save(docs, event.getFile().getContents(), event.getFile().getFileName(), getSessionUI().getUser(), null);
			addInfoMessage("Your file has been uploaded successfully!");
			initDoc();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepConfigDoc() {
		try {
			purchaseOrderEnumList = PurchaseOrderEnum.values();
			configDoc = new ConfigDoc();
			configDocList = new ArrayList<ConfigDoc>();
			configDocList = configDocService.allConfigDocActive();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<ConfigDoc> completeConfigDoc(String desc) {
		List<ConfigDoc> l = new ArrayList<ConfigDoc>();
		try {
			l = new ConfigDocService().findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = null;
		try {
			if (getSessionUI().getCustomer() != null)
				l = contractService.findByCustomerByEnquiry(getSessionUI().getCustomer().getId(), desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

}