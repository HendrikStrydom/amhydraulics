package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;

import haj.com.entity.CollectDeliverContract;
import haj.com.entity.Contract;
import haj.com.entity.Doc;
import haj.com.entity.DocumentTracker;
import haj.com.entity.InspectionReport;
import haj.com.entity.PurchaseOrder;
import haj.com.entity.Quote;
import haj.com.entity.RouteCard;
import haj.com.entity.Users;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractOverviewDownloadService;
import haj.com.service.ContractService;
import haj.com.service.DocumentTrackerService;
import haj.com.service.JasperService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "codUI")
@ViewScoped
public class ContractOverviewDownloadUI extends AbstractUI {

	private static final long serialVersionUID = 1L;

	private JasperService jasperService = new JasperService();
	private ContractService contractService = new ContractService();
	private ContractOverviewDownloadService codService = new ContractOverviewDownloadService();

	private InspectionReport si;
	private Quote quote;
	private RouteCard rc;
	private PurchaseOrder po;
	private CollectDeliverContract cdc;
	private Doc doc;
	private Contract contract;

	private ContractStatusEnum[] constatus = ContractStatusEnum.values();
	private ContractStatusEnum status;

	private List<InspectionReport> siList = new ArrayList<InspectionReport>();
	private List<Quote> qList = new ArrayList<Quote>();
	private List<RouteCard> rcList = new ArrayList<RouteCard>();
	private List<PurchaseOrder> poList = new ArrayList<PurchaseOrder>();
	private List<CollectDeliverContract> cdList = new ArrayList<CollectDeliverContract>();
	private List<Doc> cuList = new ArrayList<Doc>();

	private boolean custB, contB, siB, qB, rcB, poB, cdB, cuB, otB;

	private List<DocumentTracker> documentTrackers;
	private DocumentTrackerService documentTrackerService = new DocumentTrackerService();

	private Double totalPO;

	private Integer overrideNumber;
	private Boolean enqCon;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (super.getParameter("enqId", false) != null) {
			this.contract = (contractService
					.findByKey(Long.parseLong((String) super.getParameter("contractId", false))));
			getSessionUI().setCustomer(this.contract.getCustomer());
			getSessionUI().setContract(this.contract);
			prepareList();
			prepareSelections();
		}
		if (super.getParameter("contractId", false) != null) {
			this.contract = contractService.findByGuid("" + super.getParameter("contractId", false));
			getSessionUI().setCustomer(this.contract.getCustomer());
			getSessionUI().setContract(this.contract);
			prepareList();
			prepareSelections();
		} else {
			super.redirect("/pages/userTasks.jsf");
		}
	}

	public void submitNewOverrideNumber() {
		try {
			if (overrideNumber < 5000)
				throw new Exception("Number provided is to low");
			if (overrideNumber > 999999999)
				throw new Exception("Number provided is to high");
			contractService.saveOverrideNumber(overrideNumber, true, null, getSessionUI().getContract());
			addInfoMessage("Contract/Enquiry numbers have been overrided.");
			overrideNumber = null;
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void showHistory() {
		try {
			this.documentTrackers = documentTrackerService.byRoot(doc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void download() {
		try {
			Users user = getSessionUI().getUser();
			DocumentTrackerService.create(user, doc, DocumentTrackerEnum.Downloaded);
			Faces.sendFile(doc.getFileContent(), GenericUtility.buidFileName(doc), true);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void downloadJasperReport(int switchInt) {
		try {
			switch (switchInt) {
			case 1:// StripInspect
				jasperService.jasperDownloadPdfForStripInspectionReport(this.si);
				break;
			case 2:// Quote
				jasperService.jasperDownloadPdfForQuote(this.quote);
				break;
			case 3:// RouteCard
				jasperService.jasperDownloadPdfForRouteCard(this.rc);
				break;
			case 4:// PurchaseOrder
				jasperService.jasperDownloadPdfForPurchaseOrder(this.po);
				break;
			case 5:// CollectDeliver
				break;
			case 6:// Contract
				jasperService.jasperReportDownloadPDF(this.contract);
				break;
			case 7:// Conform
				jasperService.jasperDownloadPdfForConformityCert(this.contract);
				break;
			default:
				break;
			}
			prepareSelections();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * this zooms into the enquiryForm.jsf to view/edit selected contract from data
	 * 
	 */
	public void zoomToContractDetails() {
		if (this.contract.getId() != null) {
			getSessionUI().setContract(this.contract);
			getSessionUI().setCustomer(this.contract.getCustomer());
			super.redirect("/pages/enquiryForm.jsf?contractId=" + this.contract.getContractGuid());
		}
	}

	public void zoomTo(int switchInt) {
		try {
			switch (switchInt) {
			case 1:// StripInspect
				if (this.si.getId() != null) {
					getSessionUI().setContract(this.si.getContract());
					getSessionUI().setCustomer(this.si.getContract().getCustomer());
					super.redirect("/pages/inspectionReportForm.jsf?inspecId=" + this.si.getId());
				}
				break;
			case 2:// Quote
				if (this.quote.getId() != null) {
					getSessionUI().setContract(this.quote.getContract());
					getSessionUI().setCustomer(this.quote.getContract().getCustomer());
					super.redirect("/pages/quoteForm.jsf?quoteId=" + this.quote.getQuoteGuid());
				}
				break;
			case 3:// RouteCard
				if (this.rc.getId() != null) {
					getSessionUI().setContract(this.rc.getContract());
					getSessionUI().setCustomer(this.rc.getContract().getCustomer());
					super.redirect("/pages/routeCard.jsf?rcardId=" + this.rc.getId());
				}
				break;
			case 4:// PurchaseOrder
				if (this.po.getId() != null) {
					getSessionUI().setContract(this.po.getContract());
					getSessionUI().setCustomer(this.po.getContract().getCustomer());
					super.redirect("/pages/purchaseOrderForm.jsf?poId=" + this.po.getId());
				}
				break;
			case 5:// CollectDeliver
				if (this.cdc.getId() != null) {
					getSessionUI().setContract(this.cdc.getContract());
					getSessionUI().setCustomer(this.cdc.getContract().getCustomer());
					super.redirect("/pages/collectionDeliveryForm.jsf?cdId=" + this.cdc.getId());
				}
				break;
			case 6:// Contract
				break;
			default:
				break;
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareSelections() {
		this.si = new InspectionReport();
		this.quote = new Quote();
		this.rc = new RouteCard();
		this.po = new PurchaseOrder();
		this.cdc = new CollectDeliverContract();
		this.doc = new Doc();
	}

	public void prepareList() {
		try {
			this.siList = codService.findInspectionReportByContract(this.contract);
			this.qList = codService.findQuotesByContract(this.contract);
			this.rcList = codService.findRouteCardByContract(this.contract);
			this.poList = codService.findPurchaseOrderByContract(this.contract);
			calculatePOTotal();
			this.cdList = codService.findCollectDeliverByContract(this.contract);
			this.cuList = codService.findDocsByContract(this.contract);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void calculatePOTotal() {
		totalPO = 0.00;
		for (PurchaseOrder po : poList) {
			if (po.getTotalPrice() != null)
				totalPO += po.getTotalPrice();
		}
	}

	public void switchPanel(int switchInt) {
		switch (switchInt) {
		case 1:// Custom
			custB = true;
			contB = false;
			siB = false;
			qB = false;
			rcB = false;
			poB = false;
			cdB = false;
			cuB = false;
			otB = false;
			break;
		case 2:// Contract
			custB = false;
			contB = true;
			siB = false;
			qB = false;
			rcB = false;
			poB = false;
			cdB = false;
			cuB = false;
			otB = false;
			break;
		case 3:// SI
			custB = false;
			contB = false;
			siB = true;
			qB = false;
			rcB = false;
			poB = false;
			cdB = false;
			cuB = false;
			otB = false;
			break;
		case 4:// Quote
			custB = false;
			contB = false;
			siB = false;
			qB = true;
			rcB = false;
			poB = false;
			cdB = false;
			cuB = false;
			otB = false;
			break;
		case 5:// Rc
			custB = false;
			contB = false;
			siB = false;
			qB = false;
			rcB = true;
			poB = false;
			cdB = false;
			cuB = false;
			otB = false;
			break;
		case 6:// Po
			custB = false;
			contB = false;
			siB = false;
			qB = false;
			rcB = false;
			poB = true;
			cdB = false;
			cuB = false;
			otB = false;
			break;
		case 7:// Collection
			custB = false;
			contB = false;
			siB = false;
			qB = false;
			rcB = false;
			poB = false;
			cdB = true;
			cuB = false;
			otB = false;
			break;
		case 8:// Upload
			custB = false;
			contB = false;
			siB = false;
			qB = false;
			rcB = false;
			poB = false;
			cdB = false;
			cuB = true;
			otB = false;
			break;
		case 9:// Other
			custB = false;
			contB = false;
			siB = false;
			qB = false;
			rcB = false;
			poB = false;
			cdB = false;
			cuB = false;
			otB = true;
			break;
		default:
			custB = false;
			contB = false;
			siB = false;
			qB = false;
			rcB = false;
			poB = false;
			cdB = false;
			cuB = false;
			otB = false;
			break;
		}
	}

	public void setContractStatusTo() {
		try {
			this.contract.setContractStatus(this.status);
			contractService.update(this.contract);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public boolean isCustB() {
		return custB;
	}

	public void setCustB(boolean custB) {
		this.custB = custB;
	}

	public boolean isContB() {
		return contB;
	}

	public void setContB(boolean contB) {
		this.contB = contB;
	}

	public boolean isSiB() {
		return siB;
	}

	public void setSiB(boolean siB) {
		this.siB = siB;
	}

	public boolean isqB() {
		return qB;
	}

	public void setqB(boolean qB) {
		this.qB = qB;
	}

	public boolean isRcB() {
		return rcB;
	}

	public void setRcB(boolean rcB) {
		this.rcB = rcB;
	}

	public boolean isPoB() {
		return poB;
	}

	public void setPoB(boolean poB) {
		this.poB = poB;
	}

	public boolean isCdB() {
		return cdB;
	}

	public void setCdB(boolean cdB) {
		this.cdB = cdB;
	}

	public boolean isCuB() {
		return cuB;
	}

	public void setCuB(boolean cuB) {
		this.cuB = cuB;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public List<InspectionReport> getSiList() {
		return siList;
	}

	public void setSiList(List<InspectionReport> siList) {
		this.siList = siList;
	}

	public List<Quote> getqList() {
		return qList;
	}

	public void setqList(List<Quote> qList) {
		this.qList = qList;
	}

	public List<RouteCard> getRcList() {
		return rcList;
	}

	public void setRcList(List<RouteCard> rcList) {
		this.rcList = rcList;
	}

	public List<PurchaseOrder> getPoList() {
		return poList;
	}

	public void setPoList(List<PurchaseOrder> poList) {
		this.poList = poList;
	}

	public List<CollectDeliverContract> getCdList() {
		return cdList;
	}

	public void setCdList(List<CollectDeliverContract> cdList) {
		this.cdList = cdList;
	}

	public List<Doc> getCuList() {
		return cuList;
	}

	public void setCuList(List<Doc> cuList) {
		this.cuList = cuList;
	}

	public InspectionReport getSi() {
		return si;
	}

	public void setSi(InspectionReport si) {
		this.si = si;
	}

	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote qoute) {
		this.quote = qoute;
	}

	public RouteCard getRc() {
		return rc;
	}

	public void setRc(RouteCard rc) {
		this.rc = rc;
	}

	public PurchaseOrder getPo() {
		return po;
	}

	public void setPo(PurchaseOrder po) {
		this.po = po;
	}

	public CollectDeliverContract getCdc() {
		return cdc;
	}

	public void setCdc(CollectDeliverContract cdc) {
		this.cdc = cdc;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public List<DocumentTracker> getDocumentTrackers() {
		return documentTrackers;
	}

	public void setDocumentTrackers(List<DocumentTracker> documentTrackers) {
		this.documentTrackers = documentTrackers;
	}

	public boolean isOtB() {
		return otB;
	}

	public void setOtB(boolean otB) {
		this.otB = otB;
	}

	public Double getTotalPO() {
		return totalPO;
	}

	public void setTotalPO(Double totalPO) {
		this.totalPO = totalPO;
	}

	public ContractStatusEnum[] getConstatus() {
		return constatus;
	}

	public void setConstatus(ContractStatusEnum[] constatus) {
		this.constatus = constatus;
	}

	public ContractStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ContractStatusEnum status) {
		this.status = status;
	}

	public Integer getOverrideNumber() {
		return overrideNumber;
	}

	public void setOverrideNumber(Integer overrideNumber) {
		this.overrideNumber = overrideNumber;
	}

	public Boolean getEnqCon() {
		return enqCon;
	}

	public void setEnqCon(Boolean enqCon) {
		this.enqCon = enqCon;
	}

}
