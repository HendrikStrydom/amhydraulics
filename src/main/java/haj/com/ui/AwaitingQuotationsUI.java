package haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.techfinium.entity.ResourceType;

import haj.com.entity.Contract;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;

@ManagedBean(name = "awaitingQuotationsUI")
@ViewScoped
public class AwaitingQuotationsUI extends AbstractUI {

	private ContractService service = new ContractService();
	private List<Contract> quotationList = null;
	private List<Contract> quotationfilteredList = null;
	private LazyDataModel<Contract> quotationDataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		quotationInfo();
	}

	public void quotationInfo() {

		quotationDataModel = new LazyDataModel<Contract>() {

			private static final long serialVersionUID = 1L;
			private List<Contract> retorno = new ArrayList<Contract>();

			@Override
			public List<Contract> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				try {
					retorno = service.allContract(Contract.class, first, pageSize, sortField, sortOrder,
							filters);
					quotationDataModel.setRowCount(service.count(Contract.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Contract obj) {
				return obj.getId();
			}

			@Override
			public Contract getRowData(String rowKey) {
				for (Contract obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

		try {
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Contract> getQuotationList() {
		return quotationList;
	}

	public void setQuotationList(List<Contract> quotationList) {
		this.quotationList = quotationList;
	}

	public List<Contract> getQuotationfilteredList() {
		return quotationfilteredList;
	}

	public void setQuotationfilteredList(List<Contract> quotationfilteredList) {
		this.quotationfilteredList = quotationfilteredList;
	}

	public LazyDataModel<Contract> getQuotationDataModel() {
		return quotationDataModel;
	}

	public void setQuotationDataModel(LazyDataModel<Contract> quotationDataModel) {
		this.quotationDataModel = quotationDataModel;
	}

}
