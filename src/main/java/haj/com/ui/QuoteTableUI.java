package haj.com.ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Contract;
import haj.com.entity.Quote;
import haj.com.entity.enums.QuoteStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.JasperService;
import haj.com.service.QuoteService;
import haj.com.service.TasksService;

/**
 * Here is the quotes look up table. There is two dialogs, one searches for
 * quote on relative fields and the other creates a quote for the chosen
 * contract
 * 
 * @author User
 *
 */

@ManagedBean(name = "quoteTableUI")
@ViewScoped
public class QuoteTableUI extends AbstractUI {

	private QuoteService quoteService = new QuoteService();
	private TasksService tasksService = new TasksService();
	private ContractService contractService = new ContractService();
	private JasperService jasperService = new JasperService();

	private List<Quote> quoteList = new ArrayList<Quote>();
	private Quote quote;
	private Contract contract = new Contract();

	private QuoteStatusEnum[] quoteStatusEnum = QuoteStatusEnum.values();

	private Map<String, Object> queryParameters = new Hashtable<String, Object>();

	private String ceNumber, quoteNumber;
	private QuoteStatusEnum qsEnum;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.quoteList = quoteService.allQuoteBySoftDelete(false);
		// TODO: Will be a lazy datamodal once all items are signed off
	}
	
	public void downloadQuote() {
		try {
			if (this.quote.getId() != null) {
				jasperService.jasperDownloadPdfForQuote(this.quote);
				addInfoMessage("Quote Downloaded");
			} else {
				addWarningMessage("No Quote");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	/**
	 * this softDeletes quote and closes any task assigned.
	 */
	public void deleteQuote() {
		try {
			if (this.quote.getTask() != null){
				tasksService.closeTask(this.quote.getTask(), getSessionUI().getUser());
				quoteService.delete(this.quote);
				this.quoteList = quoteService.allQuoteBySoftDelete(false);
				addInfoMessage("Quote deleted " );
			}else
			{
				addInfoMessage("Quote cannot be deleted " );
			}
					
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/***
	 * Here is where the Map<String, Object> gets built with its relevant
	 * information
	 */
	public void searchFor() {
		try {
			queryParameters.put("ConEnqNumber", "%" + ceNumber + "%");
			queryParameters.put("quoteNumber", "%" + quoteNumber + "%");
			if (qsEnum != null)
				queryParameters.put("qsEnum", qsEnum);
			this.quoteList = quoteService.findQuoteByQueryParameter(queryParameters);
			if (this.quoteList.size() > 0)
				addInfoMessage("Search results found: " + this.quoteList.size());
			else
				addWarningMessage("No results found");

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/***
	 * Clears search criterias
	 */
	public void clearSearch() {
		this.ceNumber = null;
		this.quoteNumber = null;
		this.qsEnum = null;
		queryParameters = new Hashtable<String, Object>();
	}

	public void clearContract() {
		this.contract = new Contract();
	}

	public List<Contract> completeContract(String desc) {
		List<Contract> l = new ArrayList<Contract>();
		try {
			l = contractService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	/**
	 * this zooms into the quoteForm.jsf to view/edit selected quote from data
	 * table
	 */
	public void zoomTo() {
		if (this.quote.getId() != null) {
			getSessionUI().setContract(this.quote.getContract());
			getSessionUI().setCustomer(this.quote.getContract().getCustomer());
			super.redirect("/pages/quoteForm.jsf?quoteId=" + this.quote.getQuoteGuid());
		}
	}

	/**
	 * This will set contract into the session and redirect to quoteForm.jsf
	 */
	public void createQuoteWithContract() {
		super.redirect("/pages/quoteForm.jsf?contractId=" + this.contract.getContractGuid());
	}

	/**
	 * Getter and Setters
	 * 
	 * @return
	 */
	public List<Quote> getQuoteList() {
		return quoteList;
	}

	public void setQuoteList(List<Quote> quoteList) {
		this.quoteList = quoteList;
	}

	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote quote) {
		this.quote = quote;
	}

	public QuoteStatusEnum[] getQuoteStatusEnum() {
		return quoteStatusEnum;
	}

	public void setQuoteStatusEnum(QuoteStatusEnum[] quoteStatusEnum) {
		this.quoteStatusEnum = quoteStatusEnum;
	}

	public Map<String, Object> getQueryParameters() {
		return queryParameters;
	}

	public void setQueryParameters(Map<String, Object> queryParameters) {
		this.queryParameters = queryParameters;
	}

	public String getCeNumber() {
		return ceNumber;
	}

	public void setCeNumber(String ceNumber) {
		this.ceNumber = ceNumber;
	}

	public String getQuoteNumber() {
		return quoteNumber;
	}

	public void setQuoteNumber(String quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public QuoteStatusEnum getQsEnum() {
		return qsEnum;
	}

	public void setQsEnum(QuoteStatusEnum qsEnum) {
		this.qsEnum = qsEnum;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

}
