package haj.com.ui;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import haj.com.framework.AbstractUI;
import haj.com.entity.Address;
import haj.com.entity.Customer;
import haj.com.entity.CustomerUsers;
import haj.com.service.AddressLookupService;
import haj.com.service.AddressService;
import haj.com.service.CustomerService;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@ManagedBean(name = "customerUI")
@ViewScoped
public class CustomerUI extends AbstractUI {

	private CustomerService service = new CustomerService();
	private List<Customer> customerList = null;
	private List<Customer> customerfilteredList = null;
	private Customer customer = null;
	private LazyDataModel<Customer> dataModel;

	// adding address
	private AddressService addressService = new AddressService();
	private AddressLookupService addressLookupService = new AddressLookupService();
	private Address address = null;


	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all Customer and prepare a for a create of a new
	 * Customer
	 * 
	 * @author TechFinium
	 * @see Customer
	 * @throws Exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		customerInfo();
	}

	/**
	 * Get all Customer for data table
	 * 
	 * @author TechFinium
	 * @see Customer
	 */
	public void customerInfo() {

		dataModel = new LazyDataModel<Customer>() {

			private static final long serialVersionUID = 1L;
			private List<Customer> retorno = new ArrayList<Customer>();

			@Override
			public List<Customer> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				try {
					retorno = service.allCustomer(Customer.class, first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(service.count(Customer.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Customer obj) {
				return obj.getId();
			}

			@Override
			public Customer getRowData(String rowKey) {
				for (Customer obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

	}

	/**
	 * Insert Customer into database
	 * 
	 * @author TechFinium
	 * @see Customer
	 */
	public void customerInsert() {
		try {
			addressService.create(address);
			service.create(this.customer);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			customerInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete Customer from database
	 * 
	 * @author TechFinium
	 * @see Customer
	 */
	public void customerDelete() {
		try {
			addressService.delete(address);
			service.delete(this.customer);
			prepareNew();
			customerInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted "));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Customer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<Customer> customerList) {
		this.customerList = customerList;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * Create new instance of Customer
	 * 
	 * @author TechFinium
	 * @see Customer
	 */
	public void prepareNew() {
		customer = new Customer();
		address = new Address();

	}

	/**
	 * Update Customer in database
	 * 
	 * @author TechFinium
	 * @see Customer
	 */
	public void customerUpdate() {
		try {
			addressService.update(address);
			service.update(this.customer);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			customerInfo();

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Prepare select one menu data
	 * 
	 * @author TechFinium
	 * @see Customer
	 */
	public List<SelectItem> getCustomerDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), "-------------Select-------------"));
		customerInfo();
		for (Customer ug : customerList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<Customer> getCustomerfilteredList() {
		return customerfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @see Customer
	 */
	public void setCustomerfilteredList(List<Customer> customerfilteredList) {
		this.customerfilteredList = customerfilteredList;
	}

	public List<Customer> complete(String desc) {
		List<Customer> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public LazyDataModel<Customer> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Customer> dataModel) {
		this.dataModel = dataModel;
	}

	public AddressService getAddressService() {
		return addressService;
	}

	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}

	public AddressLookupService getAddressLookupService() {
		return addressLookupService;
	}

	public void setAddressLookupService(AddressLookupService addressLookupService) {
		this.addressLookupService = addressLookupService;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
