package haj.com.ui;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import haj.com.constants.HAJConstants;
import haj.com.entity.AMResourceAllocation;
import haj.com.framework.AbstractUI;
import haj.com.service.ResourceAllocationService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "resourceAllocationUI")
@ViewScoped
public class ResourceAllocationUI extends AbstractUI {

	private ResourceAllocationService service = new ResourceAllocationService();
	private List<AMResourceAllocation> allocations = null;
	private Date now;
	private Date initialDate;
	private ScheduleModel eventModel;
	private DefaultScheduleEvent event = new DefaultScheduleEvent();
	private String css;
	private boolean poll;
	private AMResourceAllocation resourceAllocation;

	@PostConstruct
	public void init() {
		try {
			poll = false;
			now = new Date();
			initialDate = GenericUtility.getFirstDayOfMonth(now);
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		css = "";
		prepCalendar();
	}

	private void prepCalendar() throws Exception {
		Map<String, String> m = new HashMap<String, String>();
		poll = false;
		eventModel = new LazyScheduleModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void loadEvents(Date start, Date end) {
				try {
					css = "";
					allocations = service.resourceAllocation(start, end);
					if (allocations.isEmpty())
						poll = true;
					for (AMResourceAllocation resourceAllocation : allocations) {
						DefaultScheduleEvent defaultScheduleEvent = new DefaultScheduleEvent(resourceAllocation.getResource().getDescription(), resourceAllocation.getFromDateTime(), resourceAllocation.getToDateTime());
						defaultScheduleEvent.setDescription(buildToolTip(resourceAllocation));

						defaultScheduleEvent.setStyleClass(HAJConstants.CSS_PREFIX + resourceAllocation.getResource().getId().toString());
						defaultScheduleEvent.setData(resourceAllocation);
						eventModel.addEvent(defaultScheduleEvent);
						if (!m.containsKey(resourceAllocation.getResource().getId())) {
							m.put(HAJConstants.CSS_PREFIX + resourceAllocation.getResource().getId().toString(), resourceAllocation.getResource().getColorCode());
						}
					}

					if (!m.isEmpty()) {
						prepCSS(m);
					}
				} catch (Exception e) {
					logger.fatal(e);

				}
			}

			private String buildToolTip(AMResourceAllocation resourceAllocation) {
				// defaultScheduleEvent.setDescription("<span
				// class=\"customToolTip\">"+resourceAllocation.getDescription()+
				// "<br/>"+ "</span>");
				if (resourceAllocation.getDescription() == null && resourceAllocation.getContract() == null && resourceAllocation.getUser() == null)
					return null;
				else {
					String tt = "<span class=\"customToolTip\">";
					tt = tt + (resourceAllocation.getDescription() == null ? "" : "<b>" + resourceAllocation.getDescription()) + "</b>";
					tt = tt + (resourceAllocation.getContract() == null ? "" : (" (" + resourceAllocation.getContract().getTitle()) + ")");
					tt = tt + (resourceAllocation.getUser() == null ? "" : "" + resourceAllocation.getUser().getFirstName() + " " + resourceAllocation.getUser().getLastName());
					tt = tt + "</span>";
					return tt;
				}
			}
		};

		/*
		 * eventModel = new DefaultScheduleModel(); for (ResourceAllocation
		 * resourceAllocation : allocations) { DefaultScheduleEvent
		 * defaultScheduleEvent = new
		 * DefaultScheduleEvent(resourceAllocation.getResource().getDescription(
		 * ), resourceAllocation.getFromDateTime(),
		 * resourceAllocation.getFromDateTime());
		 * defaultScheduleEvent.setDescription(resourceAllocation.getDescription
		 * ()); eventModel.addEvent(defaultScheduleEvent); }
		 */
	}

	private void prepCSS(Map<String, String> m) {
		/*
		 * .hsMedium { background-color: #9CCC65 !important; border: solid 1px
		 * #9CCC65 !important; }
		 */
		css = "";
		for (Entry<String, String> me : m.entrySet()) {
			css += "." + me.getKey().trim() + "{ ";
			css += "background-color: #" + me.getValue().trim() + " !important; ";
			css += "border: solid 1px #" + me.getValue().trim() + " !important; }";
		}
		poll = true;
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}

	public boolean isPoll() {
		return poll;
	}

	public void setPoll(boolean poll) {
		this.poll = poll;
	}

	public void addEvent(ActionEvent actionEvent) {
		if (event.getId() == null)
			eventModel.addEvent(event);
		else
			eventModel.updateEvent(event);

		event = new DefaultScheduleEvent();
		updateRA();
	}

	public DefaultScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(DefaultScheduleEvent event) {
		this.event = event;
	}

	public AMResourceAllocation getResourceAllocation() {
		return resourceAllocation;
	}

	public void setResourceAllocation(AMResourceAllocation resourceAllocation) {
		this.resourceAllocation = resourceAllocation;
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (DefaultScheduleEvent) selectEvent.getObject();
		this.resourceAllocation = (AMResourceAllocation) event.getData();
	}

	public void onDateSelect(SelectEvent selectEvent) {
		event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
		this.resourceAllocation = new AMResourceAllocation();
		this.resourceAllocation.setFromDateTime((Date) selectEvent.getObject());
		this.resourceAllocation.setToDateTime((Date) selectEvent.getObject());
		event.setData(this.resourceAllocation);
	}

	public void onEventMove(ScheduleEntryMoveEvent event) {
		this.event = (DefaultScheduleEvent) event.getScheduleEvent();
		this.resourceAllocation = (AMResourceAllocation) this.event.getData();
		this.resourceAllocation.setFromDateTime(this.event.getStartDate());
		this.resourceAllocation.setToDateTime(this.event.getEndDate());
		updateRA();
	}

	public void onEventResize(ScheduleEntryResizeEvent event) {
		this.event = (DefaultScheduleEvent) event.getScheduleEvent();
		this.resourceAllocation = (AMResourceAllocation) this.event.getData();
		this.resourceAllocation.setFromDateTime(this.event.getStartDate());
		this.resourceAllocation.setToDateTime(this.event.getEndDate());
		updateRA();
	}

	private void updateRA() {
		try {
			service.create(this.resourceAllocation);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void deleteRA() {
		try {
			service.delete(this.resourceAllocation);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
}
