package haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.techfinium.entity.ResourceType;
import com.techfinium.service.ResourceTypeService;

import haj.com.framework.AbstractUI;

@ManagedBean(name = "resourceTypeUI")
@ViewScoped
public class ResourceTypeUI extends AbstractUI {

	// resource management
	private ResourceTypeService resourceTypeService = new ResourceTypeService();
	private ResourceType resourceType = null;
	private List<ResourceType> resourceTypeList = null;
	private List<ResourceType> resourceTypefilteredList = null;
	private LazyDataModel<ResourceType> resourceTypeDataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepareNew();
		resourceTypeInfo();
	}

	public void prepareNew() {
		resourceType = new ResourceType();
		/* resourceType.setResourceType(new ResourceType()); */
	}

	public void resourceTypeInfo() {

		resourceTypeDataModel = new LazyDataModel<ResourceType>() {

			private static final long serialVersionUID = 1L;
			private List<ResourceType> retorno = new ArrayList<ResourceType>();

			@Override
			public List<ResourceType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				try {
					retorno = resourceTypeService.allResourceType(ResourceType.class, first, pageSize, sortField,
							sortOrder, filters);
					resourceTypeDataModel.setRowCount(resourceTypeService.count(ResourceType.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(ResourceType obj) {
				return obj.getId();
			}

			@Override
			public ResourceType getRowData(String rowKey) {
				for (ResourceType obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

		try {
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void resourceTypeInsert() {
		try {
			resourceTypeService.create(this.resourceType);
			prepareNew();
			addInfoMessage("Update successful");
			resourceTypeInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void resourceTypeDelete() {
		try {
			resourceTypeService.delete(this.resourceType);
			prepareNew();
			resourceTypeInfo();
			super.addWarningMessage("Row deleted.");
		} catch (PersistenceException e) {
			super.addErrorMessage("You cannot delete this resource type as it is assigned to a resource.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void resourceTypeUpdate() {
		try {
			resourceTypeService.update(this.resourceType);
			addInfoMessage("Update successful");
			resourceTypeInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public ResourceType getResourceType() {
		return resourceType;
	}

	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}

	public List<ResourceType> getResourceTypeList() {
		return resourceTypeList;
	}

	public void setResourceTypeList(List<ResourceType> resourceTypeList) {
		this.resourceTypeList = resourceTypeList;
	}

	public List<ResourceType> getResourceTypefilteredList() {
		return resourceTypefilteredList;
	}

	public void setResourceTypefilteredList(List<ResourceType> resourceTypefilteredList) {
		this.resourceTypefilteredList = resourceTypefilteredList;
	}

	public LazyDataModel<ResourceType> getResourceTypeDataModel() {
		return resourceTypeDataModel;
	}

	public void setResourceTypeDataModel(LazyDataModel<ResourceType> resourceTypeDataModel) {
		this.resourceTypeDataModel = resourceTypeDataModel;
	}

}
