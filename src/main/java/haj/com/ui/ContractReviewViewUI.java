package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.techfinium.entity.Resource;
import com.techfinium.service.ResourceService;

import haj.com.entity.Contract;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.enums.ContractDetailTypeEnum;
import haj.com.entity.enums.MaterialTypeEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractReviewItemService;
import haj.com.service.ContractService;
import haj.com.service.TasksService;

@ManagedBean(name = "contractReviewViewUI")
@ViewScoped
public class ContractReviewViewUI extends AbstractUI {

	private ContractReviewItemService contractReviewItemService = new ContractReviewItemService();
	private ResourceService resourceService = new ResourceService();
	private ContractReviewItem contractReviewItem;
	private List<ContractReviewItem> contractReviewItemList;
	private Contract contract;
	private Double totalPrice;

	private ContractDetailTypeEnum cdtEnum;

	
	public void prepareContractReview(){
		try {
			this.contractReviewItemList = new ArrayList<ContractReviewItem>();
			this.contractReviewItemList = contractReviewItemService.findByContract(getSessionUI().getContract().getId());
			this.contract = new Contract();
			this.contract = getSessionUI().getContract();
			prepareNewContractReviewItem();
			this.totalPrice = 0.00;
			calculateTotal();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void calculateTotal() {
		this.totalPrice = 0.00;
		for (ContractReviewItem contractReviewItem : contractReviewItemList) {
			if (contractReviewItem.getPrice() != null)
				this.totalPrice += contractReviewItem.getPrice();
		}
	}

	public void prepareContractReviewItemList() {
		try {
			this.contractReviewItemList = new ArrayList<ContractReviewItem>();
			this.contractReviewItemList = contractReviewItemService.findByContract(this.contract.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void saveContractReviewItem() {
		try {
			contractReviewItemService.create(this.contractReviewItem);
			prepareNewContractReviewItem();
			prepareContractReviewItemList();
			calculateTotal();
			addInfoMessage("Review Item add/update successful");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void deleteContractReviewItem() {
		try {
			if (this.contractReviewItem.getId() != null) {
				contractReviewItemService.delete(this.contractReviewItem);
				prepareNewContractReviewItem();
				prepareContractReviewItemList();
				calculateTotal();
				addInfoMessage("Review Item delete successfull");
			} else {
				addWarningMessage("Detail could not be deleted");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void prepareNewContractReviewItem() {
		this.contractReviewItem = new ContractReviewItem();
		this.contractReviewItem.setContract(this.contract);
	}

	public List<SelectItem> getContractReviewDetailTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ContractDetailTypeEnum selectItem : ContractDetailTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getMaterialTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (MaterialTypeEnum selectItem : MaterialTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<Resource> completeResource(String desc) {
		List<Resource> l = null;
		try {
			l = resourceService.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public ContractReviewItem getContractReviewItem() {
		return contractReviewItem;
	}

	public void setContractReviewItem(ContractReviewItem contractReviewItem) {
		this.contractReviewItem = contractReviewItem;
	}

	public List<ContractReviewItem> getContractReviewItemList() {
		return contractReviewItemList;
	}

	public void setContractReviewItemList(List<ContractReviewItem> contractReviewItemList) {
		this.contractReviewItemList = contractReviewItemList;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public ContractDetailTypeEnum getCdtEnum() {
		return cdtEnum;
	}

	public void setCdtEnum(ContractDetailTypeEnum cdtEnum) {
		this.cdtEnum = cdtEnum;
	}

}
