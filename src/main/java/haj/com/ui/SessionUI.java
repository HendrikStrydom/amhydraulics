package haj.com.ui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import haj.com.entity.Contract;
import haj.com.entity.Customer;
import haj.com.entity.Doc;
import haj.com.entity.Users;

@ManagedBean(name = "sessionUI")
@SessionScoped
/**
 * This is the only object that should ever exist in session, anything required
 * on the session object should be defined as Serializable and access modifiers
 * to instance given here.
 * 
 *
 */
public class SessionUI implements Serializable {

	private Users user;
	private Doc selDoc;
	private Customer customer;
	private Contract contract;

	/**
	 * Should be executed before log off and after login, reset bean to default
	 * values.
	 */
	public void clear() {

	}

	/**
	 * This is vital for audit logging, any new items added to the SessionUI
	 * that may be required in the audit logging should be included into the map
	 * below.
	 * 
	 * @return
	 */
	public Map<String, Object> getMap() {
		Map<String, Object> map = new HashMap<String, Object>();

		// logged in user
		Long userId = null;
		/*
		 * if (user != null) userId = user.getUid(); map.put("userId", userId);
		 */
		return map;

	}

	@Override
	public String toString() {
		return getMap().toString();
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Doc getSelDoc() {
		return selDoc;
	}

	public void setSelDoc(Doc selDoc) {
		this.selDoc = selDoc;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

}
