package haj.com.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.PurchaseOrder;
import haj.com.entity.enums.PurchaseOrderEnum;
import haj.com.entity.enums.PurchaseOrderForEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.JasperService;
import haj.com.service.PurchaseOrderService;

@ManagedBean(name = "purchaseOrderTableUI")
@ViewScoped
public class PurchaseOrderTableUI extends AbstractUI {

	private PurchaseOrderService purchaseOrderService = new PurchaseOrderService();

	private List<PurchaseOrder> purchaseOrderList = new ArrayList<PurchaseOrder>();

	private PurchaseOrder purchaseOrder = new PurchaseOrder();

	private PurchaseOrderEnum[] purchasesOrderEnum = PurchaseOrderEnum.values();
	private PurchaseOrderForEnum[] purchasesOrderForEnum = PurchaseOrderForEnum.values();

	private Map<String, Object> queryParameters = new Hashtable<String, Object>();

	private String ceNumber, poNumber;
	private PurchaseOrderEnum poEnum;
	private PurchaseOrderForEnum poForEnum;

	private JasperService jasperService = new JasperService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.purchaseOrderList = purchaseOrderService.allPurchaseOrder();
	}

	public void downloadPurchaseOrder() {
		try {
			if (this.purchaseOrder.getId() != null) {
				// TODO task for P.O.
				jasperService.jasperDownloadPdfForPurchaseOrder(this.purchaseOrder);
				addInfoMessage("Purchase Order Downloaded");
			} else {
				addWarningMessage("No Purchase Order");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
			e.printStackTrace();
		}
	}

	public void deletePurchaseOrder() {
		try {
			this.purchaseOrder.setActionUser(getSessionUI().getUser());
			this.purchaseOrder.setSoftDelete(true);
			purchaseOrderService.updatePurchaseOrder(this.purchaseOrder);
			this.purchaseOrderList = purchaseOrderService.allPurchaseOrder();
			addInfoMessage("Purchase Order Deleted");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void zoomTo() {
		if (this.purchaseOrder.getId() != null) {
			if (this.purchaseOrder.getContract() != null) {
			getSessionUI().setContract(this.purchaseOrder.getContract());
			getSessionUI().setCustomer(this.purchaseOrder.getContract().getCustomer());
			}
			super.redirect("/pages/purchaseOrderForm.jsf?poId=" + this.purchaseOrder.getId());
		} else
			super.ajaxRedirect("/pages/purchaseOrderForm.jsf");
	}

	public void searchFor() {
		try {
			queryParameters.put("ConEnqNumber", "%" + ceNumber + "%");
			queryParameters.put("poNumber", "%" + poNumber + "%");
			if (poForEnum != null)
				queryParameters.put("poForEnum", poForEnum);
			if (poEnum != null)
				queryParameters.put("poEnum", poEnum);
			this.purchaseOrderList = purchaseOrderService.findPurchaseOrderByQueryParameter(queryParameters);
			if (this.purchaseOrderList.size() > 0)
				addInfoMessage("Search results found: " + this.purchaseOrderList.size());
			else
				addWarningMessage("No results found");

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void clearSearch() {
		this.ceNumber = null;
		this.poNumber = null;
		this.poEnum = null;
		this.poForEnum = null;
		queryParameters = new Hashtable<String, Object>();
	}

	public List<PurchaseOrder> getPurchaseOrderList() {
		return purchaseOrderList;
	}

	public void setPurchaseOrderList(List<PurchaseOrder> purchaseOrderList) {
		this.purchaseOrderList = purchaseOrderList;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public PurchaseOrderEnum[] getPurchasesOrderEnum() {
		return purchasesOrderEnum;
	}

	public void setPurchasesOrderEnum(PurchaseOrderEnum[] purchasesOrderEnum) {
		this.purchasesOrderEnum = purchasesOrderEnum;
	}

	public PurchaseOrderForEnum[] getPurchasesOrderForEnum() {
		return purchasesOrderForEnum;
	}

	public void setPurchasesOrderForEnum(PurchaseOrderForEnum[] purchasesOrderForEnum) {
		this.purchasesOrderForEnum = purchasesOrderForEnum;
	}

	public Map<String, Object> getQueryParameters() {
		return queryParameters;
	}

	public void setQueryParameters(Map<String, Object> queryParameters) {
		this.queryParameters = queryParameters;
	}

	public String getCeNumber() {
		return ceNumber;
	}

	public void setCeNumber(String ceNumber) {
		this.ceNumber = ceNumber;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public PurchaseOrderEnum getPoEnum() {
		return poEnum;
	}

	public void setPoEnum(PurchaseOrderEnum poEnum) {
		this.poEnum = poEnum;
	}

	public PurchaseOrderForEnum getPoForEnum() {
		return poForEnum;
	}

	public void setPoForEnum(PurchaseOrderForEnum poForEnum) {
		this.poForEnum = poForEnum;
	}

}
