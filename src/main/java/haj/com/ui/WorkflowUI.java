package  haj.com.ui;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import haj.com.framework.AbstractUI;
import haj.com.entity.Workflow;
import haj.com.service.WorkflowService;
import javax.faces.model.SelectItem;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@ManagedBean(name = "workflowUI")
@ViewScoped
public class WorkflowUI extends AbstractUI {

	private WorkflowService service = new WorkflowService();
	private List<Workflow> workflowList = null;
	private List<Workflow> workflowfilteredList = null;
	private Workflow workflow = null;
	private LazyDataModel<Workflow> dataModel; 

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all Workflow and prepare a for a create of a new Workflow
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @throws Exception
 	 */
	private void runInit() throws Exception {
		prepareNew();
		workflowInfo();
	}

	/**
	 * Get all Workflow for data table
 	 * @author TechFinium 
 	 * @see    Workflow
 	 */
	public void workflowInfo() {
	 
			
			 dataModel = new LazyDataModel<Workflow>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Workflow> retorno = new  ArrayList<Workflow>();
			   
			   @Override 
			   public List<Workflow> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					retorno = service.allWorkflow(Workflow.class,first,pageSize,sortField,sortOrder,filters);
					dataModel.setRowCount(service.count(Workflow.class,filters));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return retorno; 
			   }
			   
			    @Override
			    public Object getRowKey(Workflow obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Workflow getRowData(String rowKey) {
			        for(Workflow obj : retorno) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
			
	
	}

	/**
	 * Insert Workflow into database
 	 * @author TechFinium 
 	 * @see    Workflow
 	 */
	public void workflowInsert() {
		try {
			 service.create(this.workflow);
			 prepareNew();
			 addInfoMessage(super.getEntryLanguage("update.successful"));
			 workflowInfo();
		} catch (Exception e) {
		    super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Delete Workflow from database
 	 * @author TechFinium 
 	 * @see    Workflow
 	 */
	public void workflowDelete() {
		try {
			 service.delete(this.workflow);
			  prepareNew();
			 workflowInfo();
			 super.addWarningMessage(super.getEntryLanguage("row.deleted "));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Workflow> getWorkflowList() {
		return workflowList;
	}
	public void setWorkflowList(List<Workflow> workflowList) {
		this.workflowList = workflowList;
	}
	public Workflow getWorkflow() {
		return workflow;
	}
	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	/**
	 * Create new instance of Workflow 
 	 * @author TechFinium 
 	 * @see    Workflow
 	 */
	public void prepareNew() {
		workflow = new Workflow();
	}


	/**
	 * Update Workflow in database
 	 * @author TechFinium 
 	 * @see    Workflow
 	 */
    public void workflowUpdate() {
		try {
			 service.update(this.workflow);
			  addInfoMessage(super.getEntryLanguage("update.successful"));
			 workflowInfo();
		} catch (Exception e) {
		   super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Prepare select one menu data
 	 * @author TechFinium 
 	 * @see    Workflow
 	 */	
    public List<SelectItem> getWorkflowDD() {
    	List<SelectItem> l =new ArrayList<SelectItem>();
    	l.add(new SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
    	workflowInfo();
    	for (Workflow ug : workflowList) {
    		// l.add(new SelectItem(ug.getUserGroupId(), ug.getUserGroupDesc()));
		}
    	return l;
    }

    public List<Workflow> getWorkflowfilteredList() {
		return workflowfilteredList;
	}
	
	/**
	 * Prepare auto complete data
 	 * @author TechFinium 
 	 * @see    Workflow
 	 */	
	public void setWorkflowfilteredList(List<Workflow> workflowfilteredList) {
		this.workflowfilteredList = workflowfilteredList;
	}

		public List<Workflow> complete(String desc) {
		List<Workflow> l = null;
		try {
			l = service.findByName(desc);
		}
		catch (Exception e) {
		addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
	
	
	public LazyDataModel<Workflow> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Workflow> dataModel) {
		this.dataModel = dataModel;
	}
	
}
