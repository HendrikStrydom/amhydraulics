package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Tasks;
import haj.com.framework.AbstractUI;
import haj.com.service.TasksService;

@ManagedBean(name = "countUI")
@ViewScoped
public class CountUI extends AbstractUI {

	public TasksService service;
	public Long outstandingTasks;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		service = new TasksService(getSessionUI().getMap());
		outStandingTasks();
	}

	public void outStandingTasks() {
		try {
			List<Tasks> list = service.findTasksByUserIncomplete(getSessionUI().getUser());
			outstandingTasks = (long) list.size();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	
	public Long getOutstandingTasks() {
		return outstandingTasks;
	}

	public void setOutstandingTasks(Long outstandingTasks) {
		this.outstandingTasks = outstandingTasks;
	}

}
