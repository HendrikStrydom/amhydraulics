package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Contract;
import haj.com.entity.TaskUsers;
import haj.com.entity.Tasks;
import haj.com.entity.enums.TaskStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.ContractService;
import haj.com.service.TaskUsersService;
import haj.com.service.TasksService;

@ManagedBean(name = "priorityUserTaskUI")
@ViewScoped
public class PriorityUserTaskUI extends AbstractUI {

	// Service layers
	private TasksService taskService = new TasksService();

	// Lists
	private List<Tasks> taskList = new ArrayList<Tasks>();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (getSessionUI().getUser() != null)
			populateUsersTasksList();
	}

	/**
	 * Locates all tasks assigned to session user
	 * 
	 * @throws Exception
	 */
	private void populateUsersTasksList() throws Exception {
		taskList = new ArrayList<>();
		// locates all incomplete tasks
		taskList = taskService.findPriorityTasksByUserIncomplete(getSessionUI().getUser());
	}

	public List<Tasks> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Tasks> taskList) {
		this.taskList = taskList;
	}

}
