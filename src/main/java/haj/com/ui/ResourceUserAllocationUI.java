package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.model.DualListModel;

import com.techfinium.entity.Resource;

import haj.com.entity.ResourceUser;
import haj.com.entity.ResourceUsers;
import haj.com.entity.Users;
import haj.com.framework.AbstractUI;
import haj.com.service.ResourceUserAllocationService;

@ManagedBean(name = "ruaUI")
@ViewScoped
public class ResourceUserAllocationUI extends AbstractUI {

	private ResourceUserAllocationService ruaService = new ResourceUserAllocationService();

	private DualListModel<Users> userDualList;

	private Resource resource;

	private List<ResourceUsers> ruList;
	private List<ResourceUsers> ruFilterList;
	private ResourceUsers ru;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepareNewResourceUser();
	}

	public void prepareNewResourceUser() {
		prepareNewResource();
		prepareNewUsersDualList();
		prepareResourceUserDataList();
	}

	public void clearResourceUser() {
		prepareNewResource();
		prepareNewUsersDualList();
		prepareResourceUserDataList();
		addInfoMessage("Form Cleared");
	}

	public void prepareNewResource() {
		this.resource = new Resource();
	}

	public void prepareResourceUserDataList() {
		try {
			this.ru = new ResourceUsers();
			this.ruList = new ArrayList<ResourceUsers>();
			this.ruList = ruaService.getAllResourceUsers();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepareNewUsersDualList() {
		try {
			this.userDualList = new DualListModel<Users>(ruaService.findUsersThatAreNotAdmin(), new ArrayList<Users>());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Resource> completeResource(String desc) {
		List<Resource> l = null;
		try {
			l = ruaService.findResourceByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public void createResourceUsers() {
		try {
			if (this.userDualList.getTarget().size() > 0) {
				ruaService.createResourceUsers(this.resource, this.userDualList.getTarget());
				addInfoMessage("Users have been Assigned");
				prepareNewResourceUser();
			} else {
				addWarningMessage("No Users Selected for Assignment");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void deleteResourceUsers() {
		try {
			ruaService.deleteResourceUsers(this.ru);
			addInfoMessage("User removed from Resource");
			prepareResourceUserDataList();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public DualListModel<Users> getUserDualList() {
		return userDualList;
	}

	public void setUserDualList(DualListModel<Users> userDualList) {
		this.userDualList = userDualList;
	}

	public Resource getResource() {
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public List<ResourceUsers> getRuList() {
		return ruList;
	}

	public void setRuList(List<ResourceUsers> ruList) {
		this.ruList = ruList;
	}

	public ResourceUsers getRu() {
		return ru;
	}

	public void setRu(ResourceUsers ru) {
		this.ru = ru;
	}

	public List<ResourceUsers> getRuFilterList() {
		return ruFilterList;
	}

	public void setRuFilterList(List<ResourceUsers> ruFilterList) {
		this.ruFilterList = ruFilterList;
	}

}
