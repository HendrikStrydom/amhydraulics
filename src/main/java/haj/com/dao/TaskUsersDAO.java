package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.TaskUsers;

public class TaskUsersDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}


	/**
	 * Get all TaskUsers
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @return List<TaskUsers>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<TaskUsers> allTaskUsers() throws Exception {
		return (List<TaskUsers>)super.getList("select o from TaskUsers o");
	}


	@SuppressWarnings("unchecked")
	public List<TaskUsers> allTaskUsers(Long from, int noRows) throws Exception {
	 	String hql = "select o from TaskUsers o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<TaskUsers>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @return TaskUsers 
 	 * @throws Exception
 	 */
	public TaskUsers findByKey(Long id) throws Exception {
	 	String hql = "select o from TaskUsers o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (TaskUsers)super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find TaskUsers by description
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @return List<TaskUsers>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<TaskUsers> findByName(String description) throws Exception {
	 	String hql = "select o from TaskUsers o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<TaskUsers>)super.getList(hql, parameters);
	}
	
	/**
	 * Find TaskUsers by description
 	 * @author TechFinium 
 	 * @see    TaskUsers
 	 * @return List<TaskUsers>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<TaskUsers> findByTask(Long taskId) throws Exception {
	 	String hql = "select o from TaskUsers o where o.task = :taskId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("taskId", taskId);
		return (List<TaskUsers>)super.getList(hql, parameters);
	}
}

