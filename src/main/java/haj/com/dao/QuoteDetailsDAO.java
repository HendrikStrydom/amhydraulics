package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.QuoteItem;

public class QuoteDetailsDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see Quote
	 * @return a List of Quote objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<QuoteItem> allQuoteDetails() throws Exception {
		return (List<QuoteItem>) super.getList("select o from QuoteDetails o");
	}

	public QuoteItem findByKey(Long id) throws Exception {
		String hql = "select o from QuoteDetails o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (QuoteItem) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> findByQuoteReturnDetails(Long quoteId) throws Exception {
		String hql = "select o.detailsId from QuoteDetails o where o.quoteId.id = :quoteId  and (o.detailsId.softDelete = false or o.detailsId.softDelete is null)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("quoteId", quoteId);
		return (List<ContractReviewItem>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<QuoteItem> findByQuote(Long quoteId) throws Exception {
		String hql = "select o from QuoteDetails o where o.quoteId.id = :quoteId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("quoteId", quoteId);
		return (List<QuoteItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<QuoteItem> findByQuoteAndDetail(Long quoteId, Long detailsId) throws Exception {
		String hql = "select o from QuoteDetails o where o.quoteId.id = :quoteId and o.detailsId.id = :detailsId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("quoteId", quoteId);
		parameters.put("detailsId", detailsId);
		return (List<QuoteItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<QuoteItem> findByName(String description) throws Exception {
		String hql = "select o from QuoteDetails o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<QuoteItem>) super.getList(hql, parameters);
	}
}
