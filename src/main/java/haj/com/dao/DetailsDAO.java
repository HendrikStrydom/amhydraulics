package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Contract;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.enums.ContractDetailTypeEnum;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class DetailsDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> allDetails() throws Exception {
		return (List<ContractReviewItem>) super.getList("select o from Details o");
	}

	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> allDetails(Long from, int noRows) throws Exception {
		String hql = "select o from Details o ";
		Map<String, Object> parameters = new Hashtable<String, Object>();

		return (List<ContractReviewItem>) super.getList(hql, parameters, from.intValue(), noRows);
	}

	public ContractReviewItem findByKey(Long id) throws Exception {
		String hql = "select o from Details o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (ContractReviewItem) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> findByContract(Long id) throws Exception {
		String hql = "select o from Details o where o.contractId.id = :id and (o.softDelete = false or o.softDelete is null) order by o.createDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<ContractReviewItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> findByContractResources(Long id) throws Exception {
		String hql = "select o from Details o where o.contractId.id = :id and o.detailType = :qdTypeResource order by o.createDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		parameters.put("qdTypeResource", ContractDetailTypeEnum.Resource);
		return (List<ContractReviewItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> findByContractMaterials(Long id) throws Exception {
		String hql = "select o from Details o where o.contractId.id = :id and o.detailType = :qdTypeMaterials  order by o.createDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		parameters.put("qdTypeMaterials", ContractDetailTypeEnum.Material);
		return (List<ContractReviewItem>) super.getList(hql, parameters);
	}

}
