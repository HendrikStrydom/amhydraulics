package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.ResourceUser;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ResourceUserDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
	@SuppressWarnings("unchecked")
	public List<ResourceUser> allResourceUser() throws Exception {
		return (List<ResourceUser>)super.getList("select o from ResourceUser o");
	}


	public ResourceUser findByKey(Long id) throws Exception {
	 	String hql = "select o from ResourceUser o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (ResourceUser)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ResourceUser> findByName(String description) throws Exception {
	 	String hql = "select o from ResourceUser o where o.description like :description" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<ResourceUser>)super.getList(hql, parameters);
	}
}

