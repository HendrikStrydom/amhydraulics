package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Blank;
import haj.com.entity.CollectDeliverContract;
import haj.com.entity.Contract;
import haj.com.entity.Doc;
import haj.com.entity.InspectionReport;
import haj.com.entity.PurchaseOrder;
import haj.com.entity.Quote;
import haj.com.entity.RouteCard;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ContractOverviewDownloadDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<InspectionReport> findInspectionReportByContract(Contract contract) throws Exception {
		String hql = "select o from InspectionReport o where o.contract.id =  :contract order by o.createDate desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contract", contract.getId());
		return (List<InspectionReport>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Quote> findQuotesByContract(Contract contract) throws Exception {
		String hql = "select o from Quote o where o.contract.id =  :contract order by o.createDate desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contract", contract.getId());
		return (List<Quote>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<RouteCard> findRouteCardByContract(Contract contract) throws Exception {
		String hql = "select o from RouteCard o where o.contract.id =  :contract order by o.createDate desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contract", contract.getId());
		return (List<RouteCard>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<PurchaseOrder> findPurchaseOrderByContract(Contract contract) throws Exception {
		String hql = "select o from PurchaseOrder o where o.contract.id =  :contract order by o.createDate desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contract", contract.getId());
		return (List<PurchaseOrder>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverContract> findCollectDeliverByContract(Contract contract) throws Exception {
		String hql = "select o from CollectDeliverContract o where o.contract.id =  :contract order by o.createDate desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contract", contract.getId());
		return (List<CollectDeliverContract>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Doc> findDocsByContract(Contract contract) throws Exception {
		String hql = "select o from Doc o where o.contract.id =  :contract";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contract", contract.getId());
		return (List<Doc>) super.getList(hql, parameters);
	}

}
