package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Blank;
import haj.com.entity.Contract;
import haj.com.entity.ContractOverride;
import haj.com.entity.Quote;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ContractDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
	public ContractOverride getOverrideNumber() throws Exception {
		String hql = "select o from ContractOverride o where o.id = 1";
		return (ContractOverride) super.getUniqueResult(hql);
	}
	
	public Long getOverrideNumberByNumber(Integer number) throws Exception {
		String hql = "select count(o) from ContractOverride o where o.overrideNumber = :number";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("number", number);
		return (Long) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> allContract() throws Exception {
		String hql = "select o from Contract o where o.softDelete = :softDelete";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("softDelete", false);
		return (List<Contract>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> allContract(Long from, int noRows) throws Exception {
		String hql = "select o from Contract o ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		return (List<Contract>) super.getList(hql, parameters, from.intValue(), noRows);
	}

	public Contract findByKey(Long id) throws Exception {
		String hql = "select o from Contract o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Contract) super.getUniqueResult(hql, parameters);
	}

	public Contract findByGuid(String guid) throws Exception {
		String hql = "select o from Contract o where o.contractGuid = :guid ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("guid", guid.trim());
		return (Contract) super.getUniqueResult(hql, parameters);
	}

	/**
	 * find by Long ID and where softDelete Boolean is #CONDITION
	 * 
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Contract> findByCustomer(Long customerId) throws Exception {
		String hql = "select o from Contract o where o.customer.id = :customerId and o.softDelete = :softDelete order by o.createDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("customerId", customerId);
		parameters.put("softDelete", false);
		return (List<Contract>) super.getList(hql, parameters);
	}

	/**
	 * find by Long ID and where softDelete Boolean is #CONDITION
	 * 
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Contract> findByCustomerByEnquiry(Long customerId, String description) throws Exception {
		String hql = "select o from Contract o where o.customer.id = :customerId and (o.title like  :description or o.details like  :description or o.enquiryNumber like :description) and o.softDelete = :softDelete order by o.createDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("customerId", customerId);
		parameters.put("description", "%" + description.trim() + "%");
		parameters.put("softDelete", false);
		return (List<Contract>) super.getList(hql, parameters);
	}

	/**
	 * find by Long ID and where softDelete Boolean is #CONDITION and
	 * contractStatusEnum is #CONDITION
	 * 
	 * @param customerId
	 * @param contractStatusEnum
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Contract> findByCustomerContractStatus(Long customerId, ContractStatusEnum contractStatusEnum) throws Exception {
		String hql = "select o from Contract o where o.customer.id = :customerId and o.softDelete = :softDelete and o.contractStatus = :contractStatusEnum order by o.createDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("customerId", customerId);
		parameters.put("contractStatusEnum", contractStatusEnum);
		parameters.put("softDelete", false);
		return (List<Contract>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> findByContractStatus(ContractStatusEnum contractStatusEnum) throws Exception {
		String hql = "select o from Contract o where o.softDelete = :softDelete and o.contractStatus = :contractStatusEnum order by o.createDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contractStatusEnum", contractStatusEnum);
		parameters.put("softDelete", false);
		return (List<Contract>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> findByPossibleNumber(Long id) throws Exception {
		String hql = "select o from Contract o where o.id >=  :id order by o.id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<Contract>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> findByName(String description) throws Exception {
		String hql = "select o from Contract o where (o.title like  :description or o.details like  :description or o.enquiryNumber like  :description or o.contractNumber like  :description) and o.softDelete = :softDelete order by o.title desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "%" + description.trim() + "%");
		parameters.put("softDelete", false);
		return (List<Contract>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> findContractByQueryParameter(String hql, Map<String, Object> parameters) throws Exception {
		return (List<Contract>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public Long findByEnquiryOrContractNumber(String number) throws Exception {
		String hql = "select count(o) from Contract o where o.contractNumber like :number or o.enquiryNumber like :number";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("number", "%"+number+"%");
		return (Long) super.getUniqueResult(hql, parameters);
	}

}
