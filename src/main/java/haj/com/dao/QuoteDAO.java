package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.Contract;
import haj.com.entity.PurchaseOrder;
import haj.com.entity.Quote;
import haj.com.entity.enums.ContractStatusEnum;
import haj.com.entity.enums.QuoteStatusEnum;

public class QuoteDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Quote> allQuote() throws Exception {
		return (List<Quote>) super.getList("select o from Quote o");
	}
	
	@SuppressWarnings("unchecked")
	public List<Quote> allQuoteWithoutSofDel() throws Exception {
		return (List<Quote>) super.getList("select o from Quote o Where o.softDelete = false");
	}
	
	@SuppressWarnings("unchecked")
	public List<Quote> allQuoteBySoftDelete(Boolean softDelete) throws Exception {
		String hql = "select o from Quote o where o.softDelete = :softDelete ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("softDelete", softDelete);
		return (List<Quote>) super.getList(hql, parameters);
	}


	public Quote findByKey(Long id) throws Exception {
		String hql = "select o from Quote o where o.id = :id and o.softDelete = false";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Quote) super.getUniqueResult(hql, parameters);
	}
	
	public Quote findByGuid(String guid) throws Exception {
		String hql = "select o from Quote o where o.quoteGuid = :guid and o.softDelete = false";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("guid", guid);
		return (Quote) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Quote> findByName(String description) throws Exception {
		String hql = "select o from Quote o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<Quote>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Quote> findByContract(Long contractId) throws Exception {
		String hql = "select o from Quote o where o.contract.id = :contractId and o.softDelete = :softDelete ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contractId", contractId);
		parameters.put("softDelete", false);
		return (List<Quote>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Contract> findByContractStatusReturnListOfContracts(QuoteStatusEnum quoteStatus) throws Exception {
		String hql = "select distinct(o.contract) from Quote o where o.quoteStatusEnum = :quoteStatus and o.softDelete = :softDelete ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("quoteStatus", quoteStatus);
		parameters.put("softDelete", false);
		return (List<Contract>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Quote> findQuoteByQueryParameter(String hql, Map<String, Object> parameters) throws Exception {
		return (List<Quote>) super.getList(hql, parameters);
	}
}
