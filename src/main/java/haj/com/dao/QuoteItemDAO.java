package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.QuoteItem;

public class QuoteItemDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see QuoteItem
	 * @return a List of QuoteItem objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<QuoteItem> allQuoteItem() throws Exception {
		return (List<QuoteItem>) super.getList("select o from QuoteItem o");
	}

	public QuoteItem findByKey(Long id) throws Exception {
		String hql = "select o from QuoteItem o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (QuoteItem) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<QuoteItem> findByName(String description) throws Exception {
		String hql = "select o from QuoteItem o where o.description like  :description order by o.description ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<QuoteItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<QuoteItem> findByQuote(Long quoteId) throws Exception {
		String hql = "select o from QuoteItem o where o.quote.id = :quoteId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("quoteId", quoteId);
		return (List<QuoteItem>) super.getList(hql, parameters);
	}
}
