package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.Workflow;

public class WorkflowDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}


	/**
	 * Get all Workflow
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @return List<Workflow>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<Workflow> allWorkflow() throws Exception {
		return (List<Workflow>)super.getList("select o from Workflow o");
	}


	@SuppressWarnings("unchecked")
	public List<Workflow> allWorkflow(Long from, int noRows) throws Exception {
	 	String hql = "select o from Workflow o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<Workflow>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @return Workflow 
 	 * @throws Exception
 	 */
	public Workflow findByKey(Long id) throws Exception {
	 	String hql = "select o from Workflow o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Workflow)super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find Workflow by description
 	 * @author TechFinium 
 	 * @see    Workflow
 	 * @return List<Workflow>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<Workflow> findByName(String description) throws Exception {
	 	String hql = "select o from Workflow o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<Workflow>)super.getList(hql, parameters);
	}
}

