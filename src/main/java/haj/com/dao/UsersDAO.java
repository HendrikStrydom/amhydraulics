package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.Users;
import haj.com.entity.enums.UserTypeEnum;

public class UsersDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * Get all Users
	 * 
	 * @author TechFinium
	 * @see Users
	 * @return List<Users>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Users> allUsers() throws Exception {
		return (List<Users>) super.getList("select o from Users o");
	}

	@SuppressWarnings("unchecked")
	public List<Users> allUsers(Long from, int noRows) throws Exception {
		String hql = "select o from Users o ";
		Map<String, Object> parameters = new Hashtable<String, Object>();

		return (List<Users>) super.getList(hql, parameters, from.intValue(), noRows);
	}

	/**
	 * Find object by primary key
	 * 
	 * @author TechFinium
	 * @see Users
	 * @return Users
	 * @throws Exception
	 */
	public Users findByKey(Long id) throws Exception {
		String hql = "select o from Users o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find Users by description
	 * 
	 * @author TechFinium
	 * @see Users
	 * @return List<Users>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Users> findByName(String description) throws Exception {
		String hql = "select o from Users o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> allUsersNotConfirmed() throws Exception {
		return (List<Users>) super.getList("select o from Users o where o.emailConfirmDate is null");
	}

	public Users getUserByEmail(String email) throws Exception {
		String hql = "select o from Users o where upper(o.email) = :email ";
		// + "and (o.status = " + UsersStatusEnum.Active.ordinal() + ")";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("email", email.toUpperCase());
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public boolean emailAvailable(String email) throws Exception {
		boolean available = false;
		List<Users> l = getAllUserByEmail(email);
		if (l == null || l.size() == 0)
			available = true;
		return available;
	}

	@SuppressWarnings("unchecked")
	public List<Users> getAllUserByEmail(String email) throws Exception {
		String hql = "select o from Users o where upper(o.email) = :email ";
		// + "and (o.status = " + UsersStatusEnum.Active.ordinal() + ")";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("email", email.toUpperCase());
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> findUsersMangers() throws Exception {
		String hql = "select o from Users o where o.userType = :userManager or o.userType = :userEmployee";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userManager", UserTypeEnum.Management);
		parameters.put("userEmployee", UserTypeEnum.Employee);
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> findManagementAndEmployee() throws Exception {
		String hql = "select o from Users o where o.userType = :userManager or o.userType = :userEmployee";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userManager", UserTypeEnum.Management);
		parameters.put("userEmployee", UserTypeEnum.Employee);
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> findMechanic() throws Exception {
		String hql = "select o from Users o where o.userType = :mechanic";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("mechanic", UserTypeEnum.Mechanic);
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> findByUserType(UserTypeEnum userTypeEnum) throws Exception {
		String hql = "select o from Users o where o.userType = :userTypeEnum";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userTypeEnum", userTypeEnum);
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> findUsersEmployee() throws Exception {
		String hql = "select o from Users o where o.userType = :userEmployee ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userEmployee", UserTypeEnum.Employee);
		return (List<Users>) super.getList(hql, parameters);
	}

	public Users checkEmailUsed(String email, Long uid) throws Exception {
		String hql = "select o from Users o where upper(o.email) = :email and o.id <> :uid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("email", email);
		parameters.put("uid", uid);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users checkEmailUsedEmailOnly(String email) throws Exception {
		String hql = "select o from Users o where upper(o.email) = :email";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("email", email);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users findUserByEmailGUID(String emailGuid) throws Exception {
		String hql = "select o from Users o where o.emailGuid = :emailGuid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("emailGuid", emailGuid);
		return (Users) super.getUniqueResult(hql, parameters);
	}

}
