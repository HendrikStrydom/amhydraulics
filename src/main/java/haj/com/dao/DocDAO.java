package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Contract;
import haj.com.entity.Doc;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class DocDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Doc> search(Long id) throws Exception {
		String hql = "select o from Doc o  where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<Doc>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Doc> findByContract(Contract contract) throws Exception {
		String hql = "select o from Doc o  where o.contract.id = :id " + " order by o.id desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", contract.getId());
		return (List<Doc>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Doc> findByCollectDeliverContract(Long collectDeliverContractId) throws Exception {
		String hql = "select o from Doc o  where o.collectDeliverContract.id = :collectDeliverContractId " + " order by o.id desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("collectDeliverContractId", collectDeliverContractId);
		return (List<Doc>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Doc> findByCollectDeliverItem(Long collectDeliverItemId) throws Exception {
		String hql = "select o from Doc o  where o.collectDeliverItem.id = :collectDeliverItemId " + " order by o.id desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("collectDeliverItemId", collectDeliverItemId);
		return (List<Doc>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Doc> findFileName(String filename, Contract contract) throws Exception {
		String hql = "select o from Doc o  where o.contract.id = :id and o.originalFname like :filename order by o.id desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", contract.getId());
		parameters.put("filename", "%" + filename + "%");
		return (List<Doc>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Doc> searchParent(Long id) throws Exception {
		String hql = "select o from Doc o  where o.doc.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<Doc>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Doc> getVersions(Doc d) throws Exception {
		String hql = "select o from Doc o  where o.doc.id = :id order by o.versionNo desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", d.getId());
		return (List<Doc>) super.getList(hql, parameters);
	}
}
