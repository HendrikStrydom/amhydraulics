package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.Customer;

public class CustomerDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}


	/**
	 * Get all Customer
 	 * @author TechFinium 
 	 * @see    Customer
 	 * @return List<Customer>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<Customer> allCustomer() throws Exception {
		return (List<Customer>)super.getList("select o from Customer o");
	}


	@SuppressWarnings("unchecked")
	public List<Customer> allCustomer(Long from, int noRows) throws Exception {
	 	String hql = "select o from Customer o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<Customer>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    Customer
 	 * @return Customer 
 	 * @throws Exception
 	 */
	public Customer findByKey(Long id) throws Exception {
	 	String hql = "select o from Customer o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Customer)super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find Customer by description
 	 * @author TechFinium 
 	 * @see    Customer
 	 * @return List<Customer>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<Customer> findByName(String customerName) throws Exception {
	 	String hql = "select o from Customer o where o.customerName like  :customerName order by o.customerName " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("customerName", "%"+customerName.trim()+"%");
		return (List<Customer>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Customer> find(Customer customer) throws Exception {
	 	String hql = "select o from Customer o "
	 			+ "where (o.customerName like  :customerName) "
	 			+ " or (o.phoneNumber like :phoneNumber) "
	 			+ " or (o.email like :email) "
	 			+ " order by o.customerName " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("customerName", "%"+customer.getCustomerName()==null?"":customer.getCustomerName().trim()+"%");
	    parameters.put("phoneNumber", "%"+customer.getPhoneNumber()==null?"":customer.getPhoneNumber().trim()+"%");
	    parameters.put("email", "%"+customer.getCompanyEmail()==null?"":customer.getCompanyEmail().trim()+"%");
		return (List<Customer>)super.getList(hql, parameters);
	}
}

