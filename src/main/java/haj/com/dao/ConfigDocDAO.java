package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Blank;
import haj.com.entity.ConfigDoc;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ConfigDocDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see Blank
	 * @return a List of Blank objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<ConfigDoc> allConfigDoc() throws Exception {
		return (List<ConfigDoc>) super.getList("select o from ConfigDoc o");
	}

	@SuppressWarnings("unchecked")
	public List<ConfigDoc> allConfigDocActive() throws Exception {
		return (List<ConfigDoc>) super.getList("select o from ConfigDoc o where o.active = 1");
	}

	public ConfigDoc findByKey(Long id) throws Exception {
		String hql = "select o from ConfigDoc o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (ConfigDoc) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ConfigDoc> findByName(String name) throws Exception {
		String hql = "select o from ConfigDoc o where o.name like  :name and  o.active = 1 order by o.name desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("name", "%" + name.trim() + "%");
		return (List<ConfigDoc>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public Boolean findIfUniqueUsed(String uniqueId) throws Exception {
		String hql = "select o from ConfigDoc o where o.uniqueId like  :uniqueId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uniqueId", "" + uniqueId.trim() + "");
		return (Boolean) (super.getList(hql, parameters).size() > 0);
	}
}
