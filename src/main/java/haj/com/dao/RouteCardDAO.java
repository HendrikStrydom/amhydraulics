package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.Blank;
import haj.com.entity.ConfigDoc;
import haj.com.entity.ContractReviewItem;
import haj.com.entity.Quote;
import haj.com.entity.RouteCard;
import haj.com.entity.RouteCardItem;

public class RouteCardDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<RouteCard> allRouteCard() throws Exception {
		return (List<RouteCard>) super.getList("select o from RouteCard o where o.softDelete = false");
	}

	@SuppressWarnings("unchecked")
	public List<RouteCard> allDeletedRouteCard() throws Exception {
		return (List<RouteCard>) super.getList("select o from RouteCard o where o.softDelete = true");
	}

	@SuppressWarnings("unchecked")
	public List<RouteCard> allNotDeletedRouteCard() throws Exception {
		return (List<RouteCard>) super.getList("select o from RouteCard o where o.softDelete = false");
	}

	public RouteCard findByKey(Long id) throws Exception {
		String hql = "select o from RouteCard o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (RouteCard) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<RouteCardItem> findRCIByRCId(Long rcId) throws Exception {
		String hql = "select o from RouteCardItem o where o.routeCard.id = :rcId and o.routeCard.softDelete = false";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("rcId", rcId);
		return (List<RouteCardItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<RouteCard> findByName(String description) throws Exception {
		String hql = "select o from RouteCard o where o.description like  :description and o.softDelete = false order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<RouteCard>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<RouteCard> findByContract(Long contractId) throws Exception {
		String hql = "select o from RouteCard o where o.contract.id = :contractId and o.softDelete = false";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contractId", contractId);
		return (List<RouteCard>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<RouteCard> findRouteCardByQueryParameter(String hql, Map<String, Object> parameters) throws Exception {
		return (List<RouteCard>) super.getList(hql, parameters);
	}

}
