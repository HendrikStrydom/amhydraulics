package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.Address;
import haj.com.entity.enums.AddressTypeEnum;

public class AddressDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}


	/**
	 * Get all Address
 	 * @author TechFinium 
 	 * @see    Address
 	 * @return List<Address>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<Address> allAddress() throws Exception {
		return (List<Address>)super.getList("select o from Address o");
	}


	@SuppressWarnings("unchecked")
	public List<Address> allAddress(Long from, int noRows) throws Exception {
	 	String hql = "select o from Address o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<Address>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    Address
 	 * @return Address 
 	 * @throws Exception
 	 */
	public Address findByKey(Long id) throws Exception {
	 	String hql = "select o from Address o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Address)super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find Address by description
 	 * @author TechFinium 
 	 * @see    Address
 	 * @return List<Address>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<Address> findByName(String description) throws Exception {
	 	String hql = "select o from Address o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<Address>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Address> findByCompany(Long id) throws Exception {
	 	String hql = "select o from Address o where o.company.id = :id order by o.createDate desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (List<Address>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Address> findByUser(Long id) throws Exception {
	 	String hql = "select o from Address o where o.user.id = :id order by o.createDate desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (List<Address>)super.getList(hql, parameters);
	}
	
	
	public Address findByCustAndType(Long customerId, AddressTypeEnum addressType) throws Exception {
	 	String hql = "select o from Address o where o.customer.id = :customerId and o.addressType = :addressType" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("customerId", customerId);
	    parameters.put("addressType", addressType);
		return (Address)super.getUniqueResult(hql, parameters);
	}
}

