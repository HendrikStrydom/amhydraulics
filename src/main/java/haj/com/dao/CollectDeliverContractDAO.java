package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.CollectDeliverContract;
import haj.com.entity.enums.CollectDeliverEnum;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class CollectDeliverContractDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverContract> allCollectDeliverContract() throws Exception {
		return (List<CollectDeliverContract>) super.getList("select o from CollectDeliverContract o");
	}

	public CollectDeliverContract findByKey(Long id) throws Exception {
		String hql = "select o from CollectDeliverContract o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (CollectDeliverContract) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverContract> findByName(String description) throws Exception {
		String hql = "select o from CollectDeliverContract o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<CollectDeliverContract>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<CollectDeliverEnum> findByOverallStatus(CollectDeliverContract collectDeliverContract) throws Exception {
		String hql = "select o.collectionDelivery from CollectDeliverItem o where o.collectDeliverContract = :collectDeliverContract order by o.collectionDelivery asc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("collectDeliverContract", collectDeliverContract);
		return (List<CollectDeliverEnum>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverContract> findCollectDeliverContractByQueryParameter(String hql, Map<String, Object> parameters) throws Exception {
		return (List<CollectDeliverContract>) super.getList(hql, parameters);
	}
}
