package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.CustomerUsers;

public class CustomerUsersDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}


	/**
	 * Get all CustomerUsers
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @return List<CustomerUsers>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<CustomerUsers> allCustomerUsers() throws Exception {
		return (List<CustomerUsers>)super.getList("select o from CustomerUsers o");
	}


	@SuppressWarnings("unchecked")
	public List<CustomerUsers> allCustomerUsers(Long from, int noRows) throws Exception {
	 	String hql = "select o from CustomerUsers o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<CustomerUsers>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @return CustomerUsers 
 	 * @throws Exception
 	 */
	public CustomerUsers findByKey(Long id) throws Exception {
	 	String hql = "select o from CustomerUsers o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (CustomerUsers)super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find CustomerUsers by description
 	 * @author TechFinium 
 	 * @see    CustomerUsers
 	 * @return List<CustomerUsers>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<CustomerUsers> findByName(String description) throws Exception {
	 	String hql = "select o from CustomerUsers o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<CustomerUsers>)super.getList(hql, parameters);
	}
}

