package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.CollectDeliverItem;
import haj.com.entity.PurchaseOrder;

public class CollectDeliverItemDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverItem> allCollectDeliverItem() throws Exception {
		return (List<CollectDeliverItem>) super.getList("select o from CollectDeliverItem o");
	}

	public CollectDeliverItem findByKey(Long id) throws Exception {
		String hql = "select o from CollectDeliverItem o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (CollectDeliverItem) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverItem> findByName(String description) throws Exception {
		String hql = "select o from CollectDeliverItem o where o.description like  :description";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<CollectDeliverItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverItem> findByCollectionDeliveryContract(Long cdcId) throws Exception {
		String hql = "select o from CollectDeliverItem o where o.collectDeliverContract.id = :cdcId and o.softDelete = false";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("cdcId", cdcId);
		return (List<CollectDeliverItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CollectDeliverItem> findCollectDeliverItemByQueryParameter(String hql, Map<String, Object> parameters) throws Exception {
		return (List<CollectDeliverItem>) super.getList(hql, parameters);
	}
}
