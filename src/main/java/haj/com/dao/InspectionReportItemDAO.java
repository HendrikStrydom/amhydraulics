package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.InspectionReportItem;

public class InspectionReportItemDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see InspectionReportItem
	 * @return a List of InspectionReportItem objects inspectionReport
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<InspectionReportItem> allInspectionReportItem() throws Exception {
		return (List<InspectionReportItem>) super.getList("select o from InspectionReportItem o");
	}

	@SuppressWarnings("unchecked")
	public List<InspectionReportItem> allInspectionReportItemWhereInspectionReportIsNull() throws Exception {
		return (List<InspectionReportItem>) super.getList("select o from InspectionReportItem o where o.inspectionReport is null order by o.lineNumber asc");
	}

	public InspectionReportItem findByKey(Long id) throws Exception {
		String hql = "select o from InspectionReportItem o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (InspectionReportItem) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<InspectionReportItem> findByName(String description) throws Exception {
		String hql = "select o from InspectionReportItem o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<InspectionReportItem>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<InspectionReportItem> findByInspectionReport(Long inspectionReportId) throws Exception {
		String hql = "select o from InspectionReportItem o where o.inspectionReport.id = :inspectionReportId  order by o.lineNumber asc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("inspectionReportId",inspectionReportId);
		return (List<InspectionReportItem>) super.getList(hql, parameters);
	}
}
