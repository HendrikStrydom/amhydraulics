package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.Tasks;
import haj.com.entity.enums.TaskStatusEnum;

public class TasksDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> allTasks() throws Exception {
		return (List<Tasks>) super.getList("select o from Tasks o");
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> allTasks(Long from, int noRows) throws Exception {
		String hql = "select o from Tasks o ";
		Map<String, Object> parameters = new Hashtable<String, Object>();

		return (List<Tasks>) super.getList(hql, parameters, from.intValue(), noRows);
	}

	public Tasks findByKey(Long id) throws Exception {
		String hql = "select o from Tasks o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Tasks) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> findTaskListByKey(Long id) throws Exception {
		String hql = "select o from Tasks o where o.id = :id and (o.taskStatus != :taskStatusCompleted and o.taskStatus != :taskStatusClosed)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		parameters.put("taskStatusCompleted", TaskStatusEnum.Completed);
		parameters.put("taskStatusClosed", TaskStatusEnum.Closed);
		return (List<Tasks>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> findByName(String description) throws Exception {
		String hql = "select o from Tasks o where o.taskDescription like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<Tasks>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> findTasksByUser(Long userId) throws Exception {
		String hql = "select distinct(o.task) from TaskUsers o where o.user.id  = :userId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		return (List<Tasks>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> findTasksByUserContract(Long userId, Long contractId) throws Exception {
		String hql = "select distinct(o.task) from TaskUsers o where o.user.id  = :userId and o.task.targetKey = :contractId and (o.task.taskStatus != :taskStatusCompleted and o.task.taskStatus != :taskStatusClosed)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		parameters.put("contractId", contractId);
		parameters.put("taskStatusCompleted", TaskStatusEnum.Completed);
		parameters.put("taskStatusClosed", TaskStatusEnum.Closed);
		return (List<Tasks>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public Boolean findTasksByContractB(Long contractId) throws Exception {
		String hql = "select distinct(o.task) from TaskUsers o where o.task.targetKey = :contractId and (o.task.taskStatus != :taskStatusCompleted and o.task.taskStatus != :taskStatusClosed)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contractId", contractId);
		parameters.put("taskStatusCompleted", TaskStatusEnum.Completed);
		parameters.put("taskStatusClosed", TaskStatusEnum.Closed);
		return (Boolean) ((super.getList(hql, parameters)).size() > 0);
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> findTasksByUserIncomplete(Long userId) throws Exception {
		String hql = "select distinct(o.task) from TaskUsers o where o.user.id  = :userId and o.task.taskStatus <> :taskStatusCompleted and o.task.taskStatus <> :taskStatusClosed order by o.task.createDate desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		parameters.put("taskStatusCompleted", TaskStatusEnum.Completed);
		parameters.put("taskStatusClosed", TaskStatusEnum.Closed);
		return (List<Tasks>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> findPriorityTasksByUserIncomplete(Long userId) throws Exception {
		String hql = "select distinct(o.task) from TaskUsers o where o.user.id  = :userId " + "and o.task.taskStatus <> :taskStatusCompleted and o.task.taskStatus <> :taskStatusClosed order by o.task.dueDate";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		parameters.put("taskStatusCompleted", TaskStatusEnum.Completed);
		parameters.put("taskStatusClosed", TaskStatusEnum.Closed);
		return (List<Tasks>) super.getList(hql, parameters, 0, 5);
	}

	@SuppressWarnings("unchecked")
	public List<Tasks> findTasksByUserAndStatus(Long userId, TaskStatusEnum status) throws Exception {
		String hql = "select distinct(o.task) from TaskUsers o where o.user.id  = :userId " + "and o.task.taskStatus = :status";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		parameters.put("status", status);
		return (List<Tasks>) super.getList(hql, parameters);
	}
}
