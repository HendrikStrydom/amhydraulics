package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.InspectionReport;
import haj.com.entity.RouteCard;

public class InspectionReportDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see InspectionReport
	 * @return a List of InspectionReport objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<InspectionReport> allInspectionReport() throws Exception {
		return (List<InspectionReport>) super.getList("select o from InspectionReport o where o.softDelete = false");
	}

	public InspectionReport findByKey(Long id) throws Exception {
		String hql = "select o from InspectionReport o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (InspectionReport) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<InspectionReport> findByName(String description) throws Exception {
		String hql = "select o from InspectionReport o where o.formNumber like  :description ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "%" + description.trim() + "%");
		return (List<InspectionReport>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<InspectionReport> findByContract(Long contractId) throws Exception {
		String hql = "select o from InspectionReport o where o.contract.id = :contractId and o.softDelete = :softDelete";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contractId", contractId);
		parameters.put("softDelete", false);
		return (List<InspectionReport>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<InspectionReport> findInspectionReportByQueryParameter(String hql, Map<String, Object> parameters) throws Exception {
		return (List<InspectionReport>) super.getList(hql, parameters);
	}

}
