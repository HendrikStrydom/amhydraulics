package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.techfinium.entity.Resource;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;
import haj.com.entity.Blank;
import haj.com.entity.ResourceUsers;
import haj.com.entity.Users;

public class ResourceUserAllocationDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Users> findUsersThatAreNotAdmin() throws Exception {
		return (List<Users>) super.getList("select o from Users o where o.userType != 0");
	}

	@SuppressWarnings("unchecked")
	public List<Resource> findResourceByName(String description) throws Exception {
		String hql = "select o from Resource o where o.description like  :description ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "%" + description.trim() + "%");
		return (List<Resource>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ResourceUsers> getAllResourceUsers() throws Exception {
		String hql = "select o from ResourceUsers o";
		return (List<ResourceUsers>) super.getList(hql);
	}

	@SuppressWarnings("unchecked")
	public List<Blank> allBlank() throws Exception {
		return (List<Blank>) super.getList("select o from Blank o");
	}

	public Blank findByKey(Long id) throws Exception {
		String hql = "select o from Blank o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Blank) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Blank> findByName(String description) throws Exception {
		String hql = "select o from Blank o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<Blank>) super.getList(hql, parameters);
	}
}
