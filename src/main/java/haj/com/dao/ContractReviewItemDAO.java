package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.ContractReviewItem;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ContractReviewItemDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see ContractReviewItem
	 * @return a List of ContractReviewItem objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> allContractReviewItem() throws Exception {
		return (List<ContractReviewItem>) super.getList("select o from ContractReviewItem o");
	}

	public ContractReviewItem findByKey(Long id) throws Exception {
		String hql = "select o from ContractReviewItem o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (ContractReviewItem) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> findByDescription(String description) throws Exception {
		String hql = "select o from ContractReviewItem o where o.description like  :description order by o.description desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<ContractReviewItem>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ContractReviewItem> findByContract(Long contractId) throws Exception {
		String hql = "select o from ContractReviewItem o where o.contract.id = :contractId and o.softDelete = :softDelete order by o.createDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("contractId", contractId);
		parameters.put("softDelete", false);
		return (List<ContractReviewItem>) super.getList(hql, parameters);
	}
}
