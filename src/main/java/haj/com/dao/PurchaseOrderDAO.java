package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.PurchaseOrder;
import haj.com.entity.PurchaseOrderItem;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class PurchaseOrderDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see PurchaseOrderItem
	 * @return a List of PurchaseOrderItem objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<PurchaseOrderItem> allPurchaseOrderItem() throws Exception {
		return (List<PurchaseOrderItem>) super.getList("select o from PurchaseOrderItem o");
	}

	/**
	 * @author JMRUK
	 * @see PurchaseOrder
	 * @return a List of PurchaseOrder objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<PurchaseOrder> allPurchaseOrder() throws Exception {
		String hql = "select o from PurchaseOrder o where o.softDelete = :softDelete";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("softDelete", false);
		return (List<PurchaseOrder>) super.getList(hql, parameters);
	}

	public PurchaseOrderItem findPurchaseOrderItemByKey(Long id) throws Exception {
		String hql = "select o from PurchaseOrderItem o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (PurchaseOrderItem) super.getUniqueResult(hql, parameters);
	}

	public PurchaseOrder findPurchaseOrderByKey(Long id) throws Exception {
		String hql = "select o from PurchaseOrder o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (PurchaseOrder) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<PurchaseOrder> findPurchaseOrderByName(String description) throws Exception {
		String hql = "select o from PurchaseOrder o where o.description like  :description";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<PurchaseOrder>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<PurchaseOrderItem> findPurchaseOrderItemByName(String description) throws Exception {
		String hql = "select o from PurchaseOrderItem o where o.description like  :description ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<PurchaseOrderItem>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<PurchaseOrderItem> findPurchaseOrderItemByPurchaseOrderId(Long purchaseOrderId) throws Exception {
		String hql = "select o from PurchaseOrderItem o where o.purchaseOrder.id = :purchaseOrderId order by o.createDate asc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("purchaseOrderId", purchaseOrderId);
		return (List<PurchaseOrderItem>) super.getList(hql, parameters);
	}

	public Double calculateTotalForPurchaseOrder(Long purchaseOrderId) throws Exception {
		String hql = "select SUM(o.price*o.quantity) from PurchaseOrderItem o where o.purchaseOrder.id = :purchaseOrderId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("purchaseOrderId", purchaseOrderId);
		return (Double) super.getUniqueResult(hql, parameters);
	}

	public List<PurchaseOrder> findPurchaseOrderByQueryParameter(String hql, Map<String, Object> parameters) throws Exception {
		return (List<PurchaseOrder>) super.getList(hql, parameters);
	}

}
