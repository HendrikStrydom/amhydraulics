import java.util.Date;
import java.util.List;

import com.ibm.icu.text.SimpleDateFormat;
import com.techfinium.entity.ResourceAllocation;
import com.techfinium.service.ResourceAllocationService;

import haj.com.entity.AMResourceAllocation;
import haj.com.entity.Blank;
import haj.com.entity.Users;
import haj.com.service.BlankService;
import haj.com.utils.GenericUtility;

public class TestHibernate {

	public static void main(String[] args) {
		try {
		    System.out.println("START");
			ResourceAllocationService service = new ResourceAllocationService();
			ResourceAllocation ra = new ResourceAllocation();
            ra.setDescription("Sample");
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            
            ra.setFromDateTime(sdf.parse("14-07-2017 12:30"));
            ra.setToDateTime(sdf.parse("14-07-2017 13:30"));
            service.create(ra);
			 System.out.println("DONE");
			
			 
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
       System.exit(0);
	}
}
