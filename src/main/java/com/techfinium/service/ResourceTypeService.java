package com.techfinium.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.techfinium.dao.ResourceTypeDAO;
import com.techfinium.entity.Resource;
import com.techfinium.entity.ResourceType;
import com.techfinium.framework.AbstractService;

public class ResourceTypeService extends AbstractService {

	private ResourceTypeDAO dao = new ResourceTypeDAO();
	
public void create(ResourceType entity) throws Exception {
		
		if(entity.getCreateDate() == null)
			entity.setCreateDate(new Date());
		
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else
			this.dao.update(entity);
	}

	public void delete(ResourceType entity) throws Exception {
		this.dao.delete(entity);
	}

	public void update(ResourceType entity) throws Exception {
		this.dao.update(entity);
	}
	
	
	public void save(ResourceType entity) throws Exception  {
		   if (entity.getId() ==null)
			 this.dao.create(entity);
			else
			 this.dao.update(entity);
		}
	
	
	public ResourceType findByKey(Long id) throws Exception { 
		return dao.findByKey(id);
	}
	

    /**
     * @author TechFinium 
     * @param class1 Resource class
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<Resource> containing data
     */	
	@SuppressWarnings("unchecked")
	public List<ResourceType> allResourceType(Class<ResourceType> class1, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<ResourceType>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}
	
	public List<ResourceType> allResourceType() throws Exception {
	  	return dao.allResourceType();
	}
	
	
    /**
     * 
     * @param entity Resource class
     * @param filters
     * @return Number of rows in the Contract entity
     */	
	public int count(Class<ResourceType> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
	
}
