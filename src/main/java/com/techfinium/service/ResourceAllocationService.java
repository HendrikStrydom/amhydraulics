package com.techfinium.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.techfinium.dao.ResourceAllocationDAO;
import com.techfinium.entity.Resource;
import com.techfinium.entity.ResourceAllocation;
import com.techfinium.entity.enums.IntervalTypeEnum;
import com.techfinium.framework.AbstractService;
import com.techfinium.framework.IDataEntity;
import com.techfinium.utils.GenericUtils;



public class ResourceAllocationService extends AbstractService {

	public ResourceAllocationDAO dao = new ResourceAllocationDAO();

	public void create(IDataEntity entity) throws Exception {
		if (entity==null) throw new Exception("The object can't be null before persistance");
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else
			this.dao.update(entity);
	}

	public void delete(IDataEntity entity) throws Exception {
		this.dao.delete(entity);
	}



	public ResourceAllocation findByKey(Long id) throws Exception {
		return dao.findByKey(id);
	}
	

/*	public boolean resourceAvailable(Resource resource,Date from, IntervalTypeEnum interval) throws Exception {
		boolean available = true;
		
		return available;
	}
	*/
	
	
   public List<ResourceAllocation> allResourceAllocation(Date fromDate) throws Exception {
			  return dao.allResourceAllocation( fromDate);
	}	
	
   
   public List<IDataEntity> allResourceAllocation(Date fromDate, Date toDate) throws Exception {
	
		  return dao.allResourceAllocation(fromDate,toDate);
}	
   
   public List<ResourceAllocation> resourceAllocation(Resource resource,Date fromDate) throws Exception {
	  return dao.resourceAllocation(resource.getId(), fromDate);
   }

}
