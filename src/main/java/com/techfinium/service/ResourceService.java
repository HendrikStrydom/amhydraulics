package com.techfinium.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.techfinium.dao.ResourceDAO;
import com.techfinium.entity.Resource;
import com.techfinium.entity.ResourceAllocation;
import com.techfinium.framework.AbstractService;



public class ResourceService extends AbstractService {

	private ResourceDAO dao = new ResourceDAO();

	
	public List<Resource> allResources() throws Exception {
		return dao.allResources();
	}
	
	public void create(Resource entity) throws Exception {
		
		if(entity.getCreateDate() == null)
			entity.setCreateDate(new Date());
		
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else
			this.dao.update(entity);
	}

	public void delete(Resource entity) throws Exception {
		this.dao.delete(entity);
	}

	public void update(Resource entity) throws Exception {
		this.dao.update(entity);
	}

	public void save(Resource entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

	public Resource findByKey(Long id) throws Exception {
		return dao.findByKey(id);
	}
	
	public List<Resource> findByName(String description) throws Exception{
		return dao.findByName(description);
	}
	


    /**
     * @author TechFinium 
     * @param class1 Resource class
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<Resource> containing data
     */	
	@SuppressWarnings("unchecked")
	public List<Resource> allResource(Class<Resource> class1, int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<Resource>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}
	
	
    /**
     * 
     * @param entity Resource class
     * @param filters
     * @return Number of rows in the Contract entity
     */	
	public int count(Class<Resource> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
	
}
