package com.techfinium.entity.enums;

public enum IntervalTypeEnum {

  Days("Days"), Hours("Hours")  ;

  private String displayName;

  private IntervalTypeEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
