package com.techfinium.framework;

import java.io.Serializable;

public interface IDataEntity extends Serializable {
	public Long getId();
	public void setId(Long id);
}
