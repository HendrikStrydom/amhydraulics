package com.techfinium.framework;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class AbstractService implements Serializable
{
  private static final long serialVersionUID = 1L;
  
  protected final Log logger = LogFactory.getLog(this.getClass());
 
}
