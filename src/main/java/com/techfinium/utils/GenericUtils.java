package com.techfinium.utils;

import java.util.Date;
import org.joda.time.LocalDateTime;

public class GenericUtils {

	public static Date getStartOfDay(Date date) {
		return (new LocalDateTime(date.getTime()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0)
				.withMillisOfSecond(0)).toDate();
	}

	public static Date getEndOfDay(Date date) {
		return (new LocalDateTime(date.getTime()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59)
				.withMillisOfSecond(999)).toDate();
	}
}
