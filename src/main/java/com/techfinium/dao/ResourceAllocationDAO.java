package com.techfinium.dao;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.techfinium.entity.ResourceAllocation;
import com.techfinium.framework.AbstractDAO;
import com.techfinium.framework.AbstractDataProvider;
import com.techfinium.framework.IDataEntity;
import com.techfinium.framework.MySQLProvider;


public class ResourceAllocationDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    Blank
 * @return a List of Blank objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<ResourceAllocation> allBlank() throws Exception {
		return (List<ResourceAllocation>)super.getList("select o from ResourceAllocation o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<Blank> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from Blank o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<Blank>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<Blank> byField(long key) throws Exception  {
		String hql = "select o from Blank o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<Blank>)super.getList(hql, parameters);
	}
*/

	public ResourceAllocation findByKey(Long id) throws Exception {
	 	String hql = "select o from ResourceAllocation o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (ResourceAllocation)super.getUniqueResult(hql, parameters);
	}

	
	@SuppressWarnings("unchecked")
	public List<ResourceAllocation> resourceAllocation(Long resourceId, Date fromDate) throws Exception {
	 	String hql = "select o from ResourceAllocation o where o.resource.id = :resourceId and o.fromDateTime >= :fromDate order by o.fromDateTime" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("resourceId", resourceId);
	    parameters.put("fromDate", fromDate);
		return (List<ResourceAllocation>)super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ResourceAllocation> allResourceAllocation(Date fromDate) throws Exception {
	 	String hql = "select o from ResourceAllocation o where o.fromDateTime >= :fromDate order by o.fromDateTime" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("fromDate", fromDate);
		return (List<ResourceAllocation>)super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<IDataEntity> allResourceAllocation(Date fromDate, Date toDate) throws Exception {
	 	String hql = "select o from ResourceAllocation o "
	 			+ " where o.fromDateTime between :fromDate and :toDate "
	 			+ " or o.toDateTime between :fromDate and :toDate ";
	 
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("fromDate", fromDate);
	    parameters.put("toDate", toDate);
		return (List<IDataEntity>)super.getList(hql, parameters);
	}
	

}

