package com.techfinium.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.techfinium.entity.Resource;
import com.techfinium.entity.ResourceType;
import com.techfinium.framework.AbstractDAO;
import com.techfinium.framework.AbstractDataProvider;
import com.techfinium.framework.MySQLProvider;


public class ResourceTypeDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    Blank
 * @return a List of Blank objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<ResourceType> allBlank() throws Exception {
		return (List<ResourceType>)super.getList("select o from ResourceType o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<Blank> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from Blank o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<Blank>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<Blank> byField(long key) throws Exception  {
		String hql = "select o from Blank o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<Blank>)super.getList(hql, parameters);
	}
*/

	public ResourceType findByKey(Long id) throws Exception {
	 	String hql = "select o from ResourceType o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (ResourceType)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ResourceType> findByName(String description) throws Exception {
	 	String hql = "select o from ResourceType o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<ResourceType>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ResourceType> allResourceType() throws Exception {
		return (List<ResourceType>)super.getList("select o from ResourceType o");
	}
	
}

