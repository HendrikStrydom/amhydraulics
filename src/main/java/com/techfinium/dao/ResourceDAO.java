package com.techfinium.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.techfinium.entity.Resource;
import com.techfinium.entity.ResourceAllocation;
import com.techfinium.framework.AbstractDAO;
import com.techfinium.framework.AbstractDataProvider;
import com.techfinium.framework.MySQLProvider;


public class ResourceDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    Blank
 * @return a List of Blank objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<Resource> allResources() throws Exception {
		return (List<Resource>)super.getList("select o from Resource o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<Blank> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from Blank o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<Blank>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<Blank> byField(long key) throws Exception  {
		String hql = "select o from Blank o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<Blank>)super.getList(hql, parameters);
	}
*/

	public Resource findByKey(Long id) throws Exception {
	 	String hql = "select o from Resource o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Resource)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Resource> findByName(String description) throws Exception {
	 	String hql = "select o from Resource o where o.description like  :description " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", "%"+description.trim()+"%");
		return (List<Resource>)super.getList(hql, parameters);
	}

	
	

}

