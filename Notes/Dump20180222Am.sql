CREATE DATABASE  IF NOT EXISTS `amhydraulics` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `amhydraulics`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: amhydraulics
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address_line_1` varchar(100) DEFAULT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `address_line_3` varchar(100) DEFAULT NULL,
  `address_line_4` varchar(100) DEFAULT NULL,
  `address_type` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `primary_addr` bit(1) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK93c3js0e22ll1xlu21nvrhqgg` (`customer_id`),
  KEY `FK6i66ijb8twgcqtetl8eeeed6v` (`user_id`),
  CONSTRAINT `FK6i66ijb8twgcqtetl8eeeed6v` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK93c3js0e22ll1xlu21nvrhqgg` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collect_deliver_contract`
--

DROP TABLE IF EXISTS `collect_deliver_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collect_deliver_contract` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_date` datetime DEFAULT NULL,
  `cdn_number` varchar(15) DEFAULT NULL,
  `collect_deliver_enum` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `note` longtext,
  `revision_number` int(11) DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `contract_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5ae43jso2qhe46bn188dnoijs` (`action_user`),
  KEY `FK2uqj4oslkxv5p59n9vad0akf` (`contract_id`),
  CONSTRAINT `FK2uqj4oslkxv5p59n9vad0akf` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`),
  CONSTRAINT `FK5ae43jso2qhe46bn188dnoijs` FOREIGN KEY (`action_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collect_deliver_contract`
--

LOCK TABLES `collect_deliver_contract` WRITE;
/*!40000 ALTER TABLE `collect_deliver_contract` DISABLE KEYS */;
/*!40000 ALTER TABLE `collect_deliver_contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collect_deliver_contract_hist_v2`
--

DROP TABLE IF EXISTS `collect_deliver_contract_hist_v2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collect_deliver_contract_hist_v2` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `action_date` datetime DEFAULT NULL,
  `cdn_number` varchar(15) DEFAULT NULL,
  `collect_deliver_enum` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `note` longtext,
  `revision_number` int(11) DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `contract_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKanjjmfhk4jg4eq3clrnwa23g` (`REV`),
  CONSTRAINT `FKanjjmfhk4jg4eq3clrnwa23g` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collect_deliver_contract_hist_v2`
--

LOCK TABLES `collect_deliver_contract_hist_v2` WRITE;
/*!40000 ALTER TABLE `collect_deliver_contract_hist_v2` DISABLE KEYS */;
/*!40000 ALTER TABLE `collect_deliver_contract_hist_v2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collect_deliver_item`
--

DROP TABLE IF EXISTS `collect_deliver_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collect_deliver_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_date` datetime DEFAULT NULL,
  `collect_deliver_enum` int(11) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `description` longtext,
  `price_per_item` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `size` longtext,
  `soft_delete` bit(1) DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `collect_deliver_contract_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKm98460i63nvmjxci9b0e4yfno` (`action_user`),
  KEY `FKq9syt90nga96kq0dq1t7adntc` (`collect_deliver_contract_id`),
  CONSTRAINT `FKm98460i63nvmjxci9b0e4yfno` FOREIGN KEY (`action_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FKq9syt90nga96kq0dq1t7adntc` FOREIGN KEY (`collect_deliver_contract_id`) REFERENCES `collect_deliver_contract` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collect_deliver_item`
--

LOCK TABLES `collect_deliver_item` WRITE;
/*!40000 ALTER TABLE `collect_deliver_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `collect_deliver_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_doc`
--

DROP TABLE IF EXISTS `config_doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_doc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `abreviation` varchar(50) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `unique_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_doc`
--

LOCK TABLES `config_doc` WRITE;
/*!40000 ALTER TABLE `config_doc` DISABLE KEYS */;
INSERT INTO `config_doc` VALUES (1,'poc','','Purchase Order (Customer)','poc'),(2,'Pgf','','Program File','Pgf'),(3,'Drw','','Drawing','Drw'),(4,'CAD','','CAD File','CAD'),(5,'Mis','','Miscellaneous','Mis'),(6,'Irpt','','Inspection Report','Irpt'),(7,'SIR','','Strip and Inspect Report','SIR'),(8,'Ins','','Invoice (Supplier)','Ins');
/*!40000 ALTER TABLE `config_doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract`
--

DROP TABLE IF EXISTS `contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_date` datetime DEFAULT NULL,
  `chief_inspector_comment` longtext,
  `conformity_certificate_b` bit(1) DEFAULT NULL,
  `contract_guid` varchar(100) DEFAULT NULL,
  `contract_number` varchar(15) DEFAULT NULL,
  `contract_status_enum` int(11) DEFAULT NULL,
  `contract_type_enum` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `details` longtext,
  `deviations_comment` longtext,
  `dimensional_report_b` bit(1) DEFAULT NULL,
  `drawing_number` longtext,
  `enquiry_number` varchar(15) DEFAULT NULL,
  `item_number` longtext,
  `material_certificate_b` bit(1) DEFAULT NULL,
  `order_number` longtext,
  `quantity` varchar(15) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `stock_number` longtext,
  `test_pressure_certificate_b` bit(1) DEFAULT NULL,
  `title` longtext,
  `your_comment` longtext,
  `action_user` bigint(20) DEFAULT NULL,
  `customer` bigint(20) DEFAULT NULL,
  `previous_contract` bigint(20) DEFAULT NULL,
  `task` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgu0bcouvp0mqqp4obvsqn5xqe` (`action_user`),
  KEY `FK2epvrn5j2qxx1cqxa5vx7i1hc` (`customer`),
  KEY `FKb53h9hmvo6k7xhc0gl46ufys9` (`previous_contract`),
  KEY `FK55cr5hq683qjkxlkgdgq3kwmj` (`task`),
  CONSTRAINT `FK2epvrn5j2qxx1cqxa5vx7i1hc` FOREIGN KEY (`customer`) REFERENCES `customer` (`id`),
  CONSTRAINT `FK55cr5hq683qjkxlkgdgq3kwmj` FOREIGN KEY (`task`) REFERENCES `tasks` (`id`),
  CONSTRAINT `FKb53h9hmvo6k7xhc0gl46ufys9` FOREIGN KEY (`previous_contract`) REFERENCES `contract` (`id`),
  CONSTRAINT `FKgu0bcouvp0mqqp4obvsqn5xqe` FOREIGN KEY (`action_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract`
--

LOCK TABLES `contract` WRITE;
/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_hist_v2`
--

DROP TABLE IF EXISTS `contract_hist_v2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_hist_v2` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `action_date` datetime DEFAULT NULL,
  `chief_inspector_comment` longtext,
  `conformity_certificate_b` bit(1) DEFAULT NULL,
  `contract_guid` varchar(100) DEFAULT NULL,
  `contract_number` varchar(15) DEFAULT NULL,
  `contract_status_enum` int(11) DEFAULT NULL,
  `contract_type_enum` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `details` longtext,
  `deviations_comment` longtext,
  `dimensional_report_b` bit(1) DEFAULT NULL,
  `drawing_number` longtext,
  `enquiry_number` varchar(15) DEFAULT NULL,
  `item_number` longtext,
  `material_certificate_b` bit(1) DEFAULT NULL,
  `order_number` longtext,
  `quantity` varchar(15) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `stock_number` longtext,
  `test_pressure_certificate_b` bit(1) DEFAULT NULL,
  `title` longtext,
  `your_comment` longtext,
  `action_user` bigint(20) DEFAULT NULL,
  `customer` bigint(20) DEFAULT NULL,
  `previous_contract` bigint(20) DEFAULT NULL,
  `task` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKrtmn7mcyevp3esuoj92cc05k1` (`REV`),
  CONSTRAINT `FKrtmn7mcyevp3esuoj92cc05k1` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_hist_v2`
--

LOCK TABLES `contract_hist_v2` WRITE;
/*!40000 ALTER TABLE `contract_hist_v2` DISABLE KEYS */;
/*!40000 ALTER TABLE `contract_hist_v2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_review_item`
--

DROP TABLE IF EXISTS `contract_review_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_review_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount_materials` int(11) DEFAULT NULL,
  `detail_type` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `description` longtext,
  `estimated_hours` double DEFAULT NULL,
  `material_type` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `resource_allocation_date` datetime DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  `resource` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9hwmtdmwn638dis91wlqdc3x7` (`contract`),
  KEY `FKlyosxijx8ktpunratke7n9bj6` (`resource`),
  CONSTRAINT `FK9hwmtdmwn638dis91wlqdc3x7` FOREIGN KEY (`contract`) REFERENCES `contract` (`id`),
  CONSTRAINT `FKlyosxijx8ktpunratke7n9bj6` FOREIGN KEY (`resource`) REFERENCES `resource` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_review_item`
--

LOCK TABLES `contract_review_item` WRITE;
/*!40000 ALTER TABLE `contract_review_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `contract_review_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_email` varchar(200) DEFAULT NULL,
  `company_number` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `customer_name` longtext,
  `fax_number` varchar(20) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `point_of_contact_email` varchar(200) DEFAULT NULL,
  `point_of_contact_name` longtext,
  `point_of_contact_phone_number` longtext,
  `vat_number` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_users`
--

DROP TABLE IF EXISTS `customer_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKii45xar9d8mm5h2717ng8swfb` (`customer_id`),
  KEY `FK8h3brnn8cu5ky7kvudn7vd1bu` (`user_id`),
  CONSTRAINT `FK8h3brnn8cu5ky7kvudn7vd1bu` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKii45xar9d8mm5h2717ng8swfb` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_users`
--

LOCK TABLES `customer_users` WRITE;
/*!40000 ALTER TABLE `customer_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc`
--

DROP TABLE IF EXISTS `doc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `add_date` datetime DEFAULT NULL,
  `doc_guid` varchar(100) DEFAULT NULL,
  `doc_ref` varchar(200) DEFAULT NULL,
  `document_content` longtext,
  `extension` varchar(15) DEFAULT NULL,
  `file_content` longblob,
  `keyword` varchar(1000) DEFAULT NULL,
  `keyword1` varchar(200) DEFAULT NULL,
  `keyword2` varchar(200) DEFAULT NULL,
  `keyword3` varchar(200) DEFAULT NULL,
  `meta_data` longtext,
  `note` longtext,
  `notify_parties` bit(1) DEFAULT NULL,
  `original_fname` varchar(500) DEFAULT NULL,
  `purchaseOrderDate` datetime DEFAULT NULL,
  `purchaseOrderEnum` int(11) DEFAULT NULL,
  `purchaseOrderNumber` varchar(255) DEFAULT NULL,
  `ref1` varchar(100) DEFAULT NULL,
  `ref2` varchar(100) DEFAULT NULL,
  `search` bit(1) DEFAULT NULL,
  `search_field` varchar(10000) DEFAULT NULL,
  `server` varchar(200) DEFAULT NULL,
  `version_no` int(11) DEFAULT NULL,
  `collect_deliver_contract_id` bigint(20) DEFAULT NULL,
  `collect_deliver_item_id` bigint(20) DEFAULT NULL,
  `config_doc_id` bigint(20) DEFAULT NULL,
  `contract_id` bigint(20) DEFAULT NULL,
  `doc_id` bigint(20) DEFAULT NULL,
  `uid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3wv1u9dekbxwag8f90ylyf1rt` (`collect_deliver_contract_id`),
  KEY `FKno0wlj6ba38ght3vw5c4tj3fl` (`collect_deliver_item_id`),
  KEY `FK58x416jr27s3f7pvy1083w7vr` (`config_doc_id`),
  KEY `FKhnpkfmhoftvdtiult80j60qyb` (`contract_id`),
  KEY `FKe4w834djt57v9qg27yyp96vei` (`doc_id`),
  KEY `FK7qx04uv3elgjijwti8dmha6ww` (`uid`),
  CONSTRAINT `FK3wv1u9dekbxwag8f90ylyf1rt` FOREIGN KEY (`collect_deliver_contract_id`) REFERENCES `collect_deliver_contract` (`id`),
  CONSTRAINT `FK58x416jr27s3f7pvy1083w7vr` FOREIGN KEY (`config_doc_id`) REFERENCES `config_doc` (`id`),
  CONSTRAINT `FK7qx04uv3elgjijwti8dmha6ww` FOREIGN KEY (`uid`) REFERENCES `users` (`id`),
  CONSTRAINT `FKe4w834djt57v9qg27yyp96vei` FOREIGN KEY (`doc_id`) REFERENCES `doc` (`id`),
  CONSTRAINT `FKhnpkfmhoftvdtiult80j60qyb` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`),
  CONSTRAINT `FKno0wlj6ba38ght3vw5c4tj3fl` FOREIGN KEY (`collect_deliver_item_id`) REFERENCES `collect_deliver_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc`
--

LOCK TABLES `doc` WRITE;
/*!40000 ALTER TABLE `doc` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_tracker`
--

DROP TABLE IF EXISTS `document_tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_tracker` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `document_tracker_enum` int(11) DEFAULT NULL,
  `doc_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKptvt0loefjk0ut5r7jp4ovol4` (`doc_id`),
  KEY `FK6f7dis82p3jvoolvq1ns6j9be` (`user_id`),
  CONSTRAINT `FK6f7dis82p3jvoolvq1ns6j9be` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKptvt0loefjk0ut5r7jp4ovol4` FOREIGN KEY (`doc_id`) REFERENCES `doc` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_tracker`
--

LOCK TABLES `document_tracker` WRITE;
/*!40000 ALTER TABLE `document_tracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `haj_properties`
--

DROP TABLE IF EXISTS `haj_properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `haj_properties` (
  `e_property_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `e_property` varchar(100) NOT NULL,
  `e_value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`e_property_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `haj_properties`
--

LOCK TABLES `haj_properties` WRITE;
/*!40000 ALTER TABLE `haj_properties` DISABLE KEYS */;
INSERT INTO `haj_properties` VALUES (1,'docServer','http://52.30.116.203/docstore/'),(2,'docPath','/data/docstore/'),(3,'SMTP_AUTH_PWD_gmail',''),(4,'SMTP_AUTH_PWD_PN',''),(5,'SMTP_FROM',''),(6,'SMTP_HOST_PORT_gmail',''),(7,'SMTP_HOST_PORT_PN',''),(8,'mailServer','gmail'),(9,'SMTP_AUTH_USER_gmail',''),(10,'SMTP_AUTH_USER_PN',''),(11,'SMTP_AUTH_REQUIRED_PN','true'),(12,'SMTP_AUTH_REQUIRED_gmail','true'),(13,'PL','http://52.30.116.203/'),(14,'MAIL_DEBUG','false'),(15,'HAJ','http://52.30.116.203/'),(16,'SMTP_HOST_NAME_gmail','smtp.gmail.com'),(17,'SMTP_HOST_NAME_PN','197.242.144.189'),(18,'apnsCertificate',''),(19,'PIXU_APP_URL',''),(20,'INFO_REQ_MAIL',''),(21,'APNS_PROD',''),(22,'APNS_CERTIFICATE',''),(23,'BILLING_ENABLED',''),(24,'NEGATIVE_THREASHOLD',''),(25,'ds_mailserver','smtp.gmail.com'),(26,'ds_mailserver_uid','amhydraulics2013@gmail.com'),(27,'ds_mailserver_pwd','Mercedes400?'),(28,'ds_mailserver_port','587'),(29,'ds_mailserver_mailFrom','amhydraulics2013@gmail.com'),(30,'ds_mailserver_mailTo','amhydraulics2013@gmail.com'),(31,'WEBSOCKET_SERVER',''),(32,'DEV_TEST_PROD','P');
/*!40000 ALTER TABLE `haj_properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspection_report`
--

DROP TABLE IF EXISTS `inspection_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inspection_report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_date` datetime DEFAULT NULL,
  `actual_bore_size` longtext,
  `actual_rod_size` longtext,
  `bore_size` longtext,
  `close_report` bit(1) DEFAULT NULL,
  `completed_report` bit(1) DEFAULT NULL,
  `contruction_type` longtext,
  `contract_type_enum` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `customer_order_number` longtext,
  `cylinder_type` longtext,
  `drawing_number` longtext,
  `form_number` longtext,
  `medium` longtext,
  `mounting` longtext,
  `revision_number` int(11) DEFAULT NULL,
  `rod_size` longtext,
  `soft_delete` bit(1) DEFAULT NULL,
  `stoke` longtext,
  `action_user` bigint(20) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  `fitter` bigint(20) DEFAULT NULL,
  `task` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb59qvmwrsynx24gvqxdjkxgmt` (`action_user`),
  KEY `FK52xofpgslh1piv05899b7uk4d` (`contract`),
  KEY `FKtlhkaanhxjr3bxcved59mglra` (`fitter`),
  KEY `FKpul9xhmn9oompponu1v4ng9s1` (`task`),
  CONSTRAINT `FK52xofpgslh1piv05899b7uk4d` FOREIGN KEY (`contract`) REFERENCES `contract` (`id`),
  CONSTRAINT `FKb59qvmwrsynx24gvqxdjkxgmt` FOREIGN KEY (`action_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FKpul9xhmn9oompponu1v4ng9s1` FOREIGN KEY (`task`) REFERENCES `tasks` (`id`),
  CONSTRAINT `FKtlhkaanhxjr3bxcved59mglra` FOREIGN KEY (`fitter`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspection_report`
--

LOCK TABLES `inspection_report` WRITE;
/*!40000 ALTER TABLE `inspection_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `inspection_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspection_report_hist_v2`
--

DROP TABLE IF EXISTS `inspection_report_hist_v2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inspection_report_hist_v2` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `action_date` datetime DEFAULT NULL,
  `actual_bore_size` longtext,
  `actual_rod_size` longtext,
  `bore_size` longtext,
  `close_report` bit(1) DEFAULT NULL,
  `completed_report` bit(1) DEFAULT NULL,
  `contruction_type` longtext,
  `contract_type_enum` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `customer_order_number` longtext,
  `cylinder_type` longtext,
  `drawing_number` longtext,
  `form_number` longtext,
  `medium` longtext,
  `mounting` longtext,
  `revision_number` int(11) DEFAULT NULL,
  `rod_size` longtext,
  `soft_delete` bit(1) DEFAULT NULL,
  `stoke` longtext,
  `action_user` bigint(20) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  `fitter` bigint(20) DEFAULT NULL,
  `task` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKaqi7vri04imftkfonbjp1lyy6` (`REV`),
  CONSTRAINT `FKaqi7vri04imftkfonbjp1lyy6` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspection_report_hist_v2`
--

LOCK TABLES `inspection_report_hist_v2` WRITE;
/*!40000 ALTER TABLE `inspection_report_hist_v2` DISABLE KEYS */;
/*!40000 ALTER TABLE `inspection_report_hist_v2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inspection_report_item`
--

DROP TABLE IF EXISTS `inspection_report_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inspection_report_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `component_description` longtext,
  `condition_work_required` longtext,
  `labour_cost` double DEFAULT NULL,
  `line_number` int(11) DEFAULT NULL,
  `material_cost` double DEFAULT NULL,
  `material_description` longtext,
  `inspection_report` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKco2fdacd5m3k51322axcava88` (`inspection_report`),
  CONSTRAINT `FKco2fdacd5m3k51322axcava88` FOREIGN KEY (`inspection_report`) REFERENCES `inspection_report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inspection_report_item`
--

LOCK TABLES `inspection_report_item` WRITE;
/*!40000 ALTER TABLE `inspection_report_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `inspection_report_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_log`
--

DROP TABLE IF EXISTS `mail_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `body` longtext,
  `create_date` datetime DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `error_msg` longtext,
  `failed` bit(1) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `subject` varchar(500) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKec5tof2vew9s64r4bb38x7d5d` (`user_id`),
  CONSTRAINT `FKec5tof2vew9s64r4bb38x7d5d` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_log`
--

LOCK TABLES `mail_log` WRITE;
/*!40000 ALTER TABLE `mail_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_order`
--

DROP TABLE IF EXISTS `purchase_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `name_of` longtext,
  `purchase_order_enum` int(11) DEFAULT NULL,
  `purchase_order_for_enum` int(11) DEFAULT NULL,
  `purchase_order_number` varchar(15) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKglihwlwrbo2nkgu7do6vqfbhv` (`action_user`),
  KEY `FK349vfnjjc7b2bwmrixxqs6wa` (`contract`),
  CONSTRAINT `FK349vfnjjc7b2bwmrixxqs6wa` FOREIGN KEY (`contract`) REFERENCES `contract` (`id`),
  CONSTRAINT `FKglihwlwrbo2nkgu7do6vqfbhv` FOREIGN KEY (`action_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_order`
--

LOCK TABLES `purchase_order` WRITE;
/*!40000 ALTER TABLE `purchase_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_order_hist_v2`
--

DROP TABLE IF EXISTS `purchase_order_hist_v2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_order_hist_v2` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `action_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `name_of` longtext,
  `purchase_order_enum` int(11) DEFAULT NULL,
  `purchase_order_for_enum` int(11) DEFAULT NULL,
  `purchase_order_number` varchar(15) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKqf560yctyoh08uh5aer4ov5c1` (`REV`),
  CONSTRAINT `FKqf560yctyoh08uh5aer4ov5c1` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_order_hist_v2`
--

LOCK TABLES `purchase_order_hist_v2` WRITE;
/*!40000 ALTER TABLE `purchase_order_hist_v2` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order_hist_v2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_order_item`
--

DROP TABLE IF EXISTS `purchase_order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_order_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `description` longtext,
  `exclude_price_b` bit(1) DEFAULT NULL,
  `line_number` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `purchase_order` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3gsmcewod1meeg1r20c02oleo` (`purchase_order`),
  CONSTRAINT `FK3gsmcewod1meeg1r20c02oleo` FOREIGN KEY (`purchase_order`) REFERENCES `purchase_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_order_item`
--

LOCK TABLES `purchase_order_item` WRITE;
/*!40000 ALTER TABLE `purchase_order_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote`
--

DROP TABLE IF EXISTS `quote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_date` datetime DEFAULT NULL,
  `additional_notes` longtext,
  `create_date` datetime DEFAULT NULL,
  `customer_reference` varchar(60) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `for_attention_of` varchar(60) DEFAULT NULL,
  `quote_guid` varchar(100) DEFAULT NULL,
  `quote_number` varchar(255) DEFAULT NULL,
  `quote_status_enum` int(11) DEFAULT NULL,
  `reject_reason` longtext,
  `revision_number` int(11) DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `to_do` varchar(80) DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  `task` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK99kcpp29krnnmx1svytc4n8ub` (`action_user`),
  KEY `FKrt2p7q3wjmjox5ufm63hmhhpk` (`contract`),
  KEY `FKitboew1i40qo46tmj9jynymx0` (`task`),
  CONSTRAINT `FK99kcpp29krnnmx1svytc4n8ub` FOREIGN KEY (`action_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FKitboew1i40qo46tmj9jynymx0` FOREIGN KEY (`task`) REFERENCES `tasks` (`id`),
  CONSTRAINT `FKrt2p7q3wjmjox5ufm63hmhhpk` FOREIGN KEY (`contract`) REFERENCES `contract` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote`
--

LOCK TABLES `quote` WRITE;
/*!40000 ALTER TABLE `quote` DISABLE KEYS */;
/*!40000 ALTER TABLE `quote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_hist_v2`
--

DROP TABLE IF EXISTS `quote_hist_v2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_hist_v2` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `action_date` datetime DEFAULT NULL,
  `additional_notes` longtext,
  `create_date` datetime DEFAULT NULL,
  `customer_reference` varchar(60) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `for_attention_of` varchar(60) DEFAULT NULL,
  `quote_guid` varchar(100) DEFAULT NULL,
  `quote_number` varchar(255) DEFAULT NULL,
  `quote_status_enum` int(11) DEFAULT NULL,
  `reject_reason` longtext,
  `revision_number` int(11) DEFAULT NULL,
  `soft_delete` bit(1) DEFAULT NULL,
  `to_do` varchar(80) DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  `task` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKa1ld6mct31y8p6l97pq2kjcr1` (`REV`),
  CONSTRAINT `FKa1ld6mct31y8p6l97pq2kjcr1` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_hist_v2`
--

LOCK TABLES `quote_hist_v2` WRITE;
/*!40000 ALTER TABLE `quote_hist_v2` DISABLE KEYS */;
/*!40000 ALTER TABLE `quote_hist_v2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quote_item`
--

DROP TABLE IF EXISTS `quote_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quote_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` longtext,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quote` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK34nbi38k7awgou8144oy4gdhx` (`quote`),
  CONSTRAINT `FK34nbi38k7awgou8144oy4gdhx` FOREIGN KEY (`quote`) REFERENCES `quote` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quote_item`
--

LOCK TABLES `quote_item` WRITE;
/*!40000 ALTER TABLE `quote_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `quote_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `color_code` varchar(7) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `resource_type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbma5417n022ufdra8d99qscxv` (`resource_type_id`),
  CONSTRAINT `FKbma5417n022ufdra8d99qscxv` FOREIGN KEY (`resource_type_id`) REFERENCES `resource_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource`
--

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_allocation`
--

DROP TABLE IF EXISTS `resource_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_allocation` (
  `type` varchar(500) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `description` longtext,
  `from_date_time` datetime DEFAULT NULL,
  `to_date_time` datetime DEFAULT NULL,
  `resource_id` bigint(20) DEFAULT NULL,
  `contract_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKg44o1lildv2h3pgex1y7777s1` (`resource_id`),
  KEY `FK6ur3b1g4cqbjb3v0livimlr7p` (`contract_id`),
  KEY `FKq88u4w3i8mwgt821uijmgucdd` (`user_id`),
  CONSTRAINT `FK6ur3b1g4cqbjb3v0livimlr7p` FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`),
  CONSTRAINT `FKg44o1lildv2h3pgex1y7777s1` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`),
  CONSTRAINT `FKq88u4w3i8mwgt821uijmgucdd` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_allocation`
--

LOCK TABLES `resource_allocation` WRITE;
/*!40000 ALTER TABLE `resource_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_allocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_type`
--

DROP TABLE IF EXISTS `resource_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_type`
--

LOCK TABLES `resource_type` WRITE;
/*!40000 ALTER TABLE `resource_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_users`
--

DROP TABLE IF EXISTS `resource_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `resource_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdej6eqaem77btvriogl5041u1` (`resource_id`),
  KEY `FKl6utc52ooiyjthuxr38j5s7bj` (`user_id`),
  CONSTRAINT `FKdej6eqaem77btvriogl5041u1` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`),
  CONSTRAINT `FKl6utc52ooiyjthuxr38j5s7bj` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_users`
--

LOCK TABLES `resource_users` WRITE;
/*!40000 ALTER TABLE `resource_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revinfo`
--

DROP TABLE IF EXISTS `revinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revinfo` (
  `REV` int(11) NOT NULL AUTO_INCREMENT,
  `REVTSTMP` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revinfo`
--

LOCK TABLES `revinfo` WRITE;
/*!40000 ALTER TABLE `revinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `revinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route_card`
--

DROP TABLE IF EXISTS `route_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_card` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `material_spec` longtext,
  `revision_number` int(11) DEFAULT NULL,
  `route_status` int(11) DEFAULT NULL,
  `shop_instruction` longtext,
  `soft_delete` bit(1) DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  `task` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKkiku8lm7lcq6y9gwxqht8ii56` (`action_user`),
  KEY `FKmoja8medw8yfrqu9vy4gay6ki` (`contract`),
  KEY `FKmxw0llknes5plrvaqr4umpc5p` (`task`),
  CONSTRAINT `FKkiku8lm7lcq6y9gwxqht8ii56` FOREIGN KEY (`action_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FKmoja8medw8yfrqu9vy4gay6ki` FOREIGN KEY (`contract`) REFERENCES `contract` (`id`),
  CONSTRAINT `FKmxw0llknes5plrvaqr4umpc5p` FOREIGN KEY (`task`) REFERENCES `tasks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route_card`
--

LOCK TABLES `route_card` WRITE;
/*!40000 ALTER TABLE `route_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route_card_hist_v2`
--

DROP TABLE IF EXISTS `route_card_hist_v2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_card_hist_v2` (
  `id` bigint(20) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `action_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `material_spec` longtext,
  `revision_number` int(11) DEFAULT NULL,
  `route_status` int(11) DEFAULT NULL,
  `shop_instruction` longtext,
  `soft_delete` bit(1) DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `contract` bigint(20) DEFAULT NULL,
  `task` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FKihw3hlogcxyns1xmn4ie67gmi` (`REV`),
  CONSTRAINT `FKihw3hlogcxyns1xmn4ie67gmi` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route_card_hist_v2`
--

LOCK TABLES `route_card_hist_v2` WRITE;
/*!40000 ALTER TABLE `route_card_hist_v2` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_card_hist_v2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route_card_item`
--

DROP TABLE IF EXISTS `route_card_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route_card_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actual_time` bigint(20) DEFAULT NULL,
  `completed_b` bit(1) DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `estimated_time` bigint(20) DEFAULT NULL,
  `op_number` longtext,
  `process_description` longtext,
  `resource_id` bigint(20) DEFAULT NULL,
  `route_card_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7n2fnowuhtkt2hn8hjdwag75a` (`resource_id`),
  KEY `FKhef4jn1cw1brru4x6v71pn9pw` (`route_card_id`),
  CONSTRAINT `FK7n2fnowuhtkt2hn8hjdwag75a` FOREIGN KEY (`resource_id`) REFERENCES `resource` (`id`),
  CONSTRAINT `FKhef4jn1cw1brru4x6v71pn9pw` FOREIGN KEY (`route_card_id`) REFERENCES `route_card` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route_card_item`
--

LOCK TABLES `route_card_item` WRITE;
/*!40000 ALTER TABLE `route_card_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `route_card_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_users`
--

DROP TABLE IF EXISTS `task_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `task_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa7gicndcly21nk29xxfiuu9o7` (`task_id`),
  KEY `FK6cv479hyfnkedc7nqc46tmag6` (`user_id`),
  CONSTRAINT `FK6cv479hyfnkedc7nqc46tmag6` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKa7gicndcly21nk29xxfiuu9o7` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_users`
--

LOCK TABLES `task_users` WRITE;
/*!40000 ALTER TABLE `task_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_date` datetime DEFAULT NULL,
  `completion_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `target_class` varchar(255) DEFAULT NULL,
  `target_key` bigint(20) DEFAULT NULL,
  `task_description` longtext,
  `task_direct_page` longtext,
  `task_guid` varchar(100) DEFAULT NULL,
  `task_status` int(11) DEFAULT NULL,
  `action_user` bigint(20) DEFAULT NULL,
  `create_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1fv8t8k2h4tw1nk6xmonidiut` (`action_user`),
  KEY `FKqaf6v7wfwe5lrfvh0wx8ftrxj` (`create_user`),
  CONSTRAINT `FK1fv8t8k2h4tw1nk6xmonidiut` FOREIGN KEY (`action_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FKqaf6v7wfwe5lrfvh0wx8ftrxj` FOREIGN KEY (`create_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accepted_t_and_c` bit(1) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `alternative_contact_name` varchar(100) DEFAULT NULL,
  `alternative_contact_number` varchar(20) DEFAULT NULL,
  `cell_number` varchar(20) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email_confirm_date` datetime DEFAULT NULL,
  `email_guid` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `internal_cell_number` varchar(20) DEFAULT NULL,
  `ios_android` varchar(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_name` varchar(100) NOT NULL,
  `license_valid_from` datetime DEFAULT NULL,
  `license_valid_to` datetime DEFAULT NULL,
  `passport_number` varchar(30) DEFAULT NULL,
  `password` varchar(1000) NOT NULL,
  `phone_id` varchar(100) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `registration_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `tel_number` varchar(20) DEFAULT NULL,
  `user_id` varchar(15) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `last_chg_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfy1finmkkaakxitgxa14vuihc` (`last_chg_user_id`),
  CONSTRAINT `FKfy1finmkkaakxitgxa14vuihc` FOREIGN KEY (`last_chg_user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,'',NULL,NULL,NULL,NULL,'admin@amhydraulics.com',NULL,NULL,'Admin',NULL,NULL,'2018-02-12 14:33:35','Admin',NULL,NULL,NULL,'AhtBQLGd+O3PXkaPeqvT4XngrTZ4BbZ32rWeYKO1lBgAqhBCucdyvwvntMLqTY/T',NULL,NULL,NULL,NULL,0,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workflow`
--

DROP TABLE IF EXISTS `workflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflow` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `jsf_form` varchar(100) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4y30assvpkolk8qhirdav1lh6` (`parent_id`),
  CONSTRAINT `FK4y30assvpkolk8qhirdav1lh6` FOREIGN KEY (`parent_id`) REFERENCES `workflow` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workflow`
--

LOCK TABLES `workflow` WRITE;
/*!40000 ALTER TABLE `workflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `workflow` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-22 10:55:07
